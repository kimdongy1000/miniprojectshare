const add_chat_room_btn = document.querySelector("#add_chat_room_btn");
const radio_group_form = document.querySelector("#radio_group_form");
const chat_password = document.querySelector("#chat_password");
const btn_chat_modal_close = document.querySelector("#btn_chat_modal_close");
const btn_chat_modal_create = document.querySelector("#btn_chat_modal_create");
const chat_title = document.querySelector("#chat_title");
const chat_max_number = document.querySelector("#chat_max_number");
const div_category = document.querySelector("#div_category");
const chat_category = document.querySelector("#chat_category");
const chat_row = document.querySelector("#chat_row");





init();

function init(){

    renderHtlm();

}

function renderHtlm(){

    console.log(chat_render_item);

    let chat_map_categoty_ = chat_render_item.map(x => {

        let temp = {
            "chatsequence" : x.chatSequence ,
            "chatcategori_" : x.chatCategori_
        }

        return temp;

    });

    console.log(chat_map_categoty_);


    for(let i = 0; i < chat_render_item.length; i++){

        let chatSequence = chat_render_item[i].chatSequence;
        let temp_chat_html = document.createElement('div');
        temp_chat_html.setAttribute('class' , 'col-lg-4');


        let temp_chat_img = document.createElement('img');
        temp_chat_img.setAttribute('class' , 'bd-placeholder-img rounded-circle')
        temp_chat_img.setAttribute('width' , '100')
        temp_chat_img.setAttribute('height' ,'100')

        let temp_chat_h3 = document.createElement('h3');
        temp_chat_h3.setAttribute('class' , 'fw-normal');
        temp_chat_h3.innerText = chat_render_item[i].chatTitle;

        let temp_chat_div = document.createElement('h5');
        temp_chat_div.setAttribute('class' , 'col align-self-center');
        let hh_inner_text = "";


        for(let j = 0; j < chat_map_categoty_.length; j++){

        if(chatSequence === chat_map_categoty_[j].chatsequence){
            hh_inner_text = hh_inner_text + chat_map_categoty_[j].chatcategori_;
        }else{
            i = i+j;
            chat_map_categoty_ = chat_map_categoty_.slice(j);
            break;
        }


        }

        temp_chat_div.innerText = hh_inner_text;

        chat_row.appendChild(temp_chat_html);
        temp_chat_html.appendChild(temp_chat_img)
        temp_chat_html.appendChild(temp_chat_h3)
        temp_chat_html.appendChild(temp_chat_div)


    }


}



function removeSelf(object){

    object.parentNode.remove();
}




chat_category.addEventListener('keyup' , (e) => {

    const match_char = ',';
    const input_chat = e.key;
    let chat_category_value = chat_category.value;
    chat_category_value = chat_category_value.trim();
    chat_category_value = chat_category_value.slice(0, -1);

    if(match_char === input_chat){




        let temp_document = document.createElement('div');
        temp_document.setAttribute('class' , 'w-25 p-3')
        temp_document.setAttribute('style' , 'background-color: #eee;')

        let temp_document2 = document.createElement('button');
        temp_document2.setAttribute('onClick' , 'removeSelf(this)')
        temp_document2.innerText =  "#" + chat_category_value;

        temp_document.appendChild(temp_document2);
        div_category.appendChild(temp_document)

        chat_category.value = '';












    }
})


add_chat_room_btn.addEventListener('click' , (e) => {

    $("#createChatRommModal").modal("show");

})

radio_group_form.addEventListener('change' , (e) => {

    const target_value = e.target.value;

    if(target_value === 'Y'){
        chat_password.disabled = true;
    }else{
        chat_password.disabled = false;
    }

    chat_password.value = '';
})

btn_chat_modal_close.addEventListener('click' , (e) => {

        $("#createChatRommModal").modal("hide");

})

btn_chat_modal_create.addEventListener('click' ,  (e) => {

    const chat_title_value = chat_title.value;

    const radio_checked = document.querySelector('input[name="inlineRadioOptions"]:checked');
    const radio_value = radio_checked.value;
    const chat_password_value = chat_password.value;
    const chat_max_number_value = Number(chat_max_number.value);
    const chat_category_value = chat_category.value;




    if(!chat_title_value){
        alert('제목을 입력해주세요.')
        return
    }

    if(radio_value === 'N'){

        if(!chat_password_value){
            alert('비공개 방은 비밀번호 입력이 필수 입니다.')
            return
        }

    }



    if(!chat_max_number_value){
        alert('채팅방 최대 인원을 설정해주새요')
        return false;
    }

    if( !(chat_max_number_value >= 1 && chat_max_number_value <= 50) ){
        alert('채팅방 최대인원은 1명 이상 50명 이하로 설정 해주세요.')
        return false;
    }



    const chat_category_value_array = new Array();

    const categoryChildNode = div_category.childNodes;

    for(let i = 0; i < categoryChildNode.length; i++){
        chat_category_value_array.push(categoryChildNode[i].innerText);

    }




    const  api = "/main/chat/createChat";
    const method = "POST"
    const param = JSON.stringify({

        "chatTitle" : chat_title_value ,
        "chatisOpened" : radio_value ,
        "chatPassword" : chat_password_value,
        "chatCurrentMaxNumber" : chat_max_number_value ,
        "chatCategori" : chat_category_value_array

    })
   const header = {"Content-Type": "application/json"}


    fetch_api(api , method , param , header)
             .then( (response) => response.json() )
             .then((data) => {

                 alert(data.message);



             })







})