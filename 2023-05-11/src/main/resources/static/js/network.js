const domain = "http://localhost:9090"

function fetch_api(api , method , param , header){



    return fetch(   domain + api , {
                    method : method ,
                    headers : header ,
                    body : param
            });


}


function fetch_api_get(api , param , header){

    const key_array = Object.keys(param);
    const value_array = Object.values(param);

    let request_address = domain + api

    if(key_array.length > 0 ){

        request_address += "?"

        for(let i =0; i < key_array.length; i++){

            request_address += key_array[i] + "="
            request_address += value_array[i] + "&"
        }

    }

    return fetch(   request_address , {
                    method : "GET" ,
                    headers : header ,

    });

 }

