const btn_register = document.querySelector("#btn_register");
const floatingInput = document.querySelector("#floatingInput"); // 이메일
const floatingPassword = document.querySelector("#floatingPassword"); // 비밀번호
const floatingPassword_confirm = document.querySelector("#floatingPassword_confirm"); // 확인
const floatingNickName = document.querySelector("#floatingNickName"); // 닉네임



btn_register.addEventListener('click' , (e) => {

    const email_value = floatingInput.value;
    const password_value = floatingPassword.value;
    const confirm_password = floatingPassword_confirm.value;
    const nickname_value =  floatingNickName.value;

    if(!email_value){
        alert('이메일을 입력해주세요');
        return;
    }

   if(!password_value){
        alert('비밀번호를 입력해주세요')
        return;
   }

   if(!confirm_password){
        alert('비밀번호를 확인해주세요')
        return;
   }

   if(!nickname_value){
        alert('닉네임을 입력해주세요.');
        return;
   }

   const emailFlag = validationEmail(email_value);

   if(!emailFlag){
        alert('이메일 형식이 아닙니다.')
        return;
   }

   if(password_value != confirm_password){
        alert('비밀번호를 확인해주세요.')
        return
   }

   const registerData = {
    "email" : email_value ,
    "password" : password_value ,
    "password_confirm" : confirm_password ,
    "nickName" : nickname_value
   }

   const api = "/register/createUser";
   const method = "POST";
   const param = JSON.stringify(registerData);
   const header = {"Content-Type": "application/json"}

   fetch_api(api , method , param , header)
    .then((response) => response.json())
    .then((data) => {

        alert(data.message);

        location.href = "/login"

    })
    .catch((error) => {
        alert(error.message);
    })








})