const btn_send_authen_number = document.querySelector("#btn_send_authen_number");
const btn_numeber_submit = document.querySelector("#btn_numeber_submit");
const stop_watch = document.querySelector("#stop_watch");
const stopwatch_vale = document.querySelector("#stopwatch_vale");
const floatingInput_email = document.querySelector("#floatingInput_email");
const floatingEmailAuthenNumber = document.querySelector("#floatingEmailAuthenNumber");

const btn_password_modal_close = document.querySelector("#btn_password_modal_close");
const btn_password_modal_change = document.querySelector("#btn_password_modal_change");
const change_password = document.querySelector("#change_password");
const change_password_confirm = document.querySelector("#change_password_confirm");







btn_send_authen_number.addEventListener('click' , (e) => {

    const api = "/emailAuthentication/sendNumber";
    const method = "post"
    const param = JSON.stringify({"email" : floatingInput_email.value});
    const header = {"Content-Type": "application/json"}

    fetch_api(api , method , param , header)
    .then((response) => response.json())
        .then((data) => {
            if(data.httpStatusCode === 200){
                alert('인증번호 전송이 완료 되었습니다.')
                btn_numeber_submit.setAttribute("style" , "display:block;");
                stop_watch.setAttribute("style" , "display:block;");

                btn_send_authen_number.setAttribute("style" , "display:none;");

                let watch_time = new Date();

                watch_time.setMinutes(new Date().getMinutes() + 5)
                let date = new Date(watch_time);



                const start_date = new Date();
                let stopMillTime = 0;
                let togle = true;

                stopwatch_vale.innerText  =  "00 : 05 : 00"


                let watch_time_interVal = setInterval(() => {

                    if(stopMillTime === 300000){
                        togle = false;
                    }

                    if(togle){

                        watch_time = watch_time - 1000
                        date = new Date(watch_time);
                        let any_date = new Date(date -  start_date);
                        stopMillTime += 1000

                        let any_minute = any_date.getMinutes()
                        let any_seconds = any_date.getSeconds()

                        any_minute = any_minute.toString();
                        any_seconds = any_seconds.toString();

                        any_minute = any_minute.length === 1 ? "0" + any_minute : any_minute;

                        any_seconds = any_seconds.length === 1 ? "0" + any_seconds : any_seconds;

                        let setTime = "00 : " +  any_minute  + " : "+ any_seconds

                        stopwatch_vale.innerText  =  setTime;




                    }else{
                          alert('인증시간이 초과 되었습니다.')
                          clearInterval(watch_time_interVal);
                          btn_numeber_submit.setAttribute("style" , "display:none;");
                          stop_watch.setAttribute("style" , "display:none;");
                          btn_send_authen_number.setAttribute("style" , "display:block;");
                    }

                } , 1000)


            }else{

                alert(data.message);

            }
        })
    .catch((error) => {
        alert(error.message);
    })

})

btn_numeber_submit.addEventListener('click' , (e) => {

    const floatingEmailAuthenNumber_value = floatingEmailAuthenNumber.value; // 인증번호
    const floatingInput_email_value = floatingInput_email.value;

    if(!floatingEmailAuthenNumber_value){

        alert('인증번호를 입력해주세요.')
        return;
    }

    const api = "/emailAuthentication/authenNumber";
    const method = "post"
    const param = JSON.stringify({
                            "authenticationNumber" :floatingEmailAuthenNumber_value ,
                            "email" : floatingInput_email_value
    });
    const header = {"Content-Type": "application/json"}

    fetch_api(api , method , param , header)
        .then((response) =>  response.json())
        .then( (data) => {

        console.log(data);



       if(data.httpStatusCode === 200){

            const returnList = data.list;
            const isValid = returnList.isVaild;
            const isFirst = returnList.isFirst;

            if(isFirst === 'Y'){

                alert(data.message + " 로그인 화면으로 돌아가겠습니다");
                sleep(5000)
                location.href = "/login"
            }else{
                $("#changePasswordModal").modal("show");
            }

       }else{
        alert(data.message );
       }


        })
        .catch((error) => {
              alert(error.message);
        })

})

btn_password_modal_close.addEventListener('click' , (e) => {

    if(confirm('정말로 닫기 하시겠습니까?\n 이메일 인증은 24시간에 한번만 가능합니다.')){
        $("#changePasswordModal").modal("hide");
        location.href = "/login";
    }

})

btn_password_modal_change.addEventListener('click' , (e) => {

    const change_password_value = change_password.value;
    const change_password_confirm_value = change_password_confirm.value;
    const floatingInput_email_value = floatingInput_email.value;

    if(change_password_value != change_password_confirm_value){

        alert('비밀번호를 확인해주세요.')
        return;
    }



    if(confirm('비밀번호를 변경하시겠습니까?')){

        const api = "/emailAuthentication/changePassword"
        const method = "POST"
        const param = JSON.stringify({"email" : floatingInput_email_value ,"change_password" : change_password_value ,"change_password_confirm" :  change_password_confirm_value});
        const header = {"Content-Type": "application/json"}

        fetch_api(api , method , param , header)
        .then((response) => response.json())
        .then((data) => {

            if(data.httpStatusCode === 200){
                alert(data.message);

                location.href = "/login"

            }else{

                alert(data.message);

                location.href = "/login"

            }
        })


    }


})



