package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.*;

@Configuration
@EnableWebSocketMessageBroker
public class WebsocketConfig implements WebSocketMessageBrokerConfigurer {



    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/main/ws").withSockJS(); // 최초 웹소캣을 연결할떄 사용되는 접두사
        // let socket = new SockJS("/ws")
    }



    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/topic"); // 웹소캣에서 제공되는 기본적인 내장브로커를 사용하겠다는 뜻입니다 이때 /topic 붙은 메세지가 송신되었을때 그 메세지를 브로커가 처리하겠다는 뜻
        //이는 우리가 앞으로 구현할 @SendTo("/topic/{roomId}") 로 구현된 곳으로 핸들러 처리가 됩니다

        registry.setApplicationDestinationPrefixes("/app"); // /app prefix 로 시작하는 모든 메세지가 @MessageMapping  처리되도록 합니다

    }
}
