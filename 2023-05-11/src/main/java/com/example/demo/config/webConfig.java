package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class webConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/bootStrap/js/**").addResourceLocations("classpath:/static/bootstrap/bootstrap-5.2.2-dist/js/");
        registry.addResourceHandler("/bootStrap/css/**").addResourceLocations("classpath:/static/bootstrap/bootstrap-5.2.2-dist/css/");



        registry.addResourceHandler("/resources/js/**").addResourceLocations("classpath:/static/js/");
        registry.addResourceHandler("/resources/css/**").addResourceLocations("classpath:/static/css/");
        registry.addResourceHandler("/resources/html/**").addResourceLocations("classpath:/static/html/");
        registry.addResourceHandler("/resources/jquery/**").addResourceLocations("classpath:/static/jquery/");
        registry.addResourceHandler("/resources/img/**").addResourceLocations("classpath:/static/img/");
    }
}
