package com.example.demo.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Getter
public class MailConfig {

    @Value("${my.mail.host}")
    private String host;

    @Value("${my.mail.address}")
    private String address;

    @Value("${my.mail.userName}")
    private String userName;

    @Value("${my.mail.password}")
    private String password;

    @Value("${my.mail.port}")
    private int port;

    @Value("${my.mail.auth}")
    private String auth;

    @Value("${my.mail.sslEnable}")
    private String sslEnable;



}

