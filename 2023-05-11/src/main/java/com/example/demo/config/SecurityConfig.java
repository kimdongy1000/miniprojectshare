package com.example.demo.config;


import com.example.demo.filter.AuthenticationFilter;
import com.example.demo.filter.EmailAuthenicationFilter;
import com.example.demo.repository.LoginUserRepository;
import com.example.demo.security.LoginUserAuthProvider;
import com.example.demo.security.LoginUserFailHandler;
import com.example.demo.security.LoginUserSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
public class SecurityConfig {

    @Autowired
    private LoginUserRepository loginUserRepository;

    @Autowired
    private LoginUserSuccessHandler successHandler;

    @Autowired
    private LoginUserFailHandler loginUserFailHandler;

    @Autowired
    private LoginUserAuthProvider loginUserAuthProvider;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{

        http.authorizeRequests().antMatchers(

                "/bootStrap/js/**" ,
                "/bootStrap/css/**",

                "/resources/js/**",
                "/resources/css/**",
                "/resources/html/**",
                "/resources/jquery/**",
                "/resources/img/**",



                "/register/**" ,
                "/emailAuthentication/**",

                "/error/**" ,

                "/find/**"



        ).permitAll();

//        http.authorizeRequests().anyRequest().permitAll();

        http.authorizeRequests().antMatchers(

                "/main/**"



        ).authenticated();








        http.formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/startLogin")
                .usernameParameter("email")
                .passwordParameter("password")
                .successHandler(successHandler)
                .failureHandler(loginUserFailHandler)
                .permitAll();



        http.csrf().disable();

        http.addFilterBefore(new EmailAuthenicationFilter(loginUserRepository) , UsernamePasswordAuthenticationFilter.class);
        http.addFilterAfter(new AuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);



        http.authenticationProvider(loginUserAuthProvider); // 커스텀 권한 부여 class


        return http.build();


    }


}
