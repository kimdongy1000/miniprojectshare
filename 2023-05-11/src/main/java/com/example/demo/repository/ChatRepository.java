package com.example.demo.repository;

import com.example.demo.dao.ChatDao;
import com.example.demo.dto.ChatDto;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ChatRepository {

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;


    public ChatDao createChat(ChatDto chatDto) {

        sqlSessionTemplate.insert(this.getClass().getName() + ".createChat" , chatDto);

        return ChatDao.builder().chatSequence(chatDto.getChatSequence()).build();

    }

    public List<ChatDto> chatList(Map<String, Object> param) {

        return sqlSessionTemplate.selectList(this.getClass().getName() + ".chatList" , param);
    }

    public void insertChatCategory(ChatDto chatDto) {

        for(int i = 0; i < chatDto.getChatCategori().size(); i++){

            Map<String , Object> param = new HashMap<>();
            param.put("cahtSequence" , chatDto.getChatSequence());
            param.put("chatCategori" , chatDto.getChatCategori().get(i));
            sqlSessionTemplate.insert(this.getClass().getName() + ".insertChatCategory" , param);

        }
    }
}
