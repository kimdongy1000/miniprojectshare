package com.example.demo.repository;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class EmailAuthenicationRepository {

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;


    public void insertAuthenNumber(Map<String, Object> queryParam) {
        sqlSessionTemplate.insert(this.getClass().getName() + ".insertAuthenNumber" , queryParam);
    }

    public int authenticationNumber(Map<String, Object> queryParam)
    {
        return sqlSessionTemplate.selectOne(this.getClass().getName() + ".authenticationNumber" , queryParam);
    }

    public void updateAuthenUser(Map<String, Object> queryParam)
    {
        sqlSessionTemplate.update(this.getClass().getName() + ".updateAuthenUser" , queryParam);
    }

    public int isSendEmail(Map<String, Object> queryParam)
    {
        int isSend =  sqlSessionTemplate.selectOne(this.getClass().getName()  + ".isSendEmail" , queryParam);
        if(isSend !=  1){
            throw new RuntimeException("인증번호는 하루에 한번씩만 발송할 수 있습니다");
        }

        return isSend;
    }

    public void changePassword(Map<String, Object> queryParam) {

        sqlSessionTemplate.update(this.getClass().getName() + ".change_password" , queryParam);
    }

    public int isChangeUser(Map<String, Object> queryParam) {

        return sqlSessionTemplate.selectOne(this.getClass().getName() + ".isChangeUser" , queryParam);
    }
}
