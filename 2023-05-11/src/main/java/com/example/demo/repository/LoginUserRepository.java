package com.example.demo.repository;

import com.example.demo.dto.LoginUserDto;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class LoginUserRepository {

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;


    public LoginUserDto findByUserEmail(Map<String , Object> queryParam) {
        return sqlSessionTemplate.selectOne(this.getClass().getName() + ".findByUserEmail" , queryParam);
    }

    public void updateLoginFailCount(Map<String , Object> queryParam) {

        sqlSessionTemplate.update(this.getClass().getName() + ".updateLoginFailCount" , queryParam);
    }
}
