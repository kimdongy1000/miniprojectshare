package com.example.demo.repository;

import com.example.demo.dto.RegisterUserDto;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RegisterUserRepository {

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;


    public int createUser(RegisterUserDto registerUserDto) {

        return sqlSessionTemplate.insert(this.getClass().getName() + ".createUser" , registerUserDto);
    }
}
