package com.example.demo.filter;


import com.example.demo.dto.LoginUserDto;
import com.example.demo.repository.LoginUserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/*
* 최초 로그인시 만약 이메일 인증이 없다면 이메일 인증 페이지로 이동 그렇지 않으면 로그인 로직 계속 진행
* OncePerRequestFilter 모든 요청에 대해서 단 한번만 동작됨
*
*
* */
public class EmailAuthenicationFilter extends OncePerRequestFilter {

    private final LoginUserRepository loginUserRepository;

    public EmailAuthenicationFilter ( LoginUserRepository loginUserRepository) {
        this.loginUserRepository = loginUserRepository;
    }


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        try{

            String requestUrl = request.getRequestURI();
            SecurityContext securityContext = SecurityContextHolder.getContext();

            Authentication authentication = securityContext.getAuthentication();


            if(!requestUrl.equals(("/startLogin"))){
                filterChain.doFilter(request , response);
                return;
            }

            String requestEmail = request.getParameter("email");

            Map<String , Object> queryParam = new HashMap<>();
            queryParam.put("email" , requestEmail);
            LoginUserDto loginUserDto = loginUserRepository.findByUserEmail(queryParam);

            if(loginUserDto == null){
                throw new UsernameNotFoundException("아이디 또는 비밀번호가 맞지 않습니다 다시 확인해주세요.");
            }

            if("N".equals(loginUserDto.getYnValid())){ //
                response.sendRedirect("/emailAuthentication/" + requestEmail);
                return;
            }else if(authentication == null && "Y".equals(loginUserDto.getYnValid())){ // 인증은 없지만 계정 상태가 Y 이면 올 수 없음
                filterChain.doFilter(request , response);
                return;

            }else if(loginUserDto == null ){
                response.sendRedirect("/login");
                return;
            }else{
                filterChain.doFilter(request , response);
                return;
            }





        }catch(Exception e){

            StringBuffer errorMessage = new StringBuffer();
            errorMessage.append("아이디 또는 비밀번호가 맞지 않습니다 다시 확인해주세요.");
            String redirectUrl = "/error/loginFail?message="+ URLEncoder.encode(errorMessage.toString() , "UTF-8");
            response.sendRedirect(redirectUrl);
            return;
        }




    }
}
