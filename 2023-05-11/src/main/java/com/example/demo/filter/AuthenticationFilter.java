package com.example.demo.filter;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class  AuthenticationFilter extends OncePerRequestFilter {

    private static final String[] nonAuthenticationRequest = {

            "register" ,
            "emailAuthentication",
            "login" ,
            "startLogin",
            "find"


    };

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();

        String[] requestUrl_array = request.getRequestURI().split("\\/");

        String request_main_url = requestUrl_array[1];

        if (authentication != null) {

            for (int i = 0; i < nonAuthenticationRequest.length; i++) {
                if (request_main_url.equals(nonAuthenticationRequest[i])) {
                    response.sendRedirect("/main");
                    return;
                }
            }

            filterChain.doFilter(request, response);
            return;


        } else {

            filterChain.doFilter(request, response);
            return;



        }



    }
}
