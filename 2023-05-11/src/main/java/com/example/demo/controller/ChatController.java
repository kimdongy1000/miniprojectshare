package com.example.demo.controller;


import com.example.demo.dao.ChatDao;
import com.example.demo.dto.ChatDto;
import com.example.demo.message.ResponseMessage;
import com.example.demo.service.ChatService;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/main/chat")
public class ChatController {

    private static final Log LOG = LogFactory.getLog(ChatController.class);

    @Autowired
    private ChatService chatService;


    @GetMapping("")
    public String chat(Model model)
    {

        try{

            Map<String , Object> param = new HashMap<>();
            List<ChatDto> chat_list = chatService.chatList(param);

            model.addAttribute("chatList" , chat_list);

            return "chat";

        }catch(Exception e){

            LOG.error(e);
            throw new RuntimeException(e);
        }






    }

    @PostMapping("/createChat")
    public ResponseEntity<?> createChar(
                @RequestBody ChatDto chatDto ,
                Authentication authentication
            ) throws Exception

    {
        ResponseMessage<?> responseMessage = null;
        List<ChatDao> list_dao = null;

        try{

            String email = (String)authentication.getPrincipal();
            ChatDao chatDao = chatService.createChat(chatDto , email);

            list_dao = new ArrayList<>();
            list_dao.add(chatDao);

            responseMessage = ResponseMessage.<ChatDao>builder().list(list_dao).message("채팅창이 생성되었습니다").httpStatusCode(200).build();



            return ResponseEntity.created(URI.create("/AMADEUS_CHAT/" + chatDao.getChatSequence())).body(responseMessage);

        }catch(Exception e){
            LOG.error(e);
            responseMessage = ResponseMessage.<ChatDao>builder().list(null).httpStatusCode(500).message(e.getMessage()).build();
            return ResponseEntity.internalServerError().body(responseMessage);

        }

    }
}
