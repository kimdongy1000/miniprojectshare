package com.example.demo.controller;

import com.example.demo.security.LoginUserSuccessHandler;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("error")
public class ErrorController {

    private final static Log log = LogFactory.getLog(LoginUserSuccessHandler.class);

    @GetMapping("/loginFail")
    public String loginFail(Model model , @RequestParam("message") String message)
    {
        model.addAttribute("message" , message);

        return "loginFail";

    }


}
