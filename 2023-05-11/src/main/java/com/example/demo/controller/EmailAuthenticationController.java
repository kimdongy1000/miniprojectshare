package com.example.demo.controller;

import com.example.demo.config.MailConfig;
import com.example.demo.dao.EmailAuthenticationMessageDao;
import com.example.demo.dto.ChangePasswordDto;
import com.example.demo.message.ResponseMessage;
import com.example.demo.service.EmailAuthenicationService;
import oracle.jdbc.proxy.annotation.Post;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Controller
@RequestMapping("emailAuthentication")
public class EmailAuthenticationController {



    @Autowired
    private EmailAuthenicationService emailAuthenicationService;

    private final static Log log = LogFactory.getLog(EmailAuthenticationController.class);

    @GetMapping("/{email}")
    public String emailAuthenticationPage(@PathVariable String email , Model model) {

        model.addAttribute("authenEmail" , email);

        return "emailAuthenPage";
    }

    @PostMapping("/sendNumber")
    public ResponseEntity<?> send_authencation_email_number(
            @RequestBody Map<String , Object> param
    ) throws Exception
    {
        ResponseMessage<Object> responseMessage = new ResponseMessage<>();
        try {

            emailAuthenicationService.sendAuthenEmail(param);
            responseMessage = ResponseMessage.builder().list(null).message("인증번호 발송이 완료 되었습니다.").httpStatusCode(200).build();

            return ResponseEntity.created(new URI("/Amadeus_auth")).body(responseMessage);
        }catch (Exception e){
            log.error(e);
            responseMessage = ResponseMessage.builder().list(null).message(e.getMessage()).httpStatusCode(500).build();
            return ResponseEntity.internalServerError().body(responseMessage);
        }



    }

    /*
    * 보낸 인증메일 비교
    * */
    @PostMapping("/authenNumber")
    public ResponseEntity<?> authenticationNumber(@RequestBody Map<String , Object> requestParam)
    {
        ResponseMessage<EmailAuthenticationMessageDao> responseMessage = new ResponseMessage<>();
        try{

            EmailAuthenticationMessageDao returnDao = emailAuthenicationService.authenticationNumber(requestParam);

            List<EmailAuthenticationMessageDao> returnList = new ArrayList<>();
            returnList.add(returnDao);
            responseMessage = ResponseMessage.<EmailAuthenticationMessageDao>builder().list(returnList).message("인증이 완료 되었습니다.").httpStatusCode(200).build();

            return ResponseEntity.ok().body(responseMessage);

        }catch(Exception e){
            log.error(e);
            responseMessage = ResponseMessage.<EmailAuthenticationMessageDao>builder().list(null).message(e.getMessage()).httpStatusCode(500).build();
            return ResponseEntity.internalServerError().body(responseMessage);

        }

    }

    @PostMapping("/changePassword")
    public ResponseEntity<?> changePassword (@RequestBody ChangePasswordDto changePasswordDto) {

        ResponseMessage<Object> responseMessage = new ResponseMessage<>();
        try{
            emailAuthenicationService.changePassword(changePasswordDto);

            responseMessage = ResponseMessage.builder().list(null).message("비밀번호 변경이 완료 되었습니다").httpStatusCode(200).build();

            return ResponseEntity.ok().body(responseMessage);

        }catch(Exception e){
            log.error(e);
            responseMessage = ResponseMessage.builder().list(null).message(e.getMessage()).httpStatusCode(200).build();
            return ResponseEntity.internalServerError().body(responseMessage);

        }



    }
}
