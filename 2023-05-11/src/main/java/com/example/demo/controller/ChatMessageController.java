package com.example.demo.controller;

import com.example.demo.dto.ChatMessageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ChatMessageController {

    @GetMapping("/main/joinLiveChat/{chatSequence}")
    public String goLiveChat(@PathVariable String chatSequence , Model model){

        model.addAttribute("chatSequence" , chatSequence);

        return "/liveChat";
    }

    @MessageMapping("/chat/{roomId}")
    @SendTo("/topic/{roomId}")
    public ChatMessageDto sendMessage(@DestinationVariable String roomId , @Payload ChatMessageDto chatMessageDto)
    {

        return chatMessageDto;

    }

}
