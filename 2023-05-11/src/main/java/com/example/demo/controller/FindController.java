package com.example.demo.controller;

import com.example.demo.dto.LoginUserDto;
import com.example.demo.message.ResponseMessage;
import com.example.demo.security.LoginUserSuccessHandler;
import com.example.demo.service.FindService;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("find")
public class FindController {

    private final static Log log = LogFactory.getLog(FindController.class);

    @Autowired
    private FindService findService;

    @GetMapping("/isExistEmail")
    public ResponseEntity<?> isExistEmail (
            @RequestParam("email") String email
    )

    {
        ResponseMessage<LoginUserDto> responseMessage = new ResponseMessage<>();
        List<LoginUserDto> list = new ArrayList<>();

        try{

            Map<String , Object> param = new HashMap<>();
            param.put("email" , email);
            LoginUserDto loginUserDto = findService.findByUserEmail(param);
            list.add(loginUserDto);

            responseMessage = ResponseMessage.<LoginUserDto>builder().list(list).message("이메일이 존재합니다").httpStatusCode(200).build();

            return ResponseEntity.ok().body(responseMessage);


        }catch(Exception e){
            log.error(e);
            responseMessage = ResponseMessage.<LoginUserDto>builder().list(null).message("이메일이 존재하지 않습니다.").httpStatusCode(500).build();
            return ResponseEntity.internalServerError().body(responseMessage);
        }


    }


}
