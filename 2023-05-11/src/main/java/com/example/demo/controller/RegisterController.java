package com.example.demo.controller;

import com.example.demo.dto.RegisterUserDto;
import com.example.demo.message.ResponseMessage;
import com.example.demo.service.RegisterUserService;
import oracle.jdbc.proxy.annotation.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URI;

@Controller
@RequestMapping("register")
public class RegisterController {

    @Autowired
    private RegisterUserService registerUserService;

    @GetMapping("")
    public String registerPage(){

        return "register";
    }

    @PostMapping("/createUser")
    public ResponseEntity<?> registerUser(@RequestBody RegisterUserDto registerUserDto)
    {
        ResponseMessage<Object> responseMessage = new ResponseMessage<>();

        try{

            RegisterUserDto registerUser = registerUserService.registerUser(registerUserDto);
            responseMessage = ResponseMessage.builder().list(null).message("생성완료 되었습니다.").build();

            return ResponseEntity.created(new URI("/Amadeus_user" + registerUser.getUserSequence())).body(responseMessage);


        }catch(Exception e){
            return ResponseEntity.internalServerError().body(e);
        }

    }


}
