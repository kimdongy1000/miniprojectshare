package com.example.demo.controller;

import com.example.demo.security.LoginUserDetails;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/main")
public class MainController {

    private final static Log log = LogFactory.getLog(MainController.class);

    @GetMapping("")
    public String mainPage(Authentication authentication , Model model)
    {

        model.addAttribute("user" , (String)authentication.getPrincipal());

        return "main";


    }
}
