package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChatDto {

    public int chatSequence;
    public String chatTitle;
    public String chatisOpened;
    public String chatPassword;
    public int chatCurrentMaxNumber;
    public String userEmail;

    public int userSequence;
    public int chatCurrentNumber;
    public String chatVaild;
    public String chatCreateDts;
    public List<String> chatCategori;

    public String chatCategori_;



}
