package com.example.demo.dto;

import org.springframework.security.access.annotation.Secured;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginUserDto {

    private int userSequence;
    private String email;
    private String password;
    private String nickName;
    private String ynValid;
    private int loginFail;
    private String lastLoginDate;
    private String insertDts;
    private String updateDts;
	
	
    
    





}
