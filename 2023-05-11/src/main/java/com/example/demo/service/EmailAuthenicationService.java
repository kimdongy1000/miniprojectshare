package com.example.demo.service;

import com.example.demo.config.MailConfig;
import com.example.demo.dao.EmailAuthenticationMessageDao;
import com.example.demo.dto.ChangePasswordDto;
import com.example.demo.dto.LoginUserDto;
import com.example.demo.repository.EmailAuthenicationRepository;
import com.example.demo.repository.LoginUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

@Service
public class EmailAuthenicationService {

    @Autowired
    private MailConfig mailConfig;

    @Autowired
    private EmailAuthenicationRepository emailAuthenicationRepository;

    @Autowired
    private LoginUserRepository loginUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Transactional(rollbackFor = Exception.class)
    public void sendAuthenEmail(Map<String, Object> param) throws Exception{

        Map<String , Object> queryParam = new HashMap<>();
        queryParam.put("email" , param.get("email"));

        emailAuthenicationRepository.isSendEmail(queryParam);



        Properties pro = System.getProperties();
        pro.put("mail.smtp.host", mailConfig.getHost());
        pro.put("mail.smtp.port", mailConfig.getPort());
        pro.put("mail.smtp.auth", mailConfig.getAuth());
        pro.put("mail.smtp.ssl.enable", mailConfig.getSslEnable());

        Session session = Session.getDefaultInstance(pro, new javax.mail.Authenticator() {

            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(mailConfig.getUserName(), mailConfig.getPassword());
            }
        });

        Random random = new Random();
        int ran_number = random.nextInt(100000);



        queryParam.put("authencationNumber" , ran_number);

        emailAuthenicationRepository.insertAuthenNumber(queryParam);




        String recipient = param.get("email").toString();                  // 수신자 메일 아이디
        String subject = "Project_Amadeus 회원가입 인증번호 발송";            // 메일 제목
        String body = "인증번호 : " + ran_number + "입니다.";                          // 메일 내용

        Message mimeMessage = new MimeMessage(session); //MimeMessage 생성 MIME 메세지 타입을 말합니다 텍트스 또는 html 등등으로 보낼 수 있는
        mimeMessage.setFrom(new InternetAddress(mailConfig.getAddress())); // 보내는 사람의 이메일 주소
        mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient)); // 받는 사람의 이메일 주소
        mimeMessage.setSubject(subject); //제목
        mimeMessage.setText(body); //내용
        Transport.send(mimeMessage); //메일 보내기




    }

    @Transactional(rollbackFor = Exception.class)
    public EmailAuthenticationMessageDao authenticationNumber(Map<String, Object> requestParam)
    {

        Map<String , Object> param = new HashMap<>();
        param.put("authenticationNumber" , requestParam.get("authenticationNumber"));
        param.put("email" , requestParam.get("email"));

        int isVaild =  emailAuthenicationRepository.authenticationNumber(param);

        if(isVaild == 0){

            throw new RuntimeException("인증번호 틀렸거나 또는 인증시간이 초과되었습니다.");

        }else{

            EmailAuthenticationMessageDao emailAuthenticationMessageDao = new EmailAuthenticationMessageDao();
            LoginUserDto loginUserDto = loginUserRepository.findByUserEmail(param);


            if(loginUserDto.getYnValid().equals("N")){
                emailAuthenicationRepository.updateAuthenUser(param);


                emailAuthenticationMessageDao.setIsVaild(isVaild);
                emailAuthenticationMessageDao.setIsFirst("Y");

            }else{

                emailAuthenticationMessageDao.setIsVaild(isVaild);
                emailAuthenticationMessageDao.setIsFirst("N");

            }

            return emailAuthenticationMessageDao;



        }






    }

    @Transactional(rollbackFor = Exception.class)
    public void changePassword(ChangePasswordDto changePasswordDto) {

        if(!changePasswordDto.getChange_password().equals(changePasswordDto.getChange_password_confirm())){
            throw new RuntimeException("비밀번호가 서로 다릅니다");
        }

        Map<String , Object> param = new HashMap<>();
        param.put("email" , changePasswordDto.getEmail());
        param.put("password" ,passwordEncoder.encode(changePasswordDto.getChange_password()));

        int isChangeUser = emailAuthenicationRepository.isChangeUser(param);
        emailAuthenicationRepository.changePassword(param);

    }
}
