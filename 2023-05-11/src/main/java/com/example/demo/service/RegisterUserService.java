package com.example.demo.service;

import com.example.demo.dto.RegisterUserDto;
import com.example.demo.repository.RegisterUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class RegisterUserService {

    private static final String reg_email = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$";

    @Autowired
    private RegisterUserRepository registerUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Transactional(rollbackFor = Exception.class)
    public RegisterUserDto registerUser(RegisterUserDto registerUserDto) throws Exception{
        boolean return_boolean = false;

        String email = registerUserDto.getEmail();
        String password = registerUserDto.getPassword();
        String password_confirm = registerUserDto.getPassword_confirm();
        String nickName = registerUserDto.getNickName();

        if(!StringUtils.hasText(email)) {
            throw new RuntimeException("이메일을 확인해주세요.");
        }

        if(!StringUtils.hasText(password) || !StringUtils.hasText(password_confirm)) {
            throw new RuntimeException("비밀빈호를 확인해주세요.");
        }

        if(!StringUtils.hasText(nickName)) {
            throw new RuntimeException("닉네임을 확인해주세요.");
        }

        Pattern emailPattern = Pattern.compile(reg_email);
        Matcher emailMatcher = emailPattern.matcher(email);

        if(!emailMatcher.matches()){
            throw new RuntimeException("이메일 형식이 올바르지 않습니다.");
        }

        if(!password_confirm.equals(password)){
            throw new RuntimeException("비밀번호가 일치하지 않습니다");
        }

        registerUserDto.setPassword(passwordEncoder.encode(registerUserDto.getPassword()));

        int isCreated = registerUserRepository.createUser(registerUserDto);

        if(isCreated == 1){
            return_boolean = true;
        }else{
            throw new RuntimeException("유저 생성에 오류가 생겼습니다.");
        }


        return registerUserDto;


    }
}
