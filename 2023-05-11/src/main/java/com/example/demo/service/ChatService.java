package com.example.demo.service;

import com.example.demo.dao.ChatDao;
import com.example.demo.dto.ChatDto;
import com.example.demo.dto.LoginUserDto;
import com.example.demo.repository.ChatRepository;
import com.example.demo.repository.LoginUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ChatService {

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private LoginUserRepository loginUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Transactional(rollbackFor = Exception.class)
    public ChatDao createChat(ChatDto chatDto , String email) {

        if(!StringUtils.hasText(chatDto.getChatTitle())){
            throw new RuntimeException("제목이 없습니다.");
        }



        if("N".equals(chatDto.getChatisOpened())){
            if(!StringUtils.hasText(chatDto.getChatPassword())){
                throw new RuntimeException("비공개 방은 비밀번호가 필수입니다.");
            }else{
                chatDto.setChatPassword(passwordEncoder.encode(chatDto.getChatPassword()));
            }
        }
        

        if(chatDto.getChatCurrentMaxNumber() < 1 || chatDto.getChatCurrentMaxNumber() > 50){
            throw new RuntimeException("채팅방은 최대 1명이상 , 50명 이하여야 합니다.");
        }

        Map<String , Object> queryParam = new HashMap<>();
        queryParam.put("email" , email);

        LoginUserDto loginUserDto =  loginUserRepository.findByUserEmail(queryParam);
        chatDto.setUserSequence(loginUserDto.getUserSequence());

        chatDto.setChatCategori(chatDto.getChatCategori().stream().distinct().collect(Collectors.toList()));


        ChatDao chatDao = chatRepository.createChat(chatDto);
        chatRepository.insertChatCategory(chatDto);

        return chatDao;

    }

    public List<ChatDto> chatList(Map<String, Object> param) {

        return chatRepository.chatList(param);

    }
}
