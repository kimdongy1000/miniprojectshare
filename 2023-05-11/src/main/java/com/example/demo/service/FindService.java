package com.example.demo.service;

import com.example.demo.dto.LoginUserDto;
import com.example.demo.repository.LoginUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class FindService {

    @Autowired
    private LoginUserRepository loginUserRepository;


    public LoginUserDto findByUserEmail(Map<String, Object> param) {

        return loginUserRepository.findByUserEmail(param);
    }
}
