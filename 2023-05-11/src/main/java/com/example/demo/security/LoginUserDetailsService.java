package com.example.demo.security;

import com.example.demo.dto.LoginUserDto;
import com.example.demo.repository.LoginUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class LoginUserDetailsService implements UserDetailsService {

    @Autowired
    private LoginUserRepository loginUserRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Map<String , Object> queryParam = new HashMap<>();
        queryParam.put("email" , username);

        LoginUserDto loginUserDto = loginUserRepository.findByUserEmail(queryParam);

        if(loginUserDto == null){
            throw new UsernameNotFoundException("일치하는 회원정보가 없습니다.");
        }

        List<SimpleGrantedAuthority> simpleGrantedAuthorityList = new ArrayList<>();
        simpleGrantedAuthorityList.add(new SimpleGrantedAuthority("User"));




        return LoginUserDetails.builder().username(loginUserDto.getEmail())
                .password(loginUserDto.getPassword())
                .simpleGrantedAuthorityList(simpleGrantedAuthorityList).build();
    }
}
