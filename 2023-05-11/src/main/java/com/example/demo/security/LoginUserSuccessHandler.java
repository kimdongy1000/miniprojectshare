package com.example.demo.security;

import com.example.demo.controller.MainController;
import com.example.demo.dto.LoginUserDto;
import com.example.demo.repository.LoginUserRepository;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class LoginUserSuccessHandler implements AuthenticationSuccessHandler {

    private final static Log log = LogFactory.getLog(LoginUserSuccessHandler.class);

    @Autowired
    private LoginUserRepository loginUserRepository;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {



        try{
            Map<String , Object> param = new HashMap<>();
            String username =  (String)authentication.getPrincipal();

            param.put("email" , username);
            param.put("status" , "success");

            LoginUserDto loginUserDto = loginUserRepository.findByUserEmail(param);

            if(loginUserDto.getLoginFail() > 5){
                Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                new SecurityContextLogoutHandler().logout(request , response , auth);
                response.sendRedirect("/emailAuthentication/" + username);


            }else{
                loginUserRepository.updateLoginFailCount(param);
                response.sendRedirect("/main");
            }




        }catch(Exception e){
            log.error(e);
            throw  new RuntimeException(e);


        }







    }
}
