package com.example.demo.security;

import com.example.demo.repository.LoginUserRepository;
import oracle.jdbc.logging.annotations.StringBlinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

@Component
public class LoginUserFailHandler implements AuthenticationFailureHandler {

    @Autowired
    private LoginUserRepository loginUserRepository;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {

        StringBuffer errorMessage = new StringBuffer();

        String username = request.getParameter("email");


        if(exception instanceof UsernameNotFoundException){
            errorMessage.append("아이디 또는 비밀번호가 맞지 않습니다 다시 확인해주세요.");

            String redirectUrl = "/error/loginFail?message="+ URLEncoder.encode(errorMessage.toString() , "UTF-8");
            response.sendRedirect(redirectUrl);

        }else if(exception instanceof BadCredentialsException){

            Map<String , Object> param = new HashMap<>();
            errorMessage.append("아이디 또는 비밀번호가 맞지 않습니다 다시 확인해주세요.");

            param.put("email" , username);
            param.put("status" , "fail");

            loginUserRepository.updateLoginFailCount(param);



            String redirectUrl = "/error/loginFail?message="+ URLEncoder.encode(errorMessage.toString() , "UTF-8");
            response.sendRedirect(redirectUrl);

        }

    }
}
