package com.spring.kimdongy1000.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class PreAspect {

    /* spring AOP 적용
     *
     *
     *
     *
     */


    @Around("execution(* com.spring.kimdongy1000.controller..*.*(..))")
    public Object logPer(ProceedingJoinPoint ajp) throws Throwable{

        log.info("===============================================");
        log.info("Start api {} " , ajp.getSignature());
        Object retVal = ajp.proceed();
        log.info("===============================================");

        return retVal;


    }
}
