package com.spring.kimdongy1000.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Table(name = "fileentity")
public class FileEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid" , strategy = "uuid")
    @Column(name = "id")
    private String id;

    @Column(name = "userid")
    private String userid;

    @Column(name = "filepath")
    private String filePath;

    @Column(name = "originname")
    private String originName;

    @Column(name = "useyn")
    private String useYn;

    @Column(name = "registerdate")
    private String registerDate;







}
