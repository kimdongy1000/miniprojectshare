package com.spring.kimdongy1000.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Table(uniqueConstraints =  {@UniqueConstraint(columnNames = "userEmail" )} , name = "userentity")
public class UserEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid" , strategy = "uuid")
    @Column(name = "id")
    private String id;

    @Column(name = "useremail")
    private String userEmail;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "useyn")
    private String useYn;

    @Column(name = "registerdate")
    private String registerDate;

    @Column(name = "profileid")
    private String profileId;




}
