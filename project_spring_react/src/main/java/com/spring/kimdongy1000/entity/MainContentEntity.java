package com.spring.kimdongy1000.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Table(name = "maincontententity")
public class MainContentEntity {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid" , strategy = "uuid")
    @Column(name = "id")
    private String id;

    @Column(name = "userid")
    private String userid;

    @Column(name = "fileid")
    private String fileId;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @Column(name = "useyn")
    private String useYn;

    @Column(name = "registerdate")
    private String registerDate;

    @Column(name = "modifydate")
    private String modifyDate;








}
