package com.spring.kimdongy1000.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserModel {

    private String id;

    private String userEmail;

    private String username;

    private String password;

    private String useYn;

    private String registerDate;
}
