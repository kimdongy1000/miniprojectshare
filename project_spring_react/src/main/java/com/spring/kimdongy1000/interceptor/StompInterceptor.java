package com.spring.kimdongy1000.interceptor;


import com.spring.kimdongy1000.security.TokenController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;

import java.util.List;
import java.util.Objects;


public class StompInterceptor implements ChannelInterceptor {

    @Autowired
    private TokenController tokenController;

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {


        StompHeaderAccessor accessor = StompHeaderAccessor.wrap(message);
        List<String> token = accessor.getNativeHeader("Authorization");
        System.out.println(token.get(0));

        if(!tokenController.validateAndGeBoolean(token.get(0))){

            return null;
        }

        return message;
    }



}
