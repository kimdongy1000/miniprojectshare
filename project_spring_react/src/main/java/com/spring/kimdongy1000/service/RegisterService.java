package com.spring.kimdongy1000.service;

import com.spring.kimdongy1000.config.BcryptPassEncoder;
import com.spring.kimdongy1000.dto.UserDto;
import com.spring.kimdongy1000.entity.UserEntity;
import com.spring.kimdongy1000.model.UserModel;
import com.spring.kimdongy1000.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;


@Service
public class RegisterService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BcryptPassEncoder bcryptPassEncoder;


    @Transactional(rollbackFor = Exception.class)
    public void registerUser(UserDto userDto) {

        if(userDto.getUserEmail().equals("") || userDto.getUserEmail() == null){
            throw new RuntimeException("이메일이 존재하지 않습니다.");
        }

        /*이메일 중복 확인 로직 생성*/
        if(isExistUserEmail(userDto.getUserEmail())){
            throw new RuntimeException("이메일이 이미 존재합니다..");
        }

        LocalDate now = LocalDate.now();


        UserEntity userEntity = UserEntity.builder()
                .userEmail(userDto.getUserEmail())
                .password(bcryptPassEncoder.encode(userDto.getPassword()))
                .username(userDto.getUsername())
                .useYn("N")
                .registerDate(now.toString())
                .build();

        userRepository.save(userEntity);







    }

    public UserDto getUserOptional(UserDto userDto) {

        Optional<UserEntity> findByPassword = userRepository.findByUserEmail(userDto.getUserEmail());

        if(!bcryptPassEncoder.matches(userDto.getPassword() , findByPassword.get().getPassword())){
            throw new RuntimeException("계정이 존재하지 않습니다.");
        }

        Optional<UserEntity> userOptional = userRepository.findByUserEmail(userDto.getUserEmail());
        if(!userOptional.isPresent()){
            throw new RuntimeException("계정이 존재하지 않습니다.");
        }

        UserEntity entity = userOptional.get();

        UserDto returnDto = UserDto.builder()
                .id(entity.getId())
                .userEmail(entity.getUserEmail())
                .username(entity.getUsername())
                .registerDate(entity.getRegisterDate())
                .build();



        return returnDto;
    }

    public boolean isExistUserEmail(String email) {

        Optional<UserEntity> userOptional = userRepository.findByUserEmail(email);
        if(userOptional.isPresent()){

            return true;
        }

        return false;
    }
}
