package com.spring.kimdongy1000.service;

import com.spring.kimdongy1000.dto.ContentDto;
import com.spring.kimdongy1000.dto.ImageFileDto;
import com.spring.kimdongy1000.dto.MyProfileDto;
import com.spring.kimdongy1000.dto.PagingDto;
import com.spring.kimdongy1000.entity.MainContentEntity;
import com.spring.kimdongy1000.entity.UserEntity;
import com.spring.kimdongy1000.mapper.MyProfileMapper;
import com.spring.kimdongy1000.repository.MainContentRepository;
import com.spring.kimdongy1000.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class MyProfileService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MainService mainService;

    @Autowired
    private MyProfileMapper myProfileMapper;

    @Autowired
    private MainContentRepository mainContentRepository;






    public MyProfileDto getMyProfile(String userId) {

        Optional<UserEntity> optionalUserEntity = userRepository.findById(userId);
        if(!optionalUserEntity.isPresent()){
            throw new RuntimeException("없는 사용자입니다");
        }

        UserEntity userEntity = optionalUserEntity.get();


        return MyProfileDto.builder().id(userEntity.getId())
                .userEmail(userEntity.getUserEmail())
                .username(userEntity.getUsername())
                .registerDate(userEntity.getRegisterDate())
                .profileId(userEntity.getProfileId())
                .build();


    }

    @Transactional(rollbackFor = Exception.class)
    public void changeMyProfile(MyProfileDto myProfileDto, String userId) {

        if(userId.equals(myProfileDto.getId())){
            throw new RuntimeException("허가되지 않은 권한입니다");
        }


        Optional<UserEntity> optionalUserEntity = userRepository.findById(userId);

        optionalUserEntity.ifPresent(item -> {

            item.setUsername(myProfileDto.getUsername() == null && myProfileDto.getUsername().equals("") ? item.getUsername() : myProfileDto.getUsername());


            userRepository.save(item);

        });




    }

    @Transactional(rollbackFor = Exception.class)
    public ImageFileDto changeMyImageChange(MultipartFile multipartFile, String clientId , String userId) throws Exception
    {

        Optional<UserEntity> optionalUserEntity = userRepository.findById(userId);
        UserEntity userEntity = optionalUserEntity.get();
        if(!clientId.equals(userEntity.getId())){
            throw new RuntimeException("권한이 없습니다");
        }

        ImageFileDto imageFileDto = mainService.uploadFile(clientId , multipartFile);

        optionalUserEntity.ifPresent(item -> {
            item.setProfileId(imageFileDto.getFileId());
            userRepository.save(item);
        });



        return imageFileDto;




    }

    public int getMyBoardCnt(String userId) {

        Map<String , Object> param = new HashMap<>();
        param.put("userId" , userId);



        return myProfileMapper.getMyBoardCnt(param);
    }

    public int getMyGoodYnCnt(String userId) {

        Map<String , Object> param = new HashMap<>();
        param.put("userId" , userId);

        return myProfileMapper.getMyBoardGoodCnt(param);
    }

    public List<ContentDto> getMyBoardContent(Map<String , Object> param) {

        return myProfileMapper.getMyBoardContent(param);
    }

    public PagingDto getCntMyBoardContent(Map<String , Object> param) {

        int totalCount = myProfileMapper.getMyBoardCnt(param);

        return PagingDto.builder()
                                .totalCount(totalCount)
                                .selectPage((Integer) param.get("selectPage"))
                                .build();
    }

    public void deleteMyBoardContent(String id, String userId) {

        Optional<MainContentEntity> optionalMainContentEntity = mainContentRepository.findById(id);

        optionalMainContentEntity.ifPresent( item -> {
            if(!item.getUserid().equals(userId)){
                throw new RuntimeException("권한이 없습니다");
            }

            item.setUseYn("N");

            mainContentRepository.save(item);

        });
    }
}
