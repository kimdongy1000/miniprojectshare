package com.spring.kimdongy1000.service;

import com.spring.kimdongy1000.security.TokenController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class PreChatService {

    @Autowired
    private TokenController tokenController;

    public Map<String , Object> parseMessage (Message<?> message){

        Map<String , Object> parseMessage = new HashMap<>();

        String convertBody = new String((byte[]) message.getPayload(), StandardCharsets.UTF_8); // 메세지 분리

        LinkedMultiValueMap nativeHeaders =  (LinkedMultiValueMap)message.getHeaders().get("nativeHeaders");
        String token = (String)nativeHeaders.get("authorization").get(0); // token 분리

        String userId = tokenController.validateAndGetUserId(token);

        parseMessage.put("userId" , userId);
        parseMessage.put("message" , convertBody);

        return parseMessage;



    }
}
