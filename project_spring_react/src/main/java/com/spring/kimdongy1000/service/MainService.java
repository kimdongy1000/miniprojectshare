package com.spring.kimdongy1000.service;

import com.spring.kimdongy1000.dto.*;
import com.spring.kimdongy1000.entity.FileEntity;
import com.spring.kimdongy1000.entity.MainContentEntity;
import com.spring.kimdongy1000.mapper.MainContentMapper;
import com.spring.kimdongy1000.repository.FileRepository;
import com.spring.kimdongy1000.repository.MainContentRepository;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;

@Service
public class MainService {

    @Value("${file.fileHomePath}")
    private String fileHomePath;

    @Value("${file.fileResize}")
    private String fileResizePath;

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private MainContentRepository mainContentRepository;



    @Autowired
    private MainContentMapper mainContentMapper;



    private static List<String> allowImgFileImage = new ArrayList<>(Arrays.asList("jpg", "png"));

    public ImageFileDto uploadFile(String userId, MultipartFile file) throws Exception {
        /*오리진 파일 생성*/
        String fileImageExt = FilenameUtils.getExtension(file.getOriginalFilename());

        if(!allowImgFileImage.contains(fileImageExt.toLowerCase())){
            throw new RuntimeException("지원되는 사진확장자가 아닙니다.");
        }


        Long fileSize = file.getSize();
        Long exchangeFileSize = (fileSize / 1024 ) / 1024; // 파일 MB 변환

        if(exchangeFileSize > 50){
            throw new RuntimeException("허용 용량을 초과 했습니다. 최대 50MB");
        }


        Date nowDate = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        String dirDate = simpleDateFormat.format(nowDate);



        File dFile = new File(fileHomePath + "/" + dirDate);

        if(!dFile.exists()){
            dFile.mkdirs();
        }

        StringBuffer filePathBuffer = new StringBuffer();
        filePathBuffer.append(fileHomePath);
        filePathBuffer.append("/");
        filePathBuffer.append(dirDate);
        filePathBuffer.append("/");
        filePathBuffer.append(UUID.randomUUID().toString());
        filePathBuffer.append(".");
        filePathBuffer.append(fileImageExt);

        byte fileByte[] = file.getBytes();


        FileOutputStream fileOutputStream = new FileOutputStream(filePathBuffer.toString());
        fileOutputStream.write(fileByte);
        fileOutputStream.close();



        FileEntity fileEntity = FileEntity.builder().userid(userId)
                                .filePath(filePathBuffer.toString())
                                .originName(file.getOriginalFilename())
                                .useYn("Y")
                                .registerDate( LocalDate.now().toString()).build();

        String fileId = fileRepository.save(fileEntity).getId();

        //return Base64.encodeBase64String(originFileResize(filePathBuffer , dirDate , fileImageExt));
        return ImageFileDto.builder().imageFileBase64( Base64.encodeBase64String(fileByte)).fileId(fileId).build();

    }

    //orginFileResize
    public byte[] originFileResize(StringBuffer filePathBuffer , String dirDate , String fileImageExt) throws Exception{

        File orignFile = new File(filePathBuffer.toString());
        InputStream inputStream = new FileInputStream(orignFile);
        Image img = new ImageIcon(orignFile.toString()).getImage();

        System.out.println("사진의 가로길이 :" +img.getWidth(null));
        System.out.println("사진의 세로길이 :" +img.getHeight(null));

        int resizeWidth = img.getWidth(null) < 512 ? img.getWidth(null) : 512 ;
        int resizeHeight = img.getHeight(null) < 512 ? img.getHeight(null) : 512 ;

        BufferedImage resizedImage = resize(inputStream , resizeWidth , resizeHeight);

        File rDFile = new File(fileResizePath + "/" + dirDate);

        if(!rDFile.exists()){
            rDFile.mkdirs();
        }


        StringBuffer resizeFilePathBuffer = new StringBuffer();
        resizeFilePathBuffer.append(fileResizePath);
        resizeFilePathBuffer.append("/");
        resizeFilePathBuffer.append(dirDate);
        resizeFilePathBuffer.append("/");
        resizeFilePathBuffer.append(UUID.randomUUID().toString());
        resizeFilePathBuffer.append(".");
        resizeFilePathBuffer.append(fileImageExt);

        File resizeFile = new File(resizeFilePathBuffer.toString());
        ImageIO.write(resizedImage , fileImageExt , resizeFile);



        return FileUtils.readFileToByteArray(resizeFile);
    }



    //파일 리사이즈
    public BufferedImage resize(InputStream inputStream , int resizeWidth , int resizeHeight) throws Exception{

        BufferedImage inputImage = ImageIO.read(inputStream);
        BufferedImage outputImage = new BufferedImage(resizeWidth , resizeHeight , inputImage.getType());


        Graphics2D graphics2D = outputImage.createGraphics();
        graphics2D.drawImage(inputImage , 0 , 0 , resizeWidth , resizeHeight , null);
        graphics2D.dispose();


        return outputImage;



    }


    @Transactional(rollbackFor = Exception.class)
    public void registerContent(PopContentDto popContentDto , String userId) {

        if(popContentDto.getTitle().equals("")){
            throw new RuntimeException("제목을 입력해주세요.");
        }

        if(popContentDto.getContent().equals("")){
            throw new RuntimeException("내용을 입력해주세요.");
        }

        LocalDateTime localDateTimeNow = LocalDateTime.now();
        String parsedLocalDateTimeNow = localDateTimeNow.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        MainContentEntity mainContentEntity = MainContentEntity.builder()
                                                                .userid(userId)
                                                                .fileId(popContentDto.getFileId())
                                                                .title(popContentDto.getTitle())
                                                                .content(popContentDto.getContent())
                                                                .useYn("Y")
                                                                .registerDate(parsedLocalDateTimeNow)
                                                                .build();

        mainContentRepository.save(mainContentEntity);
    }

    public List<ContentDto> getMainContentAll(Map<String , Object> param ) {

        return mainContentMapper.selectList(param);
    }

    public String getFilePath(String imageId) {

        Optional<FileEntity> fileEntityOptional = fileRepository.findById(imageId);

        FileEntity fileEntity = fileEntityOptional.get();

        return fileEntity.getFilePath();

    }

    public ContentDto isMyContent(String id , String userId) {
        boolean flag = false;

        Optional<MainContentEntity> mainContentEntity = mainContentRepository.findById(id);
        if(mainContentEntity.isPresent()){
            if(userId.equals(mainContentEntity.get().getUserid())){
                flag = true;
            }
        }

        MainContentEntity returnObtain = mainContentEntity.get();

        return ContentDto.builder().contentId(returnObtain.getId())
                                    .userId(returnObtain.getUserid())
                                    .fileId(returnObtain.getFileId())
                                    .title(returnObtain.getTitle())
                                    .content(returnObtain.getContent())
                                    .useYn(returnObtain.getUseYn())
                                    .registerDate(returnObtain.getRegisterDate())
                                    .isMyContent(flag).build();

    }

    @Transactional(rollbackFor = Exception.class)
    public void reviseContent(ReviseContentDto reviseContentDto, String userId) throws Exception{


        Optional<MainContentEntity> optionalMainContentEntity = mainContentRepository.findById(reviseContentDto.getContentId());

        optionalMainContentEntity.ifPresent( item -> {
            if(!item.getUserid().equals(userId)){
                throw new RuntimeException("수정할 권한이 없습니다.");
            }

            LocalDateTime localDateTimeNow = LocalDateTime.now();
            String parsedLocalDateTimeNow = localDateTimeNow.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

            item.setTitle(reviseContentDto.getTitleValue());
            item.setContent(reviseContentDto.getContent());
            item.setFileId(reviseContentDto.getFileId() == null ? item.getFileId() : reviseContentDto.getFileId());
            item.setModifyDate(parsedLocalDateTimeNow);



            mainContentRepository.save(item);






        });


    }

    @Transactional(rollbackFor = Exception.class)
    public void myfavorite(String contentId, String userId) {
        Map<String , Object> param = new HashMap<>();
        param.put("contentId" , contentId);
        param.put("userId" , userId);

        GoodContentDto goodContentDto = mainContentMapper.getGoodContent(param);
        if(goodContentDto.getGoodYn().equals("Y")){
            mainContentMapper.updateNoYn(param);
        }else{
            mainContentMapper.updateYesYn(param);
        }

    }

    public ContentDto getCntGoodYn(String id, String userId) {

        Map<String , Object> param = new HashMap<>();
        param.put("id" , id);
        param.put("userId" , userId);

        return mainContentMapper.getCntGoodYn(param);
    }

    public ContentDto getDetailContentGoodYn(String id, String userId) {

        Map<String , Object> param = new HashMap<>();
        param.put("id" , id);
        param.put("userId" , userId);

        return mainContentMapper.getDetailContentGoodYn(param);

    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteContent(String contentId, String userId) {

        Optional<MainContentEntity> mainContentEntityOptional = mainContentRepository.findById(contentId);

        if(!mainContentEntityOptional.isPresent()){
            throw new RuntimeException("게시물이 존재하지 않습니다");
        }

        mainContentEntityOptional.ifPresent( item -> {

            if(!item.getUserid().equals(userId)){
                throw new RuntimeException("게시글 삭제 권한이 없습니다");
            }
            item.setUseYn("N");
            mainContentRepository.save(item);
        });

    }
}
