package com.spring.kimdongy1000.service;

import com.spring.kimdongy1000.config.BcryptPassEncoder;
import com.spring.kimdongy1000.dto.LoginDto;
import com.spring.kimdongy1000.entity.UserEntity;
import com.spring.kimdongy1000.repository.UserRepository;
import com.spring.kimdongy1000.security.TokenController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginService {

    @Autowired
    private BcryptPassEncoder bcryptPassEncoder;

    @Autowired
    private UserRepository userRepository;



    public LoginDto findByUserEmailAndPassword(LoginDto loginDto) {

        Optional<UserEntity> optUserEntity = userRepository.findByUserEmail(loginDto.getUserEmail());
        if(!optUserEntity.isPresent()){
            throw new RuntimeException("이메일 또는 비밀번호가 올바르지 않습니다");
        }

        if(!bcryptPassEncoder.matches(loginDto.getPassword() , optUserEntity.get().getPassword()) ){
            throw new RuntimeException("이메일 또는 비밀번호가 올바르지 않습니다");
        }
        UserEntity userEntity = optUserEntity.get();


        return LoginDto.builder().id(userEntity.getId())
                .password(userEntity.getPassword())
                .useYn(userEntity.getUseYn())
                .username(userEntity.getUsername())
                .registerDate(userEntity.getRegisterDate()).build();



    }
}
