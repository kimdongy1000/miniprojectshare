package com.spring.kimdongy1000.service;

import com.spring.kimdongy1000.entity.FileEntity;
import com.spring.kimdongy1000.repository.FileRepository;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Optional;

@Service
public class OutLinedService {

    @Autowired
    private FileRepository fileRepository;

    public String imageFileBase64(String fileId) throws Exception{

        String returnMsg = null;
        Optional<FileEntity> optionalFileEntity = fileRepository.findById(fileId);

        if(optionalFileEntity.isPresent()){
            FileEntity fileEntity = optionalFileEntity.get();
            String filePath = fileEntity.getFilePath();
            File file = new File(filePath);

            byte[] fileContent = Files.readAllBytes(file.toPath());
            returnMsg  = Base64.encodeBase64String(fileContent);

        }

        return returnMsg;


    }
}
