package com.spring.kimdongy1000.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PopContentDto {


    private String fileId;
    private String title;
    private String content;


}
