package com.spring.kimdongy1000.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContentDto {

    private String contentId;
    private String userId;
    private String fileId;
    private String title;
    private String content;
    private String useYn;
    private String registerDate;
    private String username;
    private boolean isMyContent;
    private String goodYn;
    private int cntGoodYn;
    private String ROWNUM;





}
