package com.spring.kimdongy1000.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GoodContentDto {

    private String contentId;
    private String userId;
    private String goodYn;


}
