package com.spring.kimdongy1000.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PagingDto {

    private int totalCount;
    private int startCount;
    private int endCount;
    private int selectPage;


}
