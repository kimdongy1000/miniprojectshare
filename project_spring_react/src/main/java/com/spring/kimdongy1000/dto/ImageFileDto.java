package com.spring.kimdongy1000.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ImageFileDto {

    private String imageFileBase64;

    private String fileId;


}
