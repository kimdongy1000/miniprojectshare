package com.spring.kimdongy1000.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MyProfileDto {

    private String id;

    private String userEmail;

    private String username;

    private String password;

    private String useYn;

    private String registerDate;

    private String profileId;

}
