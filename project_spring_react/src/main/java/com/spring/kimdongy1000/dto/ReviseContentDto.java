package com.spring.kimdongy1000.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReviseContentDto {

    private String contentId;
    private String titleValue;
    private String content;
    private String fileId;

}
