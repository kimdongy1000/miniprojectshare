package com.spring.kimdongy1000.controller;

import com.spring.kimdongy1000.dto.*;
import com.spring.kimdongy1000.service.MainService;
import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.proxy.annotation.Post;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

@RestController
@RequestMapping("main")
@Slf4j
public class MainController {

    @Autowired
    private MainService mainService;

    @PostMapping("/upImageFileUpload")
    public ResponseEntity<?> imgFileUpload(@RequestParam(value = "file" , required = false) MultipartFile multipartFile ,
                                           @AuthenticationPrincipal String userId)
    {
        List<ImageFileDto> list = null;
        ResponseDto<ImageFileDto> responseDto = null;

        try {
            ImageFileDto imageFileBase64Encode = mainService.uploadFile(userId , multipartFile);

            list = new ArrayList<>();
            list.add(imageFileBase64Encode);

            responseDto = ResponseDto.<ImageFileDto>builder().data(list).build();



            return ResponseEntity.ok().body(responseDto);


        }catch (Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".imgFileUpload");
            responseDto = ResponseDto.<ImageFileDto>builder().error(e.getMessage()).data(null).build();
            return ResponseEntity.badRequest().body(responseDto);
        }
    }

    @PostMapping("/registerContent")
    public ResponseEntity<?> registerContent (@RequestBody PopContentDto popContentDto ,
                                              @AuthenticationPrincipal String userId)
    {
        ResponseDto<String> responseDto = null;
        List<String> list = null;
        try{
            mainService.registerContent(popContentDto , userId);

            list = new ArrayList<>();
            list.add("success");
            responseDto = ResponseDto.<String>builder().error(null).data(list).build();



            return ResponseEntity.ok().body(responseDto);

        }catch(Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".registerContent");
            responseDto = ResponseDto.<String>builder().error(e.getMessage()).data(null).build();
            return ResponseEntity.badRequest().body(responseDto);
        }
    }

    @GetMapping("/getMainContentAll")
    public ResponseEntity<?> getMainContentAll (
                                                    @AuthenticationPrincipal String userId) throws Exception{
        ResponseDto<ContentDto> responseDto = null;
        List<ContentDto> list = null;

        try{

            Map<String , Object> param = new HashMap<>();
            param.put("userId" , userId);

            list = mainService.getMainContentAll(param);

            responseDto = ResponseDto.<ContentDto>builder().data(list).build();


            return ResponseEntity.ok().body(responseDto);
        }catch (Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".getMainContentAll");
            responseDto = ResponseDto.<ContentDto>builder().error(e.getMessage()).data(null).build();
            return ResponseEntity.badRequest().body(responseDto);
        }

    }

    @GetMapping("/isMyContent/{id}")
    public ResponseEntity<?> isMyContent(@PathVariable String id ,
                                         @AuthenticationPrincipal String userId

    )
    {
        ResponseDto<ContentDto> responseDto = null;
        List<ContentDto> list = null;
        try{
            ContentDto returnData = mainService.isMyContent(id , userId);

            ContentDto dto = mainService.getCntGoodYn(id , userId);
            returnData.setCntGoodYn(dto.getCntGoodYn());
            dto = mainService.getDetailContentGoodYn(id , userId);
            returnData.setGoodYn(dto.getGoodYn());



            list = new ArrayList<>();
            list.add(returnData);

            responseDto = ResponseDto.<ContentDto>builder().data(list).build();

            return ResponseEntity.ok().body(responseDto);
        }catch(Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".isMyContent");
            responseDto = ResponseDto.<ContentDto>builder().error(e.getMessage()).data(null).build();
            return ResponseEntity.badRequest().body(responseDto);
        }

    }


    @PostMapping("/reviseContent")
    public ResponseEntity<?> reviseContent (@RequestBody ReviseContentDto reviseContentDto ,
                                            @AuthenticationPrincipal String userId ) {

        ResponseDto<Boolean> responseDto = null;
        List<Boolean> list = null;
        boolean flag = false ;

        try{


            mainService.reviseContent(reviseContentDto , userId);
            list = new ArrayList<>();
            list.add(true);
            responseDto = ResponseDto.<Boolean>builder().data(list).build();

            return ResponseEntity.ok().body(responseDto);
        }catch(Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".reviseContent");
            responseDto = ResponseDto.<Boolean>builder().data(null).error(e.getMessage()).build();

            return ResponseEntity.badRequest().body(responseDto);
        }
    }

    @GetMapping("/myfavorite/{contentId}")
    public ResponseEntity<?> myFavoriteContent ( @PathVariable("contentId") String contentId ,
                                                 @AuthenticationPrincipal String userId)
    {
        ResponseDto<Boolean> responseDto = null;
        List<Boolean> list = null;

        try{
            mainService.myfavorite(contentId , userId);
            list = new ArrayList<>();
            list.add(true);

            responseDto = ResponseDto.<Boolean>builder().data(list).build();

            return ResponseEntity.ok().body(responseDto);

        }catch(Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".myFavoriteContent");
            responseDto = ResponseDto.<Boolean>builder().data(null).error(e.getMessage()).build();

            return ResponseEntity.badRequest().body(responseDto);

        }

    }

    @DeleteMapping("/deleteContent/{contentId}")
    public ResponseEntity<?> deleteContent (@PathVariable("contentId") String contentId ,
                                            @AuthenticationPrincipal String userId
                                            )
    {
        ResponseDto<Boolean> responseDto = null;
        List<Boolean> list = null;

        try{
            mainService.deleteContent(contentId , userId);
            list = new ArrayList<>();
            list.add(true);

            responseDto = ResponseDto.<Boolean>builder().data(list).build();


            return ResponseEntity.ok().body(responseDto);

        }catch(Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".deleteContent");
            responseDto = ResponseDto.<Boolean>builder().data(null).error(e.getMessage()).build();
            return ResponseEntity.badRequest().body(responseDto);

        }

    }










}
