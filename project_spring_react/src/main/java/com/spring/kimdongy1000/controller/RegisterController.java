package com.spring.kimdongy1000.controller;

import com.google.gson.Gson;
import com.spring.kimdongy1000.dto.ResponseDto;
import com.spring.kimdongy1000.dto.UserDto;
import com.spring.kimdongy1000.service.RegisterService;
import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.proxy.annotation.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@RestController
@RequestMapping("member")
@Slf4j
public class RegisterController {

    @Autowired
    private RegisterService registerService;

    /* 회원가입을 받는 핸들러
    *  method : post
    *  RequestBody : userDto
    *  return : ResponseEntity 로 회원가입 완료한 데이터를 내 보낼 예정 (name , id , etc ..)
    * */
    @PostMapping
    public ResponseEntity<?> registerHandler(@RequestBody UserDto userDto){
        ResponseDto<UserDto> responseDto = null;
        List<UserDto> list = null;

        try{
            registerService.registerUser(userDto);

            UserDto returnDto = registerService.getUserOptional(userDto);
            list = new ArrayList<>();
            list.add(returnDto);
            responseDto = ResponseDto.<UserDto>builder().data(list).build();

            return ResponseEntity.ok().body(responseDto);
        }catch(Exception e){
            log.error("Fail api registerHandler {}" , this.getClass().getName() + ".registerHandler");
            responseDto = ResponseDto.<UserDto>builder().error(e.getMessage()).data(null).build();
            return ResponseEntity.badRequest().body(responseDto);
        }

    }

    @PostMapping("/isExistsUserEmail")
    public ResponseEntity<?> isExistsUserEmail(@RequestBody UserDto userDto) {
        ResponseDto<Boolean> responseDto = null;
        List<Boolean> list = null;

        try{
            boolean isExistFlag = registerService.isExistUserEmail(userDto.getUserEmail());
            list = new ArrayList<>();
            list.add(isExistFlag);
            responseDto = ResponseDto.<Boolean>builder().data(list).build();
            return ResponseEntity.<Boolean>ok().body(responseDto);
        }catch (Exception e){
            log.error("Fail api registerHandler {}" , this.getClass().getName() + ".isExistsUserEmail");
            responseDto = ResponseDto.<Boolean>builder().error(e.getMessage()).data(null).build();
            return ResponseEntity.badRequest().body(responseDto);
        }
    }





}
