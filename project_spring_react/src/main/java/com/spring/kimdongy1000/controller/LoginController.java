package com.spring.kimdongy1000.controller;

import com.spring.kimdongy1000.dto.LoginDto;
import com.spring.kimdongy1000.dto.ResponseDto;
import com.spring.kimdongy1000.security.TokenController;
import com.spring.kimdongy1000.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("login")
@Slf4j
public class LoginController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private TokenController tokenController;


    @PostMapping
    public ResponseEntity<?> loginApi(@RequestBody LoginDto loginDto){
        ResponseDto<LoginDto> responseDto = null;
        List<LoginDto> list = null;
        try{

            LoginDto entityReturnLoginDto =  loginService.findByUserEmailAndPassword(loginDto);
            String token = tokenController .tokenCreate(entityReturnLoginDto);

            entityReturnLoginDto.setToken(token);

            list = new ArrayList<>();
            list.add(entityReturnLoginDto);
            responseDto = ResponseDto.<LoginDto>builder().data(list).build();

            return ResponseEntity.ok().body(responseDto);


        }catch(Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".loginApi");
            list = null;
            responseDto = ResponseDto.<LoginDto>builder().data(null).error(e.getMessage()).build();
            return ResponseEntity.badRequest().body(responseDto);
        }

    }
}
