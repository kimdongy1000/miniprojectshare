package com.spring.kimdongy1000.controller;

import com.spring.kimdongy1000.dto.ResponseDto;
import com.spring.kimdongy1000.service.MainService;
import com.spring.kimdongy1000.service.OutLinedService;
import lombok.extern.slf4j.Slf4j;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("store")
@Slf4j
public class OutLinedController {

    @Autowired
    private MainService mainService;

    @Autowired
    private OutLinedService outLinedService;



    @GetMapping("/getMainContent/img/{imageId}")
    public ResponseEntity<Resource> imageFileDown(@PathVariable String imageId){

        try{
            String filePath = mainService.getFilePath(imageId);
            Resource resource = new FileSystemResource(filePath);
            HttpHeaders headers = new HttpHeaders();
            Tika tika = new Tika();
            headers.add("Content-type" , tika.detect(resource.getFile()));
            return new ResponseEntity<>(resource , headers , HttpStatus.OK);
        }catch(Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".imageFileDown");
            return ResponseEntity.badRequest().body(null);
        }
    }

    @GetMapping("/getMainCOnent/img/encodebase64/{imageId}")
    public ResponseEntity<?> encodeBase64 (@PathVariable String imageId){
        ResponseDto<String> responseDto = null;
        List<String> list = null;

        try{

            String base64  = outLinedService.imageFileBase64(imageId);
            list = new ArrayList<>();
            list.add(base64);
            responseDto = ResponseDto.<String>builder().data(list).build();

            return ResponseEntity.ok().body(responseDto);
        }catch (Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".encodeBase64");
            responseDto = ResponseDto.<String>builder().error(e.getMessage()).data(null).build();
            return ResponseEntity.badRequest().body(responseDto);
        }

    }
}
