package com.spring.kimdongy1000.controller;

import com.spring.kimdongy1000.dto.*;
import com.spring.kimdongy1000.service.MyProfileService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Delete;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("myprofile")
public class MyProfileController {

    @Autowired
    private MyProfileService myProfileService;

    @GetMapping
    public ResponseEntity<?> getMyProfile (@AuthenticationPrincipal String userId) {

        ResponseDto<MyProfileDto> responseDto = null;
        List<MyProfileDto> list = null;

        try{
            MyProfileDto myProfileDto = myProfileService.getMyProfile(userId);

            list = new ArrayList<>();
            list.add(myProfileDto);

            responseDto = ResponseDto.<MyProfileDto>builder().data(list).build();

            return ResponseEntity.ok().body(responseDto);

        }catch(Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".getMyProfile");
            responseDto = ResponseDto.<MyProfileDto>builder().data(null).error(e.getMessage()).build();

            return ResponseEntity.badRequest().body(responseDto);

        }
    }

    @PostMapping("/changeMyProfile")
    public ResponseEntity<?> changeMyProfile(@RequestBody MyProfileDto myProfileDto ,
                                             @AuthenticationPrincipal String userId
                                             ){

        ResponseDto<Boolean> responseDto = null;
        List<Boolean> list = null;

        try{

            myProfileService.changeMyProfile(myProfileDto , userId);
            list = new ArrayList<>();
            list.add(true);
            responseDto = ResponseDto.<Boolean>builder().data(list).build();

            return ResponseEntity.ok().body(responseDto);

        }catch(Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".getMyProfile");
            responseDto = ResponseDto.<Boolean>builder().data(null).error(e.getMessage()).build();
            return ResponseEntity.badRequest().body(responseDto);
        }
    }

    @PostMapping("/myImageChange")
    public ResponseEntity<?> myImageChange (@RequestParam("file") MultipartFile multipartFile ,
                                            @RequestParam("parameter") String parameter ,
                                            @AuthenticationPrincipal String userId

                                            ) {

        ResponseDto<ImageFileDto> responseDto = null;
        List<ImageFileDto> list = null;


        try{

            JSONParser jsonParser = new JSONParser();
            JSONObject json = (JSONObject)jsonParser.parse(parameter);
            String clientId = (String)json.get("clientId");

            ImageFileDto imageFileDto = myProfileService.changeMyImageChange(multipartFile , clientId , userId);

            list = new ArrayList<>();
            list.add(imageFileDto);

            responseDto = ResponseDto.<ImageFileDto>builder().data(list).build();


            return ResponseEntity.ok().body(responseDto);

        }catch(Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".myImageChange");
            responseDto = ResponseDto.<ImageFileDto>builder().error(e.getMessage()).build();

            return ResponseEntity.badRequest().body(responseDto);
        }

    }


    @GetMapping("/getMyBoardCnt")
    public ResponseEntity<?> getMyBoardCnt ( @AuthenticationPrincipal String userId)
    {
        ResponseDto<Integer> responseDto = null;
        List<Integer> list = null;

        try{

            int myBoardInt = myProfileService.getMyBoardCnt(userId);
            list = new ArrayList<>();
            list.add(myBoardInt);
            responseDto = ResponseDto.<Integer>builder().data(list).build();

            return ResponseEntity.ok().body(responseDto);

        }catch(Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".getMyBoardCnt");
            responseDto = ResponseDto.<Integer>builder().error(e.getMessage()).build();
            return ResponseEntity.badRequest().body(responseDto);
        }


    }

    @GetMapping("/getGoodYnCnt")
    public ResponseEntity<?> getBoardGoodYnCnt (@AuthenticationPrincipal String userId)
    {

        ResponseDto<Integer> responseDto = null;
        List<Integer> list = null;

        try{

            int myGoodYnCnt = myProfileService.getMyGoodYnCnt(userId);
            list = new ArrayList<>();
            list.add(myGoodYnCnt);
            responseDto = ResponseDto.<Integer>builder().data(list).build();

            return ResponseEntity.ok().body(responseDto);

        }catch(Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".getBoardGoodYnCnt");
            responseDto = ResponseDto.<Integer>builder().error(e.getMessage()).build();
            return ResponseEntity.badRequest().body(responseDto);
        }

    }

    @GetMapping("/getMyBoardContent/{selectPage}")
    public ResponseEntity<?> getMyBoardContent  ( @AuthenticationPrincipal String userId ,
                                                  @PathVariable String selectPage

                                                )
    {

        ResponseDto<List> responseDto = null;

        List<ContentDto> list = null;
        List<PagingDto> pagingDtoList = null;


        try{

            Map<String , Object> param = new HashMap<>();

            param.put("selectPage" , Integer.parseInt(selectPage));
            param.put("userId" , userId);

            List<List> returnList = new ArrayList<>();

            list = myProfileService.getMyBoardContent(param);
            PagingDto pagingDto = myProfileService.getCntMyBoardContent(param);



            pagingDtoList = new ArrayList<>();
            pagingDtoList.add(pagingDto);



            returnList.add(list);
            returnList.add(pagingDtoList);



            responseDto = ResponseDto.<List>builder().data(returnList).build();
            return ResponseEntity.ok().body(responseDto);

        }catch (Exception e ){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".getMyBoardContent");
            responseDto = ResponseDto.<List>builder().error(e.getMessage()).build();
            return ResponseEntity.badRequest().body(responseDto);
        }


    }

    @DeleteMapping("/deleteMyBoardContent/{id}")
    public ResponseEntity<?> deleteMyBoardContent(@PathVariable String id ,
                                                  @AuthenticationPrincipal String userId

    ){

        ResponseDto<Boolean> responseDto = null;
        List<Boolean> list = null;


        try{

            myProfileService.deleteMyBoardContent(id , userId);
            list = new ArrayList<>();
            list.add(true);

            responseDto = ResponseDto.<Boolean>builder().data(list).build();


            return ResponseEntity.ok().body(responseDto);


        }catch(Exception e){
            log.error("Fail api loginApi {}" , this.getClass().getName() + ".deleteMyBoardContent");
            responseDto = ResponseDto.<Boolean>builder().error(e.getMessage()).build();
            return ResponseEntity.badRequest().body(responseDto);
        }

    }






}
