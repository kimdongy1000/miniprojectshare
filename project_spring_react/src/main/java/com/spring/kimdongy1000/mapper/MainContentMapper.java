package com.spring.kimdongy1000.mapper;

import com.spring.kimdongy1000.dto.ContentDto;
import com.spring.kimdongy1000.dto.GoodContentDto;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class MainContentMapper {

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;


    public List<ContentDto> selectList(Map<String , Object> param ) {

        return this.sqlSessionTemplate.selectList(this.getClass().getName() + ".selectList" , param);
    }

    public GoodContentDto getGoodContent(Map<String, Object> param) {

        return sqlSessionTemplate.selectOne(this.getClass().getName() + ".getGoodContent" , param);
    }

    public void updateNoYn(Map<String, Object> param) {

        sqlSessionTemplate.update(this.getClass().getName() + ".updateNoYn" , param);
    }

    public void updateYesYn(Map<String, Object> param) {

        sqlSessionTemplate.update(this.getClass().getName() + ".updateYesYn" , param);
    }

    public ContentDto getCntGoodYn(Map<String, Object> param) {

        return sqlSessionTemplate.selectOne(this.getClass().getName() + ".getCntGoodYn" , param);
    }

    public ContentDto getDetailContentGoodYn(Map<String, Object> param) {

        return sqlSessionTemplate.selectOne(this.getClass().getName() + ".getDetailContentGoodYn" , param);
    }
}
