package com.spring.kimdongy1000.mapper;

import com.spring.kimdongy1000.dto.ContentDto;
import com.spring.kimdongy1000.dto.PagingDto;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class MyProfileMapper {

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    public int getMyBoardCnt(Map<String, Object> param) {

        return sqlSessionTemplate.selectOne(this.getClass().getName() + ".getMyBoardCnt" , param);
    }

    public int getMyBoardGoodCnt(Map<String, Object> param) {

        return sqlSessionTemplate.selectOne(this.getClass().getName() + ".getMyBoardGoodCnt" , param);
    }

    public List<ContentDto> getMyBoardContent(Map<String, Object> param) {

        return sqlSessionTemplate.selectList(this.getClass().getName() + ".getMyBoardContent" , param);
    }

    public List<PagingDto> getCntMyBoardContent(Map<String, Object> param) {

        return sqlSessionTemplate.selectOne(this.getClass().getName() + ".getCntMyBoardContent" , param);
    }
}
