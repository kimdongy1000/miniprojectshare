package com.spring.kimdongy1000.repository;

import com.spring.kimdongy1000.entity.FileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FileRepository extends JpaRepository<FileEntity , String> {

    Optional<FileEntity> findById(String id);

}
