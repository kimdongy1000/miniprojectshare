package com.spring.kimdongy1000.repository;

import com.spring.kimdongy1000.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity , String> {

    Optional<UserEntity> findByUserEmailAndPassword(String userEmail  , String password);

    Optional<UserEntity> findByUserEmail(String email);
}
