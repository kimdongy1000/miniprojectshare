package com.spring.kimdongy1000.repository;

import com.spring.kimdongy1000.entity.MainContentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MainContentRepository extends JpaRepository<MainContentEntity , String> {
}
