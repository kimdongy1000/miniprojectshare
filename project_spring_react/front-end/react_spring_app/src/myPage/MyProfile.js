import * as React from 'react';

import ButtonAppBar2 from '../commonComponent/AppBar2';
import SiderBar from './SiderBar';
import MyBoardInfo from './MyBoardInfo'
import MyDetailProfile from './MyDetailProfile';
import { apiFileUploadService, apiGetService, apiService } from '../commonJs/CommonApiService';
import { Box, Container} from '@mui/material';
import Paper from '@mui/material/Paper';


import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';

import TableRow from '@mui/material/TableRow';
import MyBoardInfoData from './MyBoardInfoData';
import MyBot from './MyBot';
import { ChatContainer, MainContainer, Message, MessageInput, MessageList , MessageSeparator } from '@chatscope/chat-ui-kit-react';
import styles from "@chatscope/chat-ui-kit-styles/dist/default/styles.min.css";
import Chat from './Chat';







class MyProfile extends React.Component {

  constructor(props) {
    super(props)

    this.state = {

      myEmail: null,
      myUsername: null,
      myId : null , 
      myProFileNameError : false , 
      myProFileNameHelpText : null , 

      myProfileImg : null , 
      myBoardInfo : false , 
      myBoardDetail : null ,
      
      myBot : false , 




    }
  }

  componentDidMount = () => {
    this.getMyDetailProfile();
  }


  logoutClick = () => {
    localStorage.removeItem("ACCESS_TOKEN");
    window.location = "/"
  
}

  getMyDetailProfile = () => {
    
    
    apiGetService("GET", "/myprofile").then((res) => {
    
      this.setState({
        myEmail: res.data[0].userEmail,
        myUsername: res.data[0].username,
        myId : res.data[0].id
      })



      apiGetService("GET" , `/store/getMainCOnent/img/encodebase64/${res.data[0].profileId}`).then((res) => {
        
        fetch(`data:image/jpg;base64,${res.data[0]}`).then(res2 => res2.blob()).then( (blob) => {
          
          const objectURL = URL.createObjectURL(blob);
          this.setState({myProfileImg : objectURL } , () => {
            
          })
          
  
  
        })
      })


    })





  }

  myDetailChangeUserName = (username) => {

    this.setState({
      myUsername : username
    })

  }

  myProfileChange = () => {
    if(!this.state.myUsername){
      this.setState({

        myProFileNameError : true , 
        myProFileNameHelpText : "이름을 입력해주세요."

      })
      return false
    }
    const paramData = {
      username : this.state.myUsername 
    }

    const method = "POST";

    const apiserviseName = "/myprofile/changeMyProfile"

    apiService(paramData , method , apiserviseName).then((res) => {
      
    })
  }



  myProfileImgChange = (file) => {

    const addParameter = {
      clientId : this.state.myId
    }

    apiFileUploadService(file , "POST" , "/myprofile/myImageChange" , addParameter ).then((res) => {

      fetch(`data:image/jpg;base64,${res.data[0].imageFileBase64}`).then(res => res.blob()).then( (blob) => {
          
        const objectURL = URL.createObjectURL(blob);
        
        this.setState({myProfileImg : objectURL })


      })
      
    })

  }

  myBoardInfoRender = (selectPage) => {

    if(!selectPage){
      selectPage = 1;
    }

    this.setState({ myBoardInfo : false} , () => {

      apiGetService("GET" , `/myprofile/getMyBoardContent/${selectPage}`).then( (res) => {
        this.setState({myBoardDetail : res } , () => {
          this.setState({ myBot : false , 
                          myBoardInfo : true })
        })
      })

    })
  }

  myContentDeletefun = (contentId) => {

    // 삭제 api 는 페이지 깜박임으로 처리 
    apiGetService("DELETE" , `/myprofile/deleteMyBoardContent/${contentId}`).then( (res) => {

      window.location.replace("/myProfile")
    
    })

  }

  myBotRender = () => {
    this.setState({
      myBot : true , 
      myBoardInfo : false 
    })
  }




  render() {

    let myBoardInfoComponent = null;

    if(this.state.myBoardInfo){
      myBoardInfoComponent =  (  
      
                      <MyBoardInfoData 
                            myBoardDetail = {this.state.myBoardDetail}
                            myBoardInfoRenderFun = {this.myBoardInfoRender}
                            myContentDelete = {this.myContentDeletefun}


                      
                      
                      
                      /> 
      )
    }else {
      myBoardInfoComponent =  null
    }

    let myBotChatComponent = null;

    if(this.state.myBot){
        myBotChatComponent = (

          <Chat
          
          />
          


        )


    }else{

    }

    


    return (
      <>
        <ButtonAppBar2  pathClick2={this.logoutClick}
                        myProfileImage = {this.state.myProfileImg}
                        path2="로그아웃"
                        myAppBarProfile = {this.state.myProfileImg}
                        
                        >
        </ButtonAppBar2>


        <SiderBar />


<Container  maxWidth="lg">

<TableContainer component={Paper} style = {{marginTop : "5%"}}>
      <Table sx={{ minWidth: 800 }} size="string" aria-label="a dense table"  >
        <TableBody>
          
            <TableRow>
              <TableCell component="th" scope="row" style={{width : "500px"}}>
                <MyDetailProfile 
                      myEmail = {this.state.myEmail}
                      myUsername = {this.state.myUsername}
                      myId = {this.state.myId}
                      myDetailChangeUserName = {this.myDetailChangeUserName}
                      myProfileChange = {this.myProfileChange}
                      myProFileNameError = {this.state.myProFileNameError}
                      myProFileNameHelpText = {this.state.myProFileNameHelpText}
                      myProfileImgChange = {this.myProfileImgChange}
                      myProfileImage = {this.state.myProfileImg}
                />
              </TableCell>
                
              <TableCell component="th" scope="row">
                  <MyBoardInfo 
                    myBoardInfoRender = {this.myBoardInfoRender}
                    
                  />
              </TableCell>

              <TableCell component="th" scope="row">
                <MyBot myBotRender = {this.myBotRender}/>
                  
              </TableCell>
            </TableRow>
        </TableBody>
      </Table>
    </TableContainer>

    </Container>

    <Container  maxWidth="lg">

      <Box sx={{ bgcolor: '#FFFFFF', height: '100vh' }} >
        
        {myBoardInfoComponent}
        
        {myBotChatComponent}
        
        
      
       
      
      </Box>
    
    </Container>


        


      </>















    )
  }
}

export default MyProfile