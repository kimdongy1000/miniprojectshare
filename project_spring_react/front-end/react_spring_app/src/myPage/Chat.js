import { ChatContainer, MainContainer, Message, MessageInput, MessageList, MessageSeparator } from "@chatscope/chat-ui-kit-react";
import React from "react";
import SockJS from "sockjs-client";
import Stomp from "stompjs";


class Chat extends React.Component{

    constructor(props){

        super(props)

    }

    render(){

        let sockJS = new SockJS("http://localhost:8080/chat");
        let stompClient = Stomp.over(sockJS);

        let headers = {Authorization: localStorage.getItem('ACCESS_TOKEN')};
        

        stompClient.connect(headers , () => {
           
            
            stompClient.send("/app/hello", headers, JSON.stringify({'name': "kimdongy1004" }));
        })

    



      


        

        



        return (
            <div style={{ position:"relative", height: "500px" }}> 
                <MainContainer>
                    <ChatContainer>       
                        <MessageList>
                            <Message model={{
                                    message: "Hello my friend",
                                    sentTime: "just now",
                                    sender: "Joe",
                                    direction: "incoming"
                            }} />
                        <MessageSeparator content="Saturday, 30 November 2019" />
                            <Message model={{
                                     message: "Hello my friend",
                                     sentTime: "just now",
                                    sender: "Ja",
                                    direction: "outgoing"
                                }} />
                            </MessageList>
                        <MessageInput placeholder="Type message here" />        
                    </ChatContainer>
                </MainContainer>
            </div>


        )
    }
}

export default Chat