import { Avatar, Box, Card, CardContent, Container, Grid, TextField, Typography } from '@mui/material';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import MoneyIcon from '@mui/icons-material/Money';
import React from "react";
import { apiGetService, apiService } from '../commonJs/CommonApiService';
import { checkImgFile } from '../commonJs/checkFile';
import ConfirmDisagreeComponentError from '../dialogComponent/ConfirmDisagreeComponentError';



class MyDetailProfile extends React.Component{

    constructor(props){
        super(props)
        this.myDetailChangeUserName = this.props.myDetailChangeUserName
        this.myProfileChange = this.props.myProfileChange
        this.myProfileImgChange= this.props.myProfileImgChange

        this.state = {
            
            myUsername : null , 

            ErrorPopOpen : false , 
            popTitle : null , 
            popContent : null , 





            

        }
    }

    componentDidMount = () => {
        
    }


    myNameChange = (e) => {
        this.myDetailChangeUserName(e.target.value)
    
    }

    myNameFocusOut = () => {

        this.myProfileChange()

        
    }

    myProfileClick = () => {
        const fileInputClick = document.querySelector("#upFileClick");
        fileInputClick.click()
    }

    fileChange = (e) => {

        let file_name = e.target.files[0].name
        if(!checkImgFile(file_name)){
            this.setState({

                ErrorPopOpen : true ,
                popTitle : "파일 확장자 오류" ,
                popContent : "지원하는 파일 확장자가 아닙니다. (png , jpg)"

            })

            return false;

        }

        this.myProfileImgChange(e.target.files[0]);





    }

    errorPopClose = () => {
        this.setState({
            
            ErrorPopOpen : false
        
        })
    }

   

    




    render(){
        
        
        return(

            <>

            <ConfirmDisagreeComponentError 
                open = {this.state.ErrorPopOpen}
                close = {this.errorPopClose}
                popupTitle = {this.state.popTitle}
                popupContent = {this.state.popContent}
                disaggagree = {false}
            />


            {/* <Container maxWidth="lg"> */}
                <Card
                    sx={{ bgcolor: "#cfe8fc", height: "10%"  , width : "100%"}}
                    style = {{marginTop : "3%"}}
                >
                    <CardContent>
                        <Grid
                            container
                            spacing={2}
                            sx={{ justifyContent: 'space-between' }}
                        >
                        <Grid item>
                            <Typography
                                color="textSecondary"
                                gutterBottom
                                variant="overline"
                            >
                            내정보
                            </Typography>
                            <Avatar
                                    sx={{
                                        height: 56,
                                        width: 56
                                    }}
                                    src={this.props.myProfileImage}
                                    onClick = {this.myProfileClick}
                                    style = {{ backgroundsize : "auto"}}
                                />
                                <div style = {{display : "none"}}>
                                    <input type="file" id="upFileClick" accept=".jpg, .png" onChange={this.fileChange}/>
                                </div>
                            </Grid>
                                <Grid item>
                                    <Typography
                                        color="textPrimary"
                                        variant="h5"
                                    >
                                    {this.props.myEmail}
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Box
                                sx={{
                                    pt: 2,
                                    display: 'flex',
                                    alignItems: 'center'
                                }}
                            >
                                <TextField  required
                                            id="standard-required" 
                                            label="내이름" 
                                            variant="standard"  
                                            value = {this.props.myUsername}
                                            onChange = {this.myNameChange}
                                            onBlur = {this.myNameFocusOut}
                                            error = {this.props.myProFileNameError}
                                            helperText = {this.props.myProFileNameHelpText}
                                            defaultValue="Hello World"
                                            />
                            </Box>
                        </CardContent>
                    </Card>
            {/* </Container> */}

            </>
        )
    }



}

export default MyDetailProfile