import { Avatar, Card, CardContent, Grid, Typography } from "@mui/material";
import CreateIcon from '@mui/icons-material/Create';
import FavoriteIcon from '@mui/icons-material/Favorite';
import { red } from '@mui/material/colors';
import React from "react";
import { apiGetService, apiService } from "../commonJs/CommonApiService";

class MyBoardInfo extends React.Component{

    constructor(props){
        super(props)

        this.myBoardInfoRender = this.props.myBoardInfoRender

        this.state = {

            myBoardCnt : 0 , 
            myGoodYnCnt : 0 , 


        }
    }

    componentDidMount() {
        this.getMyBoardCnt();
        this.getMyGoodYn();
    }
        
    

    getMyBoardCnt = () => {
        
        apiGetService("GET", "/myprofile/getMyBoardCnt").then((res) => {
            
            

            this.setState({
                
                myBoardCnt :  res.data[0]

            })
        })
    }

    getMyGoodYn = () => {

        apiGetService("GET", "/myprofile/getGoodYnCnt").then((res) => {
            
            

            this.setState({
                
                myGoodYnCnt :  res.data[0]

            })
        })
    }

    myBoardInfoClick = () => {

        this.myBoardInfoRender();
    }





    render(){
        
        
        return(
            <Card
                sx={{ bgcolor: "#FFFFFF", height: "10%"  , width : "100%"}}
                style = {{marginTop : "3%" , cursor : "pointer"}}
                onClick = {this.myBoardInfoClick}
            > 

                <CardContent>

                    <Grid
                            container
                            spacing={2}
                            sx={{ justifyContent: 'space-between' }}
                        >

                        <Grid item>
                            <Typography
                                color = "textSrcondary"
                                gutterBottom
                                variant="overline"
                            >        
                                내 게시글 정보
                            </Typography>
                        </Grid>
                    </Grid>
                        <Grid
                            container
                            spacing={2}
                            sx={{ justifyContent: 'space-between' }}
                        >
                            


                            <Grid item>
                                


                                <Typography
                                    color = "textSrcondary"
                                    gutterBottom
                                    variant="overline"
                                >
                                    
                                

                                </Typography>

                                <CreateIcon
                                    sx={{
                                        height: 30,
                                        width: 30
                                    }}
                                />
                                
                                
                                
                            </Grid>
                            <Grid item>
                                    <Typography
                                        color="textPrimary"
                                        variant="h5"
                                    >

                                <FavoriteIcon
                                
                                    sx={{
                                        height: 30,
                                        width: 30 , 
                                        color : red[500]
                                    }}
                                />
                                
                                    
                                    </Typography>
                                </Grid>
                                
                        </Grid>
                        <Grid
                            container
                            spacing={2}
                            sx={{ justifyContent: 'space-between' }}
                        >
                            <Grid item>
                                <Typography
                                    color = "textSrcondary"
                                    gutterBottom
                                    variant="h5"
                                >

                                    {this.state.myBoardCnt}
                                
                                </Typography>

                                
                            </Grid>
                            <Grid item>
                                    <Typography
                                        color="textPrimary"
                                        variant="h5"
                                    >

                                    {this.state.myGoodYnCnt}
                                    
                                    </Typography>
                                </Grid>
                                
                        </Grid>
                        <Grid
                            container
                            spacing={2}
                            sx={{ justifyContent: 'space-between' }}
                        >

                        <Grid item>
                            <Typography
                                color = "textSrcondary"
                                gutterBottom
                                variant="overline"
                            >        
                                내가 쓴글 
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography
                                color = "textSrcondary"
                                gutterBottom
                                variant="overline"
                            >        
                                좋아요 받은 개수 
                            </Typography>
                        </Grid>
                    </Grid>
                       

                </CardContent>


            </Card>



        )
    }



}

export default MyBoardInfo;