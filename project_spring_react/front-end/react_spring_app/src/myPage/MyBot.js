import { Avatar, Card, CardContent, Grid, Typography } from "@mui/material";
import CreateIcon from '@mui/icons-material/Create';
import FavoriteIcon from '@mui/icons-material/Favorite';
import { red } from '@mui/material/colors';
import React from "react";
import { apiGetService, apiService } from "../commonJs/CommonApiService";

class MyBot extends React.Component{

    constructor(props){
        super(props)

        this.myBotRender = this.props.myBotRender

        

        this.state = {

        


        }
    }

    componentDidMount() {


    }

    myBotClick = () => {
        this.myBotRender();
    }
        
    


    render(){
        
        
        return(
            <Card
                sx={{ bgcolor: "#FFFFFF", height: "10%"  , width : "100%"}}
                style = {{marginTop : "3%" , cursor : "pointer"}}
                onClick = {this.myBotClick}
            > 

                <CardContent>

                    <Grid
                            container
                            spacing={2}
                            sx={{ justifyContent: 'space-between' }}
                        >

                        <Grid item>
                            <Typography
                                color = "textSrcondary"
                                gutterBottom
                                variant="overline"
                            >        
                                내 비서
                            </Typography>
                        </Grid>
                    </Grid>
                        <Grid
                            container
                            spacing={2}
                            sx={{ justifyContent: 'space-between' }}
                        >
                            <Grid item>
                                <Typography
                                    color = "textSrcondary"
                                    gutterBottom
                                    variant="overline"
                                >
                                </Typography>
                                <CreateIcon
                                    sx={{
                                        height: 30,
                                        width: 30
                                    }}
                                />
                            </Grid>                                
                        </Grid>
                        <Grid
                            container
                            spacing={2}
                            sx={{ justifyContent: 'space-between' }}
                        >
                            <Grid item>
                                <Typography
                                    color = "textSrcondary"
                                    gutterBottom
                                    variant="h5"
                                >

                                    
                                
                                </Typography>

                                
                            </Grid>
                            <Grid item>
                                    <Typography
                                        color="textPrimary"
                                        variant="h5"
                                    >

                                    
                                    
                                    </Typography>
                                </Grid>
                                
                        </Grid>
                        <Grid
                            container
                            spacing={2}
                            sx={{ justifyContent: 'space-between' }}
                        >

                       
                    </Grid>
                       

                </CardContent>


            </Card>



        )
    }



}

export default MyBot;