import React from "react";
import { Button, Pagination, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";

class MyBoardInfoData extends React.Component{

    constructor(props){
        super(props)

        this.myBoardInfoRender = this.props.myBoardInfoRenderFun
        this.myContentDeletefun = this.props.myContentDelete




        this.state = {

          selectPage : null , 
          endPage : null , 
          
          

        }
    }

    componentDidMount() {
      
      let pagingArray = this.props.myBoardDetail.data[1];
      
      this.setState({
                        selectPage : pagingArray[0].selectPage , 
                        endPage : Math.ceil((pagingArray[0].totalCount )  / 10) ,
                    })
      
    }

    pagingClick = (e) => {
      
      this.myBoardInfoRender(e.target.innerText)
    }

    

    render(){
      
      let gridRowData = this.props.myBoardDetail.data[0];

      const myBoardContentDelete = (contentId) => {
        
        
        this.myContentDeletefun(contentId);
        
        
        

  
      }

   

   
      
      
      //let gridPagingData = this.props.myBoardDetail.data[1];

      //console.log(gridPagingData)
      
      
        return(
            <div>
              <TableContainer > 
                <Table>
                  <TableHead>
                    <TableRow>
                        <TableCell>id</TableCell>
                        <TableCell>제목</TableCell>
                        <TableCell>좋아요 개수</TableCell>
                        <TableCell>등록일</TableCell>
                        <TableCell></TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {gridRowData.map((row) => (
                        
                      <TableRow key = {row.rownum}>
                          <TableCell>{row.rownum}</TableCell>
                          <TableCell>{row.title}</TableCell>
                          <TableCell>{row.cntGoodYn}</TableCell>
                          <TableCell>{row.registerDate}</TableCell>
                          <TableCell align="right">
                              <Button
                                  type = "button"
                                  variant='contained'
                                  color="error"
                                  onClick={ () => { myBoardContentDelete(row.contentId) }}
                              >삭제</Button>
                          </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
              
                <Pagination size="large" style = {{float:"right"}}
                    count={this.state.endPage}
                    page = {this.state.selectPage}
                    onClick = {this.pagingClick}
                    hideNextButton = {true}
                    hidePrevButton = {true}
                    
                
                />
                


                
                
                
            </div>
        )
    }

}

export default MyBoardInfoData