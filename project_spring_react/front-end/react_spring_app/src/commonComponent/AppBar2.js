import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { Avatar } from '@mui/material';
import { apiGetService, apiService } from '../commonJs/CommonApiService';

/*로그아웃 appBar*/
class ButtonAppBar2 extends React.Component{

    constructor(props){
        super(props)
        this.pathClick1 = props.pathClick1;
        this.pathClick2 = props.pathClick2;



        this.state = {

            myProfileImg : this.props.myProfileImage

        }
        
    }

    componentDidMount = () => {
        this.usually();

   
        
    }

    usually = () => {
        apiGetService("GET" , "/myprofile").then((res) => {
            apiGetService("GET" , `/store/getMainCOnent/img/encodebase64/${res.data[0].profileId}`).then((res) => {
                fetch(`data:image/jpg;base64,${res.data[0]}`).then(res => res.blob()).then( (blob) => {
                    const objectURL = URL.createObjectURL(blob);
                    this.setState({myProfileImg : objectURL } , () => {
                      
                    })
                })
            })
        })
            
    }



  

    avartClick = () => {
        window.location = "/myProfile"
    }


    render (){
        let appProfileImage = null;

        if(Object.keys(this.props).includes("myAppBarProfile")){
            if(this.props.myAppBarProfile){
                appProfileImage = (<Avatar  onClick = {this.avartClick} src={this.props.myAppBarProfile} />)
            }
            
        }else{
            appProfileImage = (<Avatar  onClick = {this.avartClick} src={this.state.myProfileImg} />)
        }
        
        return (
            <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                <IconButton
                    size="large"
                    edge="start"
                    color="inherit"
                    aria-label="menu"
                    sx={{ mr: 2 }}
                >
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    {this.props.pageName}
                </Typography>
                <Button color="inherit" onClick={ this.pathClick1 }>
                    {this.props.path1}
                </Button>
                <Button color="inherit" onClick= {this.pathClick2 }>
                    {this.props.path2}
                </Button>
                <IconButton  sx={{ p: 0 }}>
                    {appProfileImage}
                </IconButton>
                </Toolbar>
                
                
            </AppBar>
            </Box>
        );
    }
}

export default ButtonAppBar2;