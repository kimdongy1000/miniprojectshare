import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

/*로그인 appBar*/
class ButtonAppBar extends React.Component{

    constructor(props){
        super(props)
        this.pathClick1 = props.pathClick1;
        this.pathClick2 = props.pathClick2;
        
    }


    render (){
        return (
            <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                <IconButton
                    size="large"
                    edge="start"
                    color="inherit"
                    aria-label="menu"
                    sx={{ mr: 2 }}
                >
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    {this.props.pageName}
                </Typography>
                <Button color="inherit" onClick={ this.pathClick1 }>
                    {this.props.path1}
                </Button>
                <Button color="inherit" onClick= {this.pathClick2 }>
                    {this.props.path2}
                </Button>
                </Toolbar>
            </AppBar>
            </Box>
        );
    }
}

export default ButtonAppBar;