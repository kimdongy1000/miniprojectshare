import React from 'react';
import ButtonAppBar from '../commonComponent/AppBar';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { Container, TextField } from '@mui/material';
import Button from '@mui/material/Button';
import { apiService } from '../commonJs/CommonApiService';
import ConfirmDisagreeComponentError from '../dialogComponent/ConfirmDisagreeComponentError';


class Login extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            email : null  , 
            password : null , 
            
            emailTextBoxErrorHelp : false , 
            emailTextBoxHelpMessage : null , 

            passwordTextBoxErrorHelp : false , 
            passwordTextBoxHelpMessage : null , 

            openPop : false


        }
    }

    path1Click = () =>{
        window.location = "/"
    }

    path2Click = () =>{
        window.location = "/register"
    }

    onLoginEnterTouch = () =>{
        
        if (window.event.keyCode === 13) {
            this.loginButtonOnClick()
            
        }
    }

    handleClose = () =>{
        this.setState({openPop : false})
    }

    loginButtonOnClick = () => {

        if(!(this.state.email)){
            this.setState({emailTextBoxErrorHelp : true , emailTextBoxHelpMessage : "이메일을 입력해주세요"})
            return false;
        }

        if(!(this.state.password)){
            this.setState({passwordTextBoxErrorHelp : true , passwordTextBoxHelpMessage : "비밀번호를 입력해주세요"})
            return false;
        }

        const paramData = {userEmail : this.state.email , password : this.state.password};
        const apiName = "";

        apiService(paramData , "POST"  , "/login").then((res) => {
            if(res.error != null){
                this.setState({openPop : true});
                
                return false;
            }
            /*로그인 성공*/
            /*토큰 세션추가*/
            const accessToken = res.data[0].token;
            localStorage.setItem("ACCESS_TOKEN" , accessToken); 
            window.location = "/main";

        })



        
    }

    onChangeEmail = (e) => {
        let email = e.target.value;
        this.setState({email : email})


        if(email){
            this.setState({emailTextBoxErrorHelp : false , emailTextBoxHelpMessage : null})
        }
    }

    onChangePassword = (e) => {
        let password = e.target.value;
        this.setState({password : password})

        if(password){
            this.setState({passwordTextBoxErrorHelp : false , passwordTextBoxHelpMessage : null})
        }
    }



    render(){
        return (
            <div>
                <ButtonAppBar   pageName="하루의_Spring" 
                                path1="로그인" 
                                path2="회원가입" 
                                pathClick1 = {this.path1Click} 
                                pathClick2 = {this.path2Click}/>

                <Container component="main" maxWidth="xs" style = {{marginTop : "8%"}} spacing={2}>
                    <Grid>
                        <Grid item xs = {12}>
                            <Typography component="h1" variant = "h5">
                                로그인
                            </Typography>
                        </Grid>
                    </Grid>
                </Container>

                <Container component="main" maxWidth="xs" style = {{marginTop : "3%"}}>
                    <Grid item xs={12}>
                        <TextField  variant = "outlined"
                                    required 
                                    fullWidth
                                    id = "email"
                                    label = "이메일 주소"
                                    name = "email"
                                    autoComplete='email'
                                    onChange={this.onChangeEmail}
                                    error={this.state.emailTextBoxErrorHelp}
                                    helperText={this.state.emailTextBoxHelpMessage}            
                                    onKeyPress={this.onLoginEnterTouch}
                                    
                        />
                    </Grid>

                </Container>

                <Container component="main" maxWidth="xs" style = {{marginTop : "3%"}}>
                    <Grid item xs={12}>
                        <TextField  variant = "outlined"
                                    required 
                                    fullWidth
                                    id = "password"
                                    label = "비밀번호"
                                    name = "password"
                                    autoComplete='password'
                                    type="password"
                                    error={this.state.passwordTextBoxErrorHelp}
                                    helperText={this.state.passwordTextBoxHelpMessage}            
                                    onChange={this.onChangePassword}
                                    onKeyPress={this.onLoginEnterTouch}
                        />
                    </Grid>
                </Container>

                <Container component="main" maxWidth="xs" style = {{marginTop : "2%"}} >
                <Grid item xs={12}>
                    <Button
                        type = "button"
                        fullWidth
                        variant='contained'
                        color='primary'
                        onClick={this.loginButtonOnClick}
                        
                    >
                    로그인
                    </Button>
                </Grid>
            </Container>

            <ConfirmDisagreeComponentError  open={this.state.openPop} 
                                                popupTitle = "로그인 오류" 
                                                popupContent="아이디 비밀번호가 일치하지 않습니다" 
                                                close = {this.handleClose}
                                                disaggagree = {false}/>
            </div>
        )
    }
}

export default Login;