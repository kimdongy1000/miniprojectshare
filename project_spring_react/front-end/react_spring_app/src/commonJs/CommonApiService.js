import { domainAddress } from "./domain";


const domainName = domainAddress;


export function apiService(paramData ,method, apiServiceName){
    const api = domainName + apiServiceName;
    let headers =  new Headers({
        "Content-Type" : "application/json"
    });
    const accessToken = localStorage.getItem("ACCESS_TOKEN"); 
    if(accessToken && accessToken !== null ){
        headers.append("Authorization" , "Bearer " + accessToken);
    }


    let options = {
        headers : headers,
        url : domainName + apiServiceName , 
        method : method
    
    };

    
    if(paramData){
        options.body = JSON.stringify(paramData)
    }
    
    return fetch(options.url, {
        headers :  headers , 
        method: method,
        body: JSON.stringify(paramData),
    })
    .then((response) => {
        if(response.status === 403){
            localStorage.removeItem("ACCESS_TOKEN");
            window.location = "/"
        }else{
            return response.json();
        }   
    })
    .catch((error) => {
        Promise.reject(error);
    });

}

export function apiFileUploadService(file ,method, apiServiceName){
    const api = domainName + apiServiceName;

    let headers =  new Headers({
        
    });

    const accessToken = localStorage.getItem("ACCESS_TOKEN"); 
    if(accessToken && accessToken !== null ){
        headers.append("Authorization" , "Bearer " + accessToken);
    }


    let options = {
        headers : headers,
        url : domainName + apiServiceName , 
        method : method
    
    };

    let formData = new FormData()
    formData.append('file', file)

    let args = arguments;

    if(args.length > 3) {
        formData.append("parameter" , JSON.stringify(args[3]))
    }
    
    

    return fetch(options.url, {
        headers :  headers , 
        method: method,
        processData: false,
        contentType: false,
        body: formData
    })
    .then((response) => {
        if(response.status === 403){
            localStorage.removeItem("ACCESS_TOKEN");
            window.location = "/"
        }else{
            return response.json();
        }   
        
    })
    .catch((error) => {
        Promise.reject(error);
    });
}

export function apiGetService(method,apiServiceName){

    const api = domainName + apiServiceName;

    let headers =  new Headers({
        
    });

    const accessToken = localStorage.getItem("ACCESS_TOKEN"); 
    if(accessToken && accessToken !== null ){
        headers.append("Authorization" , "Bearer " + accessToken);
    }

    let options = {
        headers : headers,
        url : domainName + apiServiceName , 
        method : method
    
    };

    return fetch(options.url, {
        headers :  headers , 
        method: method,
        
    })
    .then((response) => {
        if(response.status === 403){
            localStorage.removeItem("ACCESS_TOKEN");
            window.location = "/"
        }else{
            return response.json();
        }   
    })
    .catch((error) => {
        Promise.reject(error);
    });




}