import React from "react";
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';

class SuccessAlert extends React.Component{

    constructor(props){
        super(props)
    }

    render(){
        return (
            <Stack sx={{ width: '100%' }} spacing={2}>
            <Alert severity="success">{this.props.message}</Alert>
          </Stack>
        )
    }
}

export default SuccessAlert;