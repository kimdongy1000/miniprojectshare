import React from "react"
import Login from "../loginComponent/Login"
import {BrowserRouter as Router , Routes, Route } from "react-router-dom"
import Register from "../registerComponent/Register"
import MainComponent from "../main/MainComponent"
import MyProfile from "../myPage/MyProfile"

class AppRouter extends React.Component{

    constructor(props){
        super(props)
      
    }

    render(){
        return (
            <div>
                  <Router>
                    <div>
                        <Routes>
                            
                            <Route path="/register" element={ localStorage.getItem("ACCESS_TOKEN") === null ?  <Register />  : <MainComponent /> } >
                            </Route>
                            
                            <Route path="/" element={ localStorage.getItem("ACCESS_TOKEN") === null ?  <Login /> :    <MainComponent /> } >
                            </Route>
                            
                            <Route 
                                path="/main" element={localStorage.getItem("ACCESS_TOKEN") !== null ? <MainComponent /> : <Login/> }>
                            </Route>

                            <Route 
                                path="/myProfile" element={localStorage.getItem("ACCESS_TOKEN") !== null ? <MyProfile /> : <Login/> }>
                            </Route>

                        </Routes>
                    </div>
                    </Router>
            </div>
        )
    }
}

export default AppRouter;
