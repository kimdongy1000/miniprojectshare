import React from "react";
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import ButtonBase from '@mui/material/ButtonBase';
import { Button, Dialog, DialogActions, styled, TextField } from '@mui/material';
import { domainAddress } from "../commonJs/domain";
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import { red } from "@mui/material/colors";
import Badge , {BadgeProps } from '@mui/material/Badge';



class MainContentDetail extends React.Component{

    constructor(props){
        super(props)
        this.mainDetailImagePopClose = this.props.close
        this.mainPopDetailCloseAndReviseOpen = this.props.revise
        
        this.detailFavoriteClick = this.props.favoriteClick

        this.state = {
            img : null,
            titleValue : null,
            content : null ,
        }
    }

    handleClose = () => {
        this.mainDetailImagePopClose()
    }

    hanldeRevise = () => {
        this.mainPopDetailCloseAndReviseOpen(this.props.contentDetailId ,this.props.imgDetailPop)
    }
    
    

    


    

    render(){
        let disaggreeButton = null; 
        if(this.props.disaggagree === true){
            disaggreeButton = <Button onClick={this.handleClose}>닫기</Button>
        }

        let reviseButton = null;
        if(this.props.isMyContent === true){
            reviseButton = <Button onClick={this.hanldeRevise}>수정</Button>
        }

        const Img = styled('img')({
            margin: 'auto',
            display: 'block',
            maxWidth: '100%',
            maxHeight: '100%',
            

          });

          const favoritClick = (contentId) => {
            this.detailFavoriteClick(contentId);
          }

      


        return (
            <Dialog
                    open={this.props.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    fullWidth = {true}
                    maxWidth = "700">
            <Paper>
                <Grid container spacing={2}>
                    <Grid item>
                        <ButtonBase sx={{ width: 512, height: 500 }}>
                            <Img alt="complex" src={`${domainAddress}/store/getMainContent/img/${this.props.imgDetailPop}`} style = {{marginTop : "10%" , backgroundsize : "auto"}}  />
                        </ButtonBase>
                    </Grid>
                <Grid item xs={10} sm container>
                    <Grid item xs container direction="column" spacing={10}>
                        <Grid item xs>
                            <Typography gutterBottom variant="subtitle1" component="div" style = {{marginTop : "4%"}}>            
                                <TextField  id="standard-basic" 
                                    variant="standard" 
                                    fullWidth={true} 
                                    value = {this.props.titleDetailPop}> 
                                </TextField>
                            </Typography>
                            <Typography variant="body2" gutterBottom style = {{marginTop : "3%"}}>
                                <TextField
                                            id="outlined-multiline-static"
                                            multiline
                                            rows={15}
                                            fullWidth = {true}
                                            value = {this.props.contentDetailPop}
                                            >
                                    </TextField>
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography sx={{ cursor: 'pointer' }} variant="body2">
                            
                            
                                <Badge badgeContent={this.props.cntGoodYn} color="primary" style = {{ marginTop : "0%"}}>
                                {
                                    
                                    this.props.goodYn === 'N' ? <FavoriteBorderIcon  sx={{ fontSize: 35 }} onClick = { () => {favoritClick(this.props.contentDetailId) } } /> : <FavoriteIcon sx={{ color: red[500] , fontSize: 35}} onClick = { () => {favoritClick(this.props.contentDetailId) } }/>
                                }
                                </Badge>
                                
                            
                                
                                
                                <DialogActions>
                                    <div>
                                        {reviseButton}
                                     </div>
                                    <div>
                                        {disaggreeButton}
                                     </div>
                                </DialogActions>
                            </Typography>
                        </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
        </Dialog>
    )}
}

export default MainContentDetail;
