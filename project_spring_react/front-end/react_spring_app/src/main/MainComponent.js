import * as React from 'react';
import ButtonAppBar2 from '../commonComponent/AppBar2';
import { apiGetService, apiService } from '../commonJs/CommonApiService';
import MainAddImageComponent from './AddComponent/MainAddImageComponent';
import MainAddComponent from './MainAddComponent';
import MainContentDetail from './MainContentDetail';
import MainContentRevise from './MainContentRevise';
import MainImageList from './MainImageList';


class MainComponent extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            mainAddImagePopOpen : false ,
            mainContent : null , 



            mainDetailImagePopOpen : false , 
            isMyContent : false ,
            imgDetailPop : null ,
            titleDetailPop : null , 
            contentDetailPop : null , 
            contentDetailId : null , 
            contentDetailGoodYn : false , 
            contentDetailCntGood : null , 



            mainReviseImagePopOpen : false ,
            mainReviseImageBase64 : null , 
            mainReviseContentId : null , 
            mainReviseContentTitle : null , 
            mainReviseContentContent : null , 
            mainReviseFileId : null , 

            reviseComRender : false , 

            



            
            
        }

        
    }

    logoutClick = () => {
        localStorage.removeItem("ACCESS_TOKEN");
        window.location = "/"
      
    }

    mainAddButtonClick = () =>{
        this.setState({mainAddImagePopOpen : true})
        
    }

    mainAddButtonClickClose = () => {
        this.setState({mainAddImagePopOpen : false})
        this.mainContentGet();
    }




    mainContentGet = () => {
        apiGetService( "GET" , "/main/getMainContentAll" ).then((res) => {
            if(res.error !== null){

                return false;
            }

            

            
            const tempList = new Array();
            let tempList2 = null;


            if(res.data.length > 0 ){

                for(let i = 1; i <= Math.ceil(res.data.length / 4); i++){
                    tempList2 = new Array();
                    
                     for(let j = (i * 4) - 4; j < (i * 4) && j < res.data.length; j++){
                        
                        // if(j >= res.data.length){
                        //     tempList.push(tempList2);
                        //     break;
                        // }
                         
                        
                        tempList2.push(res.data[j]);
                         if(j % 4 === 0){
                             tempList.push(tempList2);
                         }
                     }
                     

                }
                
                this.setState({mainContent : tempList})   
                
                return false;

            }

                     
        })
    }
    componentDidMount() {
        this.mainContentGet()
    }






    contentDetail = (id) =>{
        this.setState({mainDetailImagePopOpen : true})
        apiGetService("GET" , `/main/isMyContent/${id}`).then((res) => {

            

            
            
            this.setState({ isMyContent : res.data[0].myContent , 
                            imgDetailPop : res.data[0].fileId , 
                            titleDetailPop : res.data[0].title , 
                            contentDetailPop : res.data[0].content ,  
                            contentDetailId : res.data[0].contentId , 
                            contentDetailGoodYn : res.data[0].goodYn , 
                            contentDetailCntGood : res.data[0].cntGoodYn
            })
        })
    }


    mainDetailImagePopClose = () => {
        this.setState({ mainDetailImagePopOpen : false , 
                        isMyContent : false  , 
                        
        
        })
    }

    mainPopDetailCloseAndReviseOpen = (contentId , fileId) => {
        
        apiGetService("GET" , `/store/getMainCOnent/img/encodebase64/${fileId}`).then((res) =>{
            this.setState({
                mainReviseImageBase64 : res.data[0]     
            })

            apiGetService("GET" , `/main/isMyContent/${contentId}`).then((res) => {
                this.setState({ 
    
                    reviseComRender : true , 
                    mainReviseContentId : res.data[0].contentId , 
                    mainReviseContentTitle : res.data[0].title , 
                    mainReviseContentContent : res.data[0].content , 
                    mainReviseFileId : res.data[0].fileId , 
                    mainReviseImagePopOpen : true , 
                    mainDetailImagePopOpen : false                             
                })
    
            })


        })

    
                
    }

 




    mainReviseImagePopClose = () => {
        this.setState({  mainReviseImagePopOpen : false , 
                        mainReviseImageBase64 : null , 
                        mainReviseContentId : null , 
                        mainReviseContentTitle : null , 
                        mainReviseContentContent : null , 
                        mainReviseFileId : null , 

        })
    }



    mainReviseRequest = (contentId , titleValue , content) => {

        const paramData = {
            contentId : contentId , 
            titleValue : titleValue , 
            content : content
        }

        const method = "POST" 
        const apiServiceName = "/main/reviseContent";
        apiService(paramData , method  , apiServiceName).then((res) => {
        })


    }

    favoriteClick = (contentId) => {

        apiGetService("GET" , `/main/myfavorite/${contentId}`).then((res) => {
            if(res.error != null){

                return false;
            }
            this.mainContentGet();
        })

    }

    detailFavoriteClick = (contentId) => {

        apiGetService("GET" , `/main/myfavorite/${contentId}`).then((res) => {
            if(res.error != null){

                return false;
            }

            apiGetService("GET" , `/main/isMyContent/${contentId}`).then((res) => {
                if(res.error != null){

                    return false;
                }

                this.setState({
                    contentDetailGoodYn : res.data[0].goodYn , 
                    contentDetailCntGood : res.data[0].cntGoodYn
                })
            })  
        })
    }

    deleteContent = (contentId) => {
        console.log('deleteContent',contentId)

        apiGetService("delete" , `/main/deleteContent/${contentId}`).then((res) => {
            window.location = "/main"
        })
    }




    



    render(){

        let reviseComponent = null ;
        if(this.state.reviseComRender){

            reviseComponent = (

                <MainContentRevise
                            open = {this.state.mainReviseImagePopOpen}
                            close = {this.mainReviseImagePopClose}
                            disaggagree = {true}
                
                
                            imgRevise = {this.state.mainReviseImageBase64}
                            titleRevise = {this.state.mainReviseContentTitle}
                            contentRevise = {this.state.mainReviseContentContent}
                            contentIdRevise = {this.state.mainReviseContentId}  
                            deleteContent = {this.deleteContent}
                            
                
            
            />
            )

        }



        return (
            <div>
                <ButtonAppBar2  pathClick2  = {this.logoutClick} 
                                path2 = "로그아웃"
                                pageName = "메인페이지"
                                />
                
                <MainAddComponent addClick = {this.mainAddButtonClick} />

                <MainAddImageComponent  open = {this.state.mainAddImagePopOpen}
                                        disaggagree = {true}
                                        close = {this.mainAddButtonClickClose}
                
                />
                <MainImageList content = {this.state.mainContent}
                                detailClick = {this.contentDetail}
                                favoriteClick = {this.favoriteClick}
                />

                <MainContentDetail 
                    content = {this.state.mainContent} 
                    detailClick = {this.contentDetail}
                    open = {this.state.mainDetailImagePopOpen}
                    close = {this.mainDetailImagePopClose}
                    revise = {this.mainPopDetailCloseAndReviseOpen}
                    disaggagree = {true}
                    imgDetailPop = {this.state.imgDetailPop}
                    titleDetailPop = {this.state.titleDetailPop}
                    contentDetailPop = {this.state.contentDetailPop}    
                    contentDetailId = {this.state.contentDetailId}
                    isMyContent = {this.state.isMyContent}
                    goodYn = {this.state.contentDetailGoodYn}
                    cntGoodYn = {this.state.contentDetailCntGood}
                    favoriteClick = {this.detailFavoriteClick}
                />

                {reviseComponent}

          


            </div>
            
            
                
            
            
                
            
        )
    }
}

export default MainComponent;