import React from "react";
import SpeedDial from '@mui/material/SpeedDial';
import SpeedDialIcon from '@mui/material/SpeedDialIcon';




class MainAddComponent extends React.Component{

    constructor(props){
        super(props)
        this.addClick = props.addClick;
    }

    render(){
        return (
            <SpeedDial
                ariaLabel="SpeedDial basic example"
                sx={{ position: 'absolute', bottom: 16, right: 16 }}
                icon={<SpeedDialIcon/>}
                onClick = {this.addClick}
                
            >
</SpeedDial>
        )
    }
}

export default MainAddComponent

