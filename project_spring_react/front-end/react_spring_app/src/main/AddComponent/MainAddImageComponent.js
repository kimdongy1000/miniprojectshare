import * as React from 'react';
import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import ButtonBase from '@mui/material/ButtonBase';
import { Button, Dialog, DialogActions, TextField } from '@mui/material';

import uploadphoto from '../../img/uploadphoto.png';
import { checkImgFile } from '../../commonJs/checkFile';
import ConfirmDisagreeComponentError from '../../dialogComponent/ConfirmDisagreeComponentError';
import { apiFileUploadService, apiService } from '../../commonJs/CommonApiService';
import { domainAddress } from '../../commonJs/domain';


class MainAddImageComponent extends React.Component{

    constructor(props){
        super(props)
        this.popupClose = this.props.close

        this.state = {

          errorPopOnen : false  ,
          img : uploadphoto , 
          upFileImageId : null , 

          poptitle : null ,
          popcontent : null , 



          popTitleError : false , 
          popTitleErrorText : null , 

          popContentError : false , 
          popContentErrorText : null , 

          popInPopUpImageTitle : null , 
          popInPopUpImageConent : null 


          





        }
    }

    handleClose = () => {
        this.setState({ img : uploadphoto , 
                        upFileImageId : null , 

                        poptitle : null , 
                        popcontent : null , 

                        popTitleError : false , 
                        popTitleErrorText : null , 

                        popContentError : false , 
                        popContentErrorText : null , 
                        
                        
                        popInPopUpImageTitle : null , 
                        popInPopUpImageConent : null 
                      
                      })
        this.popupClose()
        
    }
    uploadphoto = () => {
      const photoFileUpClick = document.querySelector("#upFileClick");
      photoFileUpClick.click();
    }

    fileChange = (e) => {
      const files = e.target.files[0];
      const fileFlag = checkImgFile(files.name);
      if(!fileFlag){
        this.setState({ errorPopOnen : true   , 
                        popInPopUpImageTitle :  "파일 업로드 에러" , 
                        popInPopUpImageConent : "지원하는 파일 확장자가 아닙니다. (png , jpg)"
                      })

        return false;
      }

      
      apiFileUploadService(files , "POST" , "/main/upImageFileUpload").then((res) => {
        
        if(res.error !== null){
          this.setState({ errorPopOnen : true   , 
                          popInPopUpImageTitle :  "파일 업로드 에러" , 
                          popInPopUpImageConent : res.error
          })
          return false;
        }
        //console.log(`data:image/jpg;base64,${res.data[0].imageFileBase64}`);
        //this.setState({ img : `data:image/jpg;base64,${res.data[0].imageFileBase64}` , upFileImageId : res.data[0].fileId })

        this.setState({upFileImageId : res.data[0].fileId })
        
        fetch(`data:image/jpg;base64,${res.data[0].imageFileBase64}`).then(res => res.blob()).then( (blob) => {
          
          const objectURL = URL.createObjectURL(blob);
          
          this.setState({img : objectURL })


        })

        
        
                        
        
      })

      



    }

    errorPopClose = () =>{
      this.setState({errorPopOnen : false })
    }

    titleOnchange = (e) => {
      let title = e.target.value;
      this.setState({poptitle : title});
    }

    contentOnchange = (e) => {
      let content = e.target.value;
      this.setState({popcontent : content});

    }

    popRegister = () => {
      
      if(!this.state.poptitle){
        this.setState({
          popTitleError : true , 
          popTitleErrorText : '제목을 입력해주세요.' , 

        })
        return false;
      }

      if(!this.state.popcontent){
          this.setState({
            popContentError : true , 
            popContentErrorText : "내용을 입력해주세요"
          })
        return false;
      }

      const paramData = {
        fileId : this.state.upFileImageId , 
        title : this.state.poptitle , 
        content : this.state.popcontent
      }

      

      const method = "POST";
      const apiServiceHandler = "/main/registerContent";

      apiService(paramData , method , apiServiceHandler).then((res) =>{
        if(res.error !== null){
          this.setState({ errorPopOnen : true   , 
            popTitle :  "게시글 등록 에러" , 
            popContent : res.error
          })
          return false;
        }

        this.handleClose();

      })





    }





    render(){
        let disaggreeButton = null; 
        if(this.props.disaggagree === true){
            disaggreeButton = <Button onClick={this.handleClose}>취소</Button>
        }

        const Img = styled('img')({
            margin: 'auto',
            display: 'block',
            maxWidth: '100%',
            maxHeight: '100%',
           

          });

 


        return (
            
            <Dialog
                    open={this.props.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    fullWidth = {true}
                    maxWidth = "700"
                    
                    
                >
            <Paper
          
          >
            <ConfirmDisagreeComponentError  open = {this.state.errorPopOnen} 
                                            close = {this.errorPopClose}
                                            popupTitle = {this.state.popInPopUpImageTitle}
                                            popupContent = {this.state.popInPopUpImageConent}
                                            disaggreeButton = {false}
                                            
            />
            
            <Grid container spacing={2}>
              <Grid item>
                <ButtonBase sx={{ width: 512, height: 512 }}>
                  
                    <Img alt="complex" src={this.state.img} onClick={this.uploadphoto}  style = {{marginTop : "10%" , backgroundsize : "auto"}}  />
                    <div style = {{display : "none"}}>
                    <input type="file" id="upFileClick" accept=".jpg, .png" onChange={this.fileChange}/>
                    </div>
                </ButtonBase>
              </Grid>
              <Grid item xs={10} sm container>
                <Grid item xs container direction="column" spacing={10}>
                  <Grid item xs>
                    <Typography gutterBottom variant="subtitle1" component="div">            
                
                        <TextField  id="standard-basic" 
                                    label="제목" 
                                    variant="standard" 
                                    fullWidth={true} 
                                    onChange = {this.titleOnchange}
                                    
                                    error={this.state.popTitleError}
                                    helperText={this.state.popTitleErrorText}            
                                    > 
                                    {/* {this.state.poptitle}  */}
                                    </TextField>
                          
              
                    </Typography>
                    <Typography variant="body2" gutterBottom style = {{marginTop : "8%"}}>
                      <TextField
                            id="outlined-multiline-static"
                            label="내용"
                            multiline
                            rows={15}
                            fullWidth = {true}
                            onChange = {this.contentOnchange}
                            error={this.state.popContentError}
                            helperText={this.state.popContentErrorText}            
                        >
                          {/* {this.state.popcontent} */}
                        </TextField>
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography sx={{ cursor: 'pointer' }} variant="body2">
                    <DialogActions>
                        {disaggreeButton}
                        <Button onClick={this.popRegister} autoFocus>
                            등록
                        </Button>
                    </DialogActions>
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
          </Dialog>
        )
    }

    
}

export default MainAddImageComponent