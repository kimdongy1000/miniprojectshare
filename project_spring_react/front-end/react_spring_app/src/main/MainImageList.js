import React from "react";
import ImageList  from "@mui/material/ImageList";
import ImageListItem  from "@mui/material/ImageListItem";
import ImageListItemBar  from "@mui/material/ImageListItemBar";
import ListSubheader  from "@mui/material/ListSubheader";
import IconButton  from "@mui/material/IconButton";
import InfoIcon from '@mui/icons-material/Info';
import { Badge, ListItemSecondaryAction, Table, TableBody, TableCell, TableContainer, TableRow } from "@mui/material";
import { width } from "@mui/system";
import { domainAddress } from "../commonJs/domain";
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import { red } from "@mui/material/colors";

class MainImageList extends React.Component{

    constructor(props){
        super(props)        
        this.contentDetail = this.props.detailClick
        this.favoriteClick = this.props.favoriteClick

        this.state = {
          contentId : null 
        }

        
    }

    imgOnClick = (e) => {
      const id = e.target.id
      this.contentDetail(id);
      
    }




    render(){
    const favoritClick = (contentId) => {
      this.favoriteClick(contentId);
    }

    let favoritImg = null;


      let renderValue = null;
      if(this.props.content !== null){
        renderValue = (
          <TableContainer>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableBody>
               {this.props.content.map((data) =>
                <TableRow>
                  {
                    data.map((row) => (
                      <TableCell align="right">
                        <ImageListItem key = {row.contentId}>
                          <img
                              src = {`${domainAddress}/store/getMainContent/img/${row.fileId}`}
                              srcSet = {`${domainAddress}/store/getMainContent/img/${row.fileId}`}
                              loading="lazy"
                              backgroundsize = "auto"
                              style={{width : "300px" , height : "200px" , backgroundsize : "auto" , cursor: "pointer"}}
                              id = {row.contentId}
                              onClick = {this.imgOnClick}
                              />

                          <ImageListItemBar 
                              title = {row.title}
                              subtitle={row.username}
                              style={{width : "300px"}}
                              actionIcon={
                              <IconButton
                                  sx={{ color: 'rgba(255, 255, 255, 0.54)' }}
                                  aria-label={`info about Breakfast`}
                                >
                                  <Badge badgeContent={row.cntGoodYn} color="primary" style = {{ marginTop : "3%"}} max = {99}>
                                  {
                                    
                                    row.goodYn === "N" ? <FavoriteBorderIcon onClick ={ () => {favoritClick(row.contentId) }}/> : <FavoriteIcon sx={{ color: red[500] }} onClick ={ () => {favoritClick(row.contentId)} }/>
                                  }
                                  </Badge>
                              </IconButton >
                              }
                          />
                        </ImageListItem>
                    </TableCell>            
                    ))
                  }
                </TableRow>
               )}                
            </TableBody>
          </Table>
        </TableContainer>
        )
      }


        return (
          <div>
          {renderValue}
          </div>
        );
    }
}



export default MainImageList
