import React from "react";
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import ButtonBase from '@mui/material/ButtonBase';
import { Button, Dialog, DialogActions, styled, TextField } from '@mui/material';
import { domainAddress } from "../commonJs/domain";
import { apiFileUploadService, apiGetService, apiService } from "../commonJs/CommonApiService";
import { checkImgFile } from "../commonJs/checkFile";
import ConfirmDisagreeComponentError from "../dialogComponent/ConfirmDisagreeComponentError";
import ConfirmDisagreeComponentOk from "../dialogComponent/ConfirmDisagreeComponentOk";
import ConfirmDisagreeComponentOkFunc from "../dialogComponent/ConfirmDisagreeComponentOkFunc";



class MainContentRevise extends React.Component{

    constructor(props){
        super(props)
        
        this.mainReviseImagePopClose = this.props.close
        this.mainReviseRequest = this.props.reviseRequest
        this.deleteContent =this.props.deleteContent

        this.state = {
            img : null,
            upFileImageId : null , 
            titleValue : null,
            content : null ,
            contentId : null , 

            errorPopOnen : false , 
            popInPopUpImageTitle :  null , 
            popInPopUpImageConent : null,   


            okPopOpen : false , 


            deletePopOpen : false ,

        }
    }

    componentDidMount() {

        

        fetch(`data:image/jpg;base64,${this.props.imgRevise}`).then(res => res.blob()).then((blob) => {
            const objectURL = URL.createObjectURL(blob);
            this.setState({img : objectURL })
          })

        this.setState({
            titleValue :  this.props.titleRevise , 
            content : this.props.contentRevise , 
            contentId : this.props.contentIdRevise
        })
        
    
    }



    handleClose = () => {
        this.mainReviseImagePopClose()
    }

   

    titleOnChange = (e) => {
        let value = e.target.value;
        this.setState({titleValue : value})

        

    }

    contentOnchange = (e) => {
        let value = e.target.value;
        this.setState({content : value })
    }

    reviseRequest = () => {
        this.mainReviseRequest(
                this.state.contentId , 
                this.state.titleValue , 
                this.state.content
            
            )
    }

    imageRevise = () => {
        const inputTag = document.querySelector("#upFileClick");
        inputTag.click();
    }

    fileChange = (e) => {
        const files = e.target.files[0];
        const fileFlag = checkImgFile(files.name);
        if(!fileFlag){
          this.setState({ errorPopOnen : true   , 
                          popInPopUpImageTitle :  "파일 업로드 에러" , 
                          popInPopUpImageConent : "지원하는 파일 확장자가 아닙니다. (png , jpg)"
                        })
  
          return false;
        }
  
        
        apiFileUploadService(files , "POST" , "/main/upImageFileUpload").then((res) => {
          
          if(res.error !== null){
            this.setState({ errorPopOnen : true   , 
                            popInPopUpImageTitle :  "파일 업로드 에러" , 
                            popInPopUpImageConent : res.error
            })
            return false;
          }
          //console.log(`data:image/jpg;base64,${res.data[0].imageFileBase64}`);
          //this.setState({ img : `data:image/jpg;base64,${res.data[0].imageFileBase64}` , upFileImageId : res.data[0].fileId })
  
          this.setState({upFileImageId : res.data[0].fileId })
          
          fetch(`data:image/jpg;base64,${res.data[0].imageFileBase64}`).then(res => res.blob()).then( (blob) => {
            const objectURL = URL.createObjectURL(blob);
            this.setState({img : objectURL })
  
  
          })
    
        })
  
      }

      errorPopClose = () =>{
        this.setState({errorPopOnen : false })
      }

      okPopClose = () => {
        this.setState({ okPopOpen : false })
        window.location = "/main"
      }

      handleRevise = () => {

        const paramData =  {
            contentId : this.state.contentId , 
            fileId : this.state.upFileImageId , 
            content : this.state.content ,
            titleValue : this.state.titleValue
        }
        const method =  "POST"
        const apiServiceName =  "/main/reviseContent"

        apiService(paramData , method , apiServiceName).then((res) => {
            console.log(res);

            if(res.error != null){

                this.setState({ errorPopOnen : true   , 
                                popInPopUpImageTitle :  "게시글 수정 에러" , 
                                popInPopUpImageConent : res.error
                  })
                return false;
            }

            this.setState({
                okPopOpen : true , 
            })
        })
      }

      popOkHandle = () => {
        this.okPopClose();
        
      }

      handleDelete = () => {
        
        this.setState({deletePopOpen : true})
        
        //this.deleteContent(this.props.contentIdRevise);
      }

      deletePopClose = () => {
        this.setState({deletePopOpen : false})
      }

      deleteContentOk = () => {
        
        this.deleteContent(this.props.contentIdRevise);
      }

      





    

    

        
    


    

    render(){
        let disaggreeButton = null; 
        if(this.props.disaggagree === true){
            disaggreeButton = <Button onClick={this.handleClose}>닫기</Button>
        }

        let reviseButton = null;
        let deleteButton = null;
        
        reviseButton = <Button onClick={this.handleRevise}>수정</Button>
        deleteButton = <Button color = "error" onClick={this.handleDelete}>삭제</Button>
        

        const Img = styled('img')({
            margin: 'auto',
            display: 'block',
            maxWidth: '100%',
            maxHeight: '100%',
            

          });


        return (
           
            <Dialog
                    open={this.props.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    fullWidth = {true}
                    maxWidth = "700">
            <Paper>

            <ConfirmDisagreeComponentError  open = {this.state.errorPopOnen} 
                                            close = {this.errorPopClose}
                                            popupTitle = {this.state.popInPopUpImageTitle}
                                            popupContent = {this.state.popInPopUpImageConent}
                                            disaggreeButton = {false}
                                            
            />

            <ConfirmDisagreeComponentOkFunc 
                open  = {this.state.okPopOpen}
                popupTitle = "게시글 수정 성공"
                popupContent = "게시글 수정이 완료 되었습니다"
                ok = {this.popOkHandle}

            />

            <ConfirmDisagreeComponentOkFunc 
                open  = {this.state.deletePopOpen}
                popupTitle = "게시글 삭제"
                popupContent = "정말로 게시글을 삭제하시나요? 삭제된 게시물은 복구되지 않습니다."
                ok = {this.deleteContentOk}
                close = {this.deletePopClose}

            />




                <Grid container spacing={2}>
                    <Grid item>
                        <ButtonBase sx={{ width: 512, height: 500 }}>
                            <Img alt="complex" src={this.state.img} style = {{marginTop : "10%" , backgroundsize : "auto"}}  onClick = {this.imageRevise}/>
                            <div style = {{display : "none"}}>
                    <input type="file" id="upFileClick" accept=".jpg, .png" onChange={this.fileChange}/>
                    </div>
                        </ButtonBase>
                    </Grid>
                <Grid item xs={10} sm container>
                    <Grid item xs container direction="column" spacing={10}>
                        <Grid item xs>
                            <Typography gutterBottom variant="subtitle1" component="div" style = {{marginTop : "4%"}}>            
                                <TextField  id="standard-basic" 
                                    variant="standard" 
                                    fullWidth={true} 
                                    
                                    value = {this.state.titleValue}
                                    onChange = {this.titleOnChange}
                                    
                                    > 
                                    
                                </TextField>
                            </Typography>
                            <Typography variant="body2" gutterBottom style = {{marginTop : "3%"}}>
                                <TextField
                                            id="outlined-multiline-static"
                                            multiline
                                            rows={15}
                                            fullWidth = {true}
                                            value = {this.state.content}
                                            onChange = {this.contentOnchange}
                                            
                                            
                                            >
                                    </TextField>
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography sx={{ cursor: 'pointer' }} variant="body2">
                                <DialogActions>
                                    <div>
                                        {deleteButton}
                                     </div>
                                     <div>
                                        {reviseButton}
                                     </div>
                                    <div>
                                        {disaggreeButton}
                                     </div>
                                </DialogActions>
                            </Typography>
                        </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
        </Dialog>
    )}
}

export default MainContentRevise;
