import React from 'react';
import {Container } from '@mui/material';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import * as RexCommonJs from '../commonJs/Rex.js';
import * as ApiCommonJs from '../commonJs/CommonApiService.js';
import ButtonAppBar from '../commonComponent/AppBar.js';
import ConfirmDisagreeComponentOk from '../dialogComponent/ConfirmDisagreeComponentOk.js';
import ConfirmDisagreeComponentError from '../dialogComponent/ConfirmDisagreeComponentError.js';

class Register extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            eamilTextBoxErrorHelp : false , 
            emailTextBoxHelpMessage :  null , 
            
            firstPasswordTextBoxErrorHelp : false , 
            firstPasswordTextBoxHelpMessage : null ,
            
            secondPasswordTextBoxErrorHelp : false ,
            secondPasswordTextBoxHelpMessage : null ,

            userNameTextBoxErrorHelp : false ,
            userNameTextBoxHelpMessage : null ,

            email : "" ,  
            firstPassword : null , 
            secondPassword : null , 
            name : "" , 

            popupOkOpen : false  , 
            disagree : false  , 

            popupErrorOpen : false  ,
            popErrorMessage : null ,

            

            
        }
    }

    

    emailTextBoxOutFocus = (e) => {
        let emailValue = e.target.value;
        if(!emailValue){
            this.setState({eamilTextBoxErrorHelp : true , emailTextBoxHelpMessage : "이메일을 입력해주세요"});
            this.setState({email : null});
            return false
        }else{
            this.setState({eamilTextBoxErrorHelp : false , emailTextBoxHelpMessage : null});
            
            let rexResult = RexCommonJs.emailRex(emailValue);

            if(!rexResult){
                this.setState({eamilTextBoxErrorHelp : true , emailTextBoxHelpMessage : "이메일 형식이 올바르지 않습니다."}); 
                this.setState({email : null});
                return false;     
            }

            const data = {"userEmail" : emailValue}
            const apiServiceName = "/member/isExistsUserEmail";

            ApiCommonJs.apiService(data, "POST" , apiServiceName).then((res)=>{
                if(res.data[0]){
                    this.setState({eamilTextBoxErrorHelp : true , emailTextBoxHelpMessage : '이미 존재하는 이메일입니다.'});
                    this.setState({email : null});
                    return false;
                }else{
                    this.setState({eamilTextBoxErrorHelp : false , emailTextBoxHelpMessage : '사용가능한 이메일입니다.'});
                    this.setState({email : emailValue})
                }
            })
        }
    }

    firstPasswordKeyDown = (e) => {
        
        let password = e.target.value;
        
        if(password.length < 7){
            this.setState({firstPasswordTextBoxErrorHelp : true , firstPasswordTextBoxHelpMessage : "비밀번호는 8자리 이상 넣어주세요."})
            this.setState({firstPassword : null});
            return false;
        }else{
            let rexResult = RexCommonJs.passwordRex(password);
            if(!rexResult){
                this.setState({firstPasswordTextBoxErrorHelp : true , firstPasswordTextBoxHelpMessage : "비밀번호는 대문자 소문자 숫자 특수문자가 반드시 1개 이상들어가야 합니다."})
                this.setState({firstPassword : null});
                return false;
            }else{
                this.setState({firstPasswordTextBoxErrorHelp : false , firstPasswordTextBoxHelpMessage : "안전한 비밀번호입니다."})
                this.setState({firstPassword : password})
            }
        }
    }

    samePassword = (e) => {
        let secondPassword = e.target.value;

        if(secondPassword !== this.state.firstPassword){
            this.setState({secondPasswordTextBoxErrorHelp : true , secondPasswordTextBoxHelpMessage : "비밀번호가 서로 일치하지 않습니다."})
            this.setState({secondPassword : null});
            return false;
        }else{
            this.setState({secondPasswordTextBoxErrorHelp : false , secondPasswordTextBoxHelpMessage : "비밀번호가 일치합니다."})
            this.setState({secondPassword : secondPassword})
        }
    }

    nameChange = (e) => {
        let name = e.target.value;
        if(name){
            this.setState({userNameTextBoxErrorHelp : false , userNameTextBoxHelpMessage : null})
            this.setState({name : name})
        }else{
            this.setState({userNameTextBoxErrorHelp : true , userNameTextBoxHelpMessage : "이름을 입력해주세요."})
            this.setState({name : null});
            return false;
        }
        
    }

    registerButtonClick = () =>{
        
        if(!(this.state.email)){
            this.setState({eamilTextBoxErrorHelp : true , emailTextBoxHelpMessage : "이메일을 입력해주세요"});
            return false;
        }

        if(!(this.state.firstPassword)){
            this.setState({firstPasswordTextBoxErrorHelp : true , firstPasswordTextBoxHelpMessage : "비밀번호를 입력해주세요."})
            return false;
        }

        if(!(this.state.secondPassword)){
            this.setState({secondPasswordTextBoxErrorHelp : true , secondPasswordTextBoxHelpMessage : "비밀번호 확인을 입력해주세요."})
            return false;
        }

        if(!(this.state.name)){
            this.setState({userNameTextBoxErrorHelp : true , userNameTextBoxHelpMessage : "이름을 입력해주세요."})
            return false;
        }

        const paramData = {
            userEmail : this.state.email , 
            password : this.state.firstPassword , 
            username : this.state.name
        }
        const method = "POST"
        const apiServiceName = "/member"

        ApiCommonJs.apiService(paramData , method , apiServiceName).then((res) =>{
            if(res.error === null){
                
                /*로그인 페이지 이동*/
                
                this.setState({popupOkOpen : true})
            }else{
                this.setState({popupErrorOpen : true})
                this.setState({popErrorMessage : res.error})
                
                
                
            }
        })     
    }

    popupOkClose = () => {
        this.setState({popupOkOpen : false})
        window.location = "/"
    }

    popupErrorClose = () => {
        this.setState({popupErrorOpen : false})
    }

    
    pathClick1 = () => {
        window.location = "/"
    }

    pathClick2 = () => {
        window.location = "/"
    }

    onRegisterEnterPres = () => {
        if(window.event.keyCode === 13){
            this.registerButtonClick()
        }
    }

    allComponentClear = () => {
        this.setState({ 
                            email : ""  , 
                            name : ""
                     })
    }

    render(){
        return (
            <div>
            <ButtonAppBar  pageName="하루의_Spring" path1="로그인"  path2="홈으로" pathClick1 = {this.pathClick1} pathClick2 = {this.pathClick2}/>
            <Container component="main" maxWidth="xs" style = {{marginTop : "8%"}} spacing={2}>
                <Grid >
                    <Grid item xs={12}>
                        <Typography component="h1" variant = "h5">
                            회원가입
                        </Typography>
                    </Grid>
                </Grid>
            </Container>

            <Container component="main" maxWidth="xs" style = {{marginTop : "3%"}} >
                <Grid item xs={12}>
                    <TextField  variant = "outlined"
                                required
                                fullWidth
                                id="email"
                                label="이메일 주소"
                                name = "email"
                                autoComplete='email'
                                error={this.state.eamilTextBoxErrorHelp}
                                helperText={this.state.emailTextBoxHelpMessage}            
                                onBlur = {this.emailTextBoxOutFocus}
                                onKeyPress = {this.onRegisterEnterPres}
                                
                    >
                        
                    </TextField>
                </Grid>
            </Container>
            <Container component="main" maxWidth="xs" style = {{marginTop : "2%"}} >
                <Grid item xs={12}>
                    <TextField  variant = "outlined"
                                required
                                fullWidth
                                id="firstPassword"
                                label="비밀번호"
                                name = "firstPassword"
                                type="password"
                                autoComplete='password'
                                error={this.state.firstPasswordTextBoxErrorHelp}
                                helperText={this.state.firstPasswordTextBoxHelpMessage}            
                                onChange = {this.firstPasswordKeyDown}
                                onKeyPress = {this.onRegisterEnterPres}
                                
                    />
                </Grid>
            </Container>
            <Container component="main" maxWidth="xs" style = {{marginTop : "2%"}} >
                <Grid item xs={12}>
                    <TextField  variant = "outlined"
                                required
                                fullWidth
                                id="secondPassword"
                                label="비밀번호 확인"
                                name = "secondPassword"
                                type="password"
                                autoComplete='password'
                                error={this.state.secondPasswordTextBoxErrorHelp}
                                helperText={this.state.secondPasswordTextBoxHelpMessage}            
                                onChange={this.samePassword}
                                onKeyPress = {this.onRegisterEnterPres}
                    />
                </Grid>
            </Container>
            <Container component="main" maxWidth="xs" style = {{marginTop : "2%"}} >
                <Grid item xs={12}>
                    <TextField  variant = "outlined"
                                required
                                fullWidth
                                id="username"
                                label="이름"
                                name = "username"
                                autoComplete='name'
                                error={this.state.userNameTextBoxErrorHelp}
                                helperText={this.state.userNameTextBoxHelpMessage}  
                                onChange = {this.nameChange}     
                                onKeyPress = {this.onRegisterEnterPres}     
                                
                                
                    >
                        
                    </TextField>
                </Grid>
            </Container>

            <Container component="main" maxWidth="xs" style = {{marginTop : "2%"}} >
                <Grid item xs={12}>
                    <Button
                        type = "button"
                        fullWidth
                        variant='contained'
                        color='primary'
                        onClick = {this.registerButtonClick}

                    >
                    회원가입 요청
                    </Button>
                </Grid>
            </Container>
            
            <ConfirmDisagreeComponentOk     open = {this.state.popupOkOpen} 
                                            close = {this.popupOkClose} 
                                            disaggagree = {false} 
                                            popupTitle = '회원가입 성공.'  
                                            popupContent = '회원가입에 성공했습니다 로그인 페이지로 이동합니다.'

            />

            <ConfirmDisagreeComponentError  open = {this.state.popupErrorOpen} 
                                            close = {this.popupErrorClose} 
                                            disaggagree = {false} 
                                            popupTitle = '회원가입 실패.'  
                                            popupContent = {this.state.popErrorMessage}
            
            />
            
          

        </div>
                

            
        )
    }
}

export default Register;

