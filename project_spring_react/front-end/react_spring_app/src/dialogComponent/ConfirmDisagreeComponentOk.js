import React from "react";
import  Button  from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogTitle from "@mui/material/DialogTitle";
import Alert from '@mui/material/Alert';



class ConfirmDisagreeComponentOk extends React.Component{

    constructor(props){
        super(props)
        this.popupClose = this.props.close
    }

    handleClose = () => {
        this.popupClose()
        
    }

   

    render(){
        let disaggreeButton = null; 
        if(this.props.disaggagree === true){
            disaggreeButton = <Button onClick={this.handleClose}>Disagree</Button>
        }


        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                <DialogTitle id="alert-dialog-title">
                    {this.props.popupTitle}
                </DialogTitle>
                    
                        <Alert severity="success">{this.props.popupContent}</Alert>
                    
                <DialogActions>
                    {disaggreeButton}
                    <Button onClick={this.handleClose} autoFocus>
                        Agree
                    </Button>
                </DialogActions>
                </Dialog>
            </div>
        )
    }

    
}

export default ConfirmDisagreeComponentOk