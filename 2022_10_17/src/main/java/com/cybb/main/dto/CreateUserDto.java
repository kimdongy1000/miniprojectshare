package com.cybb.main.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CreateUserDto {
	
	private String id;
	private String username;
	private String password;
	private List<String> authorites;
	private int count;
	
	
	
	

}
