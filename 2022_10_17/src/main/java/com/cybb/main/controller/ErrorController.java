package com.cybb.main.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("error")
public class ErrorController {

    @GetMapping("/402")
    public String error_402(){

        return "error_402";
    }

    @GetMapping("/401")
    public String error_401(){

        return "error_401";
    }

}
