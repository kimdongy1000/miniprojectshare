package com.cybb.main.controller;

import com.cybb.main.dto.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cybb.main.dto.CreateUserDto;
import com.cybb.main.service.CreateUserService;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("create")
public class CreateController {
	
	@Autowired
	private CreateUserService createUserService;
	
	@PostMapping
	public ResponseEntity<?> createUser(@RequestBody CreateUserDto createUserDto){

		ResponseDto<String> responseDto = null;
		List<String> list = null;
		
		try {
			createUserService.insertUser(createUserDto);

			list = new ArrayList<>();
			list.add("유저생성이 완료 되었습니다.");

			responseDto = ResponseDto.<String>builder().list(list).build();
			
			return ResponseEntity.ok().body(responseDto);
		}catch(Exception e) {
			responseDto = ResponseDto.<String>builder().error(e.getMessage()).list(null).build();
			return ResponseEntity.badRequest().body(responseDto);
			
		}
		
		
		
		
		
		
		
		
	}

}
