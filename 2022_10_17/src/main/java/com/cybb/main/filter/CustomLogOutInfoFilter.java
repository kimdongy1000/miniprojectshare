package com.cybb.main.filter;

import com.cybb.main.repository.LoginUserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextImpl;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CustomLogOutInfoFilter implements Filter {

    private final LoginUserRepository userRepository;

    public CustomLogOutInfoFilter(LoginUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpReq = (HttpServletRequest) request;
        HttpServletResponse httpRes = (HttpServletResponse) response;


        String url = httpReq.getRequestURI();


        if(url.equals("/logout")){

            HttpSession session = httpReq.getSession();
            SecurityContextImpl securityContext =  (SecurityContextImpl)session.getAttribute("SPRING_SECURITY_CONTEXT");

            if (securityContext != null) {
                Authentication authentication = securityContext.getAuthentication();

                String username =  (String)authentication.getPrincipal();


                userRepository.logoutLogging(username);


            }


            chain.doFilter(request , response);



        }else{

            chain.doFilter(request , response);
            return;


        }







    }
}
