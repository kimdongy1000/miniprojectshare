package com.cybb.main.filter;

import com.cybb.main.repository.LoginUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.filter.GenericFilterBean;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.function.Supplier;


public class CustomLoginInfoFilter implements Filter {


    private final LoginUserRepository userRepository;

    public CustomLoginInfoFilter(LoginUserRepository userRepository) {
        this.userRepository = userRepository;
    }



    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpReq = (HttpServletRequest) request;
        HttpServletResponse httpRes = (HttpServletResponse) response;

        String url = httpReq.getRequestURI();

        if(!url.equals("/welcome/home")){
            chain.doFilter(request , response);
            return;
        }

        HttpSession session = httpReq.getSession();
        SecurityContextImpl securityContext =  (SecurityContextImpl)session.getAttribute("SPRING_SECURITY_CONTEXT");

        if (securityContext != null) {
            Authentication authentication = securityContext.getAuthentication();

            String username =  (String)authentication.getPrincipal();


           userRepository.loginLogging(username);


        }
        chain.doFilter(request , response);


    }


}
