package com.cybb.main.repository;

import com.cybb.main.dao.SecurityAythorites;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cybb.main.dao.SecurityUser;
import com.cybb.main.dto.CreateUserDto;

import java.util.List;

@Repository
public class CreateUserRepository {
	
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;
	
	

	public int insertUser(SecurityUser securityUser) {

		return this.sqlSessionTemplate.insert(this.getClass().getName() + ".insertUser" , securityUser);
	}



	public int isAlreadyUsername(SecurityUser securityUser) {
		return this.sqlSessionTemplate.selectOne(this.getClass().getName() + ".isAlreadyUsername" , securityUser);
	}

	public int insertAuthorites(List<SecurityAythorites> securityAythorites) {

		return this.sqlSessionTemplate.insert(this.getClass().getName() + ".insertAuthorites" , securityAythorites);
	}
}
