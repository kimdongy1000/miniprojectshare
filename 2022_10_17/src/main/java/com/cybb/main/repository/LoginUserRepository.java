package com.cybb.main.repository;

import com.cybb.main.dao.SecurityAythorites;
import com.cybb.main.dao.SecurityUser;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class LoginUserRepository {

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;


    public SecurityUser findByUsername(String username) {

        return sqlSessionTemplate.selectOne(this.getClass().getName() + ".findByUsername" , username);
    }

    public List<SecurityAythorites> findByuthorites(String userid) {

        return sqlSessionTemplate.selectList(this.getClass().getName() + ".findByuthorites" , userid);
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateCount(String username) {

        sqlSessionTemplate.update(this.getClass().getName() + ".updateCount" , username);
    }

    public void updatePlusCount(String username) {

        sqlSessionTemplate.update(this.getClass().getName() + ".updatePlusCount" , username);
    }

    @Transactional(rollbackFor = Exception.class)
    public void loginLogging(String username) {

        sqlSessionTemplate.insert(this.getClass().getName() + ".loginLogging" , username);
    }

    @Transactional(rollbackFor = Exception.class)
    public void logoutLogging(String username) {


        sqlSessionTemplate.update(this.getClass().getName() + ".updateLogoutLogging" , username);
    }
}
