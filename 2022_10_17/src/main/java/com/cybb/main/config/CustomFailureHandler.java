package com.cybb.main.config;

import com.cybb.main.repository.LoginUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomFailureHandler implements AuthenticationFailureHandler {

    @Autowired
    private LoginUserRepository loginUserRepository;



    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {

        String username = request.getParameter("username");

        if(exception instanceof UsernameNotFoundException){
            response.sendRedirect("/error/401");
        }else if(exception instanceof BadCredentialsException){

            loginUserRepository.updatePlusCount(username);

            response.sendRedirect("/error/401");
        }

    }
}
