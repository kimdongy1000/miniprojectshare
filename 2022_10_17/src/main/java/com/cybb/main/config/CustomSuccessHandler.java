package com.cybb.main.config;

import com.cybb.main.dao.SecurityUser;
import com.cybb.main.repository.LoginUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    private LoginUserRepository loginUserRepository;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        SecurityUser securityUser =  loginUserRepository.findByUsername(authentication.getName());

        if(securityUser.getCount() > 10){
            response.sendRedirect("/error/402");
        }else{
            loginUserRepository.updateCount(authentication.getName()); // 카운트 초기화
            response.sendRedirect("/welcome/home");
        }
    }
}
