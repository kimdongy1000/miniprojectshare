package com.cybb.main.config;

import com.cybb.main.User.CustomAuthProvider;
import com.cybb.main.filter.CustomLogOutInfoFilter;
import com.cybb.main.filter.CustomLoginInfoFilter;
import com.cybb.main.repository.LoginUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutFilter;

@Configuration
public class SecurityConfig {

    @Autowired
    private CustomAuthProvider customAuthProvider;

    @Autowired
    private CustomSuccessHandler customSuccessHandler;

    @Autowired
    private CustomFailureHandler customFailureHandler;

    @Autowired
    private LoginUserRepository userRepository;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{

        http.addFilterAfter(new CustomLoginInfoFilter(userRepository) , UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(new CustomLogOutInfoFilter(userRepository) , LogoutFilter.class);



        http.formLogin().successHandler(customSuccessHandler).failureHandler(customFailureHandler);


        /**errorPage*/
        http.authorizeRequests().mvcMatchers("/create" , "/error/402" , "/error/401").permitAll();


        /**welcomePage*/
        http.authorizeRequests().mvcMatchers("/welcome/home" , "/greeting/*").authenticated();


        http.authorizeRequests().anyRequest().denyAll();


        http.csrf().disable();

        http.authenticationProvider(customAuthProvider);





        return http.build();
    }


}
