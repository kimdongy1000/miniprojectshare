package com.cybb.main.User;

import com.cybb.main.dao.SecurityAythorites;
import com.cybb.main.dao.SecurityUser;
import com.cybb.main.repository.LoginUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private LoginUserRepository loginUserRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        SecurityUser securityUser = loginUserRepository.findByUsername(username);

        if(securityUser == null){
            throw new UsernameNotFoundException("일치하는 계정이 없습니다.");
        }

        List<SecurityAythorites> securityAythorites = loginUserRepository.findByuthorites(securityUser.getId());


        List<GrantedAuthority> array_grantedAuthority = securityAythorites.stream().map(item -> {
            return new SimpleGrantedAuthority(item.getAuthorites());
        }).collect(Collectors.toList());

        CustomUserDetails userDetails = new CustomUserDetails(  securityUser.getUsername() ,
                                                                securityUser.getPassword() ,
                                                                array_grantedAuthority
                                                              );
        return userDetails;
    }
}
