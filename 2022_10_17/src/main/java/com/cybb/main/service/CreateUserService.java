package com.cybb.main.service;

import com.cybb.main.dao.SecurityAythorites;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cybb.main.dao.SecurityUser;
import com.cybb.main.dto.CreateUserDto;
import com.cybb.main.repository.CreateUserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CreateUserService {
	
	@Autowired
	private CreateUserRepository createUserRepository;



	@Autowired
	private PasswordEncoder passwordEncoder;
	
	
	

	@Transactional(rollbackFor = Exception.class)
	public void insertUser(CreateUserDto createUserDto) {
		
		SecurityUser securityUser = SecurityUser.builder()
												.username(createUserDto.getUsername())
												.password(passwordEncoder.encode(createUserDto.getPassword())).build();
		
		
		int isAlreadyUsername = createUserRepository.isAlreadyUsername(securityUser);

		if(isAlreadyUsername > 0){
			throw new RuntimeException("이미 존재하는 유저입니다.");
		}

		int insertUser = createUserRepository.insertUser(securityUser);

		if(insertUser < 1){
			throw new RuntimeException("계정생성에 문제가 발생했습니다.");
		}

		List<SecurityAythorites> securityAythorites = createUserDto.getAuthorites().stream().map(item -> {

			SecurityAythorites authies = new SecurityAythorites();
			authies.setUserid(createUserDto.getUsername());
			authies.setAuthorites(item);

			return authies;
		}).collect(Collectors.toList());

		int insertAuthorites = createUserRepository.insertAuthorites(securityAythorites);

		if(insertAuthorites < 1){
			throw new RuntimeException("계정생성에 문제가 발생했습니다.");
		}
	}

}
