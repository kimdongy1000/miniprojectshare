function init(){
	getMember();
	setSearchBoxMakeSelecting();
	
}

$("input[name='enabledRadio']:radio").change(function () {

	let enabled = $('input[name="enabledRadio"]:checked').val();
	
	console.log(enabled)
	
	if(enabled == 1 || enabled == ''){
		 $("#userRoleSelectBox").attr('disabled' , false);
	}else{
		$("#userRoleSelectBox").attr('disabled' , true);
	}

});


function btnSearchClick(){

	let name = 	$("#exampleInputEmail1").val();
	let enabled = $('input[name="enabledRadio"]:checked').val();
	let role_code = null;
	if(enabled == 1 || enabled == ''){
		role_code = $("#userRoleSelectBox").val();
	}else{
		role_code = "";
	}
	
	
	let param = {
		"name" : name , 
		"enabled" : enabled , 
		"role_code" : role_code 
	}
	
	getMember(param);

}

function setSearchBoxMakeSelecting(){

	let searchBoxSelectng = document.querySelector(".searchBox_select");
	console.log('searchBoxSelectng' ,searchBoxSelectng);
	
	let roleList = getRole();
	
	let temp_option = document.createElement("option");
		temp_option.setAttribute("value" , '');
		temp_option.text = '전체';
		searchBoxSelectng.appendChild(temp_option);
	
	for(let i = 0; i < roleList.length; i++){
		temp_option = document.createElement("option");
		temp_option.setAttribute("value" , roleList[i].role_code);
		temp_option.text = roleList[i].role_name;
		searchBoxSelectng.appendChild(temp_option);
	}
	
	console.log('searchBoxSelectng' ,searchBoxSelectng);
	
	
	

}

function foldeSideBar() {

	$("#mainSideBar").toggle('slow', function() {

		let state = $("#mainSideBar").css("display");
		if (state === 'none') {
			$("#imdArraw").attr("src", "/image/icon/rightArrow.png");
		} else {
			$("#imdArraw").attr("src", "/image/icon/leftArrow.png");
		}

	});


}



function getMember() {

	let type = "POST";
	let url = "/admin/getMember";
	let param = {
		"name" : "",
		"enabled" : "",
		"role_code":"",
		"offset" : 0,
		"currentPageSet" : 0.0
		
	}
	
	if(arguments.length >= 1){
		param = arguments[0];
	}

	ajaxNet(type, url, param, function(data) {
		if (data.length > 0) {
			appendGrid(data);
			getMemberCount(param);
		} else {
			alert('데이터가 존재하지 않습니다');
			return;
		}
	});
}

function getMemberCount(){
	
	let type = "POST";
	let url = "/admin/getMemberCnt";
	let param = arguments[0];

	
	
	ajaxNet(type , url , param ,  function(data){
		drawPaging(data);
		
	})
	
}

function drawPaging(data){
	
	$(".pagination").empty();
	
	let pagingUI = document.querySelector(".pagination");
	console.log(data);
	
	let totalPageSet   = data.totalPageSet
	let currentPageSet = data.currentPageSet;
	let endPageSet     = data.endPageSet;
	let pageNumber     = data.pagingNumber;
	
	if(totalPageSet < 10.0){
		
		for(let i = 1; i <= totalPageSet; i++){
				
			let temp_li = document.createElement("li");
			let temp_a = document.createElement("a");
			
				
			if(i == pageNumber){
				temp_li.setAttribute('class' , 'active');
			}
			
			temp_a.text = i;
			temp_a.setAttribute('onclick' , `onclickPaging(${i})`);
			temp_a.setAttribute('style' , 'cursor:pointer;')

			temp_li.appendChild(temp_a);
			
			pagingUI.appendChild(temp_li)
		}
	}else{
		
		if(currentPageSet > 0){
				let temp_li = document.createElement("li");
				let temp_a = document.createElement("a");
				temp_a.text = "<";
				temp_a.setAttribute('style' , 'cursor:pointer;')
				temp_a.setAttribute('onclick' , `prePage(${currentPageSet})`);
				temp_li.appendChild(temp_a);
				pagingUI.appendChild(temp_li);
		}
		
		if(totalPageSet > endPageSet * 10){
			for(let i = 10 * currentPageSet + 1; i <= 10 * currentPageSet + 10; i++){
				
				temp_li = document.createElement("li");
				temp_a = document.createElement("a");
				
				if(i == pageNumber){
					temp_li.setAttribute('class' , 'active');
				}
			
				temp_a.text = i;
				temp_a.setAttribute('onclick' , `onclickPaging(${i} , ${currentPageSet})`);
				temp_a.setAttribute('style' , 'cursor:pointer;')
	
				temp_li.appendChild(temp_a);
			
				pagingUI.appendChild(temp_li)
			}
		}else{
			for(let i = 10 * currentPageSet + 1; i <= Math.ceil(totalPageSet); i++){
				
				temp_li = document.createElement("li");
				temp_a = document.createElement("a");
				
				if(i == pageNumber){
					temp_li.setAttribute('class' , 'active');
				}
			
				temp_a.text = i;
				temp_a.setAttribute('onclick' , `onclickPaging(${i} , ${currentPageSet})`);
				temp_a.setAttribute('style' , 'cursor:pointer;')
	
				temp_li.appendChild(temp_a);
			
				pagingUI.appendChild(temp_li)
			}
		}
		
		
		
			
			if(totalPageSet > endPageSet * 10){
				temp_li = document.createElement("li");
				temp_a = document.createElement("a");
				temp_a.text = ">";
				temp_a.setAttribute('style' , 'cursor:pointer;')
				temp_a.setAttribute('onclick' , `nextPageSet(${endPageSet})`)
				temp_li.appendChild(temp_a);
				pagingUI.appendChild(temp_li);	
			}
			
		
	}
	
}

function nextPageSet(nextPageSet){

	console.log(nextPageSet);
	
	let name = 	$("#exampleInputEmail1").val();
	let enabled = $('input[name="enabledRadio"]:checked').val();
	let role_code = null;
	
	if(enabled == 1){
		role_code = $("#userRoleSelectBox").val();
	}else{
		role_code = "";
	}
	
	
	let param = {
		"name" : name , 
		"enabled" : enabled , 
		"role_code" : role_code,
		"currentPageSet" : nextPageSet,
		"offset" :  100 * nextPageSet + 1,
		"pagingNumber" : 10 * nextPageSet +1
		
		
	}
	
	getMember(param);
	
	
	
}

function prePage(prePageSet){
	
	console.log(prePageSet);

	let name = 	$("#exampleInputEmail1").val();
	let enabled = $('input[name="enabledRadio"]:checked').val();
	let role_code = null;
	
	if(enabled == 1){
		role_code = $("#userRoleSelectBox").val();
	}else{
		role_code = "";
	}
	
	let offset = 100 * (prePageSet - 1) - 9
	
	if(offset < 0){
		offset = 0;
	}
	
	let param = {
		"name" : name , 
		"enabled" : enabled , 
		"role_code" : role_code,
		"currentPageSet" : prePageSet - 1,
		"offset" : offset,
		"pagingNumber" : 10 * prePageSet - 9 
		
	}
	
	getMember(param);
}

function onclickPaging(pagingNumber , currentPageSet){
	
	
	let name = 	$("#exampleInputEmail1").val();
	let enabled = $('input[name="enabledRadio"]:checked').val();
	let role_code = null;
	
	if(enabled == 1){
		role_code = $("#userRoleSelectBox").val();
	}else{
		role_code = "";
	}
	
	
	let param = {
		"name" : name , 
		"enabled" : enabled , 
		"role_code" : role_code,
		"offset" :  10 * pagingNumber - 10,
		"currentPageSet" :currentPageSet,
		"pagingNumber" : pagingNumber
	}
	
	getMember(param);
}

function getRole(){
	
	let type = "GET";
	let url = "/admin/getRole";
	let param = {
	
	}
	
	let resultData = null
	
	ajaxNet_GET_NonAsync(type , url , param , function(data){
		 
		 resultData = data;
	});
	
	return resultData;
}

function userRoleChange(id , obj){
	let startAjax = false;
	
	if(!confirm('유저 사용권한을 변경하시겠습니까?')){
		getMember()
		startAjax = false;
		return;
	}else{
		startAjax = true;
	}
	
	if(startAjax){
		let type = "PATCH"
		let url = "/admin/userRoleUpdate";
		let param = {
			"id" : id , 
			"role_code" : obj.value
		}
		
		ajaxNet(type , url , param , function(){
			getMember();
		})
		
		
	}
	
	
	
	
}

//유지 보수가 필요한 함수
function appendGrid(data) {
	
	let roleList = getRole();
	
	let tbody = document.querySelector(".memberTableBody");
	tbody.innerHTML = "";
	for (let i = 0; i < data.length; i++) {
		
		let tr = document.createElement('TR');
		let temp_param = [ data[i].id, data[i].email, data[i].enabled, data[i].nickname, data[i].lockCnt, data[i].role_code, data[i].registerDate, data[i].updateDate, data[i].id, data[i].id ]
		
		for (let j = 0; j < temp_param.length; j++) {
			let temp_td = document.createElement('TD');
			if(j == 3 || j == 4  || j == 6 || j == 7){
				temp_td.appendChild(document.createTextNode(temp_param[j]));	
				temp_td.setAttribute("style" , "text-align:center");
			}else if(j == 2){
				if(temp_param[2] == '0'){
					temp_td.appendChild(document.createTextNode('비활성'));
					temp_td.setAttribute("style" , "text-align:center");
				}else{
					temp_td.appendChild(document.createTextNode('활성'));
					temp_td.setAttribute("style" , "text-align:center");
				}
			}else if(j == 5){
				if(data[i].enabled == 1){
					let selectList = document.createElement('select');

					for(let l = 0; l < roleList.length; l++){
						let option = document.createElement('option');
						option.value = roleList[l].role_code;
						option.text = roleList[l].role_name;
						selectList.appendChild(option);
					}
				
					for(let m = 0; m < selectList.options.length; m++){
						if( selectList.options[m].value == data[i].role_code){
							selectList.options[m].selected = "selected";
						}
					}
				
					selectList.setAttribute('onchange' , `userRoleChange(${temp_param[0]} , this)`)
				
					temp_td.appendChild(selectList);
					
				}
			
				
				
				
			}else if(j == 8){
				let temp_button =  document.createElement('BUTTON');
				let temp_img = document.createElement("IMG")
				if(temp_param[2] == '0'){
					temp_img.setAttribute("src" , "/image/icon/check.png")
				}else{
					temp_img.setAttribute("src" , "/image/icon/x.png")
				}
				temp_button.appendChild(temp_img);
				
				temp_button.setAttribute('type', 'button');
				temp_td.setAttribute("onclick" , `mngUser(${temp_param[0]} , ${temp_param[2]})`)
				temp_td.setAttribute("style" , "text-align:center");
				temp_td.appendChild(temp_button);
			}else if(j == 9){
				let temp_input = document.createElement('INPUT');
				temp_input.setAttribute('id' , `inputId_${temp_param[0]}`)
				temp_input.setAttribute('type' , 'hidden');
				temp_input.setAttribute('value' , temp_param[0]);
				temp_td.appendChild(temp_input);
			}else{
				temp_td.appendChild(document.createTextNode(temp_param[j]));
			}
			tr.appendChild(temp_td);
		}
		tbody.appendChild(tr);
		temp_param = null;
	}
}

function mngUser(id , enabled){

	let type = "PATCH"
	let url = "/admin/mngUser"
	let param = {
		"id" : id
	}
	
	let startAjax = false;
		
	if(enabled == '0'){
		if(!confirm('계정을 활성화 하시겠습니까?')){
			return;
		}
		param.enabled = '1';
		startAjax = true;		
	}else{
		if(!confirm('계정을 비활성화 하시겠습니까?')){
			return;
		}
		param.enabled = '0';
		startAjax = true;
	}
	
	
	if(startAjax){
		ajaxNet(type , url , param , function(){
			alert('유저 정보가 업데이트 완료 되었습니다');
			getMember();
		});	
		
	}
}

init();
