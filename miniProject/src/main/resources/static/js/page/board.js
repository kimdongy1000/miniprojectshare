function init() {

	getBoardList();

}

function writeBoard() {

	location.href = "/board/writeBoard"
}

function getBoardList() {

	let type = "POST";
	let url = "/board/getBoard";
	let param = {

	};


	let title = $("#title").val();
	let author = $("#author").val();

	param.title = title;
	param.author = author;

	console.log(param);


	if (arguments.length == 0) {
		param.offset = 0;
		param.pagingNumber = 0;
		param.currentPageSet = 0;
	} else {
		let paging = 10 * arguments[0] + 1;
		param.offset = paging;
		param.pagingNumber = arguments[0];
		param.currentPageSet = arguments[1];

	}


	ajaxNet(type, url, param, function(data) {

		console.log(data);
		gridBoardList(data.boardList);
		gridBoardPaging(data.boardCount, data.pagingNumber, data.currentPageSet, data.endPageSet);

	});
}

function gridBoardList(data) {

	let tbody = document.querySelector(".boardList");
	tbody.innerHTML = "";
	for (let i = 0; i < data.length; i++) {
		let temp_tr = document.createElement("TR")
		for (let j = 0; j < 4; j++) {
			let temp_td = document.createElement("TD");

			if (j == 0) {
				temp_td.innerText = `${data[i].boardId}`;
			}

			if (j == 1) {
				temp_td.setAttribute("style", " text-align: center; cursor:pointer;");
				temp_td.setAttribute("onclick", `onclickBoardView(${data[i].boardId})`)
				temp_td.innerText = `${data[i].title}`;

			}

			if (j == 2) {
				temp_td.setAttribute("style", " text-align: center");
				temp_td.innerText = `${data[i].email}`;

			}

			if (j == 3) {
				temp_td.setAttribute("style", " text-align: center");
				temp_td.innerText = `${data[i].views}`;

			}
			temp_tr.appendChild(temp_td);
		}
		tbody.appendChild(temp_tr);

	}


}


function gridBoardPaging(count, currentPagingNumber, currentPageSet, endPageSet) {
	debugger;
	$(".pagination").empty();

	let pagingUI = document.querySelector(".pagination");

	if (count <= 100) {

		for (let i = 0; i <= count / 10; i++) {

			let temp_li = document.createElement("li");
			let temp_a = document.createElement("a");

			let tempIndex = i + 1;

			if (i == currentPagingNumber) {
				temp_li.setAttribute('class', "active");
			}

			temp_a.innerText = tempIndex;
			temp_a.setAttribute("onclick", `getBoardList(${i} , 0)`);

			temp_li.setAttribute("style", " cursor:pointer;");
			temp_li.appendChild(temp_a);

			pagingUI.appendChild(temp_li)

		}

	} else {

		if (currentPageSet < endPageSet) {
			for (let j = 10 * currentPageSet; j < 10 * currentPageSet + 10; j++) {

				let temp_li = document.createElement("li");
				let temp_a = document.createElement("a");

				let tempIndex = j + 1;

				if (j == currentPagingNumber) {
					temp_li.setAttribute('class', "active");
				}

				temp_a.innerText = tempIndex;
				temp_a.setAttribute("onclick", `getBoardList(${j} , ${currentPageSet})`);

				temp_li.setAttribute("style", " cursor:pointer;");
				temp_li.appendChild(temp_a);

				pagingUI.appendChild(temp_li)


			}

			let next = currentPageSet + 1;
			let nextPageStart = 10 * next;

			let temp_li = document.createElement("li");
			let temp_a = document.createElement("a");
			temp_a.text = ">";
			temp_a.setAttribute('style', 'cursor:pointer;')
			temp_a.setAttribute('onclick', `getBoardList(${nextPageStart} , ${next})`)
			temp_li.appendChild(temp_a);
			pagingUI.appendChild(temp_li);

		} else {
			
			let pre = currentPageSet - 1;
			let preStartPage = 10 * pre;

			let temp_li = document.createElement("li");
			let temp_a = document.createElement("a");
			temp_a.text = "<";
			temp_a.setAttribute('style', 'cursor:pointer;')
			temp_a.setAttribute('onclick', `getBoardList(${preStartPage} , ${pre})`)
			temp_li.appendChild(temp_a);
			pagingUI.appendChild(temp_li);
			
			
			
			for (let k = 10 * currentPageSet; k <= count / 10; k++) {

				let temp_li = document.createElement("li");
				let temp_a = document.createElement("a");

				let tempIndex = k + 1;

				if (k == currentPagingNumber) {
					temp_li.setAttribute('class', "active");
				}

				temp_a.innerText = tempIndex;
				temp_a.setAttribute("onclick", `getBoardList(${k} , ${currentPageSet})`);

				temp_li.setAttribute("style", " cursor:pointer;");
				temp_li.appendChild(temp_a);

				pagingUI.appendChild(temp_li)


			}

		}

	}


}


function onclickBoardView(viewIndex) {

	location.href = `/board/view/${viewIndex}`;

}

function alertTest(data) {

	alert(data);
}







init();