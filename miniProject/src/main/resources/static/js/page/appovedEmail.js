function init(){
	$(".btn-onclick-confirm").hide();
	$(".resendNumber").hide();

}

$(".btn-onclick-send-confrimNumber").click(function () {

	let id = $("#inputId").val();
	let email = $("#inputEmail").val();
	
	
	
	let type = "POST";
	let url = "/signIn/sendEmail";
	let param = {
		"id" : id,
		"email" : email
	};
	
	ajaxNet(type , url , param , function (){
		alert('전송에 성공했습니다 메일을 확인해주세요');
		$(".btn-onclick-confirm").show();
		$(".btn-onclick-send-confrimNumber").hide();
		$(".resendNumber").show();
	});
});

$(".btn-onclick-confirm").click(function (){
	
	let id = $("#inputId").val();
	let email = $("#inputEmail").val();
	let inputConfirmNumber = $("#inputConfirmNumber").val();
	
	let type = "POST";
	let url = "/signIn/approvedNumber";
	let param = {
		"id" : id,
		"email" : email,
		"inputConfirmNumber" : inputConfirmNumber
	};
	
	ajaxNet(type , url , param , function (data){
		if(data){
			alert('인증에 성공했습니다 \n 홈 화면으로 이동합니다');
			location.href = "/home/"
		}else{
			alert('인증에 실패했습니다 \n 인증번호를 다시 확인해주세요');			
		}
	
	});
	
	
})

function resendApprovedNumber(){
	
	let id = $("#inputId").val();
	let email = $("#inputEmail").val();
	
	let type = "PUT";
	let url = "/signIn/reSendEmail";
	let param = {
		"id" : id,
		"email" :email
	}
	
	ajaxNet(type , url , param , function (){
		alert('전송에 성공했습니다 메일을 확인해주세요');
	});
	
	
	
}


init();