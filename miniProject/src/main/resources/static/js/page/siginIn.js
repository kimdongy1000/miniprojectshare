function init(){
	$("#div_alreadEmail_green").hide();
	$("#div_alreadEmail_red").hide();
	
}


// 회원 가입 신청 버튼 눌렀을떄 
function btn_register(){

	let email            =  $("#inputEmail").val();
	let password 	     =  $("#inputPassword").val();
	let confirmPassword  =  $("#inputConfirmPassword").val();
	let nickName         =  $("#inputNickName").val();
	let isCheck 		 =  $(".chk-privacy").prop("checked"); 
	
	console.log(email , password , confirmPassword , nickName);
	
	if(!email){
		alert('이메일을 입력해주세요');
		return;
	}
	
	let isEmail = checkEmail(email);
	
	if(!isEmail){
		alert('이메일 형식을 확인해주세요')
		return;
	}
	
	if(!password){
		alert('비밀번호를 입력해주세요');
		return;
	}
	
	if(!confirmPassword){
		alert('비밀번호 확인을 입력해주세요');
		return;
	}
	
	if(password !== confirmPassword){
		alert('비밀번호를 확인해주세요');
		return;
	}
	
	if(!nickName){
		alert('닉네임을 입력해주세요');
		return;
	}
	
	if(!isCheck){
		alert('개인정보에 동의해주세요');
		return;
	}
	
	$(".form-signin").submit();

}

function checkEmail(email){
 	
 	let flag = false;
 	let emailReg = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

	if(emailReg.test(email)){
		flag = true;
	} 	

	return flag
}

function checkAlreadyEmail(obj){
	
	let email = obj.value;
	
	let type = "POST";
	let url = "/signIn/checkEmail";
	let param = {
		"email" : email
	}
	
	ajaxNet(type , url , param , function (data) {
		
		if(data){
			$("#div_alreadEmail_green").show();
			$("#div_alreadEmail_red").hide();
		}else{
			$("#div_alreadEmail_green").hide();
			$("#div_alreadEmail_red").show();
		}
	});
}




init();