$(document).ready(function() {
	$('#summernote').summernote({
		// 에디터 높이
		height: 800,
		// 에디터 한글 설정
		lang: "ko-KR",
		// 에디터에 커서 이동 (input창의 autofocus라고 생각하시면 됩니다.)
		toolbar: [

		]

	});

	$('#summernote').summernote('code', boardVO.content);

});

function updateBoard(){
	
	let form = document.querySelector(".modifyForm");
	form.action = `/board/modify/${boardVO.boardId}`;
	form.submit();
}