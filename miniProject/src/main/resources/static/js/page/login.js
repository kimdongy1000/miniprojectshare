function findPasswordPop(){
	
	$("#findPasswordPop").modal("show");
	
	$("#findReSendInputEmail").attr("readonly" , false);
	$("#findReSendInputEmail").val("");
	$("#findBtnSeendEmail").show();
	$("#findReSendInputApprovedNumber").val("");
	
	$("#changePassword").val("");
	$("#changeConfirmPassword").val("");
	
	$("#findReSendInputApprovedNumber").hide();
	$("#findReSendInputApprovedNumber").hide();
	$("#findBtnAggreedEmail").hide();
	
	$("#changePassword").hide();
	$("#changeConfirmPassword").hide();
	$("#changePasswordBtn").hide();
}

function findSendEmail(){
	
	let email = $("#findReSendInputEmail").val();
	
	if(!email){
		alert('이메일을 입력해주세요');
		return;	
	}
	
	let type = "POST"
	let url = "/signIn/findpasswordSendEmail";
	let param = {
		"email" : email
	}
	
	ajaxNet(type , url , param , function(data){
		
		if(data === null || data == ''){
			alert('회원 가입을 하지 않은 유저입니다');
			return;
		}else{
			alert('인증 이메일을 발송했습니다');
			$("#findInputUserId").val(data.id);
			$("#findInputUserEmail").val(data.email);
			$("#findReSendInputApprovedNumber").show();
			$("#findBtnAggreedEmail").show();
			$("#findBtnSeendEmail").hide();
			return;
		}
		
	});
}

function findSendMailPop(){
	
	
	let id    = $("#findInputUserId").val();
	let email = $("#findInputUSerEmail").val();
	let inputConfirmNumber = $("#findReSendInputApprovedNumber").val();
	
	if(!inputConfirmNumber){
		alert('인증번호를 입력해주세요');
		return;
	}
	
	let type = "POST"
	let url = "/signIn/findpasswordApprovedNumber"
	let param = {
		"id" : id,
		"approvedNumber" : inputConfirmNumber
		
	}
	
	ajaxNet(type , url , param , function(data){
		
		if(data){
			alert('인증 성공');
			$("#findReSendInputEmail").attr("readonly" , true);
			
			$("#changePassword").show();
			$("#changeConfirmPassword").show();
			$("#changePasswordBtn").show();
			
			$("#findBtnAggreedEmail").hide();
			$("#findReSendInputApprovedNumber").hide();
		}else{
			alert('인증 실패 인증 번호를 확인해주세요');
			return;
		}
		
	})
	
	
}

function changePassword(){
	
	
}









//이메일 인증 로직 태우는 함수 
function reSendMailPopup(){
	
	$("#reSendPop").modal("show");
	
	$("#btnSeendEmail").show();
	$("#btnAggreedEmail").hide();
	$("#reSendInputApprovedNumber").hide();
	$("#reSendInputEmail").attr("readonly",false);
}

function reSendEmail(){

	

	let email = $("#reSendInputEmail").val();
	
	if(!email){
		alert('이메일을 입력해주세요');
		return;
		
	}
	
	let type = "POST"
	let url = "/signIn/enabledResendMail";
	let param = {
		"email" : email
	}
	
	ajaxNet(type , url , param , function(data){
		console.log(data);
	
		if(data === null || data === ''){
			alert('회원가입 하지 않은 유저 입니다');
		}else{
			if(data.enabled === '1'){
				alert('이미 이메일 인증을 받은 유저입니다');
			}else{
				alert('인증 메일을 발송했습니다 인증번호를 입력해주세요');
				$("#inputUserId").val(data.id);
				$("#inputUSerEmail").val(data.email);
				
				$("#btnSeendEmail").hide();
				$("#btnAggreedEmail").show();
				
				$("#reSendInputApprovedNumber").show();
				$("#reSendInputApprovedNumber").show();
				
				$("#reSendInputEmail").attr("readonly",true);

				
			}
			
		}	
	});
}

function reApprovedEmail(){
	
	let id    = $("#inputUserId").val();
	let email = $("#inputUSerEmail").val();
	let inputConfirmNumber = $("#reSendInputApprovedNumber").val();
	
	if(!inputConfirmNumber){
		alert('인증번호를 입력해주세요');
		return;
	}
	
	let type = "POST"
	let url = "/signIn/approvedNumber";
	let param = {
		"id" : id , 
		"inputConfirmNumber" : inputConfirmNumber
	}
	
	ajaxNet(type , url , param , function(data){
		
		if(data === true){
			alert('인증을 완료했습니다 다시 로그인 해주세요');
			$("#reSendPop").modal("hide");
		}else{
			alert('인증에 실패했습니다 다시 인증해주세요');
			$("#reSendPop").modal("hide");
		}	
	});
	
	
	
}