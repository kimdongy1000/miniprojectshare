function conversion(){

	let conversionMode = $("#conversionModeSelect").val();
	let beforeText = $("#beforeText").val();
	
	if(conversionMode == 'Caesar'){
		let check_kor = /^[a-zA-Z]*$/;
		if(!check_kor.test(beforeText)){
			alert('Caesar 암호는 영어만 가능합니다');
			$("#beforeText").val("");
			return;
		}
		
		sendBeforeMsg(beforeText , 'Caesar');
		
		
		
	}
	
}


function sendBeforeMsg(msg , encodingType){
	
	let type = "POST";
	let url = "/passwdEncoding/encoding";
	let param = {
		
		"msg" : msg,
		"encodingType" : encodingType
	}
	
	ajaxNet(type , url , param , function(data){
		
		console.log(data);
		
	})
	
	
	
}