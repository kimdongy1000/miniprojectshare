function init() {

	if (existResume > 0) {
		getCompanyHistory();
		getPjHistory();
		getPjDetail();
		getIntroduce();
		getAwardHistroy();
		getImageFile();
	}


}

function getCompanyHistory() {

	let type = "GET";
	let url = "/resume/getCompanyHistory";
	let param = {};

	ajaxNet_GET_NonAsync(type, url, param, function(data) {
		gridCompanyHistory(data);

	})
}

function gridCompanyHistory(data) {

	let companyHistoryTbody = document.querySelector(".resumeCompanyHistory");

	let appendTemp = null;

	for (let i = 0; i < data.length; i++) {
		let temp_TR = document.createElement("TR");
		temp_TR.setAttribute("id", `ch_tr_${data[i].no_index}`)
		for (let j = 0; j < 7; j++) {
			let temp_TD = document.createElement("TD");
			let temp_input = document.createElement("INPUT");

			if (j == 0) {
				temp_input.setAttribute("type", "text");
				temp_input.setAttribute("name", "text");
				temp_input.setAttribute("size", "20");
				temp_input.setAttribute("style", "width:100%;text-align:center;border: 0");
				temp_input.setAttribute("readonly", "readonly");

				temp_input.setAttribute("value", data[i].no_index);
				temp_input.setAttribute("name", "index");

				appendTemp = temp_input;
			}

			if (j == 1) {
				temp_input.setAttribute("type", "text");
				temp_input.setAttribute("name", "text");
				temp_input.setAttribute("size", "20");
				temp_input.setAttribute("style", "width:100%;text-align:center;border: 0");
				temp_input.setAttribute("readonly", "readonly");

				temp_input.setAttribute("value", data[i].companyName);
				temp_input.setAttribute("name", "companyName");

				appendTemp = temp_input;

			}

			if (j == 2) {
				temp_input.setAttribute("type", "text");
				temp_input.setAttribute("name", "text");
				temp_input.setAttribute("size", "20");
				temp_input.setAttribute("style", "width:100%;text-align:center;border: 0");
				temp_input.setAttribute("readonly", "readonly");

				temp_input.setAttribute("value", data[i].enterDate);
				temp_input.setAttribute("name", "enterDate");

				appendTemp = temp_input;

			}

			if (j == 3) {
				temp_input.setAttribute("type", "text");
				temp_input.setAttribute("name", "text");
				temp_input.setAttribute("size", "20");
				temp_input.setAttribute("style", "width:100%;text-align:center;border: 0");
				temp_input.setAttribute("readonly", "readonly");

				temp_input.setAttribute("value", data[i].endDate);
				temp_input.setAttribute("name", "endDate");

				appendTemp = temp_input;

			}

			if (j == 4) {
				temp_input.setAttribute("type", "text");
				temp_input.setAttribute("name", "text");
				temp_input.setAttribute("size", "20");
				temp_input.setAttribute("style", "width:100%;text-align:center;border: 0");
				temp_input.setAttribute("readonly", "readonly");

				temp_input.setAttribute("value", data[i].lastposition);
				temp_input.setAttribute("name", "position");

				appendTemp = temp_input;

			}

			if (j == 5) {
				temp_input.setAttribute("type", "text");
				temp_input.setAttribute("name", "text");
				temp_input.setAttribute("size", "20");
				temp_input.setAttribute("style", "width:100%;text-align:center;border: 0");
				temp_input.setAttribute("readonly", "readonly");

				temp_input.setAttribute("value", data[i].description);
				temp_input.setAttribute("name", "descWork");

				appendTemp = temp_input;

			}

			if (j == 6) {
				let temp_btn = document.createElement("BUTTON");
				temp_btn.setAttribute('type', 'button');
				temp_btn.setAttribute('style', 'width:100%; text-align:center; border: 0;');
				temp_btn.setAttribute('onclick', `removeCompanyHistory(${data[i].no_index})`);
				temp_btn.innerHTML = '삭제';
				appendTemp = temp_btn;

			}


			temp_TD.appendChild(appendTemp);
			temp_TR.appendChild(temp_TD);

		}



		companyHistoryTbody.appendChild(temp_TR);

	}

}

function addCompanyHistory() {

	let companyHistoryTbody = document.querySelector(".resumeCompanyHistory");

	let TRCount = companyHistoryTbody.childNodes;
	let index = 0;
	for (let j = 0; j < TRCount.length; j++) {
		if (TRCount[j].localName == 'tr') {
			index++;
		}
	}


	let temp_tr = document.createElement("TR");
	temp_tr.setAttribute('id', `ch_tr_${index}`);

	for (let i = 0; i < 7; i++) {
		let temp_td = document.createElement("TD");
		let temp_input = document.createElement("INPUT");
		let appendTemp = null;

		if (i == 0) {
			temp_input.setAttribute("type", "text");
			temp_input.setAttribute("name", "text");
			temp_input.setAttribute("size", "20");
			temp_input.setAttribute("style", "width:100%;text-align:center;border: 0");
			temp_input.setAttribute("readonly", "readonly");
			temp_input.setAttribute("value", index);
			temp_input.setAttribute("name", "index");
			temp_input.setAttribute("id", `ch_${index}`);
			appendTemp = temp_input;
		} else if (i == 2 || i == 3) {
			temp_input.setAttribute("type", "date");
			temp_input.setAttribute("name", "text");
			temp_input.setAttribute("size", "20");
			temp_input.setAttribute("style", "width:100%;text-align:center;border: 0");
			temp_input.setAttribute("value", "");
			appendTemp = temp_input;
		} else if (i == 6) {
			let temp_btn = document.createElement("BUTTON");
			temp_btn.setAttribute('type', 'button');
			temp_btn.setAttribute('style', 'width:100%; text-align:center; border: 0;');
			temp_btn.setAttribute('onclick', `removeCompanyHistory(${index})`);
			temp_btn.innerHTML = '삭제';
			appendTemp = temp_btn;

		} else {
			temp_input.setAttribute("type", "text");
			temp_input.setAttribute("name", "text");
			temp_input.setAttribute("size", "20");
			temp_input.setAttribute("style", "width:100%;text-align:center;border: 0");
			temp_input.setAttribute("value", "");
			appendTemp = temp_input;
		}


		if (i == 1) {
			temp_input.setAttribute("name", "companyName");
		} else if (i == 2) {
			temp_input.setAttribute("name", "enterDate");

		} else if (i == 3) {
			temp_input.setAttribute("name", "endDate");

		} else if (i == 4) {
			temp_input.setAttribute("name", "position");
		} else if (i == 5) {
			temp_input.setAttribute("name", "descWork");
		}


		temp_td.appendChild(appendTemp);
		temp_tr.appendChild(temp_td);
	}

	companyHistoryTbody.appendChild(temp_tr);
}

function removeCompanyHistory(index) {

	let companyHistoryTbody = document.querySelector(".resumeCompanyHistory");
	companyHistoryTbody.deleteRow(index);
	reNumberComPany();
}

function reNumberComPany() {

	let companyHistoryTbody = document.querySelector(".resumeCompanyHistory");

	let tbodyChilds = companyHistoryTbody.childNodes;
	let temp_flag = false;
	let temp_number = 1;
	for (let i = 0; i < tbodyChilds.length; i++) {
		if (tbodyChilds[i].localName == 'tr') {
			if (temp_flag) {
				let temp_id = tbodyChilds[i].getAttribute('id');
				let tnep = document.querySelector("#" + `${temp_id} input`);
				let tnep2 = document.querySelector("#" + `${temp_id} button`);
				tnep.setAttribute('value', temp_number);
				tnep2.setAttribute('onclick', `removeCompanyHistory(${temp_number})`)
				tbodyChilds[i].setAttribute("id", `ch_tr_${temp_number}`);
				temp_number++;
			} else {
				temp_flag = true;
			}

		}

	}

}

function resumeInsert() {

	let temp = $("#companyHistoryForm").serialize();

	let form = document.querySelector("#companyHistoryForm");
	form.submit();

}



function addProjectHistory() {

	let projectHistoryTbody = document.querySelector(".resumeProjectHistory");

	let TRCount = projectHistoryTbody.childNodes;
	let index = 0;
	for (let j = 0; j < TRCount.length; j++) {
		if (TRCount[j].localName == 'tr') {
			index++;
		}
	}


	let temp_tr = document.createElement("TR");

	for (let i = 0; i < 7; i++) {
		let temp_td = document.createElement("TD");
		let temp_input = document.createElement("INPUT");
		let temp_button = document.createElement("BUTTON");

		let temp_element = null;

		temp_tr.setAttribute("id", `pj_tr_${index}`);

		if (i == 0) {
			temp_input.setAttribute("type", "text");
			temp_input.setAttribute("size", "20");
			temp_input.setAttribute("style", "width:100%; text-align:center; border: 0;");
			temp_input.setAttribute("readonly", "readonly");
			temp_input.setAttribute("id", `ch_${index}`);
			temp_input.setAttribute("value", `${index}`);
			temp_input.setAttribute("name", "pjIndex");
			temp_element = temp_input;
		}

		if (i == 1) {
			temp_input.setAttribute("type", "text");
			temp_input.setAttribute("size", "20");
			temp_input.setAttribute("style", "width:100%; text-align:center; border: 0;");
			temp_input.setAttribute("value", "");
			temp_input.setAttribute("name", "pjName");
			temp_element = temp_input;

		}

		if (i == 2) {
			temp_input.setAttribute("type", "date");
			temp_input.setAttribute("size", "20");
			temp_input.setAttribute("style", "width:100%; text-align:center; border: 0;");
			temp_input.setAttribute("value", "");
			temp_input.setAttribute("name", "pjEnterDate");
			temp_element = temp_input;

		}

		if (i == 3) {
			temp_input.setAttribute("type", "date");
			temp_input.setAttribute("size", "20");
			temp_input.setAttribute("style", "width:100%; text-align:center; border: 0;");
			temp_input.setAttribute("value", "");
			temp_input.setAttribute("name", "pjEndDate");
			temp_element = temp_input;

		}

		if (i == 4) {
			temp_input.setAttribute("type", "text");
			temp_input.setAttribute("size", "20");
			temp_input.setAttribute("style", "width:100%; text-align:center; border: 0;");
			temp_input.setAttribute("value", "");
			temp_input.setAttribute("name", "pjPosition");
			temp_element = temp_input;

		}

		if (i == 5) {
			temp_input.setAttribute("type", "text");
			temp_input.setAttribute("size", "20");
			temp_input.setAttribute("style", "width:100%; text-align:center; border: 0;");
			temp_input.setAttribute("value", "");
			temp_input.setAttribute("name", "pjDesc");
			temp_element = temp_input;

		}

		if (i == 6) {
			temp_button.setAttribute("type", "text");
			temp_button.setAttribute("style", "width:100%; text-align:center; border: 0;");
			temp_button.setAttribute("onclick", `removeProjectHistory(${index})`)
			temp_button.innerHTML = '삭제'
			temp_element = temp_button;

		}

		temp_td.appendChild(temp_element);
		temp_tr.appendChild(temp_td);
	}

	projectHistoryTbody.appendChild(temp_tr);
}

function removeProjectHistory(index) {

	let projectHistoryTbody = document.querySelector(".resumeProjectHistory");
	projectHistoryTbody.deleteRow(index);
	renumberProjectHistory();
}

function renumberProjectHistory() {

	let projectHistoryTbody = document.querySelector(".resumeProjectHistory");
	let tbodyChilds = projectHistoryTbody.childNodes;
	let temp_flag = false;
	let temp_number = 1;
	for (let i = 0; i < tbodyChilds.length; i++) {
		if (tbodyChilds[i].localName == 'tr') {
			if (temp_flag) {
				let temp_id = tbodyChilds[i].getAttribute('id');
				let tnep = document.querySelector("#" + `${temp_id} input`);
				let tnep2 = document.querySelector("#" + `${temp_id} button`);
				tnep.setAttribute('value', temp_number);
				tnep2.setAttribute('onclick', `removeProjectHistory(${temp_number})`)
				tbodyChilds[i].setAttribute("id", `pj_tr_${temp_number}`);
				temp_number++;
			} else {
				temp_flag = true;
			}

		}

	}



}

function getPjHistory() {

	let type = "GET";
	let url = "/resume/getPjHistory";
	let param = {};

	ajaxNet_GET_NonAsync(type, url, param, function(data) {
		gridPjHistory(data);

	})

}

function gridPjHistory(data) {




	let pjHistoryTbody = document.querySelector('.resumeProjectHistory');

	for (let i = 0; i < data.length; i++) {
		let temp_tr = document.createElement('TR');

		temp_tr.setAttribute("id", `pj_tr_${data[i].no_index}`);

		for (let j = 0; j < 7; j++) {

			let temp_td = document.createElement("TD")
			let temp_input = document.createElement("INPUT");
			let temp_element = null;

			if (j == 0) {

				temp_input.setAttribute("type", "text");
				temp_input.setAttribute("size", "20");
				temp_input.setAttribute("style", "width:100%; text-align:center; border: 0;");
				temp_input.setAttribute("readonly", "readonly");
				temp_input.setAttribute("id", `ph_${data[i].no_index}`);
				temp_input.setAttribute("value", `${data[i].no_index}`);
				temp_input.setAttribute("name", "pjIndex");
				temp_element = temp_input;

			}

			if (j == 1) {

				temp_input.setAttribute("type", "text");
				temp_input.setAttribute("size", "20");
				temp_input.setAttribute("style", "width:100%; text-align:center; border: 0;");
				temp_input.setAttribute("value", `${data[i].pjName}`);
				temp_input.setAttribute("name", "pjName");
				temp_element = temp_input;

			}

			if (j == 2) {

				temp_input.setAttribute("type", "text");
				temp_input.setAttribute("size", "20");
				temp_input.setAttribute("style", "width:100%; text-align:center; border: 0;");
				temp_input.setAttribute("value", `${data[i].enterDate}`);
				temp_input.setAttribute("name", "pjEnterDate");
				temp_element = temp_input;

			}

			if (j == 3) {

				temp_input.setAttribute("type", "text");
				temp_input.setAttribute("size", "20");
				temp_input.setAttribute("style", "width:100%; text-align:center; border: 0;");
				temp_input.setAttribute("value", `${data[i].endDate}`);
				temp_input.setAttribute("name", "pjEndDate");
				temp_element = temp_input;

			}

			if (j == 4) {

				temp_input.setAttribute("type", "text");
				temp_input.setAttribute("size", "20");
				temp_input.setAttribute("style", "width:100%; text-align:center; border: 0;");
				temp_input.setAttribute("value", `${data[i].lastpostion}`);
				temp_input.setAttribute("name", "pjPosition");
				temp_element = temp_input;

			}

			if (j == 5) {

				temp_input.setAttribute("type", "text");
				temp_input.setAttribute("size", "20");
				temp_input.setAttribute("style", "width:100%; text-align:center; border: 0;");
				temp_input.setAttribute("value", `${data[i].description}`);
				temp_input.setAttribute("name", "pjDesc");
				temp_element = temp_input;

			}

			if (j == 6) {
				let temp_button = document.createElement('BUTTON');

				temp_button.setAttribute("type", "text");
				temp_button.setAttribute("style", "width:100%; text-align:center; border: 0;");
				temp_button.setAttribute("onclick", `removeProjectHistory(${data[i].no_index})`)
				temp_button.innerHTML = '삭제'
				temp_element = temp_button;


			}

			temp_td.appendChild(temp_element);
			temp_tr.appendChild(temp_td);
		}

		pjHistoryTbody.appendChild(temp_tr);

	}

}

function getPjDetail() {

	let type = "GET";
	let url = "/resume/getPjDetail";
	let param = {}

	ajaxNet_GET_NonAsync(type, url, param, function(data) {
		let pjDetailTextArea = $("#projectDetailTextArea");
		pjDetailTextArea.html(data);
	});

}

function getIntroduce() {

	let type = "GET";
	let url = "/resume/getIntroduce";
	let param = {}

	ajaxNet_GET_NonAsync(type, url, param, function(data) {
		let indrocueText = $("#introduceTextArea");
		indrocueText.html(data);
	});

}

function addAwardHistory() {

	let awardHistroyTbody = document.querySelector(".awardHistory");

	let index = 0;

	let awardHistroyChildsNodes = awardHistroyTbody.childNodes;

	for (let i = 0; i < awardHistroyChildsNodes.length; i++) {
		if (awardHistroyChildsNodes[i].localName == 'tr') {
			index++;
		}
	}

	let temp_tr = document.createElement("TR");
	temp_tr.setAttribute('id', `aw_tr_${index}`);

	for (let j = 0; j < 4; j++) {

		let temp_td = document.createElement("TD");
		let temp_input = document.createElement("INPUT");
		let temp_document = null;

		if (j == 0) {
			temp_input.setAttribute('type', 'text');
			temp_input.setAttribute('name', 'aw_index');
			temp_input.setAttribute('size', '20');
			temp_input.setAttribute('readonly', 'readonly');
			temp_input.setAttribute('style', 'width:100%; text-align:center; border: 0;');
			temp_input.setAttribute('value', `${index}`);
			temp_document = temp_input;
		}

		if (j == 1) {
			temp_input.setAttribute('type', 'text');
			temp_input.setAttribute('name', 'awName');
			temp_input.setAttribute('size', '20');
			temp_input.setAttribute('style', 'width:100%; text-align:center; border: 0;');
			temp_document = temp_input;
		}

		if (j == 2) {
			temp_input.setAttribute('type', 'date');
			temp_input.setAttribute('name', 'acquisitionDate');
			temp_input.setAttribute('size', '20');
			temp_input.setAttribute('style', 'width:100%; text-align:center; border: 0;');
			temp_document = temp_input;
		}

		if (j == 3) {
			let temp_button = document.createElement('BUTTON');

			temp_button.setAttribute('type', 'button');
			temp_button.setAttribute('style', 'width:100%; text-align:center; border: 0;');
			temp_button.setAttribute('onclick', `removeAwHistory(${index})`);
			temp_button.innerText = '삭제'
			temp_document = temp_button;
		}

		temp_td.appendChild(temp_document);
		temp_tr.appendChild(temp_td);
	}

	awardHistroyTbody.appendChild(temp_tr);
}

function removeAwHistory(index) {

	let awardHistoryTbody = document.querySelector('.awardHistory');
	awardHistoryTbody.deleteRow(index);
	reNumberAwhistory();
}

function reNumberAwhistory() {

	let awardHistroyTbody = document.querySelector('.awardHistory');

	let awardHistroyChild = awardHistroyTbody.childNodes;


	let tempBoolean = false
	let index = 1;

	for (let j = 0; j < awardHistroyChild.length; j++) {
		if (awardHistroyChild[j].localName == 'tr') {
			if (tempBoolean) {

				let temp_id = awardHistroyChild[j].getAttribute('id');
				let temp_input = document.querySelector("#" + `${temp_id} td input`);
				let temp_button = document.querySelector("#" + `${temp_id} td button`);
				let temp_tr = document.querySelector("#" + `${temp_id}`);

				temp_input.setAttribute('value', index);
				temp_button.setAttribute('onclick', `removeAwHistory(${index})`);
				temp_tr.setAttribute('id', `aw_tr_${index}`);

				index++;

			} else {
				tempBoolean = true;
			}

		}

	}
}

function getAwardHistroy() {

	let type = "GET";
	let url = "/resume/getAwardHistory";
	let param = {}

	ajaxNet_GET_NonAsync(type, url, param, function(data) {
		gridAwardHistory(data);
	});
}

function gridAwardHistory(data) {

	let awardHistoryTbody = document.querySelector('.awardHistory');

	for (let i = 0; i < data.length; i++) {

		let temp_tr = document.createElement("TR");
		temp_tr.setAttribute('id', `aw_tr_${data[i].no_index}`);

		for (let j = 0; j < 4; j++) {
			let temp_td = document.createElement("TD");
			let temp_input = document.createElement("INPUT");
			let temp_element = null;

			if (j == 0) {

				temp_input.setAttribute('name', 'aw_index');
				temp_input.setAttribute('type', 'text');
				temp_input.setAttribute('size', '20');
				temp_input.setAttribute('style', 'width:100%; text-align:center; border: 0;')
				temp_input.setAttribute('readonly', 'readonly');
				temp_input.setAttribute('value', `${data[i].no_index}`);
				temp_element = temp_input;

			}

			if (j == 1) {

				temp_input.setAttribute('name', 'awName');
				temp_input.setAttribute('type', 'text');
				temp_input.setAttribute('size', '20');
				temp_input.setAttribute('style', 'width:100%; text-align:center; border: 0;')
				temp_input.setAttribute('value', `${data[i].awardName}`);
				temp_element = temp_input;

			}
			
			if (j == 2) {

				temp_input.setAttribute('name', 'acquisitionDate');
				temp_input.setAttribute('type', 'date');
				temp_input.setAttribute('size', '20');
				temp_input.setAttribute('style', 'width:100%; text-align:center; border: 0;')
				temp_input.setAttribute('value', `${data[i].acquisitionDate}`);
				temp_element = temp_input;

			}
			
			if (j == 3) {

				let temp_button = document.createElement('BUTTON');
				temp_button.setAttribute('type', 'button');
				temp_button.setAttribute('style', 'width:100%; text-align:center; border: 0;');
				temp_button.innerText = "삭제";
				temp_button.setAttribute('onclick', `removeAwHistory(${data[i].no_index})`);
				temp_element = temp_button;

			}
			
			temp_td.appendChild(temp_element);
			temp_tr.appendChild(temp_td);
		}
		
		awardHistoryTbody.appendChild(temp_tr);

	}

}

function imageUpload(){
	
	let imageFile = $("#imageFile")[0].files[0];
	
	let fileName = imageFile.name;
	let file_extension = fileName.split('.'); 
	
	let flag = file_extension_search(file_extension[1]);
	
	if(flag == -1){
		alert('사진 첨부파일은 png , jpeg , jpg 파일만 업로드 가능합니다 ');
		$('#imageFile').val('');

		return;
	} 

	
	let formData = new FormData();
	
	formData.append("uploadImage" , imageFile);
	
	let type = "POST";
	let url = "/resume/insertImage";
	let param = formData;
		
	
	
	ajaxNet_POST_UPLOADFILE(type , url , param , function(data){
		appendImageData(data);
		$('#imageFile').val('');
	})
	
}

function file_extension_search(ext){
	
	let allowImageExt = "png jpeg jpg";
	let lowerExt = ext.toLowerCase();
	
	return allowImageExt.indexOf(lowerExt);
	
	
}

function appendImageData(data){
	
	let imgDiv = document.querySelector(".imageDiv");
	
	imgDiv.innerHTML = '';
	
	for(let i = 0; i < data.length; i++){
		let temp_image = document.createElement("IMG");
		let temp_div = document.createElement("DIV");
		let temp_button = document.createElement("BUTTON");
		temp_image.setAttribute('src' , `/resume/getImage/${data[i].fileName}`);
		temp_button.setAttribute("class" , "btn btn-danger")
		temp_button.setAttribute("style" , "width:50px;height:50px;")
		temp_button.setAttribute("onclick" , `deleteImageFIle(${data[i].no})`);
		temp_button.setAttribute("type" , `button`);
		temp_button.innerText = "삭제";
		
		temp_div.setAttribute("class" , "pull-right");
		
		temp_div.appendChild(temp_button);
		
		imgDiv.appendChild(temp_image);
		imgDiv.appendChild(temp_div);
		
	}
	
}

function deleteImageFIle(image_no){
	
	if(confirm('삭제하시겠습니까?')){
		let type = "PATCH";
		let url = "/resume/patchingImgFile/"
		let param = {"image_no" : image_no}
	
		ajaxNet(type , url , param , function(){
			getImageFile();		
		});
	}		
}

function getImageFile(){
	
	let type = "GET";
	let url = "/resume/getImage";
	let param = {};
	
	ajaxNet_GET_NonAsync(type , url , param , function(data){
		appendImageData(data);
	});
}


init();


