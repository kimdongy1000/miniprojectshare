$(document).ready(function() {
	$('#summernote').summernote({
		// 에디터 높이
		height: 800,
		// 에디터 한글 설정
		lang: "ko-KR",
		// 에디터에 커서 이동 (input창의 autofocus라고 생각하시면 됩니다.)
		toolbar: [

		]

	});

	$('#summernote').summernote('code', boardVO.content);
	$('#summernote').summernote('disable');

});


function init() {

	if (mine) {
		$(".modify").show();
		$(".delete").show();
	} else {
		$(".modify").hide();
		$(".delete").hide();
	}
}

function deleteBoard () {
	if (confirm('정말로 삭제하시겠습니까?')) {

		let type = "DELETE";
		let url = `/board/delete/${boardVO.boardId}`;
		let param = {};

		ajaxNet_GET_NonAsync(type, url, param, function(data) {
			alert('삭제가 완료 되었습니다');
			location.href = "/board/";
		});

	}
}

function updateBoard(){
	
	if(confirm('수정하시겠습니까?')){
		location.href = `/board/modifyPage/${boardVO.boardId}`
	}
}


init();