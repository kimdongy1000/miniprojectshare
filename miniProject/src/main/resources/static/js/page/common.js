function logout(){

	
}

function ajaxNet_POST_UPLOADFILE(type , url , param , callback){
	
	let header = $("meta[name='_csrf_header']").attr('content');
	let token = $("meta[name='_csrf']").attr('content');
	
	console.log(type , url , param);
	
	$.ajax({
		type : type,
		url : url,
		processData: false,
		contentType: false,
		data : param,
		beforeSend : function(xhr){
			xhr.setRequestHeader(header, token);
		},
		statusCode : {
			500 : function(){
				location.href = "/error/500"
			},
			401 : function(){
				location.href = "/error/401"
			}
		},
		success : function(data , textStatus , xhr) {
			return callback(data)
			
		},	
		
		error : function(xhr, status, error) {

		}
	});
	
}

function ajaxNet_GET_NonAsync(type , url , param , callback){
	
	let header = $("meta[name='_csrf_header']").attr('content');
	let token = $("meta[name='_csrf']").attr('content');
	
	console.log(type , url , param);
	
	$.ajax({
		type : type,
		url : url,
		async:false,
		beforeSend : function(xhr){
			xhr.setRequestHeader(header, token);
		},
		statusCode : {
			500 : function(){
				location.href = "/error/500"
			},
			401 : function(){
				location.href = "/error/401"
			}
		},
		success : function(data , textStatus , xhr) {
			return callback(data)
			
		},	
		
		error : function(xhr, status, error) {

		}
	});
}

function ajaxNet(type , url , param , callback){
	
	let header = $("meta[name='_csrf_header']").attr('content');
	let token = $("meta[name='_csrf']").attr('content');

	console.log(type , url , param);
	
	let resultMsg = null;
		
	$.ajax({
		type : type,
		url : url,
		contentType : "application/json",
		data : JSON.stringify(param),
		beforeSend : function(xhr){
			xhr.setRequestHeader(header, token);
		},
		statusCode : {
			500 : function(){
				location.href = "/error/500"
			},
			401 : function(){
				location.href = "/error/401"
			}
		},
		success : function(data , textStatus , xhr) {
			return callback(data)
			
		},	
		
		error : function(xhr, status, error) {

		}
	});
	
}

function includeHTML() {
  var z, i, elmnt, file, xhr;
  /*loop through a collection of all HTML elements:*/
  z = document.getElementsByTagName("*");
  for (i = 0; i < z.length; i++) {
    elmnt = z[i];
    /*search for elements with a certain atrribute:*/
    file = elmnt.getAttribute("include-html");
    //console.log(file);
    if (file) {
      /*make an HTTP request using the attribute value as the file name:*/
      xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (this.readyState == 4) {
          if (this.status == 200) {
            elmnt.innerHTML = this.responseText;
          }
          if (this.status == 404) {
            elmnt.innerHTML = "Page not found.";
          }
          /*remove the attribute, and call this function once more:*/
          elmnt.removeAttribute("include-html");
          includeHTML();
        }
      };
      xhr.open("GET", file, true);
      xhr.send();
      /*exit the function:*/
      return;
    }
  }
  setTimeout(function() {
  }, 0);
}

function init(){
	
	includeHTML();
	
}


init();