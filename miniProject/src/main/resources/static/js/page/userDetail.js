let passwordCheck = true;

function init(){
	
	console.log(userVO);
	
	let id = userVO.id
	let email = userVO.email;
	
	
	let nickname = userVO.nickname;
	let age = userVO.age;
	
	let postNumber = userVO.postNumber;
	let address1 = userVO.address
	let address2 = userVO.address2;

	
	$("#id").val(id);
	$("#email").val(email);
	$("#nickName").val(nickname);
	$("#age").val(age);
	$("#frontPostNumber").val(postNumber);
	$("#address1").val(address1);
	$("#address2").val(address2);
	
	
}

$(".btn-userDetailUpdate").click(function(){

	let id 	            = $("#id").val();
	let email 	        = $("#email").val();
	let password 	    = $("#password").val();
	let confirmPassword = $("#confirmPassword").val();
	let nickName        = $("#nickName").val();
	let age             = $("#age").val();
	let frontPostNumber = $("#frontPostNumber").val();
	let backPostNumber  = $("#backPostNumber").val();
	let address1 	    = $("#address1").val();
	let address2        = $("#address2").val();
	
	if(!passwordCheck){

		if(!password){
			alert('비밀번호를 입력해주세요');
			return;
		}
		
		if(!confirmPassword){
			alert('비밀번호 확인을 입력해주세요');
			return;
		}
		
		if(password !== confirmPassword){
			alert('비밀번호가 서로 같지 않습니다');
			return;
		}
	}
	
	if(age){
		if(age < 0){
			alert('나이는 0 미만의 값을 입력할 수 없습니다');
			$("#age").val(0);
			return;
		}
	}
	
	let type = "Patch";
	let url = "/user/updateUserDetail";
	let param = {
		"id"              :id,
		"email" 	      : email,
		"password"        : password,
		"nickName"  	  : nickName,
		"age"       	  : age,
		"frontPostNumber" : frontPostNumber,
		"address1"        : address1,
		"address2"        : address2
		 
	}
	
	ajaxNet(type , url , param , function(){
		alert('유저 정보 업데이트 완료 했습니다');
		location.href = "/user/detail";
	});	

})

function checkPassword(){

	let password 	    = $("#password").val();
	let confirmPassword = $("#confirmPassword").val();
	
	let lengthPassword 	      = password.length;
	let lengthconfirmPassword = confirmPassword.length;
	
	if(lengthPassword === 0 && lengthconfirmPassword === 0){
		passwordCheck = true;
	}else{
		passwordCheck = false;
	}
}

$(".search-postNumber").click(function(){
	
	$("#testModal").modal("show");
	
});


init();