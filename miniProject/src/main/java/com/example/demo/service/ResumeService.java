package com.example.demo.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.Iservice.ResumeIService;
import com.example.demo.data.ResumeData;
import com.example.demo.domain.PjHistoryVO;
import com.example.demo.domain.ResumeComapnyHistoryVO;
import com.example.demo.domain.ResumeImageVO;
import com.example.demo.domain.ResumeVO;
import com.example.demo.domain.UserVO;
import com.example.demo.repository.CommonRepository;
import com.example.demo.repository.ResumeRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ResumeService implements ResumeIService {

	@Autowired
	private ResumeRepository resumeRepository;

	@Autowired
	private CommonRepository commonRepository;

	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Override
	public int countMyResume(UserVO userVO) {
		return resumeRepository.countMyResumeCount(userVO);
	}

	@Override
	public UserVO getUserDetail(Long id) {
		return resumeRepository.getUserDetail(id);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public synchronized void modifyResume(ResumeData resumeData, UserVO userVO) {

		TransactionStatus txStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());
		Map<String, Object> param = new HashMap<String, Object>();

		try {

			int resumeId = commonRepository.getAutoIncrement("resume");

			String commpanyCode = commonRepository.getCommonCode("CH");
			String projectCode = commonRepository.getCommonCode("PJ");
			String awardCode = commonRepository.getCommonCode("AW");

			int existResume = resumeRepository.existResume(userVO);

			SimpleDateFormat todayFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			Date time = new Date();

			String today = todayFormat.format(time);
			String stringResumeId = "";

			if (resumeId < 10) {
				stringResumeId = "000" + resumeId;

			} else if (resumeId < 100) {
				stringResumeId = "00" + resumeId;

			} else if (resumeId < 1000) {
				stringResumeId = "0" + resumeId;
			} else {
				stringResumeId = "" + resumeId;
			}

			String companyHistory = commpanyCode + today + stringResumeId;

			String projectHistoryCode = projectCode + today + stringResumeId;

			String awardHistoryCode = awardCode + today + stringResumeId;

			param.put("id", userVO.getId());

			param.put("companyHistoryCode", companyHistory);

			param.put("projectHistoryCode", projectHistoryCode);

			param.put("awardHistoryCode", awardHistoryCode);

			resumeData.setCompanyHistoryCode(companyHistory);
			resumeData.setPjHistoryCode(projectHistoryCode);
			resumeData.setResumeId(resumeId);

			if (existResume < 1) {
				resumeRepository.insertResume(param);
			} else {

				ResumeVO resumeVO = resumeRepository.getResume(param);

				companyHistory = resumeVO.getCompanyHistoryCode();
				projectHistoryCode = resumeVO.getPrjoectHistoryCode();
				awardHistoryCode = resumeVO.getAwardsHistoryCode();

				resumeData.setResumeId(resumeVO.getResumeid());
				resumeData.setCompanyHistoryCode(companyHistory);
				resumeData.setPjHistoryCode(projectHistoryCode);
				resumeData.setAwardsHistoryCode(awardHistoryCode);

				param.put("resumeId", resumeVO.getResumeid());
				param.replace("companyHistoryCode", resumeData.getCompanyHistoryCode());
				param.replace("projectHistoryCode", resumeData.getPjHistoryCode());
				param.replace("awardHistoryCode", resumeData.getAwardsHistoryCode());

				resumeRepository.deleteCompanyHistroy(param);
				resumeRepository.deletePjHistory(param);
				resumeRepository.deleteAwardHistory(param);

			}

			mergeCompanyHistory(userVO, resumeData);
			mergePjHistory(userVO, resumeData);
			updatePjDetail(userVO, resumeData);
			updateIntroduceText(userVO, resumeData);
			mergeAwardsHistory(userVO, resumeData);
			
			transactionManager.commit(txStatus);

		} catch (Exception e) {
			transactionManager.rollback(txStatus);
		}

	}

	@Override
	public int mergeCompanyHistory(UserVO userVO, ResumeData resumeData) {

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("userVO", userVO);

		int resultInt = 0;
		if (resumeData.getIndex() != null) {

			for (int i = 0; i < resumeData.getIndex().length; i++) {

				param.put("index", resumeData.getIndex()[i]);
				param.put("companyName", resumeData.getCompanyName()[i]);
				param.put("enterDate", resumeData.getEnterDate()[i]);
				param.put("endDate", resumeData.getEndDate()[i]);
				param.put("position", resumeData.getPosition()[i]);
				param.put("descWork", resumeData.getDescWork()[i]);
				param.put("companyHistoryCode", resumeData.getCompanyHistoryCode());
				param.put("resumeId", resumeData.getResumeId());

				resumeRepository.mergeCompanyHistory(param);

			}

		}

		return resultInt;
	}

	@Override
	public int mergePjHistory(UserVO userVO, ResumeData resumeData) {

		Map<String, Object> param = new HashMap<String, Object>();

		param.put("userVO", userVO);

		int resultInt = 0;

		if (resumeData.getPjIndex() != null) {

			for (int i = 0; i < resumeData.getPjIndex().length; i++) {

				param.put("pjIndex", resumeData.getPjIndex()[i]);
				param.put("pjName", resumeData.getPjName()[i]);
				param.put("pjEnterDate", resumeData.getPjEndDate()[i]);
				param.put("pjEndDate", resumeData.getPjEndDate()[i]);
				param.put("pjPosition", resumeData.getPjPosition()[i]);
				param.put("pjDesc", resumeData.getPjDesc()[i]);

				param.put("pjHistoryCode", resumeData.getPjHistoryCode());
				param.put("resumeId", resumeData.getResumeId());

				resumeRepository.mergePjHistory(param);

			}

		}

		return resultInt;
	}

	@Override
	public List<ResumeComapnyHistoryVO> getCompanyHistory(UserVO userVO) {

		Map<String, Object> param = new HashMap<>();
		param.put("id", userVO.getId());
		String companyHistoryCode = resumeRepository.getCompanyHistoryCode(param);

		param.put("companyHistoryCode", companyHistoryCode);

		return resumeRepository.getCompanyHistoryList(param);
	}

	@Override
	public List<PjHistoryVO> getPjHistory(UserVO userVO) {

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("id", userVO.getId());

		ResumeVO resumeVO = resumeRepository.getResume(param);

		param.put("projectHistoryCode", resumeVO.getPrjoectHistoryCode());
		param.put("resumeId", resumeVO.getResumeid());

		return resumeRepository.getPjHistory(param);
	}

	@Override
	public int updatePjDetail(UserVO userVO, ResumeData resumeData) {

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("userVO", userVO);
		param.put("resumeData", resumeData);

		return resumeRepository.updatePjDetail(param);
	}

	@Override
	public String getPjDetail(UserVO userVO) {

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("id", userVO.getId());

		ResumeVO resumeVO = resumeRepository.getResume(param);
		return resumeVO.getPrjoectDetailText();
	}

	@Override
	public int updateIntroduceText(UserVO userVO, ResumeData resumeData) {

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("userVO", userVO);
		param.put("resumeData", resumeData);

		return resumeRepository.updateIntroduceText(param);
	}

	@Override
	public String getIntroduce(UserVO userVO) {

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("id", userVO.getId());

		ResumeVO resumeVO = resumeRepository.getResume(param);

		return resumeVO.getIntroduceText();
	}

	@Override
	public int mergeAwardsHistory(UserVO userVO, ResumeData resumeData) {

		int result = 0;

		if (resumeData.getAw_index() != null) {

			Map<String, Object> param = new HashMap<String, Object>();
			param.put("userVO", userVO);

			for (int i = 0; i < resumeData.getAw_index().length; i++) {
				param.put("no_index", resumeData.getAw_index()[i]);
				param.put("acquisitionDate", resumeData.getAcquisitionDate()[i]);
				param.put("awardName", resumeData.getAwName()[i]);

				param.put("awardsHistoryCode", resumeData.getAwardsHistoryCode());
				param.put("resumeId", resumeData.getResumeId());

				resumeRepository.mergeAwardHistory(param);
			}
		}

		return result;
	}

	@Override
	public List<?> getAwardHistory(UserVO userVO) {

		Map<String, Object> param = new HashMap();
		param.put("id", userVO.getId());

		ResumeVO resumeVO = resumeRepository.getResume(param);

		return resumeRepository.getAwardHistory(resumeVO);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<?> insertAndGetImage(UserVO userVO, MultipartFile file) {
		
		TransactionStatus txStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());
		List<ResumeImageVO> imageList = null;
		
		try {
			//String filePath = "C:\\Users\\kimdongy1000\\Desktop\\test\\awardFile";
			String filePath = "/home/wasadmin/awardHistory";
			
			SimpleDateFormat formate = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			SimpleDateFormat formate2 = new SimpleDateFormat("yyyyMMdd");
			
			Date date = new Date();
			String fileName = formate.format(date);
			String director = formate2.format(date);
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("id", userVO.getId());
			
			ResumeVO resumeVO = resumeRepository.getResume(param);
			
			filePath = filePath + "/" + director;
			
			param.put("resumeId", resumeVO.getResumeid());
			param.put("filePath", filePath);
			param.put("fileName", fileName);
			param.put("originFileName", file.getOriginalFilename());
			
			resumeRepository.insertResumeImage(param);
			
			
			File newFile = new File(filePath);
			
			if(!newFile.exists()) {
				newFile.mkdirs();
				log.info("{}" , "new make driector");
			}else {
				log.info("{}" , "already driector");
			}
			
			byte [] fileByte = file.getBytes();
			FileOutputStream fos = new FileOutputStream(filePath + "/" + fileName);
			fos.write(fileByte);
			fos.close();
			transactionManager.commit(txStatus);
			
			imageList = resumeRepository.getImageFiles(resumeVO);
			
		}catch (Exception e) {
			transactionManager.rollback(txStatus);
		}
		
		return imageList;
	}

	@Override
	public ResponseEntity<byte[]> getResumeImage(Map<String, Object> param) {
		
		UserVO userVO = (UserVO)param.get("userVO");
		
		param.put("id", userVO.getId());
		
		ResumeVO resumeVO = resumeRepository.getResume(param);
		
		param.put("resumeVO", resumeVO);
		
		
		ResumeImageVO resumeImage = resumeRepository.getFileImage(param);
		
		InputStream imageStream;
		try {
			imageStream = new FileInputStream(resumeImage.getFilePath() + "/" + resumeImage.getFileName());
			byte[] imageByteArray = IOUtils.toByteArray(imageStream); imageStream.close();
			return new ResponseEntity<byte[]>(imageByteArray, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("{}" , this.getClass().toString());
			log.error("{}" , e.toString());
			return new ResponseEntity<byte[]>(HttpStatus.INTERNAL_SERVER_ERROR);
			
		} 
		
	}

	@Override
	public ResponseEntity<Boolean> deleteImageFile(Map<String, Object> param) {
		try {
			ResumeVO resumeVO = resumeRepository.getResume(param);
			
			param.put("resumeId", resumeVO.getResumeid());
			
			resumeRepository.deleteImageFile(param);
			return new ResponseEntity<Boolean>(true , HttpStatus.OK);
		}catch (Exception e) {
			log.error("{}" , this.getClass().toString());
			log.error("{}" , e.toString());
			return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public List<?> getAllImage(Map<String, Object> param) {
		
		ResumeVO resumeVO = resumeRepository.getResume(param);
		
		return resumeRepository.getImageFiles(resumeVO);
	}
	
	
	
	
	
	

}
