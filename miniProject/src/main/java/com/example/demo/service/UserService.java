package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.example.demo.Iservice.UserIService;
import com.example.demo.configuration.BcryptPassEncoder;
import com.example.demo.data.RegisterUser;
import com.example.demo.data.UserDetailData;
import com.example.demo.domain.UserVO;
import com.example.demo.repository.UserRepository;

@Service
public class UserService implements UserIService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BcryptPassEncoder bcryptPassEncoder;
	
	@Autowired
	private  DataSourceTransactionManager transactionManager;

	@Override
	public UserVO getUserDetail(Long id) {
		return userRepository.getUserDetail(id);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Void> updateUserDetail(UserVO userData, UserDetailData userParam) {

		if(userParam.getPassword() != null && !userParam.getPassword().equals("")) {
			userData.setPassword(bcryptPassEncoder.encode(userParam.getPassword()));
		}
		
		if(userParam.getNickName() != null || !userParam.getNickName().equals("")) {
			userData.setNickname(userParam.getNickName());
		}
		
		userData.setAge(userParam.getAge());
		
		
		if(userParam.getFrontPostNumber() != null || !userParam.getFrontPostNumber().equals("")) {
			userData.setPostNumber(userParam.getFrontPostNumber());
		}
		
		if(userParam.getAddress1() != null || !userParam.getAddress1().equals("")) {
			userData.setAddress(userParam.getAddress1());
		}
		
		if(userParam.getAddress2() != null || !userParam.getAddress2().equals("")) {
			userData.setAddress2(userParam.getAddress2());
		}
		
		
		TransactionStatus txStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());
		
		try {
			
			RegisterUser registerUser = userRepository.findById(userParam.getId());
			registerUser.setPassword(userData.getPassword());
			userRepository.updateRegisterUser(registerUser);
			userRepository.updateUserDetail(userData);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			transactionManager.rollback(txStatus);
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public void updateLockCnt(Long id) {
		userRepository.updateUserLockCnt(id);
	}
	
	
	
	
	
	
	

}
