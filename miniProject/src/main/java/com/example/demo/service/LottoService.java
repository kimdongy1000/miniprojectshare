package com.example.demo.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Iservice.LottoIService;
import com.example.demo.repository.LottoRepository;

@Service
public class LottoService implements LottoIService {

	@Autowired
	private LottoRepository lottoRepository;

	@Override
	public List<Entry<String, Long>> getNumber() {

		Map<String, Long> numberMap = lottoRepository.getNumber();
		List<Entry<String, Long>> list_entries = null;
		if (numberMap != null ) {

			list_entries = new ArrayList<Entry<String, Long>>(numberMap.entrySet());

			Collections.sort(list_entries, new Comparator<Entry<String, Long>>() {
				// compare로 값을 비교
				public int compare(Entry<String, Long> obj1, Entry<String, Long> obj2) {
					// 내림 차순으로 정렬
					return obj2.getValue().compareTo(obj1.getValue());
				}
			});
		}

		return list_entries;
	}

}
