package com.example.demo.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Iservice.LoginIservice;
import com.example.demo.domain.UserVO;
import com.example.demo.repository.LoginRepository;

@Service
public class LoginService implements LoginIservice {

	@Autowired
	private LoginRepository loginRepository;
	
	@Override
	public UserVO getUser(String id) {

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("email", id);
		
		return loginRepository.getUser(param);
	}

	@Override
	public void updateLock(UserVO userVo) {
		int currentLockCnt = loginRepository.currentLockCnt(userVo);
		currentLockCnt ++;
		userVo.setLockCnt(currentLockCnt);
		loginRepository.updateLock(userVo);
		
	}

	@Override
	public int getCurrentLockCnt(UserVO userVo) {
		return loginRepository.getCurrentLockCnt(userVo);
	}

	@Override
	public UserVO getAlreadyAccount(String id) {
		return loginRepository.getAlreadyAccount(id);
	}
	
	
	
	
	
	
	
	

}
