package com.example.demo.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.example.demo.Iservice.BoardIService;
import com.example.demo.domain.BoardVO;
import com.example.demo.repository.BoardRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BoardService implements BoardIService {

	@Autowired
	private BoardRepository boardRepository;

	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Override
	public void insertBoard(Map<String, Object> param) {

		TransactionStatus txStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());

		try {
			boardRepository.mergeBoard(param);
			transactionManager.commit(txStatus);

		} catch (Exception e) {
			log.error("{}", this.getClass().toString());
			log.error("{}", e.toString());
			transactionManager.rollback(txStatus);
		}

	}

	@Override
	public BoardVO getBoard(String viewIndex) {
		return boardRepository.getBoard(viewIndex);
	}

	@Override
	public void deleteBoard(String viewIndex) {

		TransactionStatus txStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());
		try {
			boardRepository.deleteBoard(viewIndex);
			transactionManager.commit(txStatus);
		} catch (Exception e) {
			log.error("{]", this.getClass().toString());
			log.error("{]", e.toString());
			transactionManager.rollback(txStatus);
		}
	}

	@Override
	public Map<String, Object> getBoard(Map<String, Object> param) {

		Map<String, Object> resultMsg = new HashMap<String, Object>();
		resultMsg.put("boardList", boardRepository.getBoardList(param));
		resultMsg.put("boardCount", boardRepository.getCount(param));
		resultMsg.put("pagingNumber", param.get("pagingNumber"));

		if (boardRepository.getCount(param) > 100) {
			resultMsg.put("currentPageSet", param.get("currentPageSet"));
			resultMsg.put("endPageSet", boardRepository.getCount(param) / 100);

		}

		return resultMsg;
	}

}
