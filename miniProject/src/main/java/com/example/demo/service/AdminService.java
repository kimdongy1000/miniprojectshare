package com.example.demo.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.example.demo.Iservice.AdminIservice;
import com.example.demo.commonData.Pageging;
import com.example.demo.data.UserData;
import com.example.demo.domain.RoleVO;
import com.example.demo.domain.UserVO;
import com.example.demo.repository.AdminRepository;

@Service
public class AdminService implements AdminIservice {

	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	private SignService signService;

	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	

	@Override
	public List<UserVO> getUserList(UserData requestParam) {
		return adminRepository.getUserList(requestParam);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Boolean> updateEnabledUser(Map<String, String> requestParam) {

		TransactionStatus txStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());

		try {

			adminRepository.updateEnabledUser(requestParam);

			if (requestParam.get("enabled").equals("1")) {
				adminRepository.updateUserDetail(requestParam);
				
				Map<String, Object> param = new HashMap<>();
				param.put("id", requestParam.get("id"));
				param.put("role_code", 2);
				signService.insertUserRole(param);
				signService.insertUserDetail(param);
			} else {
				adminRepository.deleteUserDetail(requestParam);
				adminRepository.deleteUserRole(requestParam);
			}

			return new ResponseEntity<Boolean>(true, HttpStatus.OK);
		} catch (Exception e) {
			transactionManager.rollback(txStatus);
			return new ResponseEntity<Boolean>(false, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public List<RoleVO> getRoleList() {
		return adminRepository.getRoleList();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseEntity<Boolean> updateUserRole(Map<String, String> requestParam) {

		TransactionStatus txStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());

		try {
			adminRepository.updateUserRole(requestParam);
			adminRepository.updateUserDetail(requestParam);
			return new ResponseEntity<Boolean>(true, HttpStatus.OK);
		} catch (Exception e) {
			transactionManager.rollback(txStatus);
			return new ResponseEntity<Boolean>(false, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<Pageging> getUserMemberCnt(UserData requestParam) {
		
		Pageging pageging = adminRepository.getUserMemberCnt(requestParam);
		pageging.setCurrentPageSet(requestParam.getCurrentPageSet());
		pageging.setEndPageSet(requestParam.getCurrentPageSet() + 1.0);
		pageging.setTotalPageSet(pageging.getTotalPage() / pageging.getPageForCount());
		if(requestParam.getPagingNumber() == 0 ) {
			requestParam.setPagingNumber(1);
		}
		pageging.setPagingNumber(requestParam.getPagingNumber());
		return new ResponseEntity<Pageging>(pageging , HttpStatus.OK);
	}
	
	

}
