package com.example.demo.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.example.demo.Iservice.SignInIservice;
import com.example.demo.data.ApprovedNumber;
import com.example.demo.data.RegisterUser;
import com.example.demo.properties.MailProperties;
import com.example.demo.repository.AppovedEmailRepo;
import com.example.demo.repository.UserRepository;
import com.example.demo.repository.UserRoleRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SignService implements SignInIservice {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AppovedEmailRepo appovedEmailRepo;
	
	@Autowired
	private UserRoleRepository userRoleRepository;

	@Autowired
	private MailProperties mailProperties;
	
	@Autowired
	private  DataSourceTransactionManager transactionManager;

	@Override
	public void registerUser(RegisterUser registerUser) {
		
		userRepository.registerUser(registerUser);
	}
	
	@Override
	public RegisterUser findByEmail(@Valid RegisterUser registerUser) {
		return userRepository.findByEmail(registerUser);
	}





	@Override
	public void saveAppovedEmail(ApprovedNumber approvedEntity) {
		 userRepository.saveAppovedEmail(approvedEntity);
	}

	@Override
	public RegisterUser findById(Long id) {
		return userRepository.findById(id);
	}

	@Override
	public ResponseEntity<Void> sendEmail(Map<String, String> requestParam, ApprovedNumber approvedEntity) {
		String host = mailProperties.getHost();
		String address = mailProperties.getAddress();
		String userName = mailProperties.getUserName();
		String password = mailProperties.getPassword();
		int port = mailProperties.getPort();

		Properties pro = System.getProperties();

		pro.put("mail.smtp.host", host);
		pro.put("mail.smtp.port", port);
		pro.put("mail.smtp.auth", "true");
		pro.put("mail.smtp.ssl.enable", "true");
		
		Session session  = null;

		try {
			session = Session.getDefaultInstance(pro, new javax.mail.Authenticator() {

				protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
					return new javax.mail.PasswordAuthentication(userName, password);
				}
			});

			String recipient = requestParam.get("email"); // 수신자 메일 아이디
			
			Random random = new Random();
			int approvedNumber = random.nextInt(10000000);
			String subject = "인증번호"; // 메일 제목
			String body = Integer.toString(approvedNumber); // 메일 내용

			Message mimeMessage = new MimeMessage(session); // MimeMessage 생성 MIME 메세지 타입을 말합니다 텍트스 또는 html 등등으로 보낼 수 있는
			mimeMessage.setFrom(new InternetAddress(address)); // 보내는 사람의 이메일 주소
			mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient)); // 받는 사람의 이메일 주소
			mimeMessage.setSubject(subject); // 제목
			mimeMessage.setText(body); // 내용
			Transport.send(mimeMessage); // 메일 보내기
			
			
			log.info("{} sendEmail success", this.getClass().getName());
			
			approvedEntity.setId(Long.parseLong(requestParam.get("id")));
			approvedEntity.setApprovedNumber(approvedNumber);
			
			saveAppovedEmail(approvedEntity);
			
			return new ResponseEntity<Void>(HttpStatus.OK);
		}catch (Exception e) {
			log.error("{} sendEmail Error" , this.getClass().getName());
			log.error("{}" , e.toString());
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ApprovedNumber findByApprovedNumber(Long id) {
		
		return appovedEmailRepo.findByApprovedNumber(id);
	}



	@Override
	public boolean existsEmail(String email) {
		return false;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateRegisterUser(RegisterUser registerUser) {
		userRepository.updateRegisterUser(registerUser);
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("id", registerUser.getId());
		param.put("role_code" , "2");
		
		TransactionStatus txStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());
		
		try {
			insertUserRole(param);
			insertUserDetail(param);
		}catch(Exception e) {
			log.error("{} insertError" , this.getClass().getName());
			transactionManager.rollback(txStatus);
		}
		
	
	}

	@Override
	public void insertUserRole(Map<String, Object> param) {
		
		userRoleRepository.insertinitUserRole(param);
	}

	@Override
	public void insertUserDetail(Map<String, Object> param) {
		
		userRepository.insertUserDetail(param);
	}

	@Override
	public void reSendApprovedEmailNumber() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ResponseEntity<Boolean> findpasswordApprovedNumber(Map<String, String> requestParam) {
		
		ApprovedNumber approvedNumber = findByApprovedNumber(Long.parseLong(requestParam.get("id")));
		
		if(Integer.toString(approvedNumber.getApprovedNumber()).equals(requestParam.get("approvedNumber"))) {
			return new ResponseEntity<Boolean>(true , HttpStatus.OK);
		}else {
			return new ResponseEntity<Boolean>(false , HttpStatus.OK);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
