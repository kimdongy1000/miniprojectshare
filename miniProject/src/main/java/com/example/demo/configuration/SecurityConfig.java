package com.example.demo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.example.demo.auth.AuthProvider;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private AuthProvider authprovider;
	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/logout").authenticated()
				.antMatchers("/main/*" , "/main/**" , "/main2/*" , "/main2/**" , "/user/**" , "/resume/**" , "/board/**")
				.access("hasRole('ROLE_MASTER') or hasRole('ROLE_BASIC')")
				.antMatchers("/admin/**")
				.access("hasRole('ROLE_MASTER')") // 배포용
				//.access("permitAll") // 작업용
				.antMatchers("/home/" , "/signIn/*" , "/error/500" , "/login/login" , "/logout" , "/test/")
				.access("permitAll")
		.and()	
			.formLogin()
			.loginPage("/login/")
			.loginProcessingUrl("/login/login")
			.defaultSuccessUrl("/main/")
			.usernameParameter("email")
			.passwordParameter("password")
		.and()
			.csrf();
		
		
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authprovider);
	}

}
