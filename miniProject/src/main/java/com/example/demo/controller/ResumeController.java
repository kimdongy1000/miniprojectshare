package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.example.demo.Iservice.ResumeIService;
import com.example.demo.auth.CommonAuth;
import com.example.demo.data.ResumeData;
import com.example.demo.domain.PjHistoryVO;
import com.example.demo.domain.ResumeComapnyHistoryVO;
import com.example.demo.domain.UserVO;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("resume")
@Slf4j
public class ResumeController {
	
	@Autowired
	private CommonAuth commAuth;
	
	@Autowired
	private ResumeIService resumeService;
	
	@GetMapping("/")
	public String goMyResumePage(Authentication auth , Model model) {
		
		log.info("{} goMyResumePage" , this.getClass().getName());
		
		auth = commAuth.getAuth();
		UserVO userVO = (UserVO)auth.getPrincipal();
		
		int existResume = resumeService.countMyResume(userVO);
		
		model.addAttribute("existResume" , existResume);
		
		return "/resume/resume";
	}
	
	@GetMapping("/modifyMyResume")
	public String gomodifyMyResume(Model model , Authentication auth) {
		
		log.info("{} gomodifyMyResume" , this.getClass().getName());
		
		auth = commAuth.getAuth();
		UserVO userVO = (UserVO)auth.getPrincipal();
		
		userVO = resumeService.getUserDetail(userVO.getId());
		
		int existResume = resumeService.countMyResume(userVO);

		model.addAttribute("existResume" , existResume);
		model.addAttribute("userBasicVO" , userVO);
		
		
		return "/resume/myResume";
		
	}
	
	@PostMapping("/insertResume")
	public String modifyPage(@ModelAttribute ResumeData resumeData , Authentication auth , Model model) {
		
		log.info("{} modifyPage" , this.getClass().getName());
		
		auth = commAuth.getAuth();
		UserVO userVO = (UserVO)auth.getPrincipal();
		
		try {
			resumeService.modifyResume(resumeData , userVO);
			return "redirect:/resume/modifyMyResume";
		} catch (Exception e) {
			log.error("{} modifyPage" , this.getClass().getName());
			log.error("{} modifyPage" , e.toString());
			return "redirect:/error/500";	
		}
		
		
	}
	
	@GetMapping("/getCompanyHistory")
	@ResponseBody
	public List<ResumeComapnyHistoryVO> getMyCompanyHistory(Authentication auth) {
		
		log.info("{} getMyCompanyHistory" , this.getClass().getName());
		
		auth = commAuth.getAuth();
		UserVO userVO = (UserVO)auth.getPrincipal();
	
		
		return resumeService.getCompanyHistory(userVO);
		
	}
	
	@GetMapping("/getPjHistory")
	@ResponseBody
	public List<PjHistoryVO> getMyPjHistory(Authentication auth){
		
		log.info("{} getMyPjHistory" , this.getClass().getName());
		
		auth = commAuth.getAuth();
		UserVO userVO = (UserVO)auth.getPrincipal();
		
		return resumeService.getPjHistory(userVO);
	}
	
	@GetMapping("/getPjDetail")
	@ResponseBody
	public String getPjDetail(Authentication auth) {
		
		auth = commAuth.getAuth();
		UserVO userVO = (UserVO)auth.getPrincipal();
		
		
		return resumeService.getPjDetail(userVO);
	}
	
	@GetMapping("/getIntroduce")
	@ResponseBody
	public String getIntroduce(Authentication auth) {
		
		auth = commAuth.getAuth();
		UserVO userVO = (UserVO)auth.getPrincipal();
		
		return resumeService.getIntroduce(userVO);
		
	}
	
	@GetMapping("/getAwardHistory")
	@ResponseBody
	public List<?> getAwardHistory(Authentication auth){
		
		auth = commAuth.getAuth();
		UserVO userVO = (UserVO)auth.getPrincipal();
		
		
		return resumeService.getAwardHistory(userVO);
	}
	
	@PostMapping("/insertImage")
	@ResponseBody
	public List<?> uploadImageFile(Authentication auth , @RequestParam("uploadImage") MultipartFile file) {
		
		auth = commAuth.getAuth();
		UserVO userVO = (UserVO)auth.getPrincipal();
		
		return resumeService.insertAndGetImage(userVO , file);
		
	}
	
	@GetMapping(value = "/getImage/{fileName}")
	public ResponseEntity<byte[]> getResumeImage(@PathVariable("fileName") String fileName , Authentication auth){
		
		Map<String, Object> param = new HashMap<String, Object>();
		
		auth = commAuth.getAuth();
		UserVO userVO = (UserVO)auth.getPrincipal();
		
		param.put("fileName", fileName);
		param.put("userVO", userVO);
		
		return resumeService.getResumeImage(param);
	}
	
	@PatchMapping("/patchingImgFile")
	public ResponseEntity<Boolean> deleteImageFile(Authentication auth , @RequestBody Map<String, Object> param){
		
		auth = commAuth.getAuth();
		UserVO userVO = (UserVO)auth.getPrincipal();
		
		param.put("id", userVO.getId());
		
		return resumeService.deleteImageFile(param);
	}
	
	@GetMapping("/getImage")
	@ResponseBody
	public List<?> resumeGetImage(Authentication auth){
		auth = commAuth.getAuth();
		UserVO userVO = (UserVO)auth.getPrincipal();
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("id", userVO.getId());
		
		return resumeService.getAllImage(param);
	}
	
	

}
