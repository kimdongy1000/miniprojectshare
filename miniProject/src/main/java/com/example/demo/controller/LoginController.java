package com.example.demo.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.data.RegisterUser;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("login")
@Slf4j
public class LoginController {

	@GetMapping("/")
	public String loginPage(Model model , HttpSession session) {
		
		if(session.getAttribute("sessionCheck") != null && (boolean)session.getAttribute("sessionCheck")) {
			
			return "redirect:/main/";
		}
		
		log.info("{} loginPage" , this.getClass().getName());
		
		
		model.addAttribute("registerUser", new RegisterUser());

		return "/login/login";
	}

}
