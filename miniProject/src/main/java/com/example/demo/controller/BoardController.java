package com.example.demo.controller;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.Iservice.BoardIService;
import com.example.demo.auth.CommonAuth;
import com.example.demo.data.BoardData;
import com.example.demo.domain.BoardVO;
import com.example.demo.domain.UserVO;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping("board")
public class BoardController {

	@Autowired
	private CommonAuth commAuth;

	@Autowired
	private BoardIService boardService;

	@GetMapping("/")
	public String goBoardPage(Authentication auth) {

		auth = commAuth.getAuth();
		UserVO uservo = (UserVO) auth.getPrincipal();

		return "/board/board";
	}

	@GetMapping("/writeBoard")
	public String goWritwBoardPage(Authentication auth) {

		auth = commAuth.getAuth();
		UserVO uservo = (UserVO) auth.getPrincipal();

		return "/board/writeBoard";
	}

	@PostMapping("/registerBoard")
	public ResponseEntity insertBoard(Authentication auth, @ModelAttribute BoardData boardData) {

		auth = commAuth.getAuth();
		UserVO uservo = (UserVO) auth.getPrincipal();

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("uservo", uservo);
		param.put("boardData", boardData);

		HttpHeaders responseHeaders = new HttpHeaders();

		try {
			boardService.insertBoard(param);

			responseHeaders.add("Content-Type", "text/html; charset=utf-8");
			String message = "<script>";
			message += " alert('새글 등록을 완료 했습니다.');";
			message += " location.href='/board/'; ";
			message += " </script>";

			return new ResponseEntity(message, responseHeaders, HttpStatus.OK);
		} catch (Exception e) {

			responseHeaders.add("Content-Type", "text/html; charset=utf-8");
			String message = "<script>";
			message += " alert('새글 등록에 실패했습니다 다시 시도해주세요.');";
			message += " location.href='/board/'; ";
			message += " </script>";

			return new ResponseEntity(message, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping("/view/{viewIndex}")
	public String viewBoard(Authentication auth, @PathVariable String viewIndex, Model model) {

		auth = commAuth.getAuth();
		UserVO uservo = (UserVO) auth.getPrincipal();

		BoardVO boardVO = boardService.getBoard(viewIndex);

		model.addAttribute("uservo", uservo);
		model.addAttribute("boardVO", boardVO);

		if (Long.toString(uservo.getId()).equals(boardVO.getId())) {
			model.addAttribute("mine", true);

		} else {
			model.addAttribute("mine", false);

		}

		return "/board/viewBoard";
	}

	@DeleteMapping("/delete/{viewIndex}")
	@ResponseBody
	public ResponseEntity<Boolean> deleteBoard(Authentication auth, @PathVariable String viewIndex) {

		auth = commAuth.getAuth();
		UserVO uservo = (UserVO) auth.getPrincipal();

		BoardVO boardVO = boardService.getBoard(viewIndex);

		if (!Long.toString(uservo.getId()).equals(boardVO.getId())) {

			return new ResponseEntity<Boolean>(HttpStatus.UNAUTHORIZED);

		}

		try {
			boardService.deleteBoard(viewIndex);
			return new ResponseEntity<Boolean>(HttpStatus.OK);
		} catch (Exception e) {
			log.error("{}", this.getClass().toString());
			log.error("{}", e.toString());
			return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/modifyPage/{viewIndex}")
	public String modifyPage(Authentication auth, @PathVariable String viewIndex, Model model,
			HttpServletResponse response) {

		auth = commAuth.getAuth();
		UserVO uservo = (UserVO) auth.getPrincipal();

		BoardVO boardVO = boardService.getBoard(viewIndex);

		try {

			if (!Long.toString(uservo.getId()).equals(boardVO.getId())) {
				response.setContentType("text/html; charset=UTF-8");
				PrintWriter out = response.getWriter();
				out.println("<script>alert('접근권한이 존재하지 않습니다.');</script>");
				out.flush();

				return "/board/board";

			}

		} catch (Exception e) {
			log.error("{}", this.getClass().toString());
			log.error("{}", e.toString());
		}

		model.addAttribute("boardVO", boardVO);

		return "/board/modifyView";

	}

	@PostMapping("/modify/{viewIndex}")
	public void modifyBoard(Authentication auth, @PathVariable String viewIndex, @ModelAttribute BoardData boardData,
			HttpServletResponse response) {

		auth = commAuth.getAuth();
		UserVO uservo = (UserVO) auth.getPrincipal();

		BoardVO boardVO = boardService.getBoard(viewIndex);

		if (!Long.toString(uservo.getId()).equals(boardVO.getId())) {
			try {
				response.setContentType("text/html; charset=UTF-8");
				PrintWriter out = response.getWriter();
				out.println(
						"<script>alert('글 수정이 권한이 존재하지 않습니다.'); location.href = '/board/view/" + viewIndex + "'</script>");
				out.flush();
			} catch (Exception e) {
				log.error("{}", this.getClass().toString());
				log.error("{}", e.toString());
			}
		}

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("boardId", boardVO.getBoardId());
		param.put("boardData", boardData);

		try {
			boardService.insertBoard(param);
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.println("<script>alert('글 수정이 완료 되었습니다.'); location.href = '/board/view/" + viewIndex + "'</script>");
			out.flush();

		} catch (Exception e) {
			log.error("{}", this.getClass().toString());
			log.error("{}", e.toString());

			try {
				response.setContentType("text/html; charset=UTF-8");
				PrintWriter out = response.getWriter();
				out.println(
						"<script>alert('글 수정이 완료 되었습니다.'); location.href = '/board/view/" + viewIndex + "'</script>");
				out.flush();
			} catch (Exception e1) {
				log.error("{}", e1.toString());
			}

		}
	}
	
	@PostMapping(value = "/getBoard" , consumes = "application/json")
	@ResponseBody
	public Map<String, Object> getBoardList(Authentication auth , @RequestBody Map<String, Object> param ){
		
		return boardService.getBoard(param);
		
		
	}
	

}
