package com.example.demo.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.auth.CommonAuth;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping("logout")
public class LogoutController {
	
	@Autowired
	private CommonAuth commonAuth;

	@GetMapping("/logout")
	public String logout(HttpServletRequest request, HttpServletResponse response) {

		log.info("{} logout" , this.getClass().getName() );
		
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		HttpSession session = request.getSession();

		Cookie myCookie = new Cookie("JSESSIONID", null);

		if (session != null) {
			session.invalidate();
		}

		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
			SecurityContextHolder.clearContext();
			commonAuth.setAuth(null);
		}

		return "redirect:/home/";
	}

}
