package com.example.demo.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.Iservice.UserIService;
import com.example.demo.auth.CommonAuth;
import com.example.demo.domain.UserVO;

@Controller
@RequestMapping("main")
public class MainController {
	
	@Autowired
	private CommonAuth commAuth;
	
	@Autowired
	private UserIService userService;
	
	
	@GetMapping("/")
	public String goMainPage(Authentication auth , Model model , HttpSession session) {
		
		commAuth.setAuth(SecurityContextHolder.getContext().getAuthentication());
		auth = commAuth.getAuth();

		session.setAttribute("sessionCheck", true);
		
		
		return "/main/main";
	}

}
