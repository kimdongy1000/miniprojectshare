package com.example.demo.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.example.demo.Iservice.SignInIservice;
import com.example.demo.auth.CommonAuth;
import com.example.demo.configuration.BcryptPassEncoder;
import com.example.demo.data.ApprovedNumber;
import com.example.demo.data.RegisterUser;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("signIn")
@Slf4j
@SessionAttributes("user")
public class SignInController {

	@Autowired
	private SignInIservice signInService;

	@Autowired
	private BcryptPassEncoder bcryrPassEncoder;

	@Autowired
	private CommonAuth commonAuth;

	@GetMapping("/")
	public String signInPage(Model model, HttpSession session) {

		if (session.getAttribute("sessionCheck") != null && (boolean) session.getAttribute("sessionCheck")) {

			return "redirect:/main/";
		}

		log.info("{} goSigInPage", this.getClass().getName());

		model.addAttribute(new RegisterUser());

		return "/sigin/sigin";

	}

	@PostMapping("/register")
	public String registerUser(@Valid RegisterUser registerUser, Errors errors, Model model,
			BindingResult bindingResult) {

		log.info("{} registerUser", this.getClass().getName());
		log.info("{} param", registerUser.toString());

		if (errors.hasErrors()) {

			return "/sigin/sigin";
		}

		if (!registerUser.getPassword().equals(registerUser.getConfirmPassword())) {
			FieldError fieldError = new FieldError("password", "confirmPassword", "암호가 일치하지 않습니다");
			bindingResult.addError(fieldError);
			return "/sigin/sigin";
		}

		registerUser.setUsername(registerUser.getEmail());
		registerUser.setEnabled("0");

		registerUser.setPassword(bcryrPassEncoder.encode(registerUser.getPassword()));

		try {
			signInService.registerUser(registerUser);
			RegisterUser user = signInService.findByEmail(registerUser);
			model.addAttribute("user", user);
		} catch (Exception e) {
			log.error("{}", e.toString());
			return "redirect:/error/500";
		}

		return "redirect:/signIn/appovedEmail";
	}

	@GetMapping("/appovedEmail")
	public String appovedEmail(Model model, HttpSession session) {

		if (session.getAttribute("sessionCheck") != null && (boolean) session.getAttribute("sessionCheck")) {

			return "redirect:/main/";
		}

		RegisterUser registerUser = (RegisterUser) session.getAttribute("user");
		model.addAttribute("user", registerUser);

		return "/sigin/appovedEmail";
	}

	@PostMapping(value = "/sendEmail", consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Void> sendEmail(@RequestBody Map<String, String> requestParam,
			ApprovedNumber approvedEntity) {

		log.info("{} sendEmail Start", this.getClass().getName());

		ResponseEntity<Void> responseEntity = signInService.sendEmail(requestParam, approvedEntity);
		return responseEntity;

	}

	@PostMapping(value = "/approvedNumber", consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Boolean> approvedNumber(@RequestBody Map<String, String> requestParam) {

		log.info("{} start approvedNumber", this.getClass().getName());
		String paramAppoved = requestParam.get("inputConfirmNumber");
		String saveApprovedNumber = Integer.toString(
				signInService.findByApprovedNumber(Long.parseLong(requestParam.get("id"))).getApprovedNumber());

		if (paramAppoved.equals(saveApprovedNumber)) {
			RegisterUser registerUser = signInService.findById(Long.parseLong(requestParam.get("id")));
			registerUser.setEnabled("1");
			signInService.updateRegisterUser(registerUser);

			return new ResponseEntity<>(true, HttpStatus.OK);
		}

		return new ResponseEntity<>(false, HttpStatus.OK);
	}

	@PostMapping(value = "/checkEmail", consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Boolean> checkEmail(@RequestBody Map<String, String> requestParam,
			RegisterUser registerUser) {

		registerUser.setEmail(requestParam.get("email"));

		RegisterUser existEmail = signInService.findByEmail(registerUser);

		if (existEmail != null) {

			return new ResponseEntity<Boolean>(false, HttpStatus.OK);
		}

		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}

	@PutMapping(value = "/reSendEmail", consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Void> resendEmail(@RequestBody Map<String, String> requestParam,
			ApprovedNumber approvedEntity) {

		log.info("{} start resendEmail", this.getClass().getName());

		return signInService.sendEmail(requestParam, approvedEntity);

	}

	@PostMapping(value = "/enabledResendMail", consumes = "application/json")
	@ResponseBody
	public ResponseEntity<RegisterUser> enabledResendMail(@RequestBody Map<String, String> requestParam,
			RegisterUser registerUser, ApprovedNumber approvedEntity) {

		log.info("{} start enabledResendMail", this.getClass().getName());
		log.info("{} requestParam", requestParam);

		String email = requestParam.get("email");

		registerUser.setEmail(email);
		RegisterUser user = signInService.findByEmail(registerUser);

		if (user == null) {
			return new ResponseEntity<RegisterUser>(user, HttpStatus.OK);
		} else {
			if (user.getEnabled().equals("1")) {
				return new ResponseEntity<RegisterUser>(user, HttpStatus.OK);
			}

			requestParam.put("id", Long.toString(user.getId()));
			try {
				signInService.sendEmail(requestParam, approvedEntity);
				return new ResponseEntity<RegisterUser>(user, HttpStatus.OK);
			} catch (Exception e) {
				log.error("{}", this.getClass().getName());
				log.error("{}", e.toString());
				return new ResponseEntity<RegisterUser>(user, HttpStatus.INTERNAL_SERVER_ERROR);
			}

		}

	}

	@PostMapping(value = "/findpasswordSendEmail" , consumes = "application/json")
	@ResponseBody
	public ResponseEntity<RegisterUser> findPasswordApprovedEmail(@RequestBody Map<String, String> requestParam , RegisterUser registerUser , ApprovedNumber approvedEntity){
		
		log.info("{} start findPasswordApprovedEmail" , this.getClass().getName());
		log.info("{} requestParam" , requestParam);
		
		String email = requestParam.get("email");
		registerUser.setEmail(email);
		RegisterUser user  = null;
		try {
			user = signInService.findByEmail(registerUser);
			if(user == null) {
				return new ResponseEntity<RegisterUser>(user,HttpStatus.OK);
			
			}else{
				requestParam.put("id", Long.toString(user.getId()));
				signInService.sendEmail(requestParam, approvedEntity);
				return new ResponseEntity<RegisterUser>(user, HttpStatus.OK);
			}
		}catch (Exception e) {
			return new ResponseEntity<RegisterUser>(user, HttpStatus.OK);
		}
		
	}
	
	@PostMapping("/findpasswordApprovedNumber")
	@ResponseBody
	public ResponseEntity<Boolean> findpasswordApprovedNumber(@RequestBody Map<String, String> requestParam){
		
		log.info("{} findpasswordApprovedNumber" , this.getClass().getName());
		log.info("{} requestParam" , requestParam);
		
		
		return signInService.findpasswordApprovedNumber(requestParam);
	}
}
		
		
