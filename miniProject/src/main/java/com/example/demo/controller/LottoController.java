package com.example.demo.controller;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.Iservice.LottoIService;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/lotto")
@Slf4j
public class LottoController {
	
	@Autowired
	private LottoIService lottoService;
	
	@GetMapping("/")
	public String goLottePage() {
		
		log.info("{}" , this.getClass().toString());
		
		return "/lotto/lotto";
	}
	
	@GetMapping("/getNumber")
	@ResponseBody
	public List<Entry<String, Long>> getNumber(){
		
		
		
		return lottoService.getNumber();
	}

}
