package com.example.demo.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("home")
@Slf4j
public class HomeController {
	
	@GetMapping("/")
	public String goHomePage(Model model , HttpSession session) {

		if(session.getAttribute("sessionCheck") != null && (boolean)session.getAttribute("sessionCheck")) {
			
			return "redirect:/main/";
		}
		
		log.info("{} goHomePage" , this.getClass().getName());
		
		return "/home/home";
	}
}
