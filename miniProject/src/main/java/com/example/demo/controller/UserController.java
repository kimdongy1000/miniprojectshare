package com.example.demo.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.Iservice.UserIService;
import com.example.demo.auth.CommonAuth;
import com.example.demo.data.UserDetailData;
import com.example.demo.domain.UserVO;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("user")
@Slf4j
public class UserController {
	
	@Autowired
	private CommonAuth commAuth;
	
	@Autowired
	private UserIService userService;
	
	@GetMapping("/detail")
	public String userDetailPage(Authentication auth , Model model) {
		
		auth = commAuth.getAuth();
		
		if(auth == null) {
			
			return "redirect:/main/";
		}

		UserVO userVO = (UserVO)auth.getPrincipal();
		Long Id = userVO.getId();
		
		userVO = userService.getUserDetail(Id);
		
		model.addAttribute("userVO", userVO);
		
		
		return "/user/userDetail";
	}
	
	@PatchMapping(value = "/updateUserDetail" , consumes = "application/json")
	public ResponseEntity<Void> updateUserDetail(@RequestBody UserDetailData userParam){
		
		log.info("{} startUpdateUserDetail" , this.getClass().getName());
		log.info("param ======>  {} " , userParam);
		
		UserVO userData = userService.getUserDetail(userParam.getId());
		
		ResponseEntity<Void> reuslt = userService.updateUserDetail(userData , userParam);
		
		
		return reuslt;
	}

}
