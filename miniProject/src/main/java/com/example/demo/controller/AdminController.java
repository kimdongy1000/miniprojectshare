package com.example.demo.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.Iservice.AdminIservice;
import com.example.demo.auth.CommonAuth;
import com.example.demo.commonData.Pageging;
import com.example.demo.data.UserData;
import com.example.demo.domain.RoleVO;
import com.example.demo.domain.UserVO;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("admin")
@Slf4j
public class AdminController {
	
	@Autowired
	private CommonAuth commAuth;
	
	@Autowired
	private AdminIservice adminService;
	
	@GetMapping("/")
	public String goAdminDashPage(Authentication auth ) {
		
		log.info("{} startDashBoard" , this.getClass().getName());
		
		
		/*배포용*/
		//commAuth.setAuth(SecurityContextHolder.getContext().getAuthentication());
		//auth = commAuth.getAuth();
		
		/*작업용*/
		//commAuth.setAuth(SecurityContextHolder.getContext().getAuthentication());
		//auth = commAuth.getAuth();
		
		return "/admin/adminDashPage";
	}
	
	
	@PostMapping(value = "/getMember" , consumes = "application/json")
	@ResponseBody
	public List<UserVO> getMember(@RequestBody UserData requestParam){
		
		log.info("{} startGetUser" , this.getClass().getName());
		log.info("{} param" , requestParam);
		
		List<UserVO> resultList = adminService.getUserList(requestParam);
		
		return resultList;
	}
	
	@PatchMapping(value = "/mngUser" , consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Boolean> mngEnabledUser(@RequestBody Map<String, String> requestParam){
		
		log.info("{} startMngUser" , this.getClass().getName());
		log.info("{} requestParam" , requestParam);
		
		ResponseEntity<Boolean> updateEnabledUser = adminService.updateEnabledUser(requestParam);
		
		return updateEnabledUser;
		
	}
	
	@GetMapping("/getRole")
	@ResponseBody
	public List<RoleVO> getRoleList(){
		
		log.info("{} getRoleList" , this.getClass().getName());
		
		return adminService.getRoleList();
	}
	
	@PatchMapping(value = "/userRoleUpdate" , consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Boolean> UpdateUserRole(@RequestBody Map<String, String> requestParam) {
		
		log.info("{} UpdateUserRole" , this.getClass().getName());
		log.info("{} requestParam" , requestParam);
		
		
		return adminService.updateUserRole(requestParam);
		
	}
	
	@PostMapping(value = "/getMemberCnt" , consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Pageging> getUserMemberCnt(@RequestBody UserData requestParam){
		
		log.info("{} start getUserMemberCnt" , this.getClass().getName());
		log.info("{} requestParam" , requestParam);
		
		
		return adminService.getUserMemberCnt(requestParam);
	}

}
