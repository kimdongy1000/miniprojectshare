package com.example.demo.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.Iservice.PasswdEncodingIService;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/passwdEncoding")
@Slf4j
public class PasswdEncodingController {
	
	@Autowired
	private PasswdEncodingIService passwdEncodingService;
	
	
	@GetMapping("/")
	public String goPasswdEncoding() {
		
		return "/passwd/passwdEncoding";
	}
	
	@PostMapping(value = "/encoding" , consumes = "application/json")
	@ResponseBody
	public ResponseEntity<String> encoding(@RequestBody Map<String, Object> param){
		
		log.info("{}" , this.getClass().toString());
		log.info("{} param" , param );
		
		
		return null;
		
	}

}
