package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("error")
public class ErrorController {
	
	@GetMapping("/500")
	public String errorPage500() {
		
		return "/error/500";
	}
	
	@GetMapping("/401")
	public String errorPage401() {
		
		return "/error/401";
	}
	
	@GetMapping("/test")
	public String test() {
		
		return "/test/test";
	}

}
