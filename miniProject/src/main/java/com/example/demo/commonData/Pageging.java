package com.example.demo.commonData;

import lombok.Data;

@Data
public class Pageging {
	
	private int limit;
	
	/*현재 보여질 페이지*/  
	private int offset; 
	
	/* 현재 페이지 set*/
	private double currentPageSet;
	
	/* 끝에 표기될 페이지 set*/
	private double endPageSet;
	
	/*한페이지당 표기해야 할 데이터 양*/
	private final double pageForCount = 10.0;

	/*전체 데이터 개수*/	
	private double totalPage;
	
	/*전체 페이지 set*/
	private double totalPageSet;
	
	private int pagingNumber;
	
	

}
