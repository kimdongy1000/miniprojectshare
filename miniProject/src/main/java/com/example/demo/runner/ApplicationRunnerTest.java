package com.example.demo.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

public class ApplicationRunnerTest implements org.springframework.boot.ApplicationRunner {
	
	@Autowired
	private ApplicationContext applicationContext;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		ClassPathResource resource = new ClassPathResource("/mapper/*.xml");
		System.out.println(resource.exists());
	}
	
	
	
	

}
