package com.example.demo.auth;


import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class CommonAuth {
	
	private Authentication auth;
	

	public Authentication getAuth() {
		return auth;
	}

	public void setAuth(Authentication auth) {
		this.auth = auth;
	}
	
	

}
