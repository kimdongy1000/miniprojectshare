package com.example.demo.auth;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.example.demo.Iservice.LoginIservice;
import com.example.demo.Iservice.UserIService;
import com.example.demo.configuration.BcryptPassEncoder;
import com.example.demo.domain.UserVO;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AuthProvider implements AuthenticationProvider {

	@Autowired
	private LoginIservice loginService;
	
	@Autowired
	private UserIService userService;

	@Autowired
	private BcryptPassEncoder bcryptPassEncoder;
	
	@Value("${limitLockCnt}")
	private int limitLockCnt;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		String id = authentication.getName();
		CharSequence pw = (CharSequence) authentication.getCredentials();

		UserVO userVo = loginService.getAlreadyAccount(id);
		
		if(userVo == null){
			throw new BadCredentialsException("not exists emailAccount");
		}

		boolean flag = bcryptPassEncoder.matches(pw, userVo.getPassword());
		
		if (!flag) {
			loginService.updateLock(userVo);
			userVo = null;
			throw new BadCredentialsException("not matches password");
			
		}
		
		if(userVo.getEnabled().equals("0")) {
			userVo = null;
			throw new BadCredentialsException("not approved email");
		}
		
		userVo = loginService.getUser(id);
		
		int currentLockCnt = loginService.getCurrentLockCnt(userVo);
		
		if(currentLockCnt >= limitLockCnt) {
			userVo = null;
			throw new BadCredentialsException("로그인 가능 회수를 초과 했습니다 관리자에게 문의하세요 ");
		}
		
		
		

		ArrayList<SimpleGrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(userVo.getRole_name()));
		
		//lockCnt 초기화 
		userService.updateLockCnt(userVo.getId());
		
		return new UsernamePasswordAuthenticationToken(userVo, null, authorities);
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
