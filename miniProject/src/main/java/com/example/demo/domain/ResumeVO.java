package com.example.demo.domain;

import lombok.Data;

@Data
public class ResumeVO {
	
	private int resumeid;
	private int id;
	private String companyHistoryCode;
	private String prjoectHistoryCode;
	private String prjoectDetailText;
	private String introduceText;
	private String awardsHistoryCode;

}
