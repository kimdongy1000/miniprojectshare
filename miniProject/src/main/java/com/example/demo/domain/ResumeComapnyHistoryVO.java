package com.example.demo.domain;

import lombok.Data;

@Data
public class ResumeComapnyHistoryVO {
	
	private String no;
	private String resumeId;
	private String no_index;
	private String companyHistoryCode;
	private String companyName;
	private String enterDate;
	private String endDate;
	private String lastposition;
	private String description;

}
