package com.example.demo.domain;

import lombok.Data;

@Data
public class RoleVO {
	
	public String role_code;
	public String role_name;

}
