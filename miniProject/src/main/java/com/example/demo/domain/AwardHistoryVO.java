package com.example.demo.domain;

import lombok.Data;

@Data
public class AwardHistoryVO {
	
	private String no;
	private String resumeId;
	private String id;
	private String no_index;
	private String awardsHistoryCode;
	private String acquisitionDate;
	private String awardName;


}
