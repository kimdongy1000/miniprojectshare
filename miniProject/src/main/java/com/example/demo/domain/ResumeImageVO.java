package com.example.demo.domain;

import lombok.Data;

@Data
public class ResumeImageVO {
	
	private String no;
	private String resumeId;
	private String id;
	private String filePath;
	private String fileName;
	private String originFileName;
	private String registerDate;
	private String expire;

}
