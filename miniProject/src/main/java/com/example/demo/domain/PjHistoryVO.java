package com.example.demo.domain;

import lombok.Data;

@Data
public class PjHistoryVO {
	
	private String no;
	private String resumeId;
	private String id;
	private String no_index;
	private String projectHistoryCode;
	private String pjName;
	private String enterDate;
	private String endDate;
	private String lastpostion;
	private String description;

}
