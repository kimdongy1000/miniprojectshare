package com.example.demo.domain;

import lombok.Data;

@Data
public class BoardVO {

	private String boardId;
	private String id;
	private String email;
	private String nickname;
	private String title;
	private String content;
	private String boardRegisterDate;
	private String views;
	private String expire;

}
