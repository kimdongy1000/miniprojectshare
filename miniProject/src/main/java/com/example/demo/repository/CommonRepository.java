package com.example.demo.repository;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface CommonRepository {
	
	public int getAutoIncrement(String tableName);

	public String getCommonCode(String codePreFix);


}
