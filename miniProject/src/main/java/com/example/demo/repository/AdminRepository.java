package com.example.demo.repository;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.example.demo.commonData.Pageging;
import com.example.demo.data.UserData;
import com.example.demo.domain.RoleVO;
import com.example.demo.domain.UserVO;

@Repository
@Mapper
public interface AdminRepository {

	public List<UserVO> getUserList(UserData requestParam);

	public void updateEnabledUser(Map<String, String> requestParam);
	
	public void deleteUserDetail(Map<String, String> requestParam);
	
	public void deleteUserRole(Map<String, String> requestParam);

	public void updateUserDetail(Map<String, String> requestParam);

	public List<RoleVO> getRoleList();

	public void updateUserRole(Map<String, String> requestParam);

	public Pageging getUserMemberCnt(UserData requestParam);

}
