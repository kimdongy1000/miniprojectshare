package com.example.demo.repository;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.UserVO;



@Mapper
@Repository
public interface LoginRepository  {

	public UserVO getUser(Map<String, Object> param);

	public int currentLockCnt(UserVO userVo);

	public int getCurrentLockCnt(UserVO userVo);

	public void updateLock(UserVO userVo);

	public UserVO getAlreadyAccount(String id);
	
}
