package com.example.demo.repository;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.example.demo.data.ResumeData;
import com.example.demo.domain.PjHistoryVO;
import com.example.demo.domain.ResumeComapnyHistoryVO;
import com.example.demo.domain.ResumeImageVO;
import com.example.demo.domain.ResumeVO;
import com.example.demo.domain.UserVO;

@Repository
@Mapper
public interface ResumeRepository {

	public UserVO getUserDetail(Long id);

	public void insertResume(Map<String, Object> param);

	public int countMyResumeCount(UserVO userVO);

	public int mergeCompanyHistory(Map<String, Object> param);

	public int existResume(UserVO userVO);

	public String getCompanyHistoryCode(Map<String, Object> param);

	
	
	public List<ResumeComapnyHistoryVO> getCompanyHistoryList(Map<String, Object> param);

	public void deleteCompanyHistroy(Map<String, Object> param);

	public ResumeVO getResume(Map<String, Object> param);

	public int mergePjHistory(Map<String, Object> param);

	public void deletePjHistory(Map<String, Object> param);

	public List<PjHistoryVO> getPjHistory(Map<String, Object> param);

	public int updatePjDetail(Map<String, Object> param);

	public int updateIntroduceText(Map<String, Object> param);

	public void deleteAwardHistory(Map<String, Object> param);

	public void mergeAwardHistory(Map<String, Object> param);

	public List<?> getAwardHistory(ResumeVO resumeVO);

	public int insertResumeImage(Map<String, Object> param);

	public List<ResumeImageVO> getImageFiles(ResumeVO resumeVO);

	public ResumeImageVO getFileImage(Map<String, Object> param);

	public void deleteImageFile(Map<String, Object> param);

	public List<?> getAllImage(Map<String, Object> param);


}
