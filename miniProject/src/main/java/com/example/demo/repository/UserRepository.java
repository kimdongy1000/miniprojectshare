package com.example.demo.repository;

import java.util.Map;

import javax.validation.Valid;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.example.demo.data.ApprovedNumber;
import com.example.demo.data.RegisterUser;
import com.example.demo.domain.UserVO;

@Mapper
@Repository
public interface UserRepository  {

	public void registerUser(RegisterUser registerUser);

	public RegisterUser findByEmail(@Valid RegisterUser registerUser);

	public void saveAppovedEmail(ApprovedNumber approvedEntity);

	public RegisterUser findById(Long id);

	public void updateRegisterUser(RegisterUser registerUser);

	public UserVO getUserDetail(Long id);

	public void insertUserDetail(Map<String, Object> param);

	public void updateUserDetail(UserVO userData);

	public void updateUserLockCnt(Long id);
	
	
	
	

}
