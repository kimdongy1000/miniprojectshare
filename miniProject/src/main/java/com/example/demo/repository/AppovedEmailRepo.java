package com.example.demo.repository;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.example.demo.data.ApprovedNumber;

@Repository
@Mapper
public interface AppovedEmailRepo {

	public ApprovedNumber findByApprovedNumber(Long id);

}
