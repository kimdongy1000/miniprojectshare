package com.example.demo.repository;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface LottoRepository {

	public Map<String, Long> getNumber();

}
