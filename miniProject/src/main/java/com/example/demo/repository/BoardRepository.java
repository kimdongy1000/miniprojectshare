package com.example.demo.repository;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.example.demo.domain.BoardVO;

@Repository
public interface BoardRepository {

	public void mergeBoard(Map<String, Object> param);

	public BoardVO getBoard(String viewIndex);

	public String getRegisterBoardId(String viewIndex);

	public void deleteBoard(String viewIndex);

	public List<?> getBoardList(Map<String, Object> param);

	public Integer getCount(Map<String, Object> param);

}
