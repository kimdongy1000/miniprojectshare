package com.example.demo.Iservice;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.example.demo.data.ApprovedNumber;
import com.example.demo.data.RegisterUser;

public interface SignInIservice {

	public void registerUser(RegisterUser registerUser);
	
	public RegisterUser findById(Long id);
	
	public ApprovedNumber findByApprovedNumber(Long id);
	
	public void saveAppovedEmail(ApprovedNumber approvedEntity);
	
	public ResponseEntity<Void> sendEmail( Map<String, String> requestParam , ApprovedNumber approvedEntity);

	public boolean existsEmail(String email);

	public RegisterUser findByEmail(@Valid RegisterUser registerUser);

	public void updateRegisterUser(RegisterUser registerUser);
	
	public void insertUserRole(Map<String, Object> param);
	
	public void insertUserDetail(Map<String, Object> param);
	
	public void reSendApprovedEmailNumber();

	public ResponseEntity<Boolean> findpasswordApprovedNumber(Map<String, String> requestParam);
	
	 
}
