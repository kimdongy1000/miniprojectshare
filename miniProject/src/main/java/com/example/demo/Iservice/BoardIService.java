package com.example.demo.Iservice;

import java.util.List;
import java.util.Map;

import com.example.demo.domain.BoardVO;

public interface BoardIService {

	public void insertBoard(Map<String, Object> param);

	public BoardVO getBoard(String viewIndex);

	public void deleteBoard(String viewIndex);

	public Map<String, Object> getBoard(Map<String, Object> param);


}
