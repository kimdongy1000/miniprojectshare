package com.example.demo.Iservice;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.data.ResumeData;
import com.example.demo.domain.PjHistoryVO;
import com.example.demo.domain.ResumeComapnyHistoryVO;
import com.example.demo.domain.UserVO;

public interface ResumeIService {

	public UserVO getUserDetail(Long id);

	public void modifyResume(ResumeData resumeData , UserVO userVO);

	public int countMyResume(UserVO userVO);
	
	public int mergeCompanyHistory(UserVO userVO , ResumeData resumeData);

	public List<ResumeComapnyHistoryVO> getCompanyHistory(UserVO userVO);
	
	public int mergePjHistory(UserVO userVO , ResumeData resumeData);

	public List<PjHistoryVO> getPjHistory(UserVO userVO);
	
	public int updatePjDetail(UserVO userVO , ResumeData resumeData);
	
	public int updateIntroduceText(UserVO userVO , ResumeData resumeData);

	public String getPjDetail(UserVO userVO);

	public String getIntroduce(UserVO userVO);
	
	public int mergeAwardsHistory(UserVO userVO , ResumeData resumeData);

	public List<?> getAwardHistory(UserVO userVO);

	public List<?> insertAndGetImage(UserVO userVO, MultipartFile file);

	public ResponseEntity<byte[]> getResumeImage(Map<String, Object> param);

	public ResponseEntity<Boolean> deleteImageFile(Map<String, Object> param);

	public List<?> getAllImage(Map<String, Object> param);
	
	
	

}
