package com.example.demo.Iservice;

import com.example.demo.domain.UserVO;


public interface LoginIservice {

	public UserVO getUser(String id);

	public void updateLock(UserVO userVo);

	public int getCurrentLockCnt(UserVO userVo);

	public UserVO getAlreadyAccount(String id);

}
