package com.example.demo.Iservice;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.example.demo.commonData.Pageging;
import com.example.demo.data.UserData;
import com.example.demo.domain.RoleVO;
import com.example.demo.domain.UserVO;

public interface AdminIservice {

	public List<UserVO> getUserList(UserData requestParam);

	public ResponseEntity<Boolean> updateEnabledUser(Map<String, String> requestParam);

	public List<RoleVO> getRoleList();

	public ResponseEntity<Boolean> updateUserRole(Map<String, String> requestParam);

	public ResponseEntity<Pageging> getUserMemberCnt(UserData requestParam);

}
