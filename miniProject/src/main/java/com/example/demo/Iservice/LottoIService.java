package com.example.demo.Iservice;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public interface LottoIService {

	public List<Entry<String, Long>> getNumber();

}
