package com.example.demo.Iservice;

import org.springframework.http.ResponseEntity;

import com.example.demo.data.UserDetailData;
import com.example.demo.domain.UserVO;

public interface UserIService {

	public UserVO getUserDetail(Long id);

	public ResponseEntity<Void> updateUserDetail(UserVO userData, UserDetailData userParam);

	public void updateLockCnt(Long id);

}
