package com.example.demo.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@ConfigurationProperties("my.mail")
@Component
@Data
public class MailProperties {
	
	private String host;
	private String address;
	private String userName;
	private String password;
	private String auth;
	private String sslEnable;
	private int port;


}
