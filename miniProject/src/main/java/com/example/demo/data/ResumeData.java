package com.example.demo.data;

import lombok.Data;

@Data
public class ResumeData {
	
	private String[] index;
	private String[] companyName;
	private String[] enterDate;
	private String[] endDate;
	private String[] position;
	private String[] descWork;
	
	private String[] pjIndex;
	private String[] pjName;
	private String[] pjEnterDate;
	private String[] pjEndDate;
	private String[] pjPosition;
	private String[] pjDesc;
	
	private String[] aw_index;
	private String[] awName;
	private String[] acquisitionDate;
			
			
	
	private String companyHistoryCode;
	private String pjHistoryCode;
	private int resumeId;
	
	
	private String prjoectDetailText;
	private String introduceText;
	
	private String awardsHistoryCode;

}

