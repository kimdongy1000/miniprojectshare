package com.example.demo.data;

import lombok.Data;

@Data
public class UserDetailData {
	
	private Long id;
	private String email;
	private String password;
	private String nickName;
	private int age;
	private String frontPostNumber;
	private String address1;
	private String address2;
	

}
