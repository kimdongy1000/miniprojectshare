package com.example.demo.data;

import com.example.demo.commonData.Pageging;

import lombok.Data;

@Data
public class UserData extends Pageging {
	
	private Long id;
	private String email;
	private String password;
	private String enabled;
	private String role_name;
	private String role_code;
	private int age;
	private String nickname;
	private String postNumber;
	private String address;
	private String address2;
	private int lockCnt;
	private String registerDate;
	private String updateDate;
	private String name;

}
