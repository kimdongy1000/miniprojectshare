package com.example.demo.data;

import lombok.Data;

@Data
public class BoardData {
	
	private String title;
	private String content;

}
