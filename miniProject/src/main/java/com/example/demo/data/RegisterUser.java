package com.example.demo.data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.ToString;

@Data
public class RegisterUser {
	
	@NotBlank(message = "email 은 빈칸일 수 없습니다")
	@NotNull(message = "email은 빈칸일 수 없습니다")
	@Email
	private String email;
	
	
	@NotBlank(message = "password 은 빈칸일 수 없습니다")
	@NotNull(message = "password 은 빈칸일 수 없습니다")
	@Size(min = 8 , message = "password 는 최소 8자리 이상입니다 ")
	private String password;
	
	@NotBlank(message = "confirmPassword 은 빈칸일 수 없습니다")
	@NotNull(message = "confirmPassword 은 빈칸일 수 없습니다")
	@Size(min = 8 , message = "confirmPassword 는 최소 8자리 이상입니다 ")
	private String confirmPassword;
	
	@NotBlank(message = "nickName 은 빈칸일 수 없습니다")
	@NotNull(message = "nickName 은 빈칸일 수 없습니다")
	private String nickName;
	
	private Long id;
	
	private String username;
	
	private String enabled;
	
	
	
	

}
