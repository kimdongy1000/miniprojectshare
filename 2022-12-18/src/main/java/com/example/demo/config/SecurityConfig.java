package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SecurityConfig {

    //권한 부여서버가 어디인지 인지
    private ClientRegistration clientRegistration(){

        return CommonOAuth2Provider.GITHUB // 권한 부여 서버로 GITHUB 를 제공
                                    .getBuilder("github")
                                    .clientId("965834bed8f4cd913228") // 아까 발급받은 클라이언트 아이디
                                    .clientSecret("5871f3a281aeb688fde21cb52b3637b310f51775") // 클라이언트 시크릿 입력
                                    .build();
    }

    //발급 받은 권한 부여로 어떻게 사용자 인증을 할것인지
    @Bean
    public ClientRegistrationRepository clientRegistrationRepository(){

        ClientRegistrationRepository clientRegistrationRepository = new InMemoryClientRegistrationRepository(clientRegistration());

        return clientRegistrationRepository;

    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception
    {
        httpSecurity.oauth2Login(); // 이렇게 하면 이제 우리가 로그인 창을 만들 필요도 없고 시큐리티 기본 로그인 창 또한 필요 없게 된다 설정한 권한 발급용 로그인 페이지가 나오게 된다

        httpSecurity.authorizeRequests().anyRequest().authenticated(); // 어떤 요청도 권한 만 있으면 접근 가능

        return httpSecurity.build();

    }
}
