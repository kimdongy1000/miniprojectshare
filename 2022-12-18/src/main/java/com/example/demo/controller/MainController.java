package com.example.demo.controller;


import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("main")
@ResponseBody
public class MainController {

    @GetMapping("/")
    public String who_is_this(OAuth2AuthenticationToken oAuth2AuthenticationToken){


        return oAuth2AuthenticationToken.getPrincipal().toString();

    }
}
