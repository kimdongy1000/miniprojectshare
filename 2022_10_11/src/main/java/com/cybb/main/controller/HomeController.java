package com.cybb.main.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("home")
public class HomeController {

    @GetMapping("/welcomeSecurity")

    public String welcomeSecurity (Authentication authentication){

        System.out.println(authentication.getName());
        System.out.println(authentication.getPrincipal());

        return "welcomeSecurity";
    }

}
