package com.cybb.main.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("error")
public class ErrorController {

    @GetMapping("/401")
    public String errorPageOf401(){

        return "error_401";
    }

    @GetMapping("/402")
    public String errorPageOf402(){

        return "error_402";
    }
}
