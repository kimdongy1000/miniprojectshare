package com.cybb.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybb.main.dto.CreateUserDto;
import com.cybb.main.dto.ResponseDto;
import com.cybb.main.service.CreateUserService;

@RestController
@RequestMapping("create")
public class CreateUserContoller {
	
	@Autowired
	private CreateUserService createUserService;
	
	
	@PostMapping
	public ResponseEntity<?> createUser(@RequestBody CreateUserDto createUserDto){
		
		ResponseDto<CreateUserDto> responseDto = null;
		List<CreateUserDto> list = null;
		
		try {
			
			String userid = createUserService.createUser(createUserDto);
			
			return  null;
			
		}catch(Exception e) {
			
			System.out.println(e.getMessage());
			return null;
		}
		
		
		
	}
	

}
