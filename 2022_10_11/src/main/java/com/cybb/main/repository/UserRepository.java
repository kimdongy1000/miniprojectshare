package com.cybb.main.repository;

import com.cybb.main.dao.SecurityLoginUserModel;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class UserRepository {


    @Autowired
    private SqlSessionTemplate sql;


    public List<SecurityLoginUserModel> findByUsername(String username) {

        return sql.selectList(this.getClass().getName() + ".findByUsername" , username);
    }


    public void updateFailureLoginCount(String username) {

        sql.update(this.getClass().getName() + ".updateFailureLoginCount" , username);
    }

    public void updateSuccessCount(String username) {
        sql.update(this.getClass().getName() + ".updateSuccessCount" , username);
    }
}
