package com.cybb.main.repository;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cybb.main.dao.SecurityAuthoritesModel;
import com.cybb.main.dao.SecurityUserModel;
import com.cybb.main.dto.CreateUserDto;

@Repository
public class CreateUserRepository {
	
	@Autowired
	private SqlSessionTemplate sql;

	public int selectOneUsername(String username) {
		return sql.selectOne(this.getClass().getName() + ".selectOneUsername" , username);
	}

	public int insertOneUser(SecurityUserModel securityUserModel) {
		return sql.insert(this.getClass().getName() + ".insertOneUser" , securityUserModel);
	}

	public int insertOneAuthorites(List<SecurityAuthoritesModel> SecurityAuthoritesModels) {
		return sql.insert(this.getClass().getName() + ".insertOneAuthorites" , SecurityAuthoritesModels);
	}

}
