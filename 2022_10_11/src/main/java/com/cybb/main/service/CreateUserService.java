package com.cybb.main.service;

import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cybb.main.dao.SecurityAuthoritesModel;
import com.cybb.main.dao.SecurityUserModel;
import com.cybb.main.dto.CreateUserDto;
import com.cybb.main.repository.CreateUserRepository;

@Service
public class CreateUserService {
	
	@Autowired
	private CreateUserRepository createUserRepository;
	
	@Autowired
	private PasswordEncoder passwrodEncoder;

	@Transactional(rollbackFor = Exception.class)
	public String createUser(CreateUserDto createUserDto) throws Exception{
		
		SecurityUserModel securityUserModel = SecurityUserModel.builder().username(createUserDto.getUsername())
																		 .password(passwrodEncoder.encode(createUserDto.getPassword()))
																		 .build();
		
		
		// 유저 검색
		int isExistUsername = createUserRepository.selectOneUsername(securityUserModel.getUsername());
		if(isExistUsername > 0) {
			throw new RuntimeException("이미 존재하는 계정명입니다.");
		}

		// 유저 삽입
		int userCount = createUserRepository.insertOneUser(securityUserModel);
		if(userCount < 1) {
			throw new RuntimeException("계정 입력도중 문제가 발생했습니다.");
		}
		
		
		// 유저 권한 삽입
		List<SecurityAuthoritesModel> SecurityAuthoritesModels = new ArrayList<>();
		
		for(int i = 0; i < createUserDto.getAuthorites().size(); i++) {
			SecurityAuthoritesModel authoritesModel = SecurityAuthoritesModel.builder().userid(securityUserModel.getId())
																				       .authorites(createUserDto.getAuthorites().get(i))
																				       .build();
			
			SecurityAuthoritesModels.add(authoritesModel);
		}
		
		int authorites = createUserRepository.insertOneAuthorites(SecurityAuthoritesModels);
		if(authorites < 1) {
			throw new RuntimeException("계정 입력도중 문제가 발생했습니다.");
		}
		
		
		
		return securityUserModel.getId();
	}
	
	

}
