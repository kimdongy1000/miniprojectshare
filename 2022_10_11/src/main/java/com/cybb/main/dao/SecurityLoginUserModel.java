package com.cybb.main.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class SecurityLoginUserModel {

    private String id;
    private String username;
    private String password;
    private Integer count;
    private String authorites;



}
