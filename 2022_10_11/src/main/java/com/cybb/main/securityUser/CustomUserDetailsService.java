package com.cybb.main.securityUser;

import com.cybb.main.dao.SecurityLoginUserModel;
import com.cybb.main.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        List<SecurityLoginUserModel> securityLoginUserModels = userRepository.findByUsername(username);
        if(securityLoginUserModels.size() < 1){
            throw new UsernameNotFoundException("계정 또는 비밀번호가 존재 / 일치하지 않습니다.");
        }

        List<GrantedAuthority> array_grant = securityLoginUserModels.stream().map(
                item -> new SimpleGrantedAuthority(item.getAuthorites())
        ).collect(Collectors.toList());




        CustomUserDetail customUserDetail = new CustomUserDetail(
                securityLoginUserModels.get(0).getUsername() ,
                securityLoginUserModels.get(0).getPassword() ,
                array_grant
        );

        return customUserDetail;
    }
}
