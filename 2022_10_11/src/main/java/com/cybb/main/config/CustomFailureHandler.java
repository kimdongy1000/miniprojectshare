package com.cybb.main.config;

import com.cybb.main.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomFailureHandler implements AuthenticationFailureHandler {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {

        String errorMessage = null;
        if(exception instanceof UsernameNotFoundException){
            errorMessage = "아이디 비밀번호가 올바르지 않습니다.";
            response.sendRedirect("/error/401");
        }else if(exception instanceof BadCredentialsException){
            errorMessage = "아이디 비밀번호가 올바르지 않습니다.";

            String username = request.getParameter("username");

            userRepository.updateFailureLoginCount(username);




            response.sendRedirect("/error/401");
        }





    }
}
