package com.cybb.main.config;

import com.cybb.main.securityUser.CustomAuthProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SecurityConfig {

	@Autowired
	private CustomAuthProvider customAuthProvider;

	@Autowired
	private CustomSuccessHandler customSuccessHandler;

	@Autowired
	private CustomFailureHandler customFailureHandler;
	
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{
		
		http.formLogin().successHandler(customSuccessHandler).failureHandler(customFailureHandler); // 성공 핸들러 , 실패 핸들러 작성

		
		
		
		http.authorizeRequests().antMatchers("/create/**" , "/error/401").permitAll(); // 엔드포인트별 권한 부여


		http.authorizeRequests().anyRequest().authenticated();


		http.csrf().disable(); //postman 으로 아이디 만들시 csrf 보안 닫기


		http.authenticationProvider(customAuthProvider); // 커스텀 권한 부여 class
		
		
		return http.build();
		
	}

}
