package com.cybb.main.config;

import com.cybb.main.dao.SecurityLoginUserModel;
import com.cybb.main.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Component
public class CustomSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        List<SecurityLoginUserModel> securityLoginUserModel  = userRepository.findByUsername(authentication.getName());
        int count = securityLoginUserModel.get(0).getCount();

        if(count < 10){
            userRepository.updateSuccessCount(authentication.getName());
            response.sendRedirect("/home/welcomeSecurity");
        }else{
            response.sendRedirect("/error/402");
        }




    }


}
