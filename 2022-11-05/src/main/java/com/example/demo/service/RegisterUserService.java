package com.example.demo.service;

import com.example.demo.dto.RegisterUsernameDto;
import com.example.demo.repository.RegisterUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class RegisterUserService {

    @Autowired
    private RegisterUserRepository registerUserRepository;

    @Autowired
    private CommonService commonService;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public boolean isExistUsername(String username) {
       return  registerUserRepository.isExistsUsername(username);
    }

    @Transactional(rollbackFor = Exception.class)
    public void registerUsername(RegisterUsernameDto registerUsernameDto) {




        boolean emailFlag = commonService.regExpEmail(registerUsernameDto.getUsername());
        if(!emailFlag){
            throw new RuntimeException("이메일 형식이 올바르지 않습니다");
        }

        if(registerUsernameDto.getPassword1().length() > 9){
            if(!registerUsernameDto.getPassword1().equals(registerUsernameDto.getPassword2())){
                throw new RuntimeException("비밀번호가 일치하지 않습니다");
            }else{
                String username = registerUsernameDto.getUsername();
                String encodePassword = passwordEncoder.encode(registerUsernameDto.getPassword1());

                Map<String , Object> param = new HashMap<>();
                param.put("_uid" , UUID.randomUUID().toString());
                param.put("username" , username);
                param.put("encodePassword" , encodePassword);

                registerUserRepository.registerUsername(param);




            }

        }else{
            throw new RuntimeException("비밀번호는 9자리 이상이어야 합니다");

        }





    }


}
