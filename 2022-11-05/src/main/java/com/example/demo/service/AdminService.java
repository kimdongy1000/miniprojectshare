package com.example.demo.service;

import com.example.demo.dto.AuthoritiesDto;
import com.example.demo.dto.UsersDto;
import com.example.demo.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class AdminService {

    @Autowired
    private AdminRepository adminRepository;

    public List<UsersDto> getAllUsers() {

        return adminRepository.getAllUsers();
    }

    public List<AuthoritiesDto> getAllAuthorities() {

        return adminRepository.getAllAuthorities();
    }

    @Transactional(rollbackFor = Exception.class)
    public void updatedUsers(List<Map<String, Object>> updated) {

        adminRepository.updatedUsers(updated);



    }
}
