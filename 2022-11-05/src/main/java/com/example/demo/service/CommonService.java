package com.example.demo.service;

import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CommonService {

    public boolean regExpEmail(String email){

        boolean returnFlag = false;

        String regx = "^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$";
        Pattern p = Pattern.compile(regx);
        Matcher m = p.matcher(email);

        if(m.matches()){
            returnFlag = true;
        }



        return returnFlag;

    }
}
