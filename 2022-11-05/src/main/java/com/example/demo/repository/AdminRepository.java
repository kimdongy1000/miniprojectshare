package com.example.demo.repository;

import com.example.demo.dto.AuthoritiesDto;
import com.example.demo.dto.UsersDto;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class AdminRepository {

    @Autowired
    private SqlSessionTemplate sql;

    public List<UsersDto> getAllUsers() {

        return sql.selectList(this.getClass().getName() + ".getAllUsers");
    }

    public List<AuthoritiesDto> getAllAuthorities() {

        return sql.selectList(this.getClass().getName() + ".getAllAuthorities");
    }

    public void updatedUsers(List<Map<String, Object>> updated) {
        for (Map<String , Object> param: updated) {
            sql.update(this.getClass().getName() + ".updatedUsers" , param);
        }
    }
}
