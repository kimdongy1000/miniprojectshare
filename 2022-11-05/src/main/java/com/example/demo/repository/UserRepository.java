package com.example.demo.repository;

import com.example.demo.dto.UsersDto;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;

@Repository
public class UserRepository {

    @Autowired
    private SqlSessionTemplate sql;

    @Transactional(rollbackFor = Exception.class)
    public void initializationFailCount(UsersDto usersDto) {
        sql.update(this.getClass().getName() + ".initializationFailCount" , usersDto);
    }

    public UsersDto findByUsername(String username) {
         return  sql.selectOne(this.getClass().getName() + ".findByUsername" , username);
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateFailureCnt(String username) {
        sql.update(this.getClass().getName() + ".plusFailCount" , username);


    }

    @Transactional(rollbackFor = Exception.class)
    public void mergeLastLoginData(UsersDto usersDto) {
        sql.insert(this.getClass().getName() + ".mergeLastLoginData" , usersDto);
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateLastUpdateLogout(UsersDto usersDto) {
        sql.insert(this.getClass().getName() + ".mergeLastLogOutData" , usersDto);
    }
}
