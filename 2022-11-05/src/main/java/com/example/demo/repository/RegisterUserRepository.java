package com.example.demo.repository;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class RegisterUserRepository {

    @Autowired
    private SqlSessionTemplate sql;

    public boolean isExistsUsername(String username){

        int count = sql.selectOne(this.getClass().getName() + ".isExistsUsername" , username);
        if(count > 0){
            return true;
        }else {
            return false;
        }


    }


    public void registerUsername(Map<String, Object> param) {
        sql.insert(this.getClass().getName() + ".registerUsername" , param);




    }
}
