package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsersDto {

    private String id;
    private String username;
    private String password;
    private String useYn;
    private String insertDts;
    private String authorities;
    private int failcnt;






}
