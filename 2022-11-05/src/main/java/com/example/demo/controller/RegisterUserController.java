package com.example.demo.controller;


import com.example.demo.dto.RegisterUsernameDto;
import com.example.demo.dto.ResponseDto;
import com.example.demo.service.RegisterUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("register")
@Slf4j
public class RegisterUserController {

    @Autowired
    private RegisterUserService registerUserService;

    @GetMapping("/")
    public String registerPage(){

        return "/register/register";

    }

    @GetMapping("/isExist")
    public ResponseEntity<?> isExistUsername(@RequestParam("username") String username){

        ResponseDto<Boolean> responseDto = null;
        List<Boolean> list = null;

        try{
            boolean flagUsername = registerUserService.isExistUsername(username);
            list = new ArrayList<>();
            list.add(flagUsername);
            responseDto = ResponseDto.<Boolean>builder().list(list).build();

            return ResponseEntity.ok().body(responseDto);


        }catch(Exception e) {
            log.error(e.getMessage());
            responseDto = ResponseDto.<Boolean>builder().list(null).error(e.getMessage()).build();
            return ResponseEntity.internalServerError().body(responseDto);

        }
    }

    @PostMapping("/registerUsername")
    public ResponseEntity<?> registerUsername (@RequestBody RegisterUsernameDto registerUsernameDto){
        ResponseDto<Boolean> responseDto = null;
        List<Boolean> list = null;

        try{
            registerUserService.registerUsername(registerUsernameDto);
            list = new ArrayList<>();
            list.add(true);
            responseDto = ResponseDto.<Boolean>builder().list(list).build();

            return ResponseEntity.ok().body(responseDto);

        }catch (Exception e){
            log.error(e.getMessage());
            list = new ArrayList<>();
            list.add(false);
            responseDto = ResponseDto.<Boolean>builder().list(list).error(e.getMessage()).build();
            return ResponseEntity.internalServerError().body(responseDto);

        }


    }
}
