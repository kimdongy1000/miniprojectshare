package com.example.demo.controller;

import com.example.demo.dto.AuthoritiesDto;
import com.example.demo.dto.ResponseDto;
import com.example.demo.dto.UsersDto;
import com.example.demo.service.AdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("admin")
@Slf4j
public class AdminController {

    @Autowired
    private AdminService adminService;

    @GetMapping("/")
    public String goAdminPage(){

        return "/admin/admin";
    }

    @GetMapping("/getAllUsers")
    public ResponseEntity<?> getAllUsers() throws Exception{
        ResponseDto<Object> responseDto = null;
        List<Object> list = null;

        try{
            list = new ArrayList<>();

            List<UsersDto> userList = adminService.getAllUsers();
            List<AuthoritiesDto> authoritiesList =  adminService.getAllAuthorities();



            list.add(userList) ;
            list.add(authoritiesList) ;
            responseDto  = ResponseDto.<Object>builder().list(list).build();

            return ResponseEntity.ok().body(responseDto);

        }catch(Exception e){
            log.error(e.getMessage());
            responseDto = ResponseDto.<Object>builder().list(null).error(e.getMessage()).build();
            return ResponseEntity.internalServerError().body(responseDto);
        }

    }

    @PostMapping("/updateUsers")
    public ResponseEntity<?> updateUsers (@RequestBody Map<String , Object> requestParam) throws Exception {
        ResponseDto<String> responseDto = null;
        List<String> list = null;

        try{

            System.out.println(requestParam);
            List<Map<String , Object>> updated = (List<Map<String , Object>>) requestParam.get("Updated");

            adminService.updatedUsers(updated);



            return null;

        }catch(Exception e){
            log.error(e.getMessage());

            return null;

        }

    }

}
