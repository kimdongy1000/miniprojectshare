package com.example.demo.controller;

import com.example.demo.dto.BoardWriteDto;
import oracle.jdbc.proxy.annotation.Post;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("main/board")
public class BoardController {

    @GetMapping("/")
    @ResponseBody
    public String goBoardPage(){

        return "goBoardPaged";
    }

    @GetMapping("/read")
    @ResponseBody
    public String boardPageDetail(){


        return "특정 글읽기 테스트";

    }

    @PostMapping("/write")
    @ResponseBody
    public String writeBoard(){


        return "글쓰기 테스트";

    }

    @DeleteMapping("/delete")
    @ResponseBody
    public String deleteBoard(){


        return "글삭제 테스트";
    }


}
