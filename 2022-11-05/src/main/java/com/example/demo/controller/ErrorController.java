package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("error")
public class ErrorController {

    @GetMapping("/401")
    public String go_401Error(){

        return "/error/401_error";
    }

    @GetMapping("/402")
    public String go_402Error(){

        return "/error/402_error";
    }
}
