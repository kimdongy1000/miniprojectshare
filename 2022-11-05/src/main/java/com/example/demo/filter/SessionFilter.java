package com.example.demo.filter;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SessionFilter extends OncePerRequestFilter {


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        HttpSession httpSession = request.getSession();
        String url = request.getRequestURI();
        SecurityContextImpl securityContext = (SecurityContextImpl)httpSession.getAttribute("SPRING_SECURITY_CONTEXT");

        /**
         * "/" , "/register/**"  이 두 요청은 들어올시 세션이 있다면 다시 /main/ 으로 리다이렉트가 필요함
         *
         * */

        if(securityContext != null ){

            Pattern pattern = Pattern.compile("^/register/");

            Matcher matcher = pattern.matcher(url);

            if(url.equals("/") || matcher.find()){
                response.sendRedirect("/main/");
                filterChain.doFilter(request , response);
                return;
            }else{
                filterChain.doFilter(request , response);
                return;
            }



        }

        filterChain.doFilter(request , response);






    }
}
