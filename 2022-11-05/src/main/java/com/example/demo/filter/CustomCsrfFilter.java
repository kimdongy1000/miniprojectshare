package com.example.demo.filter;

import org.springframework.core.log.LogMessage;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.codec.Utf8;
import org.springframework.security.web.csrf.*;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.Assert;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.List;

public class CustomCsrfFilter extends OncePerRequestFilter {

    private final CsrfTokenRepository tokenRepository;

    private final String[] nonCsrfMethod = {"TRACE" , "HEAD" , "GET" , "OPTIONS"};

    public CustomCsrfFilter(CsrfTokenRepository csrfTokenRepository) {
        Assert.notNull(csrfTokenRepository, "csrfTokenRepository cannot be null");
        this.tokenRepository = csrfTokenRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String httpMethod = request.getMethod();

        for(int i = 0; i < nonCsrfMethod.length; i++){
            if(nonCsrfMethod[i].equals(httpMethod.toUpperCase())){
                filterChain.doFilter(request, response);
                return;
            }
        }


        CsrfToken csrfToken = this.tokenRepository.loadToken(request);
        if(csrfToken == null ){ // 토큰이 없을때
            goRootPage(request , response);
            return;
        }

        boolean missingToken = (csrfToken == null);

        String actualToken = request.getHeader(csrfToken.getHeaderName());
        if (actualToken == null) {
            actualToken = request.getParameter(csrfToken.getParameterName());
        }

        if (!equalsConstantTime(csrfToken.getToken(), actualToken)) {
            this.logger.debug(
                    LogMessage.of(() -> "Invalid CSRF token found for " + UrlUtils.buildFullRequestUrl(request)));
            AccessDeniedException exception = (!missingToken) ? new InvalidCsrfTokenException(csrfToken, actualToken)
                    : new MissingCsrfTokenException(actualToken);

            //토큰이 일치하지 않을때
            goRootPage(request , response);
            return;
        }
        filterChain.doFilter(request, response);

    }

    private static boolean equalsConstantTime(String expected, String actual) {
        if (expected == actual) {
            return true;
        }
        if (expected == null || actual == null) {
            return false;
        }
        // Encode after ensure that the string is not null
        byte[] expectedBytes = Utf8.encode(expected);
        byte[] actualBytes = Utf8.encode(actual);
        return MessageDigest.isEqual(expectedBytes, actualBytes);
    }

    private static void goRootPage(HttpServletRequest request, HttpServletResponse response) throws IOException{
        Cookie jsessionid_cookie = new Cookie("JSESSIONID" , null);
        jsessionid_cookie.setMaxAge(0);
        response.addCookie(jsessionid_cookie);

        response.sendRedirect("/");

    }

}
