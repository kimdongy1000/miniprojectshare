package com.example.demo.user;

import com.example.demo.dto.UsersDto;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UsersDto usersDto = userRepository.findByUsername(username);
        if(usersDto == null){
            throw new UsernameNotFoundException("일치하는 회원이 존재하지 않습니다");

        }else{
            List<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();

            simpleGrantedAuthorities.add(new SimpleGrantedAuthority(usersDto.getAuthorities()));

            return CustomUserDetail.builder()
                                    .username(usersDto.getUsername())
                                    .password(usersDto.getPassword())
                                    .authorities(simpleGrantedAuthorities)
                                    .build();

        }






    }
}
