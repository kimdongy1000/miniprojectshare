package com.example.demo.config;

import com.example.demo.filter.CustomCsrfFilter;
import com.example.demo.filter.SessionFilter;
import com.example.demo.user.CustomAuthProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.csrf.*;

@Configuration
public class SecurityConfig {

    @Autowired
    private CustomAuthProvider customAuthProvider;

    @Autowired
    private CustomLoginSuccessHandler customLoginSuccessHandler;

    @Autowired
    private CustomLoginFailureHandler customLoginFailureHandler;

    @Autowired
    private CustomLogoutSuccessHandler customLogoutSuccessHandler;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception
    {

        http.authorizeRequests().mvcMatchers("/" , "/register/**" , "/error/**").permitAll(); // 루트는 제외

        http.authorizeRequests().mvcMatchers("/admin/**").hasAuthority("Admin"); // 루트는 제외


        http.authorizeRequests().mvcMatchers("/main/").hasAnyAuthority("Admin" , "Normal");


        http.authorizeRequests().mvcMatchers("/main/board/").hasAnyAuthority("Admin" , "Normal");
        http.authorizeRequests().mvcMatchers("/main/board/read/").hasAnyAuthority("Admin" , "Normal");
        http.authorizeRequests().mvcMatchers("/main/board/write/").hasAuthority("Admin");
        http.authorizeRequests().mvcMatchers("/main/board/update/").hasAuthority("Admin");
        http.authorizeRequests().mvcMatchers("/main/board/delete/").hasAuthority("Admin");










        http.authorizeRequests().antMatchers("/resources/**" , "/favicon.ico").permitAll(); // 리소스 파일은 전역 권한 해제






        http.authorizeRequests().anyRequest().denyAll(); // 그 이외 요청은 일단 무시



       http.formLogin()   //form 로그인을 요청 "/" 로
               .loginPage("/")
               .loginProcessingUrl("/login")
               .usernameParameter("id")
               .passwordParameter("pw")
               .successHandler(customLoginSuccessHandler)
               .failureHandler(customLoginFailureHandler)
               .permitAll();

       http.logout()
               .logoutUrl("/logout")
               .logoutSuccessHandler(customLogoutSuccessHandler)
               .deleteCookies("JSESSIONID");




       http.httpBasic().disable(); //httpBasic 페이지 막고

       http.authenticationProvider(customAuthProvider);

       http.csrf().csrfTokenRepository(sessionCsrfRepository());

       http.addFilterAfter(new SessionFilter() , UsernamePasswordAuthenticationFilter.class);
       http.addFilterAt(new CustomCsrfFilter(sessionCsrfRepository()) , CsrfFilter.class);


        return http.build();
    }


    @Bean
    HttpSessionCsrfTokenRepository sessionCsrfRepository() {

        HttpSessionCsrfTokenRepository csrfRepository = new HttpSessionCsrfTokenRepository();
        csrfRepository.setHeaderName("X-CSRF-TOKEN");
        csrfRepository.setParameterName("_csrf");
        csrfRepository.setSessionAttributeName("CSRF_TOKEN");
        return csrfRepository;
    }





}
