package com.example.demo.config;

import com.example.demo.dto.UsersDto;
import com.example.demo.repository.UserRepository;
import com.example.demo.user.CustomUserDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomLoginSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        /*fali Count 0 시작*/
        String username = (String)authentication.getPrincipal();



        UsersDto usersDto = userRepository.findByUsername(username);
        if(usersDto.getFailcnt() > 5){
            goRootPage(request , response);
        }else{
            userRepository.initializationFailCount(usersDto);
            userRepository.mergeLastLoginData(usersDto);
            response.sendRedirect("/main/");
        }

    }


    private static void goRootPage(HttpServletRequest request, HttpServletResponse response) throws IOException{
        Cookie jsessionid_cookie = new Cookie("JSESSIONID" , null);
        jsessionid_cookie.setMaxAge(0);
        response.addCookie(jsessionid_cookie);

        response.sendRedirect("/error/402");

    }
}
