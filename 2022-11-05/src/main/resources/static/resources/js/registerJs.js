const floatingInputUsername = document.querySelector("#floatingInputUsername"); // 아이디 form
const alertRegisterUsername = document.querySelector("#alert-register-username"); // 아이디 알림form
const floatingPassword  = document.querySelector("#floatingPassword"); // 첫번째 비밀번호
const floatingConfirmPassword  = document.querySelector("#floatingConfirmPassword"); // 두번째 비밀번호
const alertRegisterPassword = document.querySelector("#alert-register-password"); // 비밀번호 알림판
const btnRegister = document.querySelector("#btnRegister") // 제출하기 버튼 클릭시
const tokenValue = document.querySelector("#token")

let idFlag = false;
let paFlag = false;

/*아이디 form onChange 할떄 */
floatingInputUsername.addEventListener('change' , (event) => {

    idFlag = false;

    const username = floatingInputUsername.value;


    if(username.length === 0){

        return null;
    }

    let regExpUsername =  checkVaildEmailCheck(username);
    if(regExpUsername === false){

        alertRegisterUsername.setAttribute("class" , "alert alert-danger");
        alertRegisterUsername.setAttribute("style" , "display:block;");
        alertRegisterUsername.innerText = "이메일 형식이 올바르지 않습니다.";


        return null;

    }


    let param = {
        "username" : username
    };

    let data = ApiGetCall("/register/isExist" , "GET" , param).then((result) => {

        if(result.error == null){
            let data = result.list

            let flag = data[0];

            if(flag === true){
                 alertRegisterUsername.setAttribute("class" , "alert alert-warning");
                 alertRegisterUsername.setAttribute("style" , "display:block;");
                 alertRegisterUsername.innerText = "이미 존재하는 아이디 입니다.";
            }else{
                 alertRegisterUsername.setAttribute("class" , "alert alert-primary");
                 alertRegisterUsername.setAttribute("style" , "display:block;");
                 alertRegisterUsername.innerText = "사용할 수 있는 아이디 입니다.";
                 idFlag = true;
            }
        }
    })
})

floatingInputUsername.addEventListener("keydown" , (event) => {

})



floatingInputUsername.addEventListener('keyup' , (event) => {

    alertRegisterUsername.setAttribute("style" , "display:none;");


});

/*첫번째 비빌번호 */
floatingPassword.addEventListener('change' , (event) => {

    let password1 = event.target.value;
     if(password1.length < 9){
        alert('비밀번호는 9자리 이상이어야 합니다');
        floatingConfirmPassword.value = "";
        floatingPassword.value = "";
     }
})

floatingPassword.addEventListener("keydown" , (event) => {

    alertRegisterPassword.setAttribute("style" , "display:none;");


})



/*두번째 비밀번호*/
floatingConfirmPassword.addEventListener("change" , (event) => {

    paFlag = false;

    let password1 = floatingPassword.value;
    let password2 = event.target.value;

    if(password1.length === 0){
        return;
    }

    if(password2.length === 0){
        return;
    }


    if(password1 !== password2){
         alertRegisterPassword.innerText = "비밀번호가 서로 다릅니다";
         alertRegisterPassword.setAttribute("class" , "alert alert-danger");
         alertRegisterPassword.setAttribute("style" , "display:block;");

         floatingPassword.value = "";
         floatingConfirmPassword.value = "";
    }else{


        alertRegisterPassword.innerText = "사용할 수 있는 비밀번호 입니다.";
        alertRegisterPassword.setAttribute("class" , "alert alert-primary");
        alertRegisterPassword.setAttribute("style" , "display:block;");

         paFlag = true;
    }
})

floatingConfirmPassword.addEventListener("keydown" , (event) => {

    alertRegisterPassword.setAttribute("style" , "display:none;");


})

btnRegister.addEventListener('click' , (event) => {


    if( !(idFlag && paFlag) ){
        alert("값을 올바르게 전부 입력해주세요");
        return
    }else{

        let username = floatingInputUsername.value;
        let password1 = floatingPassword.value;
        let password2 = floatingConfirmPassword.value;
        let token = tokenValue.value;

        let paramData = {
                "username" : username ,
                "password1" : password1 ,
                "password2" : password2 ,
                "token" : token
        }

        ApiPostCall("/register/registerUsername" , "POST" , paramData).then((data) => {
            let errorData = data.error;
            if(errorData != null){
                alert(errorData);
                return;
            }
            let returnData = data.list[0];
            if(returnData){
                if(confirm('회원가입이 완료 되었습니다 확인을 누르면 로그인 창으로 이동합니다.')){
                    location.href = "/";
                }
            }
        })

    }

})






