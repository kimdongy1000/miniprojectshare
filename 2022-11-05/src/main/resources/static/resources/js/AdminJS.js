const adminUsersTable = document.querySelector("#adminUsersTable");
const adminUsersTableBody = document.querySelector("#adminUsersTable tbody");
const buttonSave = document.querySelector("#button-save");
const buttonRefresh = document.querySelector("#button-refresh");
const adminSaveToken = document.querySelector("#adminSaveToken");

const param = {
    "Added" : [] ,
    "Updated" : [] ,
    "Deleted" : []
}





ApiGetCall("/admin/getAllUsers" , "GET").then( (data) =>{


    //debugger;
    let userList = data.list[0]; //user 가 담긴 테이블
    let authorities = data.list[1]; // 권한이 담겨 있는 리스트
    let useYnList = ["Y" , "N"]; // 사용여부


    let mapUserList = userList.map( (user , index) => {
        return (`<tr id = ${index}>`                                                            +
                    `<td>${user.id}</td>`                                                       +
                    `<td>${user.username}</td>`                                                 +
                    `<td>
                        <select class="form-select" id = "useYnSelectList">`                    +
                            useYnList.map( (useYnData) => {
                                return  (
                                            useYnData === `${user.useYn}` ? `<option value = ${useYnData} selected> ${useYnData} </option>` : `<option value = ${useYnData}> ${useYnData} </option>`
                                        )
                                })
                                                                                                +
                                `</select>
                                </td>`                                                          +
                    `<td>${user.insertDts}</td>`                                                +
                    `<td>
                         <select class="form-select" id = "authoritiesList">`                                          +
                            authorities.map( (authList) => {
                                return (
                                        authList.authorities === `${user.authorities}` ? `<option value = ${authList.authorities } selected>${authList.authorities }</option>` : `<option value = ${authList.authorities}>${authList.authorities}</option>`
                                )
                            })
                                                                                                +
                        `</select>
                    </td>`                                                                      +
                    `<td><input id = "failCnt" value = ${user.failcnt} type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"></td>`+

                "<tr>")
    })

    for(let i = 0; i < mapUserList.length; i++){
        adminUsersTableBody.insertAdjacentHTML('beforeend' , mapUserList[i]);
    }

//    useYnSelectDoc = document.querySelector("#useYnSelectList");
//
//    useYnSelectDoc.addEventListener('change' , (event) => {
//        console.log(event);
//    })
})

document.addEventListener('change',function(e){

    if(e.target && e.target.id === 'useYnSelectList'){

            let rowIndex = e.path[2].id;
            let tempTrId = document.getElementById(rowIndex);
            let htmlCollection = tempTrId.children;

            let uuid = htmlCollection[0].innerText
            let useYnValue = e.target.value;

            updatedUsers(uuid , "useYn" , useYnValue);


    }else if(e.target && e.target.id === 'authoritiesList'){

            let rowIndex = e.path[2].id;
            let tempTrId = document.getElementById(rowIndex);
            let htmlCollection = tempTrId.children;

            let uuid = htmlCollection[0].innerText
            let authoritiesValue = e.target.value;

            updatedUsers(uuid , "authorities" , authoritiesValue);

    }else if(e.target && e.target.id === 'failCnt'){

            let rowIndex = e.path[2].id;
            let tempTrId = document.getElementById(rowIndex);
            let htmlCollection = tempTrId.children;

            let uuid = htmlCollection[0].innerText
            let failCnt = e.target.value;

            updatedUsers(uuid , "failCnt" , failCnt);
     }

 });

 buttonSave.addEventListener('click' , (event) => {

    if(param.Updated.length === 0){
        alert('수정사항이 없습니다.');
        return;
    }

    if(confirm('변경사항 전체를 정말로 수정하시겠습니까?')){

        let url = "/admin/updateUsers";
        let method = "POST";
        param.token = adminSaveToken.value;
        data = param;

        ApiPostCall(url , method , data).then ( (data) => {
            console.log(data);
        })


    }

 })

 buttonRefresh.addEventListener('click' , (event) => {

    if(confirm('변경내역이 저장되지 않습니다 그래도 초기화 하시겠습니까?')){
        location.reload()
    }

 })


 function updatedUsers(uuid , key , value){

        let temp = null;
        let flag = true;

        let updatedArray = param.Updated;
        for(let i = 0; i < updatedArray.length; i++){
            let updatedUUID = updatedArray[i].uuid;
            if(uuid === updatedUUID){
                temp = updatedArray[i];
                temp[key] = value
                updatedArray[i] = temp;
                flag = false;
                break;
            }

        }


        if(flag){
            temp = {uuid : uuid ,
                    [key] : value
             }
            param.Updated.push(temp);
        }
 }








