const domain = "http://localhost:8080"
const logoutForm = document.querySelector("#logoutForm");
const logoutButton = document.querySelector("#logoutButton");

function ApiGetCall(url , method , data){

    let callUrl = null;

    if(data != null ){
        callUrl = `${domain}${url}/?` + new URLSearchParams(data);

    }else{
            callUrl = `${domain}${url}`;
    }

    let callMethod = method;

    return fetch(callUrl , {
        method :  callMethod
    }).then((res) => res.json())

}

function ApiPostCall(url , method , data){


    return fetch(url , {

        method : method ,
        headers : {
            "Content-type" : "application/json" ,
            "X-CSRF-TOKEN" : data.token != null ? data.token : null
        },
        body : JSON.stringify(data)
    }).then((res) => res.json())

}


/**
이메일 채크하는 로직
 */
function checkVaildEmailCheck(value){

	let regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
	let resultMsg = false;

	if(value.match(regExp)){
		resultMsg = true
	}

	return resultMsg;
}

/*쿠키 세팅*/
function setCookie(cookieName , value , expireDay){
    let exCookieDate = new Date();
    exCookieDate.setDate(exCookieDate.getDate() + expireDay);
    let cookieValue = `${escape(value)}; expires=${exCookieDate.toGMTString()}`;
    document.cookie = `${cookieName}=${cookieValue}`;
}

/*쿠키 삭제*/
function deleteCookie(cookieName){

    let exCookieDate = new Date();
    exCookieDate.setDate(exCookieDate.getDate() - 1);
    let cookieValue = `${escape(cookieName)}; expires=${exCookieDate.toGMTString()}`;

}




/*로그아웃 */
logoutButton.addEventListener('click' , (event) => {
    if(confirm('정말로 로그아웃 하시겠습니까?')){
        logoutForm.submit();
    }
})

