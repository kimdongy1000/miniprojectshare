const btnSignIn = document.querySelector("#btnSignIn"); // 로그인 버튼
const floatingInputEmail = document.querySelector("#floatingInputEmail"); // 로그인 이메일
const floatingPassword = document.querySelector("#floatingPassword") // 로그인 비밀번호
const loginForm = document.querySelector("#loginForm");
const alertUsername = document.querySelector("#alert-register-username");
const alertPassword = document.querySelector("#alert-register-password");
const rememberMe = document.querySelector("#rememberMe");




const getLoginCookie = document.cookie.split(';');
for(let i = 0; i < getLoginCookie.length; i++){

    let cookieKey = getLoginCookie[i].split('=');
    if(cookieKey[0].trim() === 'AmadeuslogoinId'){
        rememberMe.checked = true
         floatingInputEmail.value = cookieKey[1];

    }
}






btnSignIn.addEventListener('click' , (event) => {

    let email = floatingInputEmail.value;
    let password = floatingPassword.value;

    if(email.length <= 0){
        alertUsername.setAttribute("style" , "display:block;")
        alertUsername.innerText = "아이디를 입력해주세요";
        return;
    }else{
        alertUsername.setAttribute("style" , "display:none;")
    }

    if(password.length <= 0){
        alertPassword.setAttribute("style" , "display:block;")
        alertPassword.innerText = "비밀번호를 입력해주세요 ";
        return;
    }else{
        alertPassword.setAttribute("style" , "display:none;")
    }

    if(rememberMe.checked){
        setCookie("AmadeuslogoinId" , email , 30);
    }else{
        deleteCookie("AmadeuslogoinId");
    }

    loginForm.submit();

})