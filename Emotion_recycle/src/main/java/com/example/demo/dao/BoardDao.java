package com.example.demo.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.BoardDto;
import com.example.demo.dto.BoardReplayDto;
import com.example.demo.vo.BoardVO;
import com.example.demo.vo.ModifyVO;
import com.example.demo.vo.ReplayVO;

/*
 * author : kimdongy1000
 * date : 2020 08 11 
 * desc : board와 관련된 DB연결 부분 
 * 
 * */



@Repository
@Mapper
public interface BoardDao {
	
	public List<BoardDto> getBoard(BoardVO boardVO) throws Exception;
	public Integer getTotalBoard(BoardVO boardVo);
	public BoardDto getOneCOntent(String boardSeq);
	public void addReplay(ReplayVO replayVO);
	public List<BoardReplayDto> getBoardReplay(String boardSeq);
	public void countView(BoardDto boardDto);
	public Integer matchingPassword(Map<String, String> param);
	public void modifyContent(ModifyVO modifyVO);
	public void deleteContent(String boardSeq);
		

}
