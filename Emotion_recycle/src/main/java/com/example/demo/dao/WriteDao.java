package com.example.demo.dao;


import com.example.demo.vo.WriteVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;



/*
 * author : kimdongy1000 
 * date : 2020 08 07
 * desc : 글쓰기 DAO myBatis 와 연결된 부분
 * 
 * 
 * */

@Repository
@Mapper
public interface WriteDao {
	
	public void addContent(WriteVO writeVo);

}
