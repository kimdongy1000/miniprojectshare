package com.example.demo.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.WeekDto;

@Mapper
@Repository
public interface AdminDashBoardDao {

	public Integer getTodayBoardCount() throws Exception;

	public Integer getTodayBoardReplayCount() throws Exception;

	public WeekDto getMonth() throws Exception;

	public WeekDto getWeekBoard() throws Exception;

	public WeekDto getWeekReplay() throws Exception;

	public Integer getDeleteRatio() throws Exception;

}
