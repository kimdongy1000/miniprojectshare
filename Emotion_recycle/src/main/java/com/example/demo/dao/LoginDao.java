package com.example.demo.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.LoginDto;
import com.example.demo.vo.LoginVO;

/*
 * author : kimdongy1000
 * date : 2020 09 02 
 * desc : loginDao 정의
 * 
 * 
 * */


@Repository
@Mapper
public interface LoginDao {
	
	public Integer requestLogin(LoginVO loginVO) throws Exception;

	public void updateSecondarySecurity(Map<String, String> param) throws Exception;

	public LoginDto getSession(LoginVO loginVO) throws Exception;
		

}
