package com.example.demo.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;



/*
 * author : kimdongy1000
 * date : 2020 09 02
 * desc : 프러퍼티에 있는 정보를 자동으로 바인딩 해주는 도메인 클래스  
 * 
 * 
 * 
 * */


@ConfigurationProperties("mail")
@Component
public class MailProperties {
	
	private String host;
	private String address;
	private String userName;
	private String password;
	private String auth;
	private String salEnalbe;
	private int port;
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAuth() {
		return auth;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
	public String getSalEnalbe() {
		return salEnalbe;
	}
	public void setSalEnalbe(String salEnalbe) {
		this.salEnalbe = salEnalbe;
	}
	
	

	
	
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	@Override
	public String toString() {
		return "MailProperties [host=" + host + ", address=" + address + ", userName=" + userName + ", password="
				+ password + ", auth=" + auth + ", salEnalbe=" + salEnalbe + ", port=" + port + "]";
	}
	
	
	
	

}
