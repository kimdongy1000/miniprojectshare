package com.example.demo.configuration;

import com.example.demo.Interceptor.AdminInterceptor;
import com.example.demo.Interceptor.CommonInterceptor;
import com.example.demo.Interceptor.ModifyInterceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/*
 * author : kimdongy1000
 * date : 2020 08 07
 * desc : 생성한 commonInterceptor 인터셉터 등록
 * 
 * */

@Configuration
public class CommonConfigur implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        
    	registry.addInterceptor(new CommonInterceptor())
                .addPathPatterns("/**");
        
        registry.addInterceptor(new AdminInterceptor())
        		.addPathPatterns("/adminUser/**");
        
        registry.addInterceptor(new ModifyInterceptor())
        	    .addPathPatterns("/main/goModifyPage/**");

    }
    
    
}
