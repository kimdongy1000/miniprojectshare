package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.iService.WriteIService;
import com.example.demo.vo.WriteVO;

/*
 * author : kimdongy1000
 * date : 2020 08 04
 * desc 글작성과 관련된 핸들러를 작성합니다 
 * 
 * */

@Controller
@RequestMapping("/write")
public class WriteController {
	
	@Autowired
	private WriteIService writeSerivce;
	
	@GetMapping("/write")
	public String goWritePage() {
		
		return "/views/write";
	}
	
	@PostMapping("/addContent")
	@ResponseBody
	public void addContent(@ModelAttribute WriteVO writeVO , HttpServletRequest request) throws Exception{
		
		String ipAddress = request.getRemoteAddr();
		writeVO.setIp_address(ipAddress);
		
		
		 writeSerivce.addContent(writeVO);
		
	}

}
