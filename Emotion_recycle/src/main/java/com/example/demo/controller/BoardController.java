package com.example.demo.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.dto.BoardDto;
import com.example.demo.dto.BoardReplayDto;
import com.example.demo.iService.BoardIservice;
import com.example.demo.vo.BoardVO;
import com.example.demo.vo.ModifyVO;
import com.example.demo.vo.ReplayVO;



/*
*  Author : kimdongy1000
*  Date   : 2020 08 03
*  desc   : 게시판과 관련된 모든 핸들러를 처리함
* */


@Controller
public class BoardController {

	
	@Autowired
	private BoardIservice boardService;
	
	
    @GetMapping("/")
    public String indexPage(){

        return "/views/index";
    }
    
    @GetMapping("/main/getBoard")
    @ResponseBody
    public List<BoardDto> getBoard(@ModelAttribute BoardVO boardVO) throws Exception{
    	return boardService.getBoard(boardVO);
    }
    
    @GetMapping("/main/getTotalBoard")
    @ResponseBody
    public Integer getTotalBoard(@ModelAttribute BoardVO boardVO) throws Exception {
    	
    	return boardService.getTotalBoard(boardVO);
    	
    }
    
    @GetMapping("/main/getOneContent/{boardSeq}")
    public String getOneCOntent(@PathVariable String boardSeq , BoardDto boardDto ,  Model model) throws Exception {
    	
    	BoardDto oneContent = boardService.getOneContent(boardSeq); // 게시글을 불러오는 서비스
    	boardDto.setBoardSeq(boardSeq);
    	boardService.countView(boardDto); // 조회수 +1 해주는 서비스 
    	model.addAttribute("oneContent" , oneContent);
    	
    	
    	return "/views/contentView";
    }
    
    @PostMapping("/main/addReplay")
    @ResponseBody
    public void addReplay(@ModelAttribute ReplayVO replayVO) throws Exception {
    	boardService.addReplay(replayVO);
    }
    
    @GetMapping("/main/getBoardReplay/{boardSeq}")
    @ResponseBody
    public List<BoardReplayDto> getBoardReplay(@PathVariable String boardSeq) throws Exception{
    	
    	return boardService.getReplay(boardSeq);
    }
    
    @PostMapping("/main/passwordMatching")
    @ResponseBody
    public Integer matchingPassword(@RequestParam("password") String password , @RequestParam("boardSeq") String boardSeq , HttpServletRequest req) {
    	
    	Integer resultMSg = boardService.matching(password , boardSeq);
    	if(resultMSg == 1){
    		HttpSession session = req.getSession();
    		session.setAttribute("session", session.getId());
    	}
    	
    	return resultMSg;
    }
    
    
    @GetMapping("/main/goModifyPage/{boardSeq}")
    public String goModifyPage(@PathVariable String boardSeq , Model model) throws Exception {
    	
    	BoardDto boardDto = boardService.getOneContent(boardSeq);
    	model.addAttribute("modifyContent", boardDto);
    	
    	return "/views/modifyPage";
    }
    
    @PutMapping("/main/modifyContent/{boardSeq}")
    @ResponseBody
    public void modifyContent(@PathVariable String boardSeq , @ModelAttribute ModifyVO modifyVO) throws Exception {
    
    	modifyVO.setBoardSeq(boardSeq);
    	boardService.modifyContent(modifyVO);
    }
    
    @DeleteMapping("/main/deleteView/{boardSeq}")
    @ResponseBody
    public void deleteContent(@PathVariable String boardSeq) throws Exception {
    	
    	boardService.deleteContent(boardSeq);
    }
    
    
    
    
  
    
 
    
  
}
