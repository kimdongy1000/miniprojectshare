package com.example.demo.controller;

import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.dto.LoginDto;
import com.example.demo.iService.LoginIservice;
import com.example.demo.vo.LoginVO;


/*
*  Author : kimdongy1000
*  Date   : 2020 08 03
*  desc   : 관리자 로그인만 처리함 회원가입 은 없고 관리자는 직권으로 ID , PW 를 만들어 줄것임
* */



@Controller
@RequestMapping("/login")
public class LoginController {
	
	@Autowired
	private LoginIservice loginService;

    @GetMapping("/")
    public String loginPage() {

        return "/views/login";
    }

    @PostMapping("/")
    @ResponseBody
    public Integer adminLogin(@ModelAttribute LoginVO loginVO , HttpServletRequest req) throws Exception{
    	
    	HttpSession session = req.getSession();
    	Integer adminuser = loginService.requestLogin(loginVO);
    	if(loginVO.getSecondaryPassword() != null && !loginVO.getSecondaryPassword().equals("")) {
    		
    		LoginDto loginDto = loginService.getSession(loginVO);

    		if(loginDto != null) {
    			session.setAttribute("adminUser", loginDto);	
    			
    		}
    		
    	}
    	
    	
    	return adminuser;
    }
    
    @PostMapping("/secondarySecurity")
    @ResponseBody
    public void secondarySecurity() throws Exception{
    	
    	loginService.secondarySecurity();
    	
    }
    
    @GetMapping("/logout")
    public String logout(HttpServletRequest req) throws Exception{
    	HttpSession session = req.getSession();
    	session.setAttribute("adminUser", null);
    	session.invalidate();
    	

    	return "redirect:/login/";
    }
    
    @GetMapping("/getSession")
    @ResponseBody
    public boolean getSession(HttpServletRequest req) throws Exception {
    	HttpSession session = req.getSession();
    	LoginDto loginDto = (LoginDto)session.getAttribute("adminUser");
    	
    	if(loginDto != null) {
    		return true;
    	}
    	return false;
    	
    }
    


}
