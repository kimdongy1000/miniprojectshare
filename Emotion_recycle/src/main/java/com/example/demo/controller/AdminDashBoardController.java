package com.example.demo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.dto.TodayDashBoardDto;
import com.example.demo.dto.WeekDto;
import com.example.demo.iService.AdminDashBoardIservice;


/*
 * author : kimdongy1000
 * date : 2020 09 02
 * desc : 관리자 페이지 컨트롤러
 * 
 * */


@Controller
@RequestMapping("/adminUser")
public class AdminDashBoardController {
	
	@Autowired
	private AdminDashBoardIservice adminDashBoardSerivce;
	
	
	
	@GetMapping("/adminMain")
	public String adminUserPage() throws Exception {
		
		return "/views/adminUserVIew";
	}
	
	@GetMapping("/getToday")
	@ResponseBody
	public TodayDashBoardDto getToday(TodayDashBoardDto todayDashBoard) throws Exception {
	
		Integer getTodayBoardCount = adminDashBoardSerivce.getTodayBoardCount();
		Integer getTodayBoardReplyCount = adminDashBoardSerivce.getTodayBoardReplyCount();
		
		todayDashBoard.setBoard(getTodayBoardCount);
		todayDashBoard.setReplay(getTodayBoardReplyCount);
		
		return todayDashBoard;
	}
	
	@GetMapping("/getWeekBoard")
	@ResponseBody
	public Map<String, Object> getWeek(WeekDto weekDto) throws Exception {
		
		Map<String, Object> resultMsg = new HashMap<String, Object>();
		resultMsg.put("board", adminDashBoardSerivce.getWeekBoard());
		resultMsg.put("replay", adminDashBoardSerivce.getReplay());
		
		
		return resultMsg;
		
	}
	
	@GetMapping("/getMonth")
	@ResponseBody
	public WeekDto getMonth(WeekDto weekDto) throws Exception {
		
		return adminDashBoardSerivce.getMonth();
	}
	
	
	@GetMapping("/deleteRatio")
	@ResponseBody Integer deleteRatio() throws Exception {
		
		Integer ratio = adminDashBoardSerivce.getDeleteRatio();
		
		return ratio;
	}
	
	
	
	
	
}
