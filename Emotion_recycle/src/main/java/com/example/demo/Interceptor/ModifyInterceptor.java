package com.example.demo.Interceptor;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class ModifyInterceptor extends HandlerInterceptorAdapter {
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		HttpSession session = request.getSession();
		String sessionId = (String)session.getAttribute("session");
		if(session.getId() != sessionId || sessionId == null) {
			
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html; charset=UTF-8");

			
			PrintWriter out = response.getWriter();
			out.print("<script>alert('잘못된 접근입니다')</script>");
			out.print("<script> location.href = '/' </script>");
			out.flush();
			return false;
		}

		return true;
	}

}
