package com.example.demo.Interceptor;

import javax.servlet.http.HttpServletRequest;


import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.example.demo.dto.LoginDto;


/*
 * author : kimdongy1000
 * date : 2020 09 02
 * desc : 관리자 페에지 만을 위한 인터셉터 
 * 
 * 
 * 
 * */


public class AdminInterceptor extends HandlerInterceptorAdapter{

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		HttpSession session = request.getSession();
		LoginDto loginDto = (LoginDto)session.getAttribute("adminUser");
		if(loginDto == null) {
				response.sendRedirect("/login/");
				return false;
		}
		
		return true;
	}
	
	

}
