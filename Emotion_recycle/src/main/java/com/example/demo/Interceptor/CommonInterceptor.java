package com.example.demo.Interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/*
 * author : kimdongy1000
 * date : 2020 08 07
 * desc : common Interceptor 을 등록
 * 
 * 
 * */


public class CommonInterceptor extends HandlerInterceptorAdapter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());



    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.info("==================================");
    	logger.info("Call Handler" + handler.toString());
    	logger.info("==================================");
        return true;
    }
}
