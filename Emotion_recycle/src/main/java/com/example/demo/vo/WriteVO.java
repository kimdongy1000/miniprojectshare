package com.example.demo.vo;

import java.sql.Date;

/*
 * author : kimdongy1000
 * date : 2020 08 04
 * desc : 클라이언트에 넘어오는 글쓰기시 필요한 데이터 모음
 * 
 * */

public class WriteVO {
	
	private String author;
	private String password;
	private String subject;
	private String content;
	private Date regitDate;
	private Integer views;
	private String ip_address;
	
	
	
	
	public Integer getViews() {
		return views;
	}
	public void setViews(Integer views) {
		this.views = views;
	}
	public Date getRegitDate() {
		return regitDate;
	}
	public void setRegitDate(Date regitDate) {
		this.regitDate = regitDate;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getIp_address() {
		return ip_address;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}

	@Override
	public String toString() {
		return "WriteVO [author=" + author + ", password=" + password + ", subject=" + subject + ", content=" + content
				+ ", regitDate=" + regitDate + ", views=" + views + "]";
	}
	
	
	
	
	

}
