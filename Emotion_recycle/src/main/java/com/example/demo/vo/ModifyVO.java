package com.example.demo.vo;

public class ModifyVO {
	
	private String boardSeq;
	private String author;
	private String password;
	private String subject;
	private String content;
	
	public String getBoardSeq() {
		return boardSeq;
	}
	public void setBoardSeq(String boardSeq) {
		this.boardSeq = boardSeq;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "ModifyVO [boardSeq=" + boardSeq + ", author=" + author + ", password=" + password + ", subject="
				+ subject + ", content=" + content + "]";
	}
	
	
	
	

	
	

	
	
	
	
	
	
	
	
	

}
