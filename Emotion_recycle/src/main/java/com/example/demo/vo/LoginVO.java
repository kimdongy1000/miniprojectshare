package com.example.demo.vo;

public class LoginVO {
	
	private String email;
	private String password;
	private String secondaryPassword;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSecondaryPassword() {
		return secondaryPassword;
	}
	public void setSecondaryPassword(String secondaryPassword) {
		this.secondaryPassword = secondaryPassword;
	}
	
	
	
	

}
