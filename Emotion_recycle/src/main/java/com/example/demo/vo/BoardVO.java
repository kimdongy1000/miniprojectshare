package com.example.demo.vo;

public class BoardVO {

	private String author;
	private String title;
	private String search_data;
	private String countPerPage;
	private Integer selectPage;
	private Integer startPage;
	private Integer endPage;
	

	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSearch_data() {
		return search_data;
	}
	public void setSearch_data(String search_data) {
		this.search_data = search_data;
	}
	public String getCountPerPage() {
		return countPerPage;
	}
	public void setCountPerPage(String countPerPage) {
		this.countPerPage = countPerPage;
	}
	public Integer getSelectPage() {
		return selectPage;
	}
	public void setSelectPage(Integer selectPage) {
		this.selectPage = selectPage;
	}
	public Integer getStartPage() {
		return startPage;
	}
	public void setStartPage(Integer startPage) {
		this.startPage = startPage;
	}
	public Integer getEndPage() {
		return endPage;
	}
	public void setEndPage(Integer endPage) {
		this.endPage = endPage;
	}
	
	

	
	
	
	
	
}
