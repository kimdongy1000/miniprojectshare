package com.example.demo.vo;

public class ReplayVO {
	
	public String boardSeq;
	public String replayAuthor;
	public String replayPassword;
	public String replay;
	
	
	
	public String getBoardSeq() {
		return boardSeq;
	}
	public void setBoardSeq(String boardSeq) {
		this.boardSeq = boardSeq;
	}
	public String getReplayAuthor() {
		return replayAuthor;
	}
	public void setReplayAuthor(String replayAuthor) {
		this.replayAuthor = replayAuthor;
	}
	public String getReplayPassword() {
		return replayPassword;
	}
	public void setReplayPassword(String replayPassword) {
		this.replayPassword = replayPassword;
	}
	public String getReplay() {
		return replay;
	}
	public void setReplay(String replay) {
		this.replay = replay;
	}
}
