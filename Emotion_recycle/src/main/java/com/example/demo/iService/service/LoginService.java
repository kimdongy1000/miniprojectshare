package com.example.demo.iService.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.example.demo.dao.LoginDao;
import com.example.demo.dto.LoginDto;
import com.example.demo.iService.LoginIservice;
import com.example.demo.properties.MailProperties;
import com.example.demo.vo.LoginVO;

/*
 * author : kimdongy1000
 * date : 2020 08 28
 * desc : 로그인 요청을 하는 서비스 구현 
 * 
 * 
 * */

@Service
public class LoginService implements LoginIservice {

	@Autowired
	private LoginDao loginDao;
	@Autowired
	private WriteService writeSerivce;
	@Autowired
	private MailProperties mailProperties;

	@Value("${app.adminAccount}")
	private String adminUser;

	@Override
	public Integer requestLogin(LoginVO loginVO) throws Exception {
		String password = loginVO.getPassword();
		password = writeSerivce.encryptionPassword(password);
		loginVO.setPassword(password);
		
		if(loginVO.getSecondaryPassword() != null && !loginVO.getSecondaryPassword().equals("")) {
			String secondaryPassword = loginVO.getSecondaryPassword();
			secondaryPassword = writeSerivce.encryptionPassword(secondaryPassword);
			loginVO.setSecondaryPassword(secondaryPassword);
		}

		return loginDao.requestLogin(loginVO);

	}

	@Override
	public void secondarySecurity() throws Exception {
		Random random = new Random();
		int rndInt = random.nextInt(99999);
		
		rndInt = 11111;
		
		String secondarySecurity = Integer.toString(rndInt);
		secondarySecurity = writeSerivce.encryptionPassword(secondarySecurity);

		Map<String, String> param = new HashMap<String, String>();

		param.put("adminMail", adminUser);
		param.put("adminPassword2", secondarySecurity);

		loginDao.updateSecondarySecurity(param);
		//sendEmail(rndInt);
	}
	@Override
	public LoginDto getSession(LoginVO loginVO) throws Exception {
		
		return loginDao.getSession(loginVO) ;
	}

	public void sendEmail(int rndNum) throws Exception {

		String host = mailProperties.getHost();
		String address = mailProperties.getAddress();
		String userName = mailProperties.getUserName();
		String password = mailProperties.getPassword();
		int port = mailProperties.getPort();
		
		Properties pro = System.getProperties(); 
		
		pro.put("mail.smtp.host", host);
		pro.put("mail.smtp.port", port);
		pro.put("mail.smtp.auth", "true");
		pro.put("mail.smtp.ssl.enable", "true");
		
		Session session = Session.getDefaultInstance(pro, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, password);
			}
		});
		
	
		
		//������ ���� 
		String recipient = "kimdongy1000@naver.com";
		String subject = "2차 비밀번호 테스트입니다 ";
		String body =  "<h1>2차 비밀번호 입력해주세요 </h1>";
			   body += "<strong>" + rndNum + "</strong>";

		
		Message mimeMessage = new MimeMessage(session); 
		mimeMessage.setFrom(new InternetAddress(address));
		mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));  
		mimeMessage.setSubject(subject); //����
		mimeMessage.setContent(body , "text/html; charset=UTF-8");
		Transport.send(mimeMessage); 


		
		
		
	}

}
