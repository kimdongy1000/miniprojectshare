package com.example.demo.iService.service;

import java.security.MessageDigest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.WriteDao;
import com.example.demo.iService.WriteIService;
import com.example.demo.vo.WriteVO;



/*
 * author : kimdongy1000
 * date : 2020 08 05
 * desc : 글쓰기 서비스 클래스 
 * 
 * */


@Service
public class WriteService implements WriteIService {
	
	@Autowired
	private WriteDao writeDao;

	@Override
	public void addContent(WriteVO writeVO) throws Exception {
		
		String password = encryptionPassword(writeVO.getPassword());
		writeVO.setPassword(password);
		
		writeDao.addContent(writeVO);
		
	}
	
	//비밀번호를 SHA - 256 으로 인코딩 
	public String encryptionPassword(String password) {
		
		StringBuilder encodingPassword = null;
		
		
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(password.getBytes());
			byte[] byteData = md.digest();
			
			encodingPassword = new StringBuilder();
			for(int i = 0; i < byteData.length; i++) {
				encodingPassword.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
				
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return encodingPassword.toString();
		
	}
	
	
	

}
