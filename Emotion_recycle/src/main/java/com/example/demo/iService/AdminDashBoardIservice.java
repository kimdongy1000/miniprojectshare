package com.example.demo.iService;

import com.example.demo.dto.WeekDto;

public interface AdminDashBoardIservice {

	public Integer getTodayBoardCount() throws Exception;

	public Integer getTodayBoardReplyCount() throws Exception;

	public WeekDto getMonth() throws Exception;

	public WeekDto getWeekBoard() throws Exception;

	public WeekDto getReplay() throws Exception;

	public Integer getDeleteRatio() throws Exception;
	
	

}
