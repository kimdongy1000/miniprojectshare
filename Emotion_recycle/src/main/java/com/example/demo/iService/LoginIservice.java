package com.example.demo.iService;

import java.util.Map;
import java.util.Random;

import com.example.demo.dto.LoginDto;
import com.example.demo.vo.LoginVO;

/*
 * author : kimdongy1000
 * date : 2020 08 28
 * desc : 로그인 요청을 담당하는 서비스 
 * 
 * 
 * 
 * */

public interface LoginIservice {
	
	public Integer requestLogin(LoginVO loginVO) throws Exception;

	public void secondarySecurity() throws Exception;

	public LoginDto getSession(LoginVO loginVO) throws Exception;

}
