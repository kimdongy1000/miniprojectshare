package com.example.demo.iService.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.AdminDashBoardDao;
import com.example.demo.dto.WeekDto;
import com.example.demo.iService.AdminDashBoardIservice;

@Service
public class AdminDashBoardService implements AdminDashBoardIservice {

	@Autowired
	private AdminDashBoardDao adminDashBoardDao; 
	
	@Override
	public Integer getTodayBoardCount() throws Exception {
		return adminDashBoardDao.getTodayBoardCount();
	}

	@Override
	public Integer getTodayBoardReplyCount() throws Exception {
		return adminDashBoardDao.getTodayBoardReplayCount();
	}

	@Override
	public WeekDto getWeekBoard() throws Exception {
		return adminDashBoardDao.getWeekBoard();
	}
	
	@Override
	public WeekDto getReplay() throws Exception {
		return adminDashBoardDao.getWeekReplay();
	}

	@Override
	public WeekDto getMonth() throws Exception {
		 return adminDashBoardDao.getMonth();
	}

	@Override
	public Integer getDeleteRatio() throws Exception {
		  return adminDashBoardDao.getDeleteRatio();
	}
	
	
	
	


	
	
	
	
	

}
