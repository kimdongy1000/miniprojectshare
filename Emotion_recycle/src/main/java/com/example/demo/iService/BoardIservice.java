package com.example.demo.iService;

import java.util.List;
import java.util.Map;

import com.example.demo.dto.BoardDto;
import com.example.demo.dto.BoardReplayDto;
import com.example.demo.vo.BoardVO;
import com.example.demo.vo.ModifyVO;
import com.example.demo.vo.ReplayVO;

/*
 * author : kimdongy1000
 * date : 2020 08 11
 * desc : 메인 board 에 대한 서비스 인터페이스 
 * 
 * */

public interface BoardIservice {
	
	public List<BoardDto> getBoard(BoardVO boardVO) throws Exception;
	public Integer getTotalBoard(BoardVO boardVo);
	public BoardDto getOneContent(String boardSeq);
	public void addReplay(ReplayVO replayVO);
	public List<BoardReplayDto> getReplay(String boardSeq);
	public void countView(BoardDto boardDto);
	public Integer matching(String password, String boardSeq);
	public void modifyContent(ModifyVO modifyVO);
	public void deleteContent(String boardSeq);

}
