package com.example.demo.iService;

import com.example.demo.vo.WriteVO;

/*
 * author : kimdongy1000
 * date : 2020 08 05
 * desc : 글쓰기 인터페이스 
 * 
 * */
public interface WriteIService {
	
	public void addContent(WriteVO writeVO) throws Exception;

}
