package com.example.demo.iService.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.BoardDao;
import com.example.demo.dto.BoardDto;
import com.example.demo.dto.BoardReplayDto;
import com.example.demo.iService.BoardIservice;
import com.example.demo.vo.BoardVO;
import com.example.demo.vo.ModifyVO;
import com.example.demo.vo.ReplayVO;

/*
 * author : kimdongy1000
 * date : 2020 08 11
 * desc : 메인 board 에 대한 서비스 로직
 * 
 * 
 * */


@Service
public class BoardService implements BoardIservice {


	@Autowired
	private BoardDao boardDao;
	
	@Autowired
	private WriteService writeSerivce;
	
	
	@Override
	public List<BoardDto> getBoard(BoardVO boardVO) throws Exception {
		Integer startPage = boardVO.getStartPage();
		Integer endPage = boardVO.getEndPage();
		Integer selectPage = boardVO.getSelectPage();
		
		endPage = selectPage * 10;
		startPage = endPage - 9;
		
		boardVO.setStartPage(startPage);
		boardVO.setEndPage(endPage);
		
		return boardDao.getBoard(boardVO);
	}


	@Override
	public Integer getTotalBoard(BoardVO boardVO) {
		return boardDao.getTotalBoard(boardVO);
	}


	@Override
	public BoardDto getOneContent(String boardSeq) {
		return boardDao.getOneCOntent(boardSeq);
	}


	@Override
	public void addReplay(ReplayVO replayVO) {
		String password = writeSerivce.encryptionPassword(replayVO.getReplayPassword());
		replayVO.setReplayPassword(password);
		boardDao.addReplay(replayVO);
	}


	@Override
	public List<BoardReplayDto> getReplay(String boardSeq) {
		return boardDao.getBoardReplay(boardSeq);
	}


	@Override
	public void countView(BoardDto boardDto) {
		boardDao.countView(boardDto);
	}

	@Override
	public Integer matching(String password, String boardSeq) {
		
		password = writeSerivce.encryptionPassword(password);
		
		Map<String, String> param = new HashMap<String, String>();
		param.put("password", password);
		param.put("boardSeq", boardSeq);
		
		return boardDao.matchingPassword(param);
	}


	@Override
	public void modifyContent(ModifyVO modifyVO) {
		String password  = writeSerivce.encryptionPassword(modifyVO.getPassword());
		modifyVO.setPassword(password);
		boardDao.modifyContent(modifyVO);
	}


	@Override
	public void deleteContent(String boardSeq) {
		boardDao.deleteContent(boardSeq);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
