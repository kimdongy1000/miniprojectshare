package com.example.demo.dto;

import java.sql.Date;

/*
 * author : kimdongy1000
 * date : 2020 09 02
 * desc: 댓글 DTO
 * 
 * */

public class BoardReplayDto {
	
	private String replayNum;
	private String replayAuthor;
	private Date replayRegitDate;
	private String replay;
	
	
	
	public Date getReplayRegitDate() {
		return replayRegitDate;
	}
	public void setReplayRegitDate(Date replayRegitDate) {
		this.replayRegitDate = replayRegitDate;
	}
	public String getReplayNum() {
		return replayNum;
	}
	public void setReplayNum(String replayNum) {
		this.replayNum = replayNum;
	}
	public String getReplayAuthor() {
		return replayAuthor;
	}
	public void setReplayAuthor(String replayAuthor) {
		this.replayAuthor = replayAuthor;
	}
	public String getReplay() {
		return replay;
	}
	public void setReplay(String replay) {
		this.replay = replay;
	}
	@Override
	public String toString() {
		return "BoardReplayDto [replayNum=" + replayNum + ", replayAuthor=" + replayAuthor + ", replay=" + replay + "]";
	}
	
	
	
	
	

}
