package com.example.demo.dto;

import java.sql.Date;

/*
 * author : kimdongy1000
 * date : 2020 08 11
 * desc : db에서 넘어오는 컬럼들을 매핑을 시켜서 view로 뿌려줄 데이터입니다 
 * 
 * 
 * 
 * */


public class BoardDto {
	
	private String no; 
	private String boardSeq;
	private String author;
	private String title;
	private String content;
	private String regitDate;
	private Integer views;
	
	
	
	
	


	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getRegitDate() {
		return regitDate;
	}
	public void setRegitDate(String regitDate) {
		this.regitDate = regitDate;
	}
	public Integer getViews() {
		return views;
	}
	public void setViews(Integer views) {
		this.views = views;
	}
	public String getBoardSeq() {
		return boardSeq;
	}
	public void setBoardSeq(String boardSeq) {
		this.boardSeq = boardSeq;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "BoardDto [no=" + no + ", boardSeq=" + boardSeq + ", author=" + author + ", title=" + title
				+ ", content=" + content + ", regitDate=" + regitDate + ", views=" + views + "]";
	}
	
	
	
	

}
