package com.example.demo.dto;

public class WeekDto {
	
	private Integer mon;
	private Integer tue;
	private Integer wed;
	private Integer thu;
	private Integer fri;
	private Integer sat;
	private Integer sun;
	
	public Integer getMon() {
		return mon;
	}
	public void setMon(Integer mon) {
		this.mon = mon;
	}
	public Integer getTue() {
		return tue;
	}
	public void setTue(Integer tue) {
		this.tue = tue;
	}
	public Integer getWed() {
		return wed;
	}
	public void setWed(Integer wed) {
		this.wed = wed;
	}
	public Integer getThu() {
		return thu;
	}
	public void setThu(Integer thu) {
		this.thu = thu;
	}
	public Integer getFri() {
		return fri;
	}
	public void setFri(Integer fri) {
		this.fri = fri;
	}
	public Integer getSat() {
		return sat;
	}
	public void setSat(Integer sat) {
		this.sat = sat;
	}
	public Integer getSun() {
		return sun;
	}
	public void setSun(Integer sun) {
		this.sun = sun;
	}
	
	@Override
	public String toString() {
		return "WeekDto [mon=" + mon + ", tue=" + tue + ", wed=" + wed + ", thu=" + thu + ", fri=" + fri + ", sat="
				+ sat + ", sun=" + sun + "]";
	}
	
	
	
	
	

}
