package com.example.demo.dto;

import java.sql.Date;


/*
 * author : kimdongy1000
 * date : 2020 09 02
 * desc : db 에서 넘어오는 로그인 정보를 담는 Dto 
 * 
 * 
 * 
 * */


public class LoginDto {
	
	private String adminUserEmail;
	private Date lastLoginDate;
	
	

	public Date getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public String getAdminUserEmail() {
		return adminUserEmail;
	}

	public void setAdminUserEmail(String adminUserEmail) {
		this.adminUserEmail = adminUserEmail;
	}

	@Override
	public String toString() {
		return "LoginDto [adminUserEmail=" + adminUserEmail + ", lastLoginDate=" + lastLoginDate + "]";
	}
	
	
	
	
	
	


	
	

}
