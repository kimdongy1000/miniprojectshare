package com.example.demo.dto;

public class TodayDashBoardDto {
	
	private Integer board;
	private Integer replay;
	
	public Integer getBoard() {
		return board;
	}
	public void setBoard(Integer board) {
		this.board = board;
	}
	public Integer getReplay() {
		return replay;
	}
	public void setReplay(Integer replay) {
		this.replay = replay;
	}
	
	@Override
	public String toString() {
		return "TodayDashBoard [board=" + board + ", replay=" + replay + "]";
	}
	
	
	
	

}
