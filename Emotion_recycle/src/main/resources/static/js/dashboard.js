init();


function setDate(num){ // 날짜 차이를 반환을 함

	let today= new Date();
	
	let year = today.getUTCFullYear();
	let month = today.getUTCMonth() + 1;
	let day = today.getUTCDate() - num;

	if(day < 10){
		day = `0${day}`;
	}

	let date = `${year}${month}${day}`
	
	return date;
	

}


function setMonth(){

	let type = "GET";
	let url = `/adminUser/getWeekBoard`;
	let param = {}
	
	ajaxNet(type , url , param , function (data , status) {
	
		let keys = _keys(data.board);
		let board_values = _values(data.board);
		let reply_values = _values(data.replay);
				
		let myChart = $('#weekChard');
        
        let chart = new Chart(myChart, {
            data: {
                labels: keys,
                datasets: [
                	{
	                    label: '지난 7일간 게시글',
	                    type : 'line',         // 'line' type
	                    fill : false,         // 채우기 없음
	                    lineTension : 0,  // 0이면 꺾은선 그래프, 숫자가 높을수록 둥글해짐
	                    backgroundColor: 'rgb(051, 000 , 255)',
	                    borderColor: 'rgb(000, 255, 051)',
	                    data: board_values
                	},
                	{
	                    label: '지난 7일간 댓글',
	                    type : 'line',         // 'line' type
	                    fill : false,         // 채우기 없음
	                    lineTension : 0,  // 0이면 꺾은선 그래프, 숫자가 높을수록 둥글해짐
	                    backgroundColor: 'rgb(000, 000 , 000)',
	                    borderColor: 'rgb(255, 255, 051)',
	                    data: reply_values
                	}
                
                
                ]
            },
        
            // Configuration options
            options: {
                legend: {
                     labels: {
                          fontColor: 'black' // label color
                         }
                      },
                scales: {
                    // y축
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            fontColor:'black' // y축 폰트 color
                        }
                     }],
                     // x축
                     xAxes: [{
                         stacked: true,
                        ticks: {
                            fontColor:'black' // x축 폰트 color
                        }
                     }]
                }
            }
        });
	});
}

function setToday(){

	let type = "GET"
	let url = `/adminUser/getToday`;
	let param = {};
	
	ajaxNet(type , url , param , function (data , status) {
	
		let key = _keys(data);
		let value = _values(data);

		let date = setDate(0)
		let todayChart = $('#todayChard');
		
		let myChart = new Chart(todayChart, {
    				type: 'bar',
    				data: {
    				labels: [date],
        			datasets: [
        						{
            						label: '게시글' ,
            						data: [value[0]],
            						backgroundColor: [
                						'rgba(255, 99, 132, 0.2)',
            						],
            						borderColor: [
                						'rgba(255, 99, 132, 1)',
            						],
            						borderWidth: 1
        						},
        						{
            						label: '댓글' ,
            						data: [value[1]],
            						backgroundColor: [
                						'rgba(54, 162, 235, 0.2)',
            						],
            						borderColor: [
                						'rgba(54, 162, 235, 1)',
            						],
            						borderWidth: 1
        						},
        						]
    			},
    options: {
    	responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                	
                	}
            	}]
        	}
    	}
	});

});
	
	
	

}

function setDeleteRatio(){
	let type = "GET"
	let url = `/adminUser/deleteRatio`;
	let param = {
	
	};
	
	ajaxNet(type , url , param , function (data) {
		let chart = $('#deleteContent');
		
		let myChart = new Chart(chart, {
    				type: 'bar',
    				data: {
    				labels: ['삭제된 게시글'],
        			datasets: [
        						{
            						label: '삭제된 게시글' ,
            						data: [data],
            						backgroundColor: [
                						'rgba(255, 99, 132, 0.2)',
            						],
            						borderColor: [
                						'rgba(255, 99, 132, 1)',
            						],
            						borderWidth: 1
        						},
        						]
    			},
    options: {
    	responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                	
                	}
            	}]
        	}
    	}
	});
		
		
	})
	
	
}


function setChart(){

setToday();
setMonth();
setDeleteRatio();

}

function init(){

	setChart();
}