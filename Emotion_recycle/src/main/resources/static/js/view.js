init(); // init 함수 호출 	

//수정하기 버튼 삭제하기 버튼 눌렀을때 동작하는 switch 
let mode = 'modify';

//수정하기 버튼 누르고 비밀번호가 맞다면 수정하는 페이지로 이동하는 최종 함수
function goModifyPage(boardSeq){

	location.href = `/main/goModifyPage/${boardSeq}`
	
}

//수정하기 버튼을 눌렀을때 하는 행동
$('.modify').click(function () {
	$('#modiyPassword_password').val("");
	 $("#modifyModal").modal();
	 mode = 'modify';
})

//삭제하기 버튼을 눌렀을때 하는 행동
$('.delete').click(function () {
	$('#modiyPassword_password').val("");
	$("#modifyModal").modal();
	mode = 'delete';
})

//게시글 삭제하기
function deleteView(boardSeq){
	
	let type = "DELETE";
	let url = `/main/deleteView/${boardSeq}`
	let param = {}
	
	ajaxNet(type , url , param , function(data){
		alert('삭제가 완료 되었습니다');
		location.href = "/";
	})
}


//수정하기 , 삭제하기 할때 동시에 사용하는 비밀번호 대조 함수
$('.modifyPassword').click(function () {
	let password = $('#modiyPassword_password').val();
	
	if(!password){
		alert('비밀번호를 입력해주세요');
		return;
	}
	
	let boardSeq = $('#viewIndex').val();
	
	
	let type = "POST"
	let url = "/main/passwordMatching"
	let param = {
		"password" :password ,
		"boardSeq" : boardSeq
	}
	
	ajaxNet(type , url , param , function(data) {
		if(data = 1){
			switch(mode){
				case 'modify':
					goModifyPage(boardSeq);
					break;
				case 'delete':
					if(confirm('정말로 삭제하시겠습니까?')){
						deleteView(boardSeq);
					}	
					break;
			}
		}else{
			alert('비밀번호를 확인해주세요');
		}
	})
})

//댓글 등록하기 버튼을 눌렀을때 사용하는 함수 
$('.addReplayButton').click(function () {
	
	let author = $('#author').val();
	let password = $('#password').val();
	let replay = $('#replay').val();
	
	if(!author){
		alert('작성자를 입력해주세요');
		return;
	}
	
	if(!password){
		alert('비밀번호를 입력해주세요');
		return;
	}
	
	if(!replay){
		alert('댓글을 입력해주세요');
		return;
	}
	
	let type = "POST"
	let url = `/main/addReplay`
	let boardSeq = $('#viewIndex').val();
	
	let param = {
		'replayAuthor' : author,
		'replayPassword' : password,
		'replay' : replay,
		'boardSeq' : boardSeq
	}
	
	ajaxNet(type , url , param , function(data) {
		alert('등록완료');	
		getReplay();
	})
	
	
	
})

//댓글 보여주는 함수 
function getReplay(){
	
	let boardSeq = $('#viewIndex').val();
	
	let type = "GET"
	let url = `/main/getBoardReplay/${boardSeq}`;
	let param = {
	
	}
	
	ajaxNet(type , url , param , function(data) {
		gridReplay(data);
	})
	
}

//댓글 그리는 함수 
function gridReplay(data){
	
	let resultMsg = "";
	let keys = _keys(data[0]);
	
	_each(data , function(val) {
		resultMsg += `<div class="commentArea" style="border-bottom:1px solid darkgray; margin-bottom: 15px;">`;
		
		resultMsg += `<div style = "display:none">`
		resultMsg += `댓글 번호: ${val.replayNum}`
		resultMsg += `</div>`
		
		resultMsg += `<div>`
		resultMsg += `작성자: ${val.replayAuthor}`
		resultMsg += `</div>`
		
		resultMsg += `<div>`
		resultMsg += `댓글: ${val.replay}`
		resultMsg += `</div>`
		
		resultMsg += `</div>`
	})
	$(".commentList").html(resultMsg);
}

function init(){

	$('#viewIndex').hide();
	getReplay();

	$('#content').summernote({
		  height: 1000,                 // 에디터 높이
		  minHeight: null,             // 최소 높이
		  maxHeight: null,             // 최대 높이
		  focus: true,                  // 에디터 로딩후 포커스를 맞출지 여부
		  lang: "ko-KR",			    // 한글 설정
	});
	
	$('#content').summernote('disable'); // view 페이지에서는 비활성화 
}