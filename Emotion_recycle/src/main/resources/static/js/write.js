init(); // init 함수 호출 	

//목록으로 버튼을 클릭했을때 
$('.btn-list').click(function(){
	location.href = "/";
});

//작성하기 버튼 클릭했을때
$('.btn-submitt').click(function () {
	
	let author = $('#author').val();
	let password = $('#password').val();
	let subject = $('#subject').val();
	let content = $('#content').val();
	
	if(!author){
		alert('작성자를 입력해주세요');
		return;
	}
	
	if(!password){
		alert('비밀번호를 입력해주세요');
		return;
	}
	
	if(!subject){
		alert('제목을 입력해주세요');
		return;
	}
	
	if(!content){
		alert('내용을 입력해주세요');
		return;
	}
	
	let type = "POST";
	let url  = `/write/addContent`;
	let param = {
			"author"   : author,
			"password" : password,
			"subject"  : subject,
			"content"  : content
	};
	
	ajaxNet(type , url , param , function (data , status) {
		
		if(status === "success"){ // 글쓰기 요청이 성공을 하면 root 페이지를 요청을 함
			if(confirm("등록이 완료 되었습니다 확인 버튼을 누르면 메인페이지로 이동합니다")){
				location.href = "/"
			}
		}
		
	});
});


function init(){

	
	$('#author').keyup(function(){
		if($(this).val().length > $(this).attr('maxlength'))
			alert('작성자는 최대 10 자리를 넘길 수 없습니다');
	});
	
	$('#password').keyup(function(){
		if($(this).val().length >= $(this).attr('maxlength'))
			alert('비밀번호는 최대 20 자리를 넘을 수 없습니다');
	});
	
	$('#subject').keyup(function(){
		if($(this).val().length >= $(this).attr('maxlength'))
			alert('제목은 최대 100 자리를 넘길 수 없습니다');
	});
	
	$('#content').summernote({
		  height: 100,                 // 에디터 높이
		  minHeight: null,             // 최소 높이
		  maxHeight: null,             // 최대 높이
		  focus: true,                  // 에디터 로딩후 포커스를 맞출지 여부
		  lang: "ko-KR",			    // 한글 설정
		  
		  
          
	});
	

	
	
}

































