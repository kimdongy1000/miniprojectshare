function ajaxNet(type , url , param , callback){

	console.log(type , url , param);
	
	var resultMsg = null;
		
	$.ajax({
		type : type,
		url : url,
		data : param,

		success : function(data , textStatus , xhr) {
			return callback(data , textStatus);
			
		},	
		
		error : function(xhr, status, error) {
			alert('에러가 발생했습니다 잠시 뒤 이용 바랍니다');
			//return callback(status);
		}
	});
	
}

function _curryr(fn){
	return function (a , b){
		return arguments.length === 2 ? fn(a , b) : function (b) { return fn(b , a); };
	}
	
}

var _get = _curryr(function (obj , key) {
	
	return obj !== null ? obj[key] : undefined
});

function _is_object(obj){

	return typeof obj === "object" && !!obj;
}

function _keys(obj){

	return _is_object(obj) ? Object.keys(obj) : [];
}

function _values(obj){

	return _is_object(obj) ? Object.values(obj): [];
}

function _each(list , iter){

	var keys = _keys(list);

	for(var i = 0; i < list.length; i++){
		iter(list[i] , keys[i]);
	}
	return list;
}

function _map(list , mapper){
	var new_list = [];
	_each(list , function(val){
		new_list.push(mapper(val));
	})
	return new_list;
}

function numToString(num){

	return num.toString();
}

function stringToNum(str){

	return Number(str);
}

function _headOfNumber(num){

	return stringToNum(_head(numToString(num)));
}




function _head(str){
	
	return str[0];
}

function getSession(){

	let type = "GET";
	let url = `/login/getSession`
	let param = {
	
	}
	
	ajaxNet(type , url , param , function(data){
		if(data){
			$('.nav_logout').show();
			$('.nav_login').hide();
			$('.dashBoard').show();
		}else{
			$('.nav_logout').hide();
			$('.nav_login').show();
			$('.dashBoard').hide();
		}
	});
}

function init(){
	getSession();
}

init();





