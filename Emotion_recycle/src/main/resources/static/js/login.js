init();


function secondaryLogin() {
	$('#secondaryPassword').show();
	$("#inputEmail").attr("readonly",true);
	$("#inputPassword").attr("readonly",true);

}

function secondary_security(){

	let type = "POST";
	let url = `/login/secondarySecurity`;
	let param = {
	
	};
	
	ajaxNet(type , url , param , function(data){
		if(data !== 'error'){
			secondaryLogin();			
			
		}else{
			alert('잠시뒤 다시 이용해주시길 바랍니다');
		}
	});
}


$('.btn-login').click(function () {
	
	let email = $('#inputEmail').val();
	let password = $('#inputPassword').val();
	let secondaryPassword = $('#secondaryPassword').val();
	
	
	if(!email){
		alert('이메일을 입력해주세요');
		return;
	}
	
	if(!password){
		alert('비밀번호를 입력해주세요');
		return;
	}
	
	let type = "POST";
	let url = `/login/`
	
	let param = {
		"email" : email,
		"password" : password,
	}
	
	
	let idFlag = $('#inputEmail').is('[readonly]');
	let pwFlag = $('#inputPassword').is('[readonly]');
	
	if(idFlag && pwFlag){
		if(!secondaryPassword){
			alert('2차 비밀번호를 입력해주세요');
			return;
		}else{
			param['secondaryPassword'] = secondaryPassword;
		}
	}
	
	console.log(param);
	
	
	ajaxNet(type , url , param , function(data){
		if(idFlag && pwFlag){
			if(data === 1){
				location.href = "/adminUser/adminMain"
			}else{
				alert('2차 비밀번호를 확인해주세요');
				return;	
			}
		}else{
			if(data === 1){
				secondary_security();
			}else{
				alert('로그인 정보가 맞지 않습니다');
				return;
			}
		}
	
		

	});
	
})






function init(){

}