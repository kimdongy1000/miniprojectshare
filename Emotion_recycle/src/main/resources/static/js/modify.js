init();

$('.btn-cancel').click(function () {
	location.href = "/";
})

$('.btn-modify').click(function () {

	let author = $('#author').val();
	let password = $('#password').val();
	let subject = $('#subject').val();
	let content = $('#content').val();

	if(!author){
		alert('작성자를 입력해주세요');
		return;
	}
	
	if(!password){
		alert('비밀번호를 입력해주세요');
		return;
	}
	
	if(!subject){
		alert('제목을 입력해주세요');
		return;
	}
	
	if(!content){
		alert('내용을 입력해주세요');
		return;
	}
	
	let boardSeq = $('#boardSeq').val();
	
	let type = "PUT";
	let url = `/main/modifyContent/${boardSeq}`;
	let param = {
		"author" : author,
		"password" : password,
		"subject" : subject,
		"content" : content
	}
	
	ajaxNet(type , url , param , function (data , ststus) {
		if(ststus === 'error'){
			alert('잠시뒤 다시 이용해주세요')
		}else{
			alert('수정이 완료 되었습니다');
			location.href= `/main/getOneContent/${boardSeq}`;
		}
	})
	
})





function init () {

	$('#content').summernote({
			  height: 300,                 // 에디터 높이
			  minHeight: null,             // 최소 높이
		  	maxHeight: null,             // 최대 높이
		  	focus: true,                  // 에디터 로딩후 포커스를 맞출지 여부
		  	lang: "ko-KR",			    // 한글 설정
	});

}