//페이징 객체 
let paging = {
	pagingSet : 1
}	 

init();



//글작성 버튼을 클릭했을 시 
$('.boardBtn').click(function () {
	location.href = "/write/write";
})

$(document).on("click" , ".boardTable tbody tr" , function () {
	let tr = $(this);
	let td = tr.children();

	let arr = _map(td , function (val) { return val.outerText});
	let boardSeq = arr[1]; // 제일 위에를 클릭해도 문제가 발생함

	location.href = `/main/getOneContent/${boardSeq}`;

})





function goPage(page) { // 페이징 처리 
	initGetBoard(page);
}

//글의 총 개수를 기반으로 페이징 작성
function setPaging(count){  
	
	let pagePerCount = count / 10; //페이지당 글 표현 개수
	let pagingEnd = Math.ceil(pagePerCount) // 끝에나올 페이징 
	
	let start = 0; // 시작 페이지(페이징 중 제일 왼쪽에 오는 수) 


	if(paging.pagingSet === 1){ // 만약 처음에 페이지 세팅이 1이면 start 를  1로 줌
		 start = 1;
	}else{
		 start = 10 * paging.pagingSet - 9; // 그것이 아니라면 이 식으로 start 를 줌  
	}
	
	let end = 10 * paging.pagingSet // 페이징 중 제일 오른쪽에 오는 수 
	
	let pagingTotalSet = count / 10; // 전체 개수를 셋팅을 함 
	
	let pagingHtml = ""; // 페이징 그려질 객체
	
	pagingHtml += `<li class = "pre"><a href = "#" onclick = PreNextPage(-1)> < </li>` // < 표시 이때 안에 있는 함수는 페이징 객체 값을 변경을 함  
	
	// 페이징 그리는 로직 
	if(end <= pagingEnd){ // 제일 오른쪽에 그려질 숫자가 전체 크기보다 작을때 
		for(let i = start; i <= end; i++){
			pagingHtml += `<li class = "pagingNum"><a href = "#" onclick = selectPage(${i})>${i}</li>`
		}
	}else{
		for(let i = start; i <= pagingEnd; i++){ // 제일 오른쪽에 그려질 숫자가 전체 크기보다 작을떄
			pagingHtml += `<li class = "pagingNum"><a href = "#" onclick = selectPage(${i})>${i}</li>`
		}
	}
	pagingHtml += `<li class = "next" style="display:none"><a href = "#" onclick = PreNextPage(1)> > </li>`
	
	$('.pagination').html(pagingHtml);
	
	if(start === 1){ // start 가 1이면 pre 를 숨기고 
		$('.pre').hide();
	}
	
	if(pagingTotalSet > paging.pagingSet * 10){ // 
		$('.next').show();
	}
	
}

// > 버튼을 눌렀을때 전역으로 설정되어 있는 paging set 변경 
function PreNextPage(num) {
	paging.pagingSet += num;
	setPaging(getTotalBoard());

}

// < 버튼을 눌렀을때 전역으로 설정되어 있는 paging set 변경 
function selectPage(selectPage){
	initGetBoard(selectPage);
}

// 페이징 처리를 위한 전체 데이터 개수를 가지고 옴
function getTotalBoard(search_data , search_category){

	
	let type= "GET";
	let url = `/main/getTotalBoard`;
	
	let param = {
		"search_data" : search_data,
		
	}
	
	if(search_category === 'author'){
		param.author = search_category
	}
	
	if(search_category === 'title'){
		param.title = search_category
	}
	
	ajaxNet(type , url , param , function(data){
		setPaging(data);		
	});
}

//검색 버튼을 클릭했을시 
$('.search').click(function(){
	let search_category = $('.board-combobox').val();
	let search_data = $('#search_data').val();
	
	
	
	let type = "GET";
	let url = `/main/getBoard`;
	let param = {
		"search_data" : search_data,
		"selectPage"   : 1
		
	}
	
	if(search_category === 'author'){
		param.author = search_category
	}
	
	if(search_category === 'title'){
		param.title = search_category
	}
	
	ajaxNet(type , url , param , function(data){
		writeBoard(data);
		getTotalBoard(search_data , search_category)	
			
		
	});
	
})


// 넘어오는 데이터를 가지고 게시판을 그림
function writeBoard(list){  

	let keys = _keys(list[0]);
	let boardData = "";
	
		
	_each(list , function (li) {
		boardData += "<tr>"
		_each(keys , function (key) {
			
			let list_data = _get(key)(li);
			
			if(list_data !== null){
				if(key === 'boardSeq'){
					boardData += '<td style = "display:none;">'
					boardData += list_data
					boardData += "</td>"				
				}else{
					boardData += "<td>"
					boardData += list_data;
					boardData += "</td>"
				}
				
			}	
		})
		boardData += "</tr>"	
	})
	$('.boardTable tbody').html(boardData);	
}


//view 에 게시글 뿌리기 
function initGetBoard(selectPage){  
	
	let search_category = $('.board-combobox').val();
	let search_data = $('#search_data').val();


	if(!selectPage){  // 초기 1페이지를 세팅을 함
		selectPage = 1;
	}
	
	let type = "GET";
	let url = `/main/getBoard`;
	let param = {
		"selectPage"   : selectPage,  
		"search_data" : search_data
	}
	
	if(search_category === 'author'){
		param.author = search_category
	}
	
	if(search_category === 'title'){
		param.title = search_category
	}

	ajaxNet(type , url , param , function(data){
		if(data.length > 0){
			writeBoard(data);
		}
		getTotalBoard(search_data , search_category); 
	});
}


//페이지 첫 로딩시 init function 
function init(){
	initGetBoard();

}