DROP TABLE Replay;

CREATE SEQUENCE REPLAYNUM;

CREATE TABLE Replay(
	replayNum VARCHAR2(4000) PRIMARY KEY,
	boardSeq VARCHAR2(4000) not null,
	replayAuthor VARCHAR2(100) not null,
	replayPassword VARCHAR2(256) not null,
	replay VARCHAR2(4000) not null,
	replayRegitDate Date not null
);

ALTER TABLE Replay ADD CONSTRAINTS fk_boardSeq FOREIGN KEY(boardSeq) REFERENCES BOARD(boardSeq);

SELECT * FROM Replay

commit;