SELECT * FROM BOARD;

CREATE SEQUENCE BOARDNUM;

CREATE TABLE BOARD(
	boardSeq VARCHAR2(4000) PRIMARY KEY,
	author VARCHAR2(100) not null,
	password VARCHAR2(256) not null, 
	title VARCHAR2(100) not null,
	content CLOB not null,
	regitDate DATE not null,
	views NUMBER not null,
	EXPIRE VARCHAR2(1) not null ,
	ipAddr VARCHAR2(50) not null
);


commit;


SELECT BOARDNUM.CURRVAL FROM DUAL;
