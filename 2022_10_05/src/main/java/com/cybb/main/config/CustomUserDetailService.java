package com.cybb.main.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.cybb.main.dao.UserModel;
import com.cybb.main.repository.UserRepository;

@Component
public class CustomUserDetailService implements UserDetailsService{
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Optional<UserModel> optUserModel = userRepository.findByUsername(username);
		
		if(!optUserModel.isPresent()) {
			throw new RuntimeException("계정 또는 비밀번호가 존재/ 일치하지 않습니다");
		}
		
		UserModel userModel = optUserModel.get();
		
		List<GrantedAuthority> array_Authorities = new ArrayList<>();
		array_Authorities.add(new SimpleGrantedAuthority(userModel.getAuthorities()));
		
		
		
		return new CustomUserDetail(userModel.getUsername() , userModel.getPassword() , array_Authorities);
	}
	
	

}
