package com.cybb.main.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SecurityConfig {
	
	@Autowired
	private CustomAuthenticationProvider customAuthenticationProvider;
	
	
	
	@Bean
	public SecurityFilterChain configure(HttpSecurity http) throws Exception{
		
		http.formLogin();
		http.authorizeRequests().antMatchers("/greeting/hello").access("hasRole('ROLE_HELLO')");
		http.authorizeRequests().antMatchers("/greeting/goodBye").access("hasRole('ROLE_GoodBye')");
		http.authorizeRequests().antMatchers("/create/**").permitAll();
		
		
		http.csrf().disable();
		
		http.authenticationProvider(customAuthenticationProvider);
		
		return http.build();
	}

}
