package com.cybb.main.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybb.main.dto.ResponseDto;
import com.cybb.main.dto.UserDto;
import com.cybb.main.service.CreateService;

@RestController
@RequestMapping("create")
public class CreateRestController {
	
	@Autowired
	private CreateService createService;
	
	@PostMapping
	public ResponseEntity<?> createUser(@RequestBody UserDto userDto){
		
		ResponseDto<UserDto> responseDto = null;
		List<UserDto> list = null;
		
		try {
			
			UserDto returnUserDto = createService.createUser(userDto);
			list = new ArrayList<>();
			list.add(returnUserDto);
			
			responseDto = ResponseDto.<UserDto>builder().error(null).list(list).build();
			
			
			return ResponseEntity.ok().body(responseDto);
		}catch(Exception e) {
			responseDto = ResponseDto.<UserDto>builder().error(e.getMessage()).list(null).build();
			return ResponseEntity.badRequest().body(responseDto);
			
		}
	}

}
