package com.cybb.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJwtJapApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJwtJapApplication.class, args);
	}

}
