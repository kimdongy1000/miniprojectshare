package com.cybb.main.config;

import com.cybb.main.security.JwtAuthenticationFilter;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.WebSecurityConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.filter.CorsFilter;

@EnableWebSecurity
@Slf4j
public class WebSecurityConfig   {

    @Autowired
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    @Bean
    protected SecurityFilterChain  configure(HttpSecurity http) throws Exception {
            http.cors()
                .and()
                .csrf() //csrf 안씀
                .disable()
                .httpBasic() //토큰 방식임으로 안씀
                .disable()
                .sessionManagement() //세션방식이 아니라고 정책을 폄
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests() // 특정한 요청에서는 필터를 가로채게 만듬
                .antMatchers("/" , "/user/**")
                .permitAll()
                .anyRequest().authenticated()
                .and().addFilterAfter(jwtAuthenticationFilter , CorsFilter.class);
        return http.build();



    }




}
