package com.cybb.main.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("http://localhost:3000") // CORS 정책을 열어줄 크로스 도메인 root 주소
                .allowedMethods("GET" , "POST" , "PUT" , "PATCH" , "DELETE" , "OPTIONS") // 허락할 HTTP Method
                .allowedHeaders("*")
                .allowCredentials(true)
                .maxAge(3600);

    }
}
