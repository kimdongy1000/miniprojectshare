package com.cybb.main.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class ResponseDto<T> {

    private String error;
    private List<T> data;
}
