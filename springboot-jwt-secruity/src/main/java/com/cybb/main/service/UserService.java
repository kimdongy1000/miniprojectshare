package com.cybb.main.service;

import com.cybb.main.dto.UserDto;
import com.cybb.main.entity.UserEntity;
import com.cybb.main.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserRepository userRepository;


    @Transactional(rollbackFor = Exception.class)
    public UserEntity createUser(UserEntity user) {

        if(user.getEmail().equals("") || user == null){
            throw new RuntimeException("필수값이 없습니다.");
        }

        Optional<UserEntity> userEntity =  userRepository.findByEmail(user.getEmail());

        userEntity.ifPresent(item ->{
            throw new RuntimeException("이미 존재하는 이메일입니다.");
        });

        userRepository.save(user);

        return userRepository.findByEmail(user.getEmail()).get();

    }

    public UserEntity getCredentialUser( UserDto userDto) {

        if(userDto.getEmail() == null || userDto.getPassword() == null){
            throw new RuntimeException("아이디 또는 비밀번호가 없습니다.");
        }
        Optional<UserEntity> userEntity = userRepository.findByEmailAndPassword(userDto.getEmail() , userDto.getPassword());
        return userEntity.get();

    }
}
