package com.cybb.main.security;

import com.cybb.main.entity.UserEntity;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

@Component
@Slf4j
public class TokenController {

    private static final String SECRET_KEY = "StringBootTime";

    public String tokenCreate(UserEntity userEntity) {

        Date expireDate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(expireDate);
        cal.add(Calendar.DATE, 1);
        expireDate = cal.getTime();

        //토큰 만료시간 1일로 고정


        return Jwts.builder() // 토큰빌더
                .signWith(SignatureAlgorithm.HS512, SECRET_KEY) // 시그니쳐 세팅을 HS512 알고리즘 , 내가 지정한 시크릿 키로
                .setSubject(userEntity.getId()) // 이때 토큰의 고유값을 userEntity 의 id 로 지정
                .setIssuer("SpringBoot_miniProject").setIssuedAt(new Date()) // 토큰을 발행한 주체로 이 토큰이 언제 부터 시작했는지
                .setExpiration(expireDate) // 토큰의 만료 시점
                .compact(); //빌더 패턴을 완성하는 기능을 포함해서 JWT 압축 문자열로 압축함

    }


    //클라이언트에서 넘어온 토큰을 해석 유효하면 넘어아고 그렇지 않으면 예외처리
    public String validateAndGetUserId(String token) {

        Claims claims = Jwts.parser() //String 형태로 넘어오는 토큰을 parse 함
                .setSigningKey(SECRET_KEY)  // 우리가 임의로 지정한 비밀키 세팅 비밀키로 전자서명을 만들었으므로 이를 바탕으로 전자 서명을 서로 비교한다
                .parseClaimsJws(token) // 토큰을 읽어서 위조여부 판단 위조면 더이상 진행하지 못하고 Exception 을 날림
                .getBody(); // 위조가 아니라면 우리는 payLoad 를 return 을 함

        return claims.getSubject(); // 그곳에서 고유한 Id 를 가져옴 위에서 setSubject(userEntity.getId())  가져오는 것임

    }
}
