package com.cybb.main.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private TokenController tokenController;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            log.info("startJwtFilter");

            String getAuthHeader = request.getHeader("Authorization");
            String token = null;

            if (StringUtils.hasText(getAuthHeader) && getAuthHeader.startsWith("Bearer ")) {// 즉 헤더의 Authorization 부분에 문자열이 있으며 Bearer 시작하면
                token = getAuthHeader.substring(7);
            }

            if(token != null){
                String userId = tokenController.validateAndGetUserId(token); // 이때 유효한 토큰이라면 User Id 가 넘어옴
                log.info("Authenticated user ID : " + userId);
                AbstractAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userId , null , AuthorityUtils.NO_AUTHORITIES);
                //securityContext 에 인증된 user 와 토큰을 저장하기 위한 구현체

                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails((request))); //?

                // 아래는 인증된 사용자에 대한 정보와 토큰을 SecurityContextHolder 에 저장해서 이 user 는 인증된 user 라는 것을 시큐리티에 알려줌
                SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
                securityContext.setAuthentication(authentication);
                SecurityContextHolder.setContext(securityContext);


            }

        }catch (Exception e){
            log.error(e.getMessage());
        }

        //필터는 여러개 있을 수 있으니 계속해서 다음 필터로 요청을 넘김
        filterChain.doFilter(request , response);







    }
}
