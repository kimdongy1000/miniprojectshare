package com.cybb.main.controller;

import com.cybb.main.dto.ResponseDto;
import com.cybb.main.dto.UserDto;
import com.cybb.main.entity.UserEntity;
import com.cybb.main.security.TokenController;
import com.cybb.main.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("user")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private TokenController tokenController;

    @PostMapping("/signingUp")
    public ResponseEntity<?> createUser(@RequestBody UserDto userDto){
        ResponseDto<UserDto> responseDto = null;
        List<UserDto> userList = null;

        try{
            log.info("startCreateUSerEntity");
            UserEntity user = UserEntity.builder().username(userDto.getUsername()).email(userDto.getEmail()).password(userDto.getPassword()).build();
            UserEntity userEntity =  userService.createUser(user);


            userList = new ArrayList<>();
            userList.add(UserDto.builder().username(userEntity.getUsername()).email(userEntity.getEmail()).password(userEntity.getPassword()).build());

            responseDto = ResponseDto.<UserDto>builder().data(userList).build();

            return ResponseEntity.ok().body(responseDto);
        }catch (Exception e){
            log.error(e.getMessage());

            responseDto = ResponseDto.<UserDto>builder().error(e.getMessage()).data(null).build();

            return ResponseEntity.badRequest().body(responseDto);

        }
    }

    @PostMapping("/signing")
    public ResponseEntity<?> getCredentialUser (@RequestBody UserDto userDto) {

        ResponseDto<UserDto> responseDto = null;
        List<UserDto> userList = null;

        try{
            log.info("startGetCredentialUser");
            UserEntity user = userService.getCredentialUser(userDto);

            String token = tokenController.tokenCreate(user);


            userList = new ArrayList<>();
            userList.add(UserDto.builder().id(user.getId()).username(user.getUsername()).password(user.getPassword()).token(token).build());

            responseDto = ResponseDto.<UserDto>builder().data(userList).build();

            return ResponseEntity.ok().body(responseDto);


        }catch(Exception e) {
            log.error(e.getMessage());
            responseDto = ResponseDto.<UserDto>builder().error(e.getMessage()).data(null).build();

            return ResponseEntity.badRequest().body(responseDto);

        }


    }
}
