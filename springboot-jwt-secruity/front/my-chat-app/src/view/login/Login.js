import React from 'react';
import  Container from '@mui/material/Container';
import { Button, Grid, TextField, Typography } from '@mui/material';

class Login extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            
            email : null , 
            password : null , 

        }
    }

    onchangeEmailBox = (e) => {
        let value = e.target.value;
        
        this.setState({email : value})
    }

    onchangePasswordBox = (e) => {
        let value = e.target.value
        
        this.setState({password : value})
    }

    loginButtonOnClick = () => {


        if(!this.state.email){
            alert('아이디를 입력해주세요');
            return ;
        }

        if(!this.state.password){
            alert('비밀번호를 입력해주세요')
            return ; 
        }

        const paramData = {
            "email" : this.state.email , 
            "password" : this.state.password
        }

        fetch ("http://localhost:8080/user/signing" , {
            method : "POST" , 
            headers : {
                "Content-Type" : "application/json"
            } , 
            body : JSON.stringify(paramData)
        }).then((res) => {
            res.json().then((data) => {
                console.log(data)

                let token = data.data[0].token
                localStorage.setItem("ACCESS_TOKEN" , token);

                window.location = "/main"
            })
        }).catch((error) => {
            console.log(error)
        })


        
    }

    render(){
        return (
            <>
                <Container component = "main" maxWidth = "xs" style = {{marginTop : "8%"}} spacing = {2}>
                    <Grid>
                        <Grid item = {12}>
                            <Typography component="h1" variant="h5">
                                로그인 
                            </Typography>
                        </Grid>
                    </Grid>
                </Container>

                <Container component = "main" maxWidth = "xs" style = {{marginTop : "3%"}} >
                    <Grid>
                        <Grid item = {12}>
                            <TextField variant='outlined' required fullWidth label = "이메일 주소" name = "email" onChange={this.onchangeEmailBox}>

                            </TextField>
                        </Grid>
                    </Grid>
                </Container>

                <Container component = "main" maxWidth = "xs" style = {{marginTop : "3%"}} >
                    <Grid>
                        <Grid item = {12}>
                            <TextField variant='outlined' required fullWidth label = "비밀번호" name = "password" onChange={this.onchangePasswordBox}>

                            </TextField>
                        </Grid>
                    </Grid>
                </Container>

                <Container component="main" maxWidth="xs" style = {{marginTop : "2%"}} >
                    <Grid item xs={12}>
                        <Button
                            type = "button"
                            fullWidth
                            variant='contained'
                            color='primary'
                            onClick={this.loginButtonOnClick}        
                        >
                            로그인
                        </Button>
                    </Grid>
                </Container>



                

            


            
            </>
        )
    }
}

export default Login