    import React from "react";
    import {BrowserRouter as Router , Routes, Route } from "react-router-dom"
import Login from "../login/Login";
import Main from "../main/Main";


    class AppRouter extends React.Component{

        constructor(props){
            super(props)
        }

        render(){
            return (
                <>
                    <Router>
                        <Routes>
                            <Route path = "/" element = { <Login /> }>

                            </Route>

                            <Route path = "/main" element = { <Main /> }>

                            </Route>
                        </Routes>
                    </Router>
                </>

            )
        }
    }

    export default AppRouter