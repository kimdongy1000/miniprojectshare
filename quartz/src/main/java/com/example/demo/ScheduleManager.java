package com.example.demo;

import javax.annotation.PostConstruct;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ScheduleManager {
	
	@Autowired
	private SchedulerFactory schedulerFactory;
	
	@Autowired
	private ApplicationContext applicationContext;
	
	
	private Scheduler scheduler;
	
	
	
	@PostConstruct
	public void start() throws Exception{
		
		scheduler = schedulerFactory.getScheduler();
		scheduler.start();

		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put("applicationContext", applicationContext);
		
		JobDetail job = JobBuilder.newJob(SampleScheduler.class).setJobData(jobDataMap).build();
		
		Trigger trigger = TriggerBuilder.newTrigger().withSchedule(CronScheduleBuilder.cronSchedule("0 0 12 * * ?"))
				.build();
		
		scheduler.scheduleJob(job, trigger);
		
	}
}
