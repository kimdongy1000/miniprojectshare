package com.example.demo;

import java.io.File;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.example.demo.domain.ResumeFileVO;
import com.example.demo.service.CommonService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SampleScheduler implements Job {

	@Autowired
	private CommonService commonService;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {

		JobDataMap jobDataMap = context.getMergedJobDataMap();
		ApplicationContext applicationContext = (ApplicationContext) jobDataMap.get("applicationContext");
		commonService = applicationContext.getBean(CommonService.class);

		List<ResumeFileVO> resultList = commonService.getNotExistsFiles();

		for (ResumeFileVO resumeFileVO : resultList) {

			try {
				File file = new File(resumeFileVO.getFilePath() + "/" + resumeFileVO.getFileName());
				if (file.exists()) {
					file.delete();
				}
			} catch (Exception e) {
				log.error("file is not exists");
			}
		}

	}

}
