package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.domain.ResumeFileVO;
import com.example.demo.repository.CommonRepository;

@Service
public class CommonService {
	
	@Autowired
	private CommonRepository repository;

	public List<ResumeFileVO> getNotExistsFiles() {
		return repository.getNotExistsFiles();
	}

}
