package com.example.demo.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.example.demo.domain.ResumeFileVO;

@Mapper
public interface CommonRepository {

	List<ResumeFileVO> getNotExistsFiles();

}
