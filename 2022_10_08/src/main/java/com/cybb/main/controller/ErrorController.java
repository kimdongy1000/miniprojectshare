package com.cybb.main.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("error")
public class ErrorController {
	
	@GetMapping("/401")
	public String errorPage401() {
		
		return "error401";
	}

	@GetMapping("/402")
	public String errorPage402() {

		return "error402";
	}



}
