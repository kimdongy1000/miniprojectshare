package com.cybb.main.controller;

import java.security.Principal;

import com.cybb.main.config.CustomUserDetail;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("greeting")
public class HelloController {
	
	@GetMapping("/hello")
	public String  hello() {

		SecurityContext context = SecurityContextHolder.getContext();

		Authentication authentication = context.getAuthentication();
		CustomUserDetail user =  (CustomUserDetail)authentication.getPrincipal();



		return "hello";
	}
	
	@GetMapping("/goodBye")
	public String  goodBye(Authentication a) {
		
		return "goodBye";
	}


	
	
	

}
