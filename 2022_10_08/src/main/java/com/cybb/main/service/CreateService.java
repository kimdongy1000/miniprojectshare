package com.cybb.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.cybb.main.dao.UserModel;
import com.cybb.main.dto.UserDto;
import com.cybb.main.repository.UserRepository;

import java.util.Optional;

@Service
public class CreateService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	public UserDto createUser(UserDto userDto) {
		
		userRepository.findByUsername(userDto.getUsername()).ifPresent(item -> {
			throw new RuntimeException("이미 등록된 유저입니다.");
			
		});
		
		UserModel userModel = UserModel.builder()
										.username(userDto.getUsername())
										.password(passwordEncoder.encode(userDto.getPassword()))
										.authorities(userDto.getAuthorities())
										.build();
		
		String id = userRepository.save(userModel).getId();
		UserModel returnModle = userRepository.findById(id).get();
		
		UserDto returnDto = UserDto.builder()
								.id(returnModle.getId())
								.username(returnModle.getUsername())
								.password(returnModle.getPassword())
								.authorities(returnModle.getAuthorities())
								.build();
		
		return returnDto;
	}


    public UserDto searchUserName(String username) {

		Optional<UserModel> optUserModel = userRepository.findByUsername(username);
		if(!optUserModel.isPresent()){
			throw new RuntimeException("유저가 없습니다.");
		}

		UserModel userModel = optUserModel.get();

		return UserDto.builder().id(userModel.getId())
								.username(userModel.getUsername())
								.authorities(userModel.getAuthorities())
								.count(userModel.getCount())
								.build();

    }
}
