package com.cybb.main.config;

import com.cybb.main.dao.UserModel;
import com.cybb.main.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Component
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {

        String errorMessage = null;

        if(exception instanceof UsernameNotFoundException){
            errorMessage = "아이디 또는 비밀번호가 맞지 않습니다 다시 확인해주세요";
            response.sendRedirect("/error/401");
        }else if(exception instanceof BadCredentialsException){
            errorMessage = "아이디 또는 비밀번호가 맞지 않습니다 다시 확인해주세요";

            String username = request.getParameter("username");
            String password = request.getParameter("password");

            Optional<UserModel> optUserModel = userRepository.findByUsername(username);
            optUserModel.ifPresent(item -> {
                int count = item.getCount();
                count++;
                item.setCount(count);

                userRepository.save(item);
            });

            response.sendRedirect("/error/401");


        }

    }
}
