package com.cybb.main.config;

import com.cybb.main.dao.UserModel;
import com.cybb.main.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;


@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    private UserRepository userRepository;


    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        CustomUserDetail userDetail = (CustomUserDetail)authentication.getPrincipal();



        Optional<UserModel> optUSerModel = userRepository.findByUsername(userDetail.getUsername());
        UserModel userModel = optUSerModel.get();

        if(userModel.getCount() < 5){
            optUSerModel.ifPresent(item -> {
                item.setCount(0);
                userRepository.save(item);
            });
            response.sendRedirect("/home/welcomeSecurity");
        }else{
            response.sendRedirect("/error/402");
        }

    }
}
