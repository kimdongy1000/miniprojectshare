package com.cybb.main.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.cybb.main.dao.UserModel;
import com.cybb.main.repository.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider{
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private CustomUserDetailService customUserDetailService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String username = authentication.getName(); // 아이디
		String passwrod = authentication.getCredentials().toString(); // 비밀번호
			
			
			
		UserDetails userDetails = customUserDetailService.loadUserByUsername(username);


			
			
		if(!passwordEncoder.matches(passwrod, userDetails.getPassword())) {
			throw new BadCredentialsException("계정 또는 비밀번호가 존재/ 일치하지 않습니다");
		}
			
		UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(
				                                         userDetails ,
				                                         userDetails.getPassword(),
				                                         userDetails.getAuthorities());
		return result;

	}

	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}


}
